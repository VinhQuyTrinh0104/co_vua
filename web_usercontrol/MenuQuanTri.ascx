﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuQuanTri.ascx.cs" Inherits="web_usercontrol_MenuQuanTri" %>
<div class="card card-block">
    <div class="col-2">
        <a href="~/admin-co-so" id="btnCoSo" runat="server" class="btn btn-primary">Cơ sở</a>
    </div>
    <div class="col-2">
        <a href="~/admin-lop" id="btnLop" runat="server" class="btn btn-primary">Lớp</a>
    </div>
      <div class="col-2">
        <a href="~/admin-giao-vien" id="btnGiaoVien" runat="server" class="btn btn-primary">Giáo viên</a>
    </div>
    <div class="col-2">
        <a href="~/admin-hoc-sinh" id="btnHocSinh" runat="server" class="btn btn-primary">Thông tin học sinh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-dong-hoc-phi" id="btnDongHocPhi" runat="server" class="btn btn-primary">Đăng kí học phí</a>
    </div>
    <div class="col-2">
        <a href="~/admin-ds-bao-luu" id="btnBaoLuu" runat="server" class="btn btn-primary">Bảo lưu</a>
    </div>
    <div class="col-2">
        <a href="~/admin-diem-danh" id="btnDiemDanh" runat="server" class="btn btn-primary">Điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-lich-su-diem-danh" id="btnLichSu" runat="server" class="btn btn-primary">Lịch sử điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-thong-ke-diem-danh" id="btnThongkeDiemDanh" runat="server" class="btn btn-primary">Thống kê điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-chuyen-lop" id="btnChuyenLop" runat="server" class="btn btn-primary">Chuyển lớp</a>
    </div>
   
    <div class="col-2">
        <a href="~/admin-thong-ke-danh-gia" id="btnThongKeDanhGia" runat="server" class="btn btn-primary">Thống kê đánh giá</a>
    </div>
    <div class="col-2">
        <a href="~/admin-ds-tong" id="btnDanhSachTong" runat="server" class="btn btn-primary">Thống kê tổng</a>
    </div>
     <div class="col-2">
        <a href="~/admin-hoc-phi-thang" id="btnHocPhiThang" runat="server" class="btn btn-primary">Học phí từng tháng</a>
    </div>
</div>
