﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="link.ascx.cs" Inherits="web_usercontrol_link" %>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<meta name="mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/x-icon" href="/images/logo.png" />
<link href="/css/themify-icons.css" rel="stylesheet" />
<link href="/css/icofont.min.css" rel="stylesheet" />
<%--<link href="../css/Boostrap_Share_5.css" rel="stylesheet" />--%>
<%--<link href="/css/Back.css" rel="stylesheet" />--%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
<link href="/css/font-awesome.min.css" rel="stylesheet" />
<link href="/css/globalGame.min.css" rel="stylesheet" />
<%--css kế thừa--%>
<script src="/js/jquery-3.5.1.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="/js/current-device.min.js"></script>
<script src="/admin_js/sweetalert.min.js"></script>
