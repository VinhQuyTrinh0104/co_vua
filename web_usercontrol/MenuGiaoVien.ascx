﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MenuGiaoVien.ascx.cs" Inherits="web_usercontrol_MenuGiaoVien" %>
<div class="card card-block">
    <div class="col-2">
        <a href="~/admin-giao-vien-diem-danh" id="btnDiemDanh" runat="server" class="btn btn-primary">Điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-lich-su-diem-danh" id="btnLichSu" runat="server" class="btn btn-primary">Lịch sử điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-thong-ke-diem-danh" id="btnThongkeDiemDanh" runat="server" class="btn btn-primary">Thống kê điểm danh</a>
    </div>
    <div class="col-2">
        <a href="~/admin-danh-gia" id="btnDanhGia" runat="server" class="btn btn-primary">Đánh giá</a>
    </div>
    <div class="col-2">
        <a href="~/admin-thong-ke-danh-gia" id="btnThongKeDanhGia" runat="server" class="btn btn-primary">Thống kê đánh giá</a>
    </div>
</div>
