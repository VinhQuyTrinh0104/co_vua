﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WebUserControl.ascx.vb" Inherits="web_usercontrol_WebUserControl" %>
<style>
    .block-color {
        display: flex;
        justify-content: center;
    }

    .block-color__item img {
        padding: 10%;
        width: 90px;
        border: 2px solid #226e54;
        border-radius: 10px;
        margin: 3px;
    }

    .block-image {
        position: relative;
    }

    .block-image__background {
        display: block;
        position: relative;
    }

    .block-image__draw {
        /* position: absolute;
            z-index: 1;
            top: 0;
            left: 0;*/
        display: block;
    }

    .block-image__dra {
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        display: block;
    }

    .block-image__dr {
        position: absolute;
        z-index: 1;
        top: 0;
        left: 0;
        display: none;
    }
</style>
    <div class="block-header">
        <div class="container">
            <div class="header-main">
                <a class="btn-menu btn-home" href="/slldt-danh-muc-sach-doodletown-nursery-8#id_61">
                    <i class="icofont-home"></i>
                </a>
                <div class="header-main__title">Unit 4 - Lesson 4</div>
                <div class="button-nav">
                    <a class="btn-menu btn-prev" href="#">
                        <i class="icofont-arrow-left"></i>
                    </a>
                    <a class="btn-menu btn-next" href="#">
                        <i class="icofont-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="frame-game --bg-body-1">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="vocabulary-list ">
                        <div class="block-image">
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_8.png" id="color-8" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_7.png" id="color-7" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_6.png" id="color-6" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_5.png" id="color-5" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_4.png" id="color-4" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_3.png" id="color-3" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_2.png" id="color-2" style="display: none;" />
                            <img class="color-picture__image-paint--color" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_1.png" id="color-1" style="display: none;" />

                            <img <%--class="color-picture__image-paint--question"--%> src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img.png" />
                            <%--<img src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_08.png" />--%>
                            <%--<img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_07.png" onclick="funDisplaySlide()" />
                            <img class="block-image__dra" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_06.png" />--%>
                            <%-- <img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_05.png" />
                            <img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_04.png" />
                            <img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_03.png" />
                            <img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_02.png" />
                            <img class="block-image__draw" src="../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img_01.png" />--%>
                        </div>
                        <div class="block-color">
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-1')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img1.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-2')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img2.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-3')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img3.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-4')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img4.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-5')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img5.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-6')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img6.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-7')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img7.png" />
                            </a>
                            <a id="" class="block-color__item hvr-push" href="javascript:void(0)" onclick="funColor('color-8')">
                                <img src="../../../imageGame/AcademyStarss/Unit4/Lesson4/choose_33_img8.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        const funColor = (id) => {
            //jQuery('.color-picture__image-paint--color').hide();
            jQuery('#' + id).show();

        }
        //function funDisplaySlide() {
        //    document.getElementById("image_slide").style.display = 'block';
        //}
        //function funDisplaySlide1() {
        //    document.getElementById("image_slide1").style.display = 'block';
        //}
        //function funDisplaySlide2() {
        //    document.getElementById("image_slide2").style.display = 'block';
        //}
        //function funDisplaySlide3() {
        //    document.getElementById("image_slide3").style.display = 'block';
        //}

        //function myDoc_NumBer(id) {
        //    $('audio').each(function () {
        //        this.pause(); // Stop all playing
        //    });
        //    let audio = document.getElementById('number_' + id);
        //    audio.load();
        //    audio.play();

        //    /*$('#number_' + id)[0].play();*/
        //}
    </script>
