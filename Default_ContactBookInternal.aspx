﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ContactBookInternalMasterPage.master" AutoEventWireup="true" CodeFile="Default_ContactBookInternal.aspx.cs" Inherits="Default_ContactBookInternal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Header" runat="Server">
    <style>
       
        .block-menu {
            box-shadow: 2px 3px 8px 5px #80808029;
            margin: 1% 0;
            height: 7rem;
            background: white;
            border-radius: 10px;
        }

        .block-btn {
            padding: 1% !important;
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
            font-size: 7px;
            font-weight: 800;
            color: #6464a9;
            box-shadow: 0px 4px 4px rgb(0 0 0 / 25%);
            background: #d5e8ef;
            height: 6rem;
            border-radius: 10px;
        }

            .block-btn img {
                padding: 1% 14% 0 14%;
            }

        .block-menu {
            justify-content: space-between;
        }

        .pad {
            padding: 0 !important;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .thong-bao {
            position: absolute;
            background: red;
            font-size: 20px;
            z-index: 10;
            display: flex;
            left: 75%;
            bottom: 68%;
            color: white;
            width: 33px;
            box-shadow: 1px 1px 1px 1px grey;
            border-radius: 18px;
            height: 33px;
            justify-content: center;
            align-items: center;
        }

        .new {
            position: absolute;
            background: #28a745;
            font-size: 12px;
            z-index: 10;
            display: flex;
            left: 75%;
            bottom: 68%;
            color: white;
            width: 33px;
            box-shadow: 1px 1px 1px 1px grey;
            border-radius: 18px;
            height: 33px;
            justify-content: center;
            align-items: center;
        }

        .icon_next {
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .owl-dot {
            display: none !important;
        }

        .owl-nav {
            display: flex;
            justify-content: space-between;
        }

        button:focus {
            outline: none !important;
        }

        .owl-prev, .owl-next {
            position: absolute !important;
            background: none !important;
            border: none !important;
            color: #8e191b !important;
            font-size: 70px !important;
        }

            .owl-prev span {
                display: block;
            }

        .owl-stage-outer {
            height: 7rem;
            padding-top: 0.75%;
        }

        .owl-prev {
            top: -5%;
            left: -3% !important;
        }

        .owl-next {
            top: -5%;
            right: -3% !important;
        }

        .item a:hover {
            cursor: pointer;
            text-decoration: none;
            background-color: aqua;
            box-shadow: 0px 14px 14px rgb(0 0 0 / 25%);
        }

        .item {
            pointer-events: auto;
        }

        .block-btn span {
            font-size: 10px;
        }

        .Active {
            background-color: aqua;
            box-shadow: 2px 4px 12px 2px #1c33688f;
            color: #940808;
        }
    </style>
    <script>

        function funcActive(id) {
            var app = $("#div_App").find("div.owl-item.active");
            //get item đang active
            var item = $(app[0]).find('a.block-btn').attr('id');
            sessionStorage.setItem('listActive', item);
        }

        $(document).ready(function () {
            var hrefs = location.pathname.replace('/', '');
            document.querySelector('a[href="/' + hrefs + '"]').classList.add("Active");
            var position = sessionStorage.getItem("listActive").split('_');
            $("#div_App").trigger("to.owl.carousel", [position[1], 10, true])
        });
        function onLoad() {
            $("#img-loading-icon").show();
            setTimeout(function () { $("#img-loading-icon").hide() }, 2000);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Menu" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopWrapper" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Wrapper" runat="Server">
    <div class="block-menu">
        <div class="owl-carousel" id="div_App">
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-thong-bao_0" class="block-btn" href="/website-vietnhatkids-thong-bao">
                    <img src="/images/icon/thong-bao.png" />
                    <span>THÔNG BÁO</span>
                    <span class="thong-bao" id="divThongBao" >0</span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-thong-tin-suc-khoe_1" class="block-btn" href="/website-vietnhatkids-thong-tin-suc-khoe">
                    <img src="/images/icon/suc-khoe.png" />
                    <span>THÔNG TIN
                        <br />
                        SỨC KHỎE</span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-hoat-dong-hang-ngay_2" class="block-btn" href="/website-vietnhatkids-hoat-dong-hang-ngay">
                    <img src="/images/icon/hdhn1.png" />
                    <span>HOẠT ĐỘNG
                        <br />
                        TRONG NGÀY</span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-su-kien-hang-tuan_3" class="block-btn" href="/website-vietnhatkids-su-kien-hang-tuan">
                    <img src="/images/icon/lich.png" />
                    <span>SỰ KIỆN
                        <br />
                        HÀNG TUẦN</span>
                    <span class="new" id="divSuKien" runat="server">New</span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-xin-nghi_4" class="block-btn" href="/website-vietnhatkids-xin-nghi">
                    <img src="/images/icon/Dangky-Xin-nghi.png" />
                    <span>XIN NGHỈ</span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-dan-thuoc_5" class="block-btn" href="/website-vietnhatkids-dan-thuoc">
                    <img src="/images/icon/Dangky-Thuoc.png" />
                    <span>DẶN THUỐC
                        <br />
                        VÀ DẶN DÒ
                    </span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-ablum-anh_6" class="block-btn" href="/website-vietnhatkids-ablum-anh">
                    <img src="/images/icon/album.png" />
                    <span>ALBUM ẢNH</span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-dang-ky-dong-phuc-le-phuc_7" class="block-btn" href="/website-vietnhatkids-dang-ky-dong-phuc-le-phuc">
                    <img src="/images/icon/Dangky-Dong-phuc.png" />
                    <span>ĐĂNG KÍ
                        <br />
                        ĐỒNG PHỤC
                    </span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" class="block-btn" href="/danh-muc-hoc-tap" id="HocTap_8">
                    <img src="/images/icon/Dangky-Hoc-tap.png" />
                    <span>HỌC TẬP</span>
                </a>
            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-dang-ky-da-ngoai_9" class="block-btn" href="/website-vietnhatkids-dang-ky-da-ngoai">
                    <img src="/images/icon/Dangky-ngoai-khoa.png" />
                    <span>ĐĂNG KÍ
                        <br />
                        DÃ NGOẠI</span>
                    <span class="new" id="divDaNgoai" runat="server">New</span>
                </a>
            </div>



            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-an-sang-uong-sua_10" class="block-btn" href="/website-vietnhatkids-an-sang-uong-sua">
                    <img src="/images/icon/Dangky-an-sang.png" />
                    <span>ĐK VÀ HỦY
                        ĂN
                        <br />
                        SÁNG, UỐNG SỮA
                    </span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-hoc-phi_11" class="block-btn" href="/website-vietnhatkids-hoc-phi">
                    <img src="/images/icon/hoc-phi1.png" />
                    <span>HỌC PHÍ</span>
                </a>

            </div>


            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-do-dung-hoc-tap_12" class="block-btn" href="/website-vietnhatkids-do-dung-hoc-tap">
                    <img src="/images/icon/do-dung.png" />
                    <span>ĐĂNG KÝ
                        <br />
                        ĐỒ DÙNG</span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="website-vietnhatkids-nang-khieu_13" class="block-btn" href="/website-vietnhatkids-nang-khieu">
                    <img src="/images/icon/earobic.png" />
                    <span>ĐĂNG KÝ
                        <br />
                        NĂNG KHIẾU</span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id),onLoad()" id="DanhGiaGiaoVien_14" class="block-btn" href="/website-vietnhatkids-danh-gia-giao-vien">
                    <img src="/images/icon/danh-gia-giao-vien.png" />
                    <span>ĐÁNH GIÁ
                        <br />
                        GIÁO VIÊN</span>
                </a>

            </div>
            <%-- <div class="item">
                <a onclick="funcActive(this.id)" class="block-btn" href="#" id="HocVe_15">
                    <img src="/images/icon/hoc-ve.png" />
                    <span>ĐĂNG KÝ HỌC VẼ</span>
                </a>

            </div>
            <div class="item">
                <a onclick="funcActive(this.id)" class="block-btn" href="#" id="TiengAnh_16">
                    <img src="/images/icon/tieng-anh.png" />
                    <span>ĐĂNG KÝ HỌC TIẾNG ANH</span>
                </a>
            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BottomWrapper" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Footer" runat="Server">
</asp:Content>

