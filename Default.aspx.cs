﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Text.RegularExpressions;

public partial class _Default : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        getGioiThieuKhoaHoc();
        getPoster();
        getSlide();
        getChuongTrinhDaotao();
        getNoiDungChay();
        getBlockThongKe();
        getalbumImage();
    }
    protected void getGioiThieuKhoaHoc()
    {
        rpGioiThieuKhoaHoc.DataSource = from gtkh in db.tbIntroduce_KhoaHocs 
                                        where gtkh.hidden == true
                                        select gtkh;
        rpGioiThieuKhoaHoc.DataBind();
        var listNV = from gtkh in db.tbIntroduce_KhoaHocs select gtkh;
        ddlKhoaHoc.Items.Clear();
        ddlKhoaHoc.Items.Insert(0, "Chương trình cần tư vấn");
        ddlKhoaHoc.AppendDataBoundItems = true;
        ddlKhoaHoc.DataTextField = "introkh_title";
        ddlKhoaHoc.DataValueField = "introkh_id";
        ddlKhoaHoc.DataSource = listNV;
        ddlKhoaHoc.DataBind();
    }
    protected void getPoster()
    {
        //rpPoster.DataSource = from p in db.tbPosters select p;
        //rpPoster.DataBind();
    }    
    protected void getSlide()
    {
        rpSlide.DataSource = from sl in db.tbSlides select sl;
        rpSlide.DataBind();
    }    
    protected void getChuongTrinhDaotao()
    {
        rpChuongTrinhDaotao.DataSource = from ctdt in db.tbChuongTrinhDaoTaos where ctdt.chuongtrinhdaotao_id==1 select ctdt;
        rpChuongTrinhDaotao.DataBind();
        rpDoiNguNhanSu_summary.DataSource = from ctdt in db.tbChuongTrinhDaoTaos where ctdt.chuongtrinhdaotao_id == 2 select ctdt;
        rpDoiNguNhanSu_summary.DataBind();
        rpBlockTuyenSinh.DataSource = from ctdt in db.tbChuongTrinhDaoTaos where ctdt.chuongtrinhdaotao_id == 5 
                                      select ctdt;
        rpBlockTuyenSinh.DataBind();
    }
    protected void getNoiDungChay()
    {
        rpNoiDungChay.DataSource = from ctdt in db.tbChuongTrinhDaoTaos where ctdt.chuongtrinhdaotao_id == 4 select ctdt;
        rpNoiDungChay.DataBind();
    }
    
    protected void getBlockThongKe()
    {
        //rpBlockThongKe.DataSource = from tk in db.tbBlock_ThongKeSoLieus select tk;
        //rpBlockThongKe.DataBind();
    }
    
    protected void getalbumImage()
    {
        //rpImage.DataSource = from i in db.tbAlbum_Images select i;
        //rpImage.DataBind();
    }

    
}