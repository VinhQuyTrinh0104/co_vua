﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using ASPSnippets.GoogleAPI;

public partial class WebsiteMasterPage : System.Web.UI.MasterPage
{
    dbcsdlDataContext db = new dbcsdlDataContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        getmn_GioiThieuKhoaHoc();
        //getmn_TinTuc();
        getft_Coso();
    }
    protected void getmn_GioiThieuKhoaHoc()
    {
        rpmn_GioiThieuKhoaHoc.DataSource = from gt in db.tbIntroduce_KhoaHocs select gt;
        rpmn_GioiThieuKhoaHoc.DataBind();
    }
    //protected void getmn_TinTuc()
    //{
    //    rpmn_TinTuc.DataSource = from gt in db.tbNews select gt;
    //    rpmn_TinTuc.DataBind();
    //}
    protected void getft_Coso()
    {
        rpft_CoSo.DataSource = from gt in db.tbCosos where gt.coso_hidden == false && gt.coso_ghichu!="" select gt;
        rpft_CoSo.DataBind();
    }

}
