﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="Admin_Default.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link href="css/pageIndex.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <style>
        .app .content {
            min-height: unset;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div id="section-alert">
        <div class="container-fluid">
            <div class="card-view">
                <h5 class="title-card">THÔNG BÁO TRUNG TÂM</h5>
                <div class="alert-slide-block">
                    <div id="slide-alert-school" class="owl-carousel owl-theme">
                        <asp:Repeater ID="rpThongBaoChung" runat="server">
                            <ItemTemplate>
                                <div class="item">
                                    <div class="block-alert">
                                        <a href="admin-xem-thong-bao-<%#Eval("thongbao_id") %>" class="block-main">
                                            <div class="cate">
                                                <%#Convert.ToDateTime(Eval("thongbao_createdate")).ToShortDateString()%>
                                                <span class="status">
                                                    <img src="/images/new-1.gif" /></span>
                                            </div>
                                            <div class="content">
                                                <span class="text"><%#Eval("thongbao_title") %></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <h5 class="title-card">THÔNG BÁO ĐÀO TẠO - GIÁO VIÊN</h5>
                <div class="alert-slide-block">
                    <div id="slide-alert-daotao" class="owl-carousel owl-theme">
                        <asp:Repeater ID="rpThongBaoDaoTao" runat="server">
                            <ItemTemplate>
                                <div class="item">
                                    <div class="block-alert">
                                        <a href="admin-xem-thong-bao-<%#Eval("thongbao_id") %>" class="block-main">
                                            <div class="cate">
                                                <%#Convert.ToDateTime(Eval("thongbao_createdate")).ToShortDateString()%>
                                                <span class="status">
                                                    <img src="/images/new-1.gif" /></span>
                                            </div>
                                            <div class="content">
                                                <span class="text"><%#Eval("thongbao_title") %></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <h5 class="title-card">THÔNG BÁO TƯ VẤN</h5>
                    <div class="alert-slide-block">
                        <div id="slide-alert-class" class="owl-carousel owl-theme">
                            <asp:Repeater ID="rpThongBaoGiaoVien" runat="server">
                                <ItemTemplate>
                                    <div class="item">
                                        <div class="block-alert">
                                            <a href="admin-xem-thong-bao-<%#Eval("thongbao_id") %>" class="block-main">
                                                <div class="cate">
                                                    <%#Convert.ToDateTime(Eval("thongbao_createdate")).ToShortDateString()%>
                                                    <span class="status">
                                                        <img src="/images/new-1.gif" /></span>
                                                </div>
                                                <div class="content">
                                                    <span class="text"><%#Eval("thongbao_title") %></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script>
        jQuery("#slide-main").owlCarousel({
            animateOut: "slideOutDown",
            animateIn: "flipInX",
            items: 1,
            loop: false,
            dots: true,
            margin: 0,
            nav: false,
            //navText: [
            //    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            //    '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            //],
        });
        jQuery('#slide-alert-class').owlCarousel({
            autoWidth: true,
            items: 3,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
        jQuery('#slide-alert-daotao').owlCarousel({
            autoWidth: true,
            items: 3,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
        jQuery('#slide-alert-school').owlCarousel({
            autoWidth: true,
            items: 3,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
        jQuery('#slide-course').owlCarousel({
            autoWidth: true,
            items: 3,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

