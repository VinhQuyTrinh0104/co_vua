﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_access_admin_UserManage : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadDataGiaoVien();
            ddlBophan.DataSource = from gr in db.admin_GroupUsers
                                   where gr.groupuser_id != 1 && gr.groupuser_id != 2
                                   select gr;
            ddlBophan.DataBind();
            //ddlGender.DataSource = from gd in db.tbGenders
            //                       select gd;
            //ddlGender.DataBind();
            ddlcoso.DataSource = from d in db.tbCosos
                                 select d;
            ddlcoso.DataBind();
        }
        else
        {
            Response.Redirect("/admin-login");
        }

    }
    // Dành cho root
    private void loadData()
    {
        var getData = from gv in db.admin_Users select gv;
        grvList.DataSource = getData;
        grvList.DataBind();


    }
    // Dành cho các trung tâm
    private void loadDataTrungTam()
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getData = from gv in db.admin_Users
                      where gv.groupuser_id == 2
                      select gv;
        grvList.DataSource = getData;
        grvList.DataBind();


    }
    // Dành cho các giáo viên
    private void loadDataGiaoVien()
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (logedMember.username_id == 1)
        {
            var getData = from gv in db.admin_Users
                          orderby gv.username_id descending
                          select new
                          {
                              gv.username_id,
                              //gv.username_fullnameEn,
                              gv.username_fullname,
                              gv.username_email,
                              gv.username_phone,
                              gv.username_username,
                              gv.username_password,
                              username_gender = gv.username_gender == true ? "Nam" : "Nữ"
                          }; ;
            grvList.DataSource = getData;
            grvList.DataBind();
        }
        else
        {
            var getData = from gv in db.admin_Users
                          where gv.username_id != 1
                          orderby gv.username_id descending
                          select new
                          {
                              gv.username_id,
                              gv.username_fullname,
                              //gv.username_fullnameEn,
                              gv.username_email,
                              gv.username_phone,
                              gv.username_username,
                              gv.username_password,
                              username_gender = gv.username_gender == true ? "Nam" : "Nữ"
                          };
            grvList.DataSource = getData;
            grvList.DataBind();
        }
    }
    private void setNULL()
    {
        txttenVN.Text = "";
        txtAccount.Text = "";
        txtEmail.Text = "";
        txtPhone.Text = "";
        txtPass.Text = "";
        txttenEn.Text = "";
        ddlGender.Text = "";
        ddlBophan.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "username_id" }));
        Session["_id"] = _id;
        var getData = (from user in db.admin_Users
                       join gr in db.admin_GroupUsers on user.groupuser_id equals gr.groupuser_id
                       where user.username_id == _id
                       select new
                       {
                           user.username_id,
                           user.username_username,
                           user.username_password,
                           user.username_fullname,
                           user.username_gender,
                           user.username_phone,
                           user.username_email,
                           //user.username_fullnameEn,
                           user.username_coso,
                           gr.groupuser_id,
                           gr.groupuser_name
                       }).Single();
        txttenVN.Text = getData.username_fullname;
        if (getData.username_gender == true)
            ddlGender.Text = "Nam";
        else
            ddlGender.Text = "Nữ";
        ddlBophan.Text = getData.groupuser_name;
        txtEmail.Text = getData.username_email;
        txtPhone.Text = getData.username_phone;
        txtAccount.Text = getData.username_username;
        txtPass.Text = getData.username_password;
        //txttenEn.Text = getData.username_fullnameEn;
        ddlcoso.Text = getData.username_coso;
        txtPass.Visible = false;
        lbmk.Visible = false;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_GiaoVien cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "username_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_GiaoVien();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttenVN.Text != "" && txtAccount.Text != "" && txtPass.Text != "" && txtPhone.Text != "" && txttenEn.Text != "")
            return true;
        else return false;
    }
    //Kiểm tra Email
    private bool isEmail(string txtEmail)
    {
        Regex re = new Regex(@"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                      RegexOptions.IgnoreCase);
        return re.IsMatch(txtEmail);
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_GiaoVien cls = new cls_GiaoVien();
        cls_security md5 = new cls_security();
        string passWord = txtPass.Text.Trim();
        string passmd5 = md5.HashCode(txtPass.Text);
        //string bophan = rdGV.SelectedValue;
        //Kiểm tra email có tồn tại
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");

        else
        {
            if (Session["_id"].ToString() == "0")
            {
                //Kiểm tra email có tồn tại
                var checkemailphone = from check in db.admin_Users where check.username_phone == txtPhone.Text select check;
                if (isEmail(txtEmail.Text) != true)
                {
                    alert.alert_Error(Page, "Vui Lòng Kiểm tra lại mail của bạn", "");
                    popupControl.ShowOnPageLoad = true;
                }
                else if (checkemailphone.Count() > 0)
                {
                    alert.alert_Error(Page, "Số điện thoại đã tồn tại!, Vui lòng kiểm tra lại", "");
                }
                bool gt = true;
                if (ddlGender.Text == "Nữ")
                {
                    gt = false;
                }

                //if
                //    (cls.Linq_Them(txttenVN.Text, gt, ddlBophan.Value.ToString(), txtEmail.Text, txtPhone.Text, txtAccount.Text, passmd5, txttenEn.Text, ddlcoso.Text))
                //{
                //    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.Refresh();})", true);

                //}
                //else
                //{
                //    alert.alert_Error(Page, "Thêm thất bại", "");
                //}
            }
            else
            {
                bool gt = true;
                if (ddlGender.Text == "Nữ")
                {
                    gt = false;
                }
                //if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttenVN.Text, gt, ddlBophan.Value.ToString(), txtEmail.Text, txtPhone.Text, txtAccount.Text, passmd5, txttenEn.Text, ddlcoso.Text))
                //    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                //else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            //Cập nhật quyền cho giáo viên và nhân viên sale
            var getAll = (from get in db.admin_Users
                          select new
                          {
                              get.username_id
                          });
            if (getAll != null)
            {
                // Cấp Quyền Cho Giáo Viên
                if (ddlBophan.Value.ToString() == "3" || ddlBophan.Value.ToString() == "4")
                {
                    foreach (var item in getAll)
                    {
                        var checkID = (from id in db.admin_AccessUserForms
                                       where id.username_id == item.username_id
                                       select new
                                       {
                                           id.username_id
                                       }).FirstOrDefault();
                        if (checkID == null)
                        {
                            admin_AccessUserForm insert = new admin_AccessUserForm();
                            insert.uf_active = true;
                            insert.username_id = item.username_id;
                            insert.form_id = 74;
                            db.admin_AccessUserForms.InsertOnSubmit(insert);
                            db.SubmitChanges();
                            admin_AccessUserForm insert2 = new admin_AccessUserForm();
                            insert2.uf_active = true;
                            insert2.username_id = item.username_id;
                            insert2.form_id = 75;
                            db.admin_AccessUserForms.InsertOnSubmit(insert2);
                            db.SubmitChanges();
                            admin_AccessUserForm insert3 = new admin_AccessUserForm();
                            insert3.uf_active = true;
                            insert3.username_id = item.username_id;
                            insert3.form_id = 77;
                            db.admin_AccessUserForms.InsertOnSubmit(insert3);
                            db.SubmitChanges();
                            admin_AccessUserForm insert4 = new admin_AccessUserForm();
                            insert4.uf_active = true;
                            insert4.username_id = item.username_id;
                            insert4.form_id = 54;
                            db.admin_AccessUserForms.InsertOnSubmit(insert4);
                            db.SubmitChanges();
                        }
                    }
                }
                else
                {
                    //Cấp Quyền Cho nhân viên sale
                    foreach (var item in getAll)
                    {
                        var checkID = (from id in db.admin_AccessUserForms
                                       where id.username_id == item.username_id
                                       select new
                                       {
                                           id.username_id
                                       }).FirstOrDefault();
                        if (checkID == null)
                        {
                            admin_AccessUserForm insert = new admin_AccessUserForm();
                            insert.uf_active = true;
                            insert.username_id = item.username_id;
                            insert.form_id = 69;
                            db.admin_AccessUserForms.InsertOnSubmit(insert);
                            db.SubmitChanges();
                            admin_AccessUserForm insert1 = new admin_AccessUserForm();
                            insert1.uf_active = true;
                            insert1.username_id = item.username_id;
                            insert1.form_id = 63;
                            db.admin_AccessUserForms.InsertOnSubmit(insert1);
                            db.SubmitChanges();
                            admin_AccessUserForm insert2 = new admin_AccessUserForm();
                            insert2.uf_active = true;
                            insert2.username_id = item.username_id;
                            insert2.form_id = 71;
                            db.admin_AccessUserForms.InsertOnSubmit(insert2);
                            db.SubmitChanges();
                        }
                    }
                }
            }
            popupControl.ShowOnPageLoad = false;
        }

    }

    //protected void grvList_HtmlRowCreated1(object sender, ASPxGridViewTableRowEventArgs e)
    //{
    //    if (e.RowType != GridViewRowType.Data) return;

    //    ASPxLabel label = grvList.FindRowCellTemplateControl(e.VisibleIndex, null,
    //    "txtGioitinh") as ASPxLabel;
    //    var getData = (from gr in db.admin_Users
    //                       //where gr.username_id == _id
    //                   select gr).FirstOrDefault();
    //    if (getData.username_gender == true)
    //    {
    //        label.Text = "Nam";
    //    }
    //}
}