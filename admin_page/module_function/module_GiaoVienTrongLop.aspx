﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_GiaoVienTrongLop.aspx.cs" Inherits="admin_page_module_function_module_GiaoVienTrongLop" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
        function btnluu() {
            document.getElementById('<%=btnLuu.ClientID%>').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= grvList.ClientID%>');

            if (CityName.value == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <div class="mar_but button">
                            <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-1 form-control-label">Lớp Học:</label>
            <div class="col-3">
                <dx:ASPxComboBox ID="ddllop" NullText="Chọn lớp" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddllop" CssClass="" Width="95%"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="username_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="4%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Họ Tên " FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Điện Thoại" FieldName="username_phone" Settings-AllowEllipsisInText="true" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Email" FieldName="username_email" Settings-AllowEllipsisInText="true" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Chức Vụ" FieldName="groupuser_name" Settings-AllowEllipsisInText="true" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
                </Columns>
                <ClientSideEvents RowDblClick="btnLuu" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="15" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
