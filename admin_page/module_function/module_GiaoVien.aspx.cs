﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    DateTime ngayketthuc;
    bool gioitinh = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (checkButton.groupuser_id == 6)
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = false;
            }
            else
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = true;
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddlLoai.DataSource = from d in db.admin_GroupUsers select d;
        ddlLoai.DataBind();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //admin_User là ROOT or ADMIN thì xem được hết danh sách account
        var getData = from nc in db.admin_Users
                      join gr in db.admin_GroupUsers on nc.groupuser_id equals gr.groupuser_id
                      where gr.groupuser_id != 1 && gr.groupuser_id != 2 && nc.username_active==true
                      orderby nc.username_id descending
                      select new
                      {
                          nc.username_id,
                          nc.username_fullname,
                          nc.username_email,
                          nc.username_phone,
                          nc.username_username,
                          gr.groupuser_name
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }
    private void setNULL()
    {
        ddlLoai.Text = "";
        txtTaiKhoan.Text = "";
        txtHoTen.Value = "";
        txtSDT.Value = "";
        txtEmail.Value = "";

    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        // loadData();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "username_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.admin_Users
                       join gr in db.admin_GroupUsers on nc.groupuser_id equals gr.groupuser_id
                       //join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                       where nc.username_id == _id
                       select new
                       {
                           nc.username_id,
                           nc.username_fullname,
                           nc.username_email,
                           nc.username_phone,
                           nc.username_username,
                           gr.groupuser_name
                       }).Single();
        ddlLoai.Text = getData.groupuser_name;
        txtTaiKhoan.Text = getData.username_username;
        txtHoTen.Value = getData.username_fullname;
        txtSDT.Value = getData.username_phone;
        txtEmail.Value = getData.username_email;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_GiaoVien cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "username_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_GiaoVien();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (txtTaiKhoan.Text != "" && txtHoTen.Value != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        // checkNgayKetThuc();
        // Nếu sale nhập liệu thì chưa chuyển về mail khách
        // Sau khi admin duyệt thì mail mới về cho phụ huynh
        cls_GiaoVien cls = new cls_GiaoVien();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        int usernameID = logedMember.username_id;
        string saleName = logedMember.username_username;
        //var checkemailphone = from data in db.tbAccounts where data.account_phone == txtPhone.Text select data;
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin dấu (*)!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {

                var checkGiaoVien = from gv in db.admin_Users where gv.username_username == txtTaiKhoan.Text select gv;
                if (checkGiaoVien.Count() > 0)
                {
                    alert.alert_Error(Page, "Tài khoản này đã tồn tại", "");
                }
                else
                {
                    if (cls.Linq_Them(Convert.ToInt16(ddlLoai.SelectedItem.Value), txtTaiKhoan.Text, txtHoTen.Value, txtEmail.Value, txtSDT.Value))
                    {

                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công!','','success').then(function(){grvList.Refresh();})", true);
                        loadData();
                        popupControl.ShowOnPageLoad = false;
                    }
                    else alert.alert_Error(Page, "Thêm thất bại", "");
                }
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), Convert.ToInt16(ddlLoai.SelectedItem.Value), txtTaiKhoan.Text, txtHoTen.Value, txtEmail.Value, txtSDT.Value))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                    popupControl.ShowOnPageLoad = false;
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
    }



    protected void btnGioiThieu_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "username_id" }));
        Response.Redirect("admin-gioi-thieu-giao-vien-" + _id);
    }
}