﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_Lop_NghiHan.aspx.cs" Inherits="admin_page_module_function_module_Lop_NghiHan" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>--%>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtLop.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên lớp không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
        function confirmDel1() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnLopCu.ClientID%>');
                        xoa.click();
                    }
                });
        }
    </script>

    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN LỚP
        </div>
        <div class="card card-block">
            <div class="form-group row" style="display:none">
                <div class="col-sm-10">
                    <asp:UpdatePanel ID="udButton" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                            <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                            <input type="submit" id="btnDlt" runat="server" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                            <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                              <input type="submit" id="btnTinhTrang" runat="server" class="btn btn-primary" value="Lớp nghỉ hẵn" onclick="confirmDel1()" />
                            <asp:Button ID="btnLopCu" runat="server" CssClass="invisible" OnClick="btnLopCu_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="form-group table-responsive">
                <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="lop_id" Width="100%">
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                        </dx:GridViewCommandColumn>
                         <dx:GridViewDataColumn Caption="Ngày nhập" FieldName="lop_NgayNghiHan" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Tên Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Sỉ số" FieldName="lop_siso" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giáo Viên Nước ngoài" FieldName="username_idEn" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giáo Viên Việt Nam" FieldName="username_idVn" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giáo Viên trợ giảng" FieldName="username_trogiang_id" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Bắt đầu, kết thúc" FieldName="thoigian" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Ghi chú" FieldName="lop_ghichu" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T2" FieldName="lop_thu2" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T3" FieldName="lop_thu3" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T4" FieldName="lop_thu4" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T5" FieldName="lop_thu5" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T6" FieldName="lop_thu6" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="T7" FieldName="lop_thu7" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="CN" FieldName="lop_chunhat" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    </Columns>
                    <ClientSideEvents RowDblClick="btnChiTiet" />
                    <SettingsSearchPanel Visible="true" />
                    <SettingsBehavior AllowFocusedRow="true" />
                    <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                    <SettingsLoadingPanel Text="Đang tải..." />
                    <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                </dx:ASPxGridView>
              

            </div>
        </div>
        <dx:ASPxPopupControl ID="popupControl" runat="server" Width="500px" Height="600px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
            HeaderText="Thông tin lớp" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <asp:UpdatePanel ID="udPopup" runat="server">
                        <ContentTemplate>
                            <div class="popup-main">
                                <div class="div_content col-12">
                                    <div class="col-12">
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Cơ sở:</label>
                                            <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Tên Lớp:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtLop" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Từ giờ:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtTuGio" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Đến giờ:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtDenGio" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Buổi học:</label>
                                            <div class="col-10">
                                                <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                                                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />
                                                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />
                                                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />
                                                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />
                                                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />
                                                <asp:CheckBox ID="ckCN" runat="server" Text="CN" />
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">gv Nước ngoài:</label>
                                            <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlNuocNgoai" runat="server" ValueType="System.Int32" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlNuocNgoai" CssClass="" Width="95%"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">gv Việt Nam:</label>
                                            <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlVietNam" runat="server" ValueType="System.Int32" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlVietNam" CssClass="" Width="95%"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">gv trợ giảng:</label>
                                            <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlTroGiang" runat="server" ValueType="System.Int32" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlTroGiang" CssClass="" Width="95%"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Ghi chú:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtGhiChu" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <FooterContentTemplate>
                <div class="mar_but button">
                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                </div>
            </FooterContentTemplate>
            <ContentStyle>
                <Paddings PaddingBottom="0px" />
            </ContentStyle>
        </dx:ASPxPopupControl>
    </div>
    <%-- popup chi tiết --%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



