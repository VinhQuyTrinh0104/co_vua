﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_Lai : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    private int user_id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            user_id = logedMember.username_id;
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
                var getLop = from tt in db.tbLops select tt;
                ddlLop.DataSource = getLop;
                ddlLop.DataBind();
            }
            //loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getDiemDanh = from hs in db.tbAccounts
                          join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                          join dd in db.tbDiemDanhs on hstl.hstl_id equals dd.hstl_id
                          where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                          && hstl.hstl_hidden == false && hs.hidden == false
                          && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                          && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                          && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                          // && dd.diemdanh_vang == "Đúng giờ"
                          select new
                          {
                              hs.account_vn,
                              hstl.hstl_id,
                              dd.diemdanh_vang,
                              dd.diemdanh_ghichu
                          };
        rpList.DataSource = getDiemDanh;
        rpList.DataBind();
        txtHocSinhTrongLop.Value = string.Join(",", getDiemDanh.Select(x => x.hstl_id));
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "setChecked()", true);
    }
    private void setNULL()
    {

    }

    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value) && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Thu2 = ""; string Thu3 = ""; string Thu4 = ""; string Thu5 = ""; string Thu6 = ""; string Thu7 = ""; string Thu8 = "";
        if (ddlLop.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
            {
                ckThu2.Checked = true;
                Thu2 = "Monday";
            }
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
            {
                ckThu3.Checked = true;
                Thu3 = "Tuesday";
            }

            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
            {
                ckThu4.Checked = true;
                Thu4 = "Wednesday";
            }
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
            {
                ckThu5.Checked = true;
                Thu5 = "Thursday";
            }
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
            {
                ckThu6.Checked = true;
                Thu6 = "Friday";
            }
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
            {
                ckThu7.Checked = true;
                Thu7 = "Saturday";
            }
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
            {
                ckChuNhat.Checked = true;
                Thu8 = "Sunday";
            }
            else
                ckChuNhat.Checked = false;
            lblGioHoc.Text = getData.giohoc_batdau + "-" + getData.giohoc_ketthuc;
            // Check thứ có đúng ko thì mới cho điểm danh
            //kiểm tra đã điểm danh chưa, nếu có rồi thì load lại tình trạng
            var checkNgayDiemDanh = from dd in db.tbDiemDanhs
                                    join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                                    join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                    join l in db.tbLops on hstl.lop_id equals l.lop_id
                                    where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                                       && hstl.hstl_hidden == false && ac.hidden == false
                                    && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                      && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                      && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                    select dd;
            if (checkNgayDiemDanh.Count() != 0)
            {
                loadData();
            }
            else
            {
                var getDiemDanh = from hs in db.tbAccounts
                                  join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                  where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value) && hs.hidden == false && hstl.hstl_hidden == false
                                  select new
                                  {
                                      hs.account_vn,
                                      hstl.hstl_id,
                                      diemdanh_vang = "",
                                      diemdanh_ghichu = "",
                                  };
                rpList.DataSource = getDiemDanh;
                rpList.DataBind();
                txtHocSinhTrongLop.Value = string.Join(",", getDiemDanh.Select(x => x.hstl_id));
            }
        }
    }

    protected void btnLuuDiemDanh_ServerClick(object sender, EventArgs e)
    {

        string[] arrListStr = txtVangHoc.Value.Split('|');
        string[] arrListGhiChu = txtGhiChu.Value.Split('|');
        string[] arrHocSinh = txtHocSinhTrongLop.Value.Split(',');
        for (int i = 0; i < arrHocSinh.Length; i++)
        {
            //kiểm tra xem ngày hnay bé đã được nhận xét chưa. Nếu đã nhận xét rồi thì cập nhật, ngược lại là thêm mới
            var checkNhanXet = (from nx in db.tbDiemDanhs
                                where nx.diemdanh_ngay.Value.Day == Convert.ToDateTime(dteNgay.Value).Day
                                && nx.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                                && nx.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                && nx.hstl_id == Convert.ToInt32(arrHocSinh[i])
                                orderby nx.diemdanh_id descending
                                select nx);
            if (checkNhanXet.Count() > 0)
            {
                checkNhanXet.First().diemdanh_vang = arrListStr[i].ToString();
                checkNhanXet.First().diemdanh_ghichu = arrListGhiChu[i].ToString();
                checkNhanXet.First().username_id = user_id;
                db.SubmitChanges();
            }
            else
            {
                tbDiemDanh insert = new tbDiemDanh();
                insert.diemdanh_ngay = Convert.ToDateTime(dteNgay.Value);
                insert.diemdanh_vang = arrListStr[i].ToString();
                insert.diemdanh_ghichu = arrListGhiChu[i].ToString();
                insert.hstl_id = Convert.ToInt32(arrHocSinh[i]);
                insert.username_id = user_id;
                db.tbDiemDanhs.InsertOnSubmit(insert);
                db.SubmitChanges();
            }
        }
        alert.alert_Success(Page, "Hoàn thành", "");
        loadData();
    }
}