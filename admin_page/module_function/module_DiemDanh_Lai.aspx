﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DiemDanh_Lai.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_Lai" %>

<%--<%@ Register Src="~/web_usercontrol/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <%--<uc1:Menu ID="Menu1" runat="server" />--%>
    <div class="card card-block box-admin">
        <%-- <asp:UpdatePanel ID="upDiemDanh" runat="server">
            <ContentTemplate>--%>
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN ĐIỂM DANH LẠI
        </div>
        <div class="col-12 form-group">
            <b>Lưu ý:</b><br />
            <b>- Form điểm danh đã được cập nhật mới, Quản lý bấm vào nút "Lưu" sau khi đã thực hiện xong tất cả các thao tác.</b><br />
            <b>- Trường hợp điểm danh lại, Quản lý cũng vào form này để thay đổi tình trạng và bấm vào nút lưu để xác nhận lại. </b>
            <br />
            <b>- Mọi thắc mắc xin liên hệ cô Lan: 0919.698.094 để được hỗ trợ.</b>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Chọn ngày:</label>
            <div class="col-4">
                <input type="date" id="dteNgay" runat="server" class="form-control" />
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Cơ sở:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Lớp:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
            </div>
            <label class="col-1 form-control-label">Giờ học:</label>
            <div class="col-4">
                <b>
                    <asp:Label ID="lblGioHoc" runat="server"></asp:Label></b>
            </div>
        </div>

        <div class="col-12 form-group">
            <label class="col-2"></label>
            <div class="col-6">
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckChuNhat" runat="server" Text="Chủ nhật" />
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">Điểm danh</th>
                    <th scope="col">Lý do</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%=STT++ %></th>
                            <td><%#Eval("account_vn") %></td>
                            <td>
                                <select id="ddlDiemDanhVang_<%#Eval("hstl_id") %>">
                                    <option value="Đúng giờ">Đúng giờ</option>
                                    <option value="Vắng có phép">Vắng có phép</option>
                                    <option value="Vắng không phép">Vắng không phép</option>
                                    <option value="Bảo lưu">Bảo lưu</option>
                                </select>
                                <input type="text" id="txttinhtrang_<%#Eval("hstl_id")%>" value="<%#Eval("diemdanh_vang")%>" hidden="hidden" />
                            </td>
                            <td>
                                <input id="txtGhiChu_<%#Eval("hstl_id") %>" type="text" value="<%#Eval("diemdanh_ghichu")%>" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <a href="javascript:void(0)" class="btn btn-primary" onclick="checkDiemDanh()">Lưu</a>
        <div style="display: none">
            <a href="javascript:void(0)" id="btnLuuDiemDanh" runat="server" class="btn btn-primary" onserverclick="btnLuuDiemDanh_ServerClick"></a>
            <input type="text" id="txtDiemDanhVangHidden" runat="server" />
            <input type="text" id="txtGhiChuHidden" runat="server" />
            <input type="text" id="txtHocSinhTrongLop" runat="server" />
            <input type="text" id="txtDanhSachHocSinhID" runat="server" />
           <%-- <a id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>--%>
        </div>
        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    <div style="display: none">
        <input id="txtVangHoc" runat="server" />
        <input id="txtGhiChu" runat="server" />
    </div>
    <script>
        function checkDiemDanh() {
            //debugger
            let str_danhsachID = document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value.split(",");
            var content = [];
            var ghichu = [];
            for (let i = 0; i < str_danhsachID.length; i++) {
                var _content = document.getElementById("ddlDiemDanhVang_" + str_danhsachID[i]).value;
                var _ghichu = document.getElementById("txtGhiChu_" + str_danhsachID[i]).value;
                content.push(_content);
                ghichu.push(_ghichu);
            }
            document.getElementById("<%=txtVangHoc.ClientID%>").value = content.join("|");
            document.getElementById("<%=txtGhiChu.ClientID%>").value = ghichu.join("|");
            document.getElementById("<%=btnLuuDiemDanh.ClientID%>").click();
        }
        function setChecked() {
            var arr_ID = document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value.split(',');
            if (arr_ID != "") {
                for (var i = 0; i < arr_ID.length; i++) {
                    var _tinhtrang = document.getElementById("txttinhtrang_" + arr_ID[i]).value;
                    if (_tinhtrang != "") {
                        var $option = $('#ddlDiemDanhVang_' + arr_ID[i]).children('option[value="' + _tinhtrang + '"]');
                        $option.attr('selected', true);
                    }
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



