﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhGia : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos where cs.coso_hidden == false select cs;
                ddlCoSo.DataBind();
                div_DanhGia.Attributes["style"] = "display: none;";
            }

        }
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getHocSinhTrongLop = (from hstl in db.tbhocsinhtronglops
                                  where hstl.account_id == Convert.ToInt32(txtHocSinh.Value) && hstl.hstl_hidden == false
                                  orderby hstl.hstl_id descending
                                  select hstl).FirstOrDefault();
        var checkDanhGia = (from dg in db.tbDanhGias
                            where dg.hstl_id == getHocSinhTrongLop.hstl_id && dg.danhgia_month == ddlThang.SelectedValue
                            orderby dg.danhgia_id descending
                            select dg);
        if (checkDanhGia.Count() > 0)
        {
            checkDanhGia.First().danhgia_ngay = DateTime.Now;
            if (rd1_1.Checked == true)
                checkDanhGia.First().understanding = "Excellent";
            else if (rd1_2.Checked == true)
                checkDanhGia.First().understanding = "Good";
            else if (rd1_3.Checked == true)
                checkDanhGia.First().understanding = "Quit Good";
            else
                checkDanhGia.First().understanding = "Not Good";
            if (rd2_1.Checked == true)
                checkDanhGia.First().vocabulary_ability = "Excellent";
            else if (rd2_2.Checked == true)
                checkDanhGia.First().vocabulary_ability = "Good";
            else if (rd2_3.Checked == true)
                checkDanhGia.First().vocabulary_ability = "Quit Good";
            else
                checkDanhGia.First().vocabulary_ability = "Not Good";
            if (rd3_1.Checked == true)
                checkDanhGia.First().grammar = "Excellent";
            else if (rd3_2.Checked == true)
                checkDanhGia.First().grammar = "Good";
            else if (rd3_3.Checked == true)
                checkDanhGia.First().grammar = "Quit Good";
            else
                checkDanhGia.First().grammar = "Not Good";
            if (rd4_1.Checked == true)
                checkDanhGia.First().interaction = "Excellent";
            else if (rd4_2.Checked == true)
                checkDanhGia.First().interaction = "Good";
            else if (rd4_3.Checked == true)
                checkDanhGia.First().interaction = "Quit Good";
            else
                checkDanhGia.First().interaction = "Not Good";
            if (rd5_1.Checked == true)
                checkDanhGia.First().concentration = "Excellent";
            else if (rd5_2.Checked == true)
                checkDanhGia.First().concentration = "Good";
            else if (rd5_3.Checked == true)
                checkDanhGia.First().concentration = "Quit Good";
            else
                checkDanhGia.First().concentration = "Not Good";
            if (rd6_1.Checked == true)
                checkDanhGia.First().homework = "Excellent";
            else if (rd6_2.Checked == true)
                checkDanhGia.First().homework = "Good";
            else if (rd6_3.Checked == true)
                checkDanhGia.First().homework = "Quit Good";
            else
                checkDanhGia.First().homework = "Not Good";
            if (rd7_1.Checked == true)
                checkDanhGia.First().rules_in_class = "Excellent";
            else if (rd7_2.Checked == true)
                checkDanhGia.First().rules_in_class = "Good";
            else if (rd7_3.Checked == true)
                checkDanhGia.First().rules_in_class = "Quit Good";
            else
                checkDanhGia.First().rules_in_class = "Not Good";
            if (rd8_1.Checked == true)
                checkDanhGia.First().cooperation = "Excellent";
            else if (rd8_2.Checked == true)
                checkDanhGia.First().cooperation = "Good";
            else if (rd8_3.Checked == true)
                checkDanhGia.First().cooperation = "Quit Good";
            else
                checkDanhGia.First().cooperation = "Not Good";
            checkDanhGia.First().username_id = logedMember.username_id;
            checkDanhGia.First().danhgia_tinhtrangxem = "Chưa xem";
            checkDanhGia.First().danhgia_ghichu = txtGhiChu.Value.Replace("\n", "<br/>");
            db.SubmitChanges();
        }
        else
        {
            tbDanhGia insert = new tbDanhGia();
            insert.danhgia_ngay = DateTime.Now;
            insert.danhgia_month = ddlThang.SelectedValue;
            insert.danhgia_tinhtrangxem = "Chưa xem";
            insert.hstl_id = getHocSinhTrongLop.hstl_id;
            if (rd1_1.Checked == true)
                insert.understanding = "Excellent";
            else if (rd1_2.Checked == true)
                insert.understanding = "Good";
            else if (rd1_3.Checked == true)
                insert.understanding = "Quit Good";
            else
                insert.understanding = "Not Good";
            if (rd2_1.Checked == true)
                insert.vocabulary_ability = "Excellent";
            else if (rd2_2.Checked == true)
                insert.vocabulary_ability = "Good";
            else if (rd2_3.Checked == true)
                insert.vocabulary_ability = "Quit Good";
            else
                insert.vocabulary_ability = "Not Good";
            if (rd3_1.Checked == true)
                insert.grammar = "Excellent";
            else if (rd3_2.Checked == true)
                insert.grammar = "Good";
            else if (rd3_3.Checked == true)
                insert.grammar = "Quit Good";
            else
                insert.grammar = "Not Good";
            if (rd4_1.Checked == true)
                insert.interaction = "Excellent";
            else if (rd4_2.Checked == true)
                insert.interaction = "Good";
            else if (rd4_3.Checked == true)
                insert.interaction = "Quit Good";
            else
                insert.interaction = "Not Good";
            if (rd5_1.Checked == true)
                insert.concentration = "Excellent";
            else if (rd5_2.Checked == true)
                insert.concentration = "Good";
            else if (rd5_3.Checked == true)
                insert.concentration = "Quit Good";
            else
                insert.concentration = "Not Good";
            if (rd6_1.Checked == true)
                insert.homework = "Excellent";
            else if (rd6_2.Checked == true)
                insert.homework = "Good";
            else if (rd6_3.Checked == true)
                insert.homework = "Quit Good";
            else
                insert.homework = "Not Good";
            if (rd7_1.Checked == true)
                insert.rules_in_class = "Excellent";
            else if (rd7_2.Checked == true)
                insert.rules_in_class = "Good";
            else if (rd7_3.Checked == true)
                insert.rules_in_class = "Quit Good";
            else
                insert.rules_in_class = "Not Good";
            if (rd8_1.Checked == true)
                insert.cooperation = "Excellent";
            else if (rd8_2.Checked == true)
                insert.cooperation = "Good";
            else if (rd8_3.Checked == true)
                insert.cooperation = "Quit Good";
            else
                insert.cooperation = "Not Good";
            insert.danhgia_ghichu = txtGhiChu.Value.Replace("\n", "<br/>");
            insert.username_id = logedMember.username_id;
            db.tbDanhGias.InsertOnSubmit(insert);
            db.SubmitChanges();
        }
        alert.alert_Success(Page, "Complete the review", "");
        rpHocSinh.DataSource = from hs in db.tbAccounts
                               join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                               join l in db.tbLops on hstl.lop_id equals l.lop_id
                               where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden == false
                               select new
                               {
                                   hs.account_id,
                                   hs.account_vn,
                                   mystyle = db.tbDanhGias.Any(x => x.hstl_id == hstl.hstl_id && x.danhgia_month == ddlThang.SelectedValue) ? "dadanhgia" : ""
                               };
        rpHocSinh.DataBind();
        div_DanhGia.Attributes["style"] = "display: none;";
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        var getHocSinhTrongLop = (from hstl in db.tbhocsinhtronglops
                                  where hstl.account_id == Convert.ToInt32(txtHocSinh.Value) && hstl.hstl_hidden == false
                                  orderby hstl.hstl_id descending
                                  select hstl).FirstOrDefault();
        var getDanhGia = from dg in db.tbDanhGias
                         where dg.hstl_id == getHocSinhTrongLop.hstl_id && dg.danhgia_month == ddlThang.SelectedValue
                         orderby dg.danhgia_id descending
                         select dg;
        if (getDanhGia.Count() > 0)
        {
            txtdanhgia_1.Value = getDanhGia.FirstOrDefault().understanding;
            txtdanhgia_2.Value = getDanhGia.FirstOrDefault().vocabulary_ability;
            txtdanhgia_3.Value = getDanhGia.FirstOrDefault().grammar;
            txtdanhgia_4.Value = getDanhGia.FirstOrDefault().interaction;
            txtdanhgia_5.Value = getDanhGia.FirstOrDefault().concentration;
            txtdanhgia_6.Value = getDanhGia.FirstOrDefault().homework;
            txtdanhgia_7.Value = getDanhGia.FirstOrDefault().rules_in_class;
            txtdanhgia_8.Value = getDanhGia.FirstOrDefault().cooperation;
            txtGhiChu.Value = getDanhGia.FirstOrDefault().danhgia_ghichu.Replace("<br/>", "\n");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "setChecked();active('" + txtHocSinh.Value + "')", true);

        }
        else
        {
            txtGhiChu.Value = "";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "setFormChecked();active('" + txtHocSinh.Value + "')", true);
        }
        div_DanhGia.Attributes["style"] = "display: block;";
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "active('" + txtHocSinh.Value + "')", true);
    }

    protected void btnXemHocSinh_ServerClick(object sender, EventArgs e)
    {
        rpHocSinh.DataSource = from hs in db.tbAccounts
                               join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                               join l in db.tbLops on hstl.lop_id equals l.lop_id
                               where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden == false
                               select new
                               {
                                   hs.account_id,
                                   hs.account_vn,
                                   mystyle = db.tbDanhGias.Any(x => x.hstl_id == hstl.hstl_id && x.danhgia_month == ddlThang.SelectedValue) ? "dadanhgia" : ""
                               };
        rpHocSinh.DataBind();
        div_DanhGia.Attributes["style"] = "display: none;";
    }
}