﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DiemDanh_DanhSach.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_DanhSach" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block box-admin">
        <div class="col-12 form-group-name">
            THỐNG KÊ SỐ BUỔI HỌC CÒN LẠI CỦA TOÀN TRUNG TÂM
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Cơ sở:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="cbbCoSoCaNhan" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="cbbCoSoCaNhan" OnSelectedIndexChanged="cbbCoSoCaNhan_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Lớp:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="cbbLopCaNhan" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="cbbLopCaNhan" OnSelectedIndexChanged="cbbLopCaNhan_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
            </div>

        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">English name</th>
                    <th scope="col">Số buổi đăng kí mới</th>
                    <th scope="col" style="text-align: center">Tổng số buổi đã học ( Tính khóa mới)</th>
                    <th scope="col" style="text-align: center">Tổng số buổi còn lại</th>
                 <%--   <th scope="col">#</th>--%>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpDanhSach" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="col"><%=STT++ %></th>
                            <th scope="col"><%#Eval("account_vn") %></th>
                            <th scope="col"><%#Eval("account_en") %></th>
                            <th scope="col"><%#Eval("so_buoi_dang_ki_moi") %></th>
                            <th scope="col" style="text-align: center"><%#Eval("tong_sobuoi_diemdanh") %></th>
                            <th scope="col"><%#Eval("tong_so_buoi_con_lai") %></th>
                           <%-- <th scope="col"><a class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<%#Eval("hstl_id") %>" style="color: white">Xem chi tiết</a></th>--%>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <asp:Repeater ID="rpDiemDanh" runat="server" OnItemDataBound="rpDiemDanh_ItemDataBound">
            <ItemTemplate>
                <div class="modal fade" id="exampleModal<%#Eval("hstl_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Danh sách chi tiết</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Họ tên</th>
                                            <th scope="col">Ngày đi học</th>
                                            <th scope="col">Tình trạng</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rpDiemDanhChiTiet" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <th scope="col"><%#Container.ItemIndex+1%></th>
                                                    <th scope="col"><%#Eval("account_vn") %></th>
                                                    <th scope="col"><%#Eval("diemdanh_ngay") %></th>
                                                    <th scope="col" style="text-align: center"><%#Eval("diemdanh_vang") %></th>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



