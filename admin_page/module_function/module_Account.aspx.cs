﻿using ClosedXML.Excel;
using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Account : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    DateTime ngayketthuc;
    bool gioitinh = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            //checkNgayKetThuc();
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            
            if (checkButton.groupuser_id == 6)
            {
                btnDlt.Visible = false;
              
            }
            else
            {
                if (checkButton.groupuser_id == 2)
                {
                 
                    btnDlt.Visible = true;
                }
                else
                {
                   
                }
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
               
            }
            loadData();
            
           
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddlcoso.DataSource = from d in db.tbCosos select d;
        ddlcoso.DataBind();
        var getLop = from tt in db.tbLops select tt;
        ddlLop.DataSource = getLop;
        ddlLop.DataBind();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //admin_User là ROOT or ADMIN thì xem được hết danh sách account
        var getData = from nc in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby nc.account_id descending
                      where hstl.hstl_hidden == false && nc.hidden == false
                      select new
                      {
                          nc.account_ngaydangky,
                          nc.account_en,
                          account_gioitinh = nc.account_gioitinh==true?"Nam":"Nữ",
                          cs.coso_name,
                          l.lop_name,
                          nc.account_id,
                          nc.account_vn,
                          nc.account_phone,
                          nc.account_tenba,
                          nc.account_sdtba,
                          nc.account_sdtme,
                          nc.account_tenme,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }
    private void setNULL()
    {
        ddlcoso.Text = "";
        ddlLop.Text = "";
        txtFullName.Text = "";
        txtenEn.Text = "";
        txtSDTHocSinh.Value = "";
        txtEmailHocSinh.Value = "";
        txtTruongHoc.Value = "";
        txtNoiSinh.Value = "";
        txtChoOHienTai.Value = "";
        txtTenBa.Text = "";
        txtsdtBa.Text = "";
        txtNgheNghiepBa.Text = "";
        txtEmailBa.Text = "";
        txtTenMe.Text = "";
        txtSDTMe.Text = "";
        txtNgheNghiepMe.Text = "";
        txtEmailMe.Text = "";
        txtMongMuon.Text = "";
        rdNam.Checked = true;
        rdNu.Checked = false;

    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        // loadData();
        //SendMail("luuvanquyetit@gmail.com", "luuvanquyet", "12345");
    }
    private bool SendMail(string email, string name, string pass)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "vjlc.vjis@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "ladpckdedqorpplz";
                string subject, title;
                title = "Xác minh tài khoản ";
                subject = "<!DOCTYPE html><html><head><title></title></head><body style=\" width:600px; margin:0px;\"><div>" +
                "<h1 style=\"margin-top:0px;text-align:center; color:#029ada\">SuperFriends</h1>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn vừa đăng ký tài khoản tại SuperFriends với: </h3><br/>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Tên Đăng Nhập: " + name + "</h3><br/>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">PassWord: " + pass + "</h3><br/>" +
                "</div></body></html>";
                //"<a href=\"" + link + "\">" + link + "</a><br />


                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "VJLC");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "account_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbAccounts
                       join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                       join l in db.tbLops on hstl.lop_id equals l.lop_id
                       join cs in db.tbCosos on l.coso_id equals cs.coso_id
                       //join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                       where nc.account_id == _id && hstl.hstl_hidden == false && nc.hidden == false
                       select new
                       {
                           cs.coso_name,
                           l.lop_name,
                           nc.account_id,
                           nc.account_vn,
                           nc.account_en,
                           nc.account_phone,
                           nc.account_tenba,
                           nc.account_sdtba,
                           nc.account_sdtme,
                           nc.account_tenme,
                           nc.account_email,
                           nc.account_truonghoc,
                           nc.account_noisinh,
                           nc.account_noiohientai,
                           nc.account_nghenghiepba,
                           nc.account_emailba,
                           nc.account_emailme,
                           nc.account_nghenghiepme,
                           nc.account_mongmuon,
                           nc.account_ngaysinh,
                           nc.account_gioitinh,
                       }).Single();
        txtFullName.Text = getData.account_vn;
        txtenEn.Text = getData.account_en;
        txtSDTHocSinh.Value = getData.account_phone;
        txtEmailHocSinh.Value = getData.account_email;
        txtTruongHoc.Value = getData.account_truonghoc;
        txtNoiSinh.Value = getData.account_noisinh;
        txtChoOHienTai.Value = getData.account_noiohientai;
        txtTenBa.Text = getData.account_tenba;
        txtsdtBa.Text = getData.account_sdtba;
        txtNgheNghiepBa.Text = getData.account_nghenghiepba;
        txtEmailBa.Text = getData.account_emailba;
        txtTenMe.Text = getData.account_tenme;
        txtSDTMe.Text = getData.account_sdtme;
        txtNgheNghiepMe.Text = getData.account_nghenghiepme;
        txtEmailMe.Text = getData.account_emailme;
        txtMongMuon.Text = getData.account_mongmuon;
        dteNgaySinh.Value = getData.account_ngaysinh.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        ddlcoso.Text = getData.coso_name;
        ddlLop.Text = getData.lop_name;
        if (getData.account_gioitinh == false)
            rdNu.Checked = true;
        else
            rdNam.Checked = true;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_Account cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "account_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_Account();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (txtFullName.Text != "" && ddlcoso.Text!="" && ddlLop.Text!="" && dteNgaySinh.Value!="")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        // checkNgayKetThuc();
        // Nếu sale nhập liệu thì chưa chuyển về mail khách
        // Sau khi admin duyệt thì mail mới về cho phụ huynh
        cls_Account cls = new cls_Account();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        int usernameID = logedMember.username_id;
        string saleName = logedMember.username_username;
        //var checkemailphone = from data in db.tbAccounts where data.account_phone == txtPhone.Text select data;
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin dấu (*)!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {

                if (rdNam.Checked == false)
                    gioitinh = false;
                if (cls.Linq_Them(txtFullName.Text, Convert.ToDateTime(dteNgaySinh.Value), txtSDTHocSinh.Value, txtTruongHoc.Value, txtenEn.Text, gioitinh, txtEmailHocSinh.Value, txtNoiSinh.Value, txtChoOHienTai.Value, txtTenBa.Text, txtsdtBa.Text, txtNgheNghiepBa.Text, txtEmailBa.Text, txtTenMe.Text, txtSDTMe.Text, txtNgheNghiepMe.Text, txtEmailMe.Text, txtMongMuon.Text, ddlcoso.Text, Convert.ToInt16(ddlLop.SelectedItem.Value)))
                {

                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công!','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                    popupControl.ShowOnPageLoad = false;
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtFullName.Text, Convert.ToDateTime(dteNgaySinh.Value), txtSDTHocSinh.Value, txtTruongHoc.Value, txtenEn.Text, gioitinh, txtEmailHocSinh.Value, txtNoiSinh.Value, txtChoOHienTai.Value, txtTenBa.Text, txtsdtBa.Text, txtNgheNghiepBa.Text, txtEmailBa.Text, txtTenMe.Text, txtSDTMe.Text, txtNgheNghiepMe.Text, txtEmailMe.Text, txtMongMuon.Text, ddlcoso.Text, Convert.ToInt16(ddlLop.SelectedItem.Value)))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                    popupControl.ShowOnPageLoad = false;
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
        //txtDanhSachChecked.Value = "";
        //txtCountChecked.Value = "";
    }

    protected void ddlcoso_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops 
                            where l.coso_id == Convert.ToInt16(ddlcoso.SelectedItem.Value) 
                            && l.hidden==false && l.lop_tinhtrang==null
                            select l;
        ddlLop.DataBind();
    }



    protected void btnXuatExcel_ServerClick(object sender, EventArgs e)
    {
        string constr = ConfigurationManager.ConnectionStrings["db_vjlcConnectionString"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand(
                "select format([account_ngaydangky], 'dd/MM/yyyy') as [Ngày nhập],coso_name as [Cơ sở], lop_name as [Lơp], account_vn as [Họ và tên], account_ngaysinh as [Ngày sinh], account_en as [English name], Cast(Case When account_gioitinh='True' Then 'Nam' ELse N'Nữ' END AS nVarchar(256)) as [Giới tính], account_phone as [Số điện thoại], account_tenba as [Tên ba], account_sdtba as [Số điện thoại ba], account_tenme as [Tên mẹ], account_sdtme as [Số điện thoại mẹ] from tbAccount,tbhocsinhtronglop,tbLop,tbCoso" +
                " where tbAccount.account_id = tbhocsinhtronglop.account_id" +
                " and tbhocsinhtronglop.lop_id = tbLop.lop_id" +
                " and tbLop.coso_id = tbCoso.coso_id" +
                " and MONTH(tbAccount.account_ngaydangky) = '"+ddlThang.Text+"' and YEAR(tbAccount.account_ngaydangky) = '"+ddlNam.Text+"'"
                ))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "Customers");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=thong-tin-hoc-sinh-vjlc.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        //admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //admin_User là ROOT or ADMIN thì xem được hết danh sách account
        var getData = from nc in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby nc.account_id descending
                      where hstl.hstl_hidden == false && nc.hidden == false && Convert.ToDateTime(nc.account_ngaydangky).Month.ToString()== ddlThang.Text && Convert.ToDateTime(nc.account_ngaydangky).Year.ToString() == ddlNam.Text
                      select new
                      {
                          nc.account_ngaydangky,
                          nc.account_en,
                          account_gioitinh = nc.account_gioitinh == true ? "Nam" : "Nữ",
                          cs.coso_name,
                          l.lop_name,
                          nc.account_id,
                          nc.account_vn,
                          nc.account_phone,
                          nc.account_tenba,
                          nc.account_sdtba,
                          nc.account_sdtme,
                          nc.account_tenme,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
}