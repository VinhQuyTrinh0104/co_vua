﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ChuyenLop.aspx.cs" Inherits="admin_page_module_function_module_ChuyenLop" %>
<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>
<%@ Register Src="~/web_usercontrol/MenuGiaoVien.ascx" TagPrefix="uc1" TagName="MenuGiaoVien" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }
    </style>
    <%--  <div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
     <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block box-admin">
         <div style="float: right">
            <a href="admin-ds-bao-luu" id="btnQuayLai" class="btn btn-primary">Quay lại</a>
        </div>
        <div class="form-group-name">CHUYỂN LỚP</div>
        <br />
        <asp:UpdatePanel ID="upChuyenLop" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp cũ:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                    <label class="col-2">Học sinh:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbHocSinh" runat="server" ValueType="System.Int32" TextField="account_vn" ValueField="account_id" ClientInstanceName="cbbHocSinh" CssClass="" NullText="Chọn học sinh" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckChuNhat" runat="server" Text="Chủ nhật" />
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlCoSoMoi" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSoMoi" OnSelectedIndexChanged="ddlCoSoMoi_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp mới:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlLopMoi" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLopMoi" OnSelectedIndexChanged="ddlLopMoi_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                    <label class="col-2">Số buổi còn lại(tự nhập):</label>
                    <div class="col-4">
                        <input class="form-control boxed" type="text" id="txtSoBuoiConLai" name="name" runat="server" />
                    </div>
                </div>

                <div class="col-12 form-group">
                    <label class="col-3 form-control-label">Ngày bắt đầu(tự nhập) :</label>
                    <div class="col-4">
                        <input class="form-control boxed" type="date" name="name" id="dteNgayBatDau" runat="server" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckMoiThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckMoiChuNhat" runat="server" Text="Chủ nhật" />
                </div>
                 <div class="col-12">
                        <a id="btnKiemtra" runat="server" class="btn btn-primary" onserverclick="btnKiemtra_ServerClick">Kiểm tra ngày kết thúc</a>
                    </div>
                 <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Ngày kết thúc :</label>
                    <div class="col-4">
                        <input class="form-control boxed" type="text" name="name" runat="server" id="dteNgayKetThuc" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lý do chuyển lớp:</label>
                    <div class="col-10">
                        <input type="text" id="txtLyDoChuyenLop" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <div style="text-align: center"><a id="btnXacNhanToanTrungTam" runat="server" class="btn btn-primary" onserverclick="btnXacNhanToanTrungTam_ServerClick">Chuyển lớp</a></div>
                </div>
               
                <div class="col-12 form-group">
                    <div style="text-align: center">--------------------  ***  --------------------  </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

