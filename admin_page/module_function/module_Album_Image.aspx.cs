﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Album_Image : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    int vitri;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddlloaisanpham.DataSource = from tb in db.tbAlbum_Image_Cates select tb;
        ddlloaisanpham.DataBind();
        var getData = from n in db.tbAlbum_Images
                      join tb in db.tbAlbum_Image_Cates on n.album_image_cate_id equals tb.album_image_cate_id
                      orderby n.album_image_id descending
                      select new
                      {
                          n.album_image_id,
                          n.album_image,
                          tb.album_image_cate_name,
                      };
        grvList.DataSource = getData;
        grvList.DataBind();


    }
    private void setNULL()
    {
       
        ddlloaisanpham.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
        loadData();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "album_image_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbAlbum_Images
                       join tb in db.tbAlbum_Image_Cates on n.album_image_cate_id equals tb.album_image_cate_id
                       where n.album_image_id == _id
                       select new
                       {
                           n.album_image_id,
                           n.album_image,
                           tb.album_image_cate_name,
                       }).Single();
        ddlloaisanpham.Text = getData.album_image_cate_name;
       
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.album_image + "'); ", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_AlbumImage cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "album_image_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_AlbumImage();
                tbAlbum_Image checkImage = (from i in db.tbAlbum_Images where i.album_image_id == Convert.ToInt32(item) select i).SingleOrDefault();
                string pathToFiles = Server.MapPath(checkImage.album_image);
                delete(pathToFiles);
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/uploadimages/album_image/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/uploadimages/album_image/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/album_image"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        cls_AlbumImage cls = new cls_AlbumImage();
        
            if (Session["_id"].ToString() == "0")
            {
              
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                else
                {
                }
                if (cls.Linq_Them( image, Convert.ToInt32(ddlloaisanpham.Value.ToString())))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();

                }
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
              
               
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()),image, Convert.ToInt32(ddlloaisanpham.Value.ToString())))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
        
    }
    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }
    
}