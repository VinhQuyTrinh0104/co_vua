﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_SoLienLac.aspx.cs" Inherits="admin_page_module_function_module_SoLienLac" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
       <%-- function btnChiTiet() {
            document.getElementById('<%=btnChuyenLop.ClientID%>').click();
        }--%>
        function popupHide() {
            document.getElementById('btnClosePopup').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= btnLuu.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
    </script>
    <div class="card card-block">
        <div class="row form-group">
            <div class="col-12 form-group">
                <label class="col-1 col-sm-1 form-control-label mb-2">Ngày:</label>
                <input type="text" id="txtNgay" class="" runat="server" disabled="disabled" />
            </div>
            <div class="col-12 form-group">
                <label class="col-1 col-sm-1 form-control-label">Lớp:</label>
                <div class="col-2">
                    <dx:ASPxComboBox ID="cbblop" runat="server" OnSelectedIndexChanged="cbblop_SelectedIndexChanged" TextField="lop_name" ValueField="lop_id" AutoPostBack="true" ClientInstanceName="ddlmh" CssClass="" Width="95%"></dx:ASPxComboBox>
                </div>
                <label class="col-1  col-sm-1 form-control-label">Môn học:</label>
                <div class="col-2">
                    <dx:ASPxComboBox ID="cbbMonhoc" runat="server" OnSelectedIndexChanged="cbbMonhoc_SelectedIndexChanged" TextField="monhoc_name" ValueField="monhoc_id" AutoPostBack="true" ClientInstanceName="ddlmh" CssClass="" Width="95%"></dx:ASPxComboBox>
                </div>
                <%--<div class="col-2">
                    <select id="slTinhtrang" runat="server" class="form-control">
                        <option value="Đi học">Đi học</option>
                        <option value="Vắng">Vắng</option>
                    </select>
                </div>--%>
            </div>
        </div>
        <div class="row form-group">
            <div class="form-group table-responsive">
                <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hstl_id" Width="95%">
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn Caption="ID" FieldName="account_id" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Họ và tên" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Tên Tiếng Anh" FieldName="account_en" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Ngày nhập học" FieldName="hstl_ngaybatdauhoc" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Ngày kết thúc" FieldName="hstl_ngayketthuchoc" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Số điện thoại" FieldName="account_phone" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    </Columns>
                    <%--<ClientSideEvents RowDblClick="btnChiTiet" />--%>
                    <SettingsSearchPanel Visible="true" />
                    <SettingsBehavior AllowFocusedRow="true" />
                    <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                    <SettingsLoadingPanel Text="Đang tải..." />
                    <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                </dx:ASPxGridView>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-8">
                <div class="col-12 form-group">
                    <label <%--class="col-2 form-control-label"--%>>Nội dung thông báo:</label>
                    <div class="col-12">
                        <textarea id="txtdanhgiaGV" runat="server" rows="5" style="width: 90%"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <label>Nhận xét điểm:</label>
                <div class="col-12">
                    <label class="col-4 form-control-label">Điểm giữa kì:</label>
                    <div class="col-8">
                        <textarea id="txtKqgiuaky" runat="server" rows="2" style="width: 90%"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <label class="col-4 form-control-label">Điểm cuối kì:</label>
                    <div class="col-8">
                        <textarea id="txtKqcuoiky" runat="server" rows="2" style="width: 90%"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnLuu" runat="server" CssClass="btn btn-primary" Text="Lưu" OnClick="btnLuu_Click" />

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
