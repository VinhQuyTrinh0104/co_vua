﻿using ClosedXML.Excel;
using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_module_NghiHan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (checkButton.groupuser_id == 6)
            {
                // btnChiTiet.Visible = false;
              
                //menuNhanVien.Visible = true;
                //menuQuanTri.Visible = false;
                //menuGiaoVien.Visible = false;
            }
            else
            {
                if (checkButton.groupuser_id == 2)
                {
                    //menuNhanVien.Visible = false;
                    //menuQuanTri.Visible = true;
                    //menuGiaoVien.Visible = false;
                    // btnChiTiet.Visible = true;
                   
                }
                else
                {
                    //menuNhanVien.Visible = false;
                    //menuQuanTri.Visible = false;
                    //menuGiaoVien.Visible = false;
                }
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            
            loadDataNghiHan();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadDataNghiHan()
    {

        // load data đổ vào var danh sách
        var getData = from bl in db.tbHocPhiBaoLuus
                      join hstl in db.tbhocsinhtronglops on bl.hstl_id equals hstl.hstl_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      //join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      where bl.baoluu_loai == "Nghỉ hẵn"
                      orderby bl.baoluu_id descending
                      select new
                      {
                          hs.account_id,
                          hstl.hstl_id,
                          bl.baoluu_name,
                          bl.baoluu_id,
                          hs.account_vn,
                          bl.baoluu_ngayketthuc,
                          bl.baoluu_ngayketthuc_hidden,
                          hocphichitiet_ngaybatdau = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_ngaybatdau,
                          hocphichitiet_sobuoihoc = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
                          bl.baoluu_sobuoi,
                          l.lop_name,
                          bl.baoluu_loai,
                          cs.coso_name,
                          bl.baoluu_NgayNhap,
                          bl.baoluu_ghichu,
                          bl.baoluu_tinhtrang
                      };
        // đẩy dữ liệu vào gridivew
        grvNghiHan.DataSource = getData;
        grvNghiHan.DataBind();
    }

    protected void btnXemthongTinHocSinh_ServerClick(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(grvNghiHan.GetRowValues(grvNghiHan.FocusedRowIndex, new string[] { "account_id" }));
        Response.Redirect("admin-xem-chi-tiet-hoc-sinh-nghi-han-" + id);
    }
}