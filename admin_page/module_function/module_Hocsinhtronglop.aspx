﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_Hocsinhtronglop.aspx.cs" Inherits="admin_page_module_function_module_Hocsinhtronglop" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function popupHide() {
            document.getElementById('btnClosePopup').click();
        }
        <%-- function checkNULL() {
            var CityName = document.getElementById('<%= btnbaoluu.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
--%>

</script>
    <style>
        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }
    </style>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-12 form-group">
                <label class="col-1 form-control-label">Lớp:</label>
                <div class="col-3">
                    <dx:ASPxComboBox ID="ddllop" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlmh" AutoPostBack="true" NullText="Chọn lớp" CssClass="" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-12 form-group">
                <label class="col-1 form-control-label">Môn Học:</label>
                <div class="col-3">
                    <dx:ASPxComboBox ID="ddlmh" runat="server" OnSelectedIndexChanged="ddlmh_SelectedIndexChanged" ValueType="System.Int32" TextField="monhoc_name" ValueField="monhoc_id" ClientInstanceName="ddlmh" CssClass="" AutoPostBack="true" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-12 form-group">
                <label class="col-1 form-control-label">Chuyển Lớp Học:</label>
                <div class="col-3">
                    <dx:ASPxComboBox ID="ddlchuyenlop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlchuyenlop" CssClass="" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <asp:UpdatePanel ID="upBtn" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-sm-2">
                    <asp:UpdatePanel ID="udButton" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnChuyenLop" runat="server" Text="Chuyển Lớp" CssClass="btn btn-primary" OnClick="btnChuyenLop_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="col-sm-2">
                    <asp:UpdatePanel ID="udButton1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnbaoluu" runat="server" Text="Bảo Lưu" CssClass="btn btn-primary" OnClick="btnbaoluu_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>--%>
                <div class="col-sm-2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnNghihoc" runat="server" Text="Nghỉ Học" CssClass="btn btn-primary" OnClick="btnNghihoc_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hstl_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Họ và tên học sinh" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Tên tiếng anh" FieldName="account_en" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Năm sinh" FieldName="account_namsinh" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Môn học" FieldName="monhoc_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tình trạng" FieldName="hstl_baoluu" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxPopupControl ID="showPopup" runat="server" Width="800px" Height="600px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="showPopup"
        HeaderText="CHI TIẾT HỌC SINH" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Học Sinh:</label>
                                        <div class="col-9">
                                            <input id="txtHoten" runat="server" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Lớp:</label>
                                        <div class="col-9">
                                            <input id="txtLop" runat="server" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Sinh ngày:</label>
                                        <div class="col-9">
                                            <input id="txtNgaysinh" runat="server" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Môn học:</label>
                                        <div class="col-9">
                                            <input id="txtMonHoc" runat="server" class="form-control" disabled="disabled" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Ngày bắt đầu học :</label>
                                                    <input class="form-control boxed" type="date" name="name" runat="server" id="txtDateStart" />
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label>Thời gian học :</label>
                                                    <div class="input-group-append">
                                                        <input class="form-control boxed" type="number" min="0" name="name" runat="server" id="txtThoiGianHoc" />
                                                        <span class="input-group-text" id="basic-addon2">Tháng</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <label style="display: block">Cơ sở :</label>
                                                    <dx:ASPxComboBox ID="ddlcoso" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlcoso" CssClass="" Height="40px" Width="95%"></dx:ASPxComboBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="col-6">
                                        <div class="mar_but button">
                                            <asp:Button ID="btnLuu" Visible="false" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="ghichu">
                                            <label>Ghi chú khi bảo lưu</label>
                                            <textarea id="txtGhichu" placeholder="nội dung ghi chú" runat="server" rows="5" style="width: 90%"></textarea>
                                        </div>
                                        <div class="mar_but button">
                                            <asp:Button ID="btnBaoLuu" runat="server" ClientIDMode="Static" Text="Bảo lưu" CssClass="btn btn-primary" OnClick="btnBaoLuu_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
        </FooterContentTemplate>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

