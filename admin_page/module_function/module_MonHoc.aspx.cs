﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_MonHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
            // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from nc in db.tbMonHocs select nc;
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txttensanpham.Text = "";
        
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "monhoc_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbMonHocs
                       where nc.monhoc_id == _id
                       select nc).Single();
        txttensanpham.Text = getData.monhoc_name;
       
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_MonHoc cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "monhoc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_MonHoc();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_MonHoc cls = new cls_MonHoc();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                if (cls.Linq_Them(txttensanpham.Text))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                } 
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
    }
    //protected void btnShow_Click(object sender, EventArgs e)
    //{
    //    List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "newscate_id" });
    //    if (selectedKey.Count > 0)
    //    {
    //        foreach (var item in selectedKey)
    //        {
    //            tbNewCate check = (from c in db.tbNewCates where c.newscate_id == Convert.ToInt32(item) select c).SingleOrDefault();
    //            if (check!=null)
    //            {
    //                check.newscate_show = true;
    //                db.SubmitChanges();
    //                alert.alert_Success(Page, "Đã cho hiển thị", "");
    //            }
                    
    //            else
    //                alert.alert_Error(Page, "Thất bại", "");
    //        }
    //    }
    //    else
    //        alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    //}
    //protected void btnShowhiden_Click(object sender, EventArgs e)
    //{
    //    List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "newscate_id" });
    //    if (selectedKey.Count > 0)
    //    {
    //        foreach (var item in selectedKey)
    //        {
    //            tbNewsCate check = (from c in db.tbNewsCates where c.newscate_id == Convert.ToInt32(item) select c).SingleOrDefault();
    //            if (check != null)
    //            {
    //                check.newscate_show = false;
    //                db.SubmitChanges();
    //                alert.alert_Success(Page, "Đã hủy", "");
    //            }

    //            else
    //                alert.alert_Error(Page, "Thất bại", "");
    //        }
    //    }
    //    else
    //        alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    //}
}