﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_TaoHocsinhtronglop.aspx.cs" Inherits="admin_page_module_function_module_TaoHocsinhtronglop" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myfocus1() 
        {
            document.getElementById("<%=txtSoTienDongKhoaHoc.ClientID%>").value = document.getElementById("<%=txtSoTienDongKhoaHoc_Hidden.ClientID%>").value
        }
        function myfocus2() {
            document.getElementById("<%=txtSoTienDaDong.ClientID%>").value = document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value
        }
        function myfocus3() {
            document.getElementById("<%=txtGiaoTrinh.ClientID%>").value = document.getElementById("<%=txtGiaoTrinh_Hidden.ClientID%>").value
        }
        function myfocus4() {
            document.getElementById("<%=txtSoTienConLai.ClientID%>").value = document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value
        }
        function myfocus5() {
            document.getElementById("<%=txtChuongTrinhGiamHocPhi.ClientID%>").value = document.getElementById("<%=txtChuongTrinhGiamHocPhi_Hidden.ClientID%>").value
         }
        function myDoiTienTe1(id) {

            const value = id.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            document.getElementById("<%=txtSoTienDongKhoaHoc.ClientID%>").value = this.value
            document.getElementById("<%=txtSoTienDongKhoaHoc_Hidden.ClientID%>").value = Number(this.value.replace(/[.]/g, ""))
            let sotienKhoaHoc = document.getElementById("<%=txtSoTienDongKhoaHoc_Hidden.ClientID%>").value;
            let sotiencoc = document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value
            if (sotienKhoaHoc != "" || sotiencoc != "") {
                let sotienconlai = sotienKhoaHoc - sotiencoc;
                document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value = sotienconlai;
                const value = document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('vn', {
                    style: 'decimal',
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0
                });
                document.getElementById("<%=txtSoTienConLai.ClientID%>").value = this.value
            }
        }
        function myDoiTienTe2(id) {
            const value = id.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            //number-format the user input
            document.getElementById("<%=txtSoTienDaDong.ClientID%>").value = this.value
            //set the numeric value to a number input
            document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value = Number(this.value.replace(/[.]/g, ""))
            let sotienKhoaHoc = document.getElementById("<%=txtSoTienDongKhoaHoc_Hidden.ClientID%>").value;
            let sotiencoc = document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value;
            let tiengiaotrinh = document.getElementById("<%=txtGiaoTrinh_Hidden.ClientID%>").value;
            let chuongtrinhgiam = document.getElementById("<%=txtChuongTrinhGiamHocPhi_Hidden.ClientID%>").value;
            let sotienconlai = Number(sotienKhoaHoc) - Number(sotiencoc) - Number(chuongtrinhgiam);
            let tongthu = Number(sotiencoc) + Number(tiengiaotrinh);
            if (sotienKhoaHoc != "" || sotiencoc != "" || chuongtrinhgiam != "") {
                document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value = sotienconlai;
                const value = document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('vn', {
                    style: 'decimal',
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0
                });
                document.getElementById("<%=txtSoTienConLai.ClientID%>").value = this.value
            }
            if (tiengiaotrinh != "" || sotiencoc != "" || chuongtrinhgiam != "") {
                document.getElementById("<%=txtTongThu_Hidden.ClientID%>").value = tongthu;
                const value = document.getElementById("<%=txtTongThu_Hidden.ClientID%>").value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('vn', {
                    style: 'decimal',
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0
                });
                document.getElementById("<%=txtTongThu.ClientID%>").value = this.value
            }
        }
        function myDoiTienTe3(id) {
            const value = id.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            //number-format the user input
            document.getElementById("<%=txtGiaoTrinh.ClientID%>").value = this.value
            //set the numeric value to a number input
            document.getElementById("<%=txtGiaoTrinh_Hidden.ClientID%>").value = Number(this.value.replace(/[.]/g, ""));
            let sotiencoc = document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value;
            let tiengiaotrinh = document.getElementById("<%=txtGiaoTrinh_Hidden.ClientID%>").value;
            let tongthu = Number(sotiencoc) + Number(tiengiaotrinh);
            if (tiengiaotrinh != "" || sotiencoc != "") {
                document.getElementById("<%=txtTongThu_Hidden.ClientID%>").value = tongthu;
                const value = document.getElementById("<%=txtTongThu_Hidden.ClientID%>").value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('vn', {
                    style: 'decimal',
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0
                });
                document.getElementById("<%=txtTongThu.ClientID%>").value = this.value
            }
        }
        <%--function myDoiTienTe4(id) {
            const value = id.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            //number-format the user input
            document.getElementById("<%=txtSoTienConLai.ClientID%>").valuee = this.value
            //set the numeric value to a number input
            document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value = Number(this.value.replace(/[.]/g, ""))
            const value = this.value.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            document.getElementById("<%=txtSoTienConLai.ClientID%>").value = this.value;
        }--%>
    
        function myDoiTienTe5(id) {
            const value = id.replace(/,/g, '');
            this.value = parseFloat(value).toLocaleString('vn', {
                style: 'decimal',
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });
            //number-format the user input
            document.getElementById("<%=txtChuongTrinhGiamHocPhi.ClientID%>").value = this.value
            //set the numeric value to a number input
            document.getElementById("<%=txtChuongTrinhGiamHocPhi_Hidden.ClientID%>").value = Number(this.value.replace(/[.]/g, ""))
            let sotienKhoaHoc = document.getElementById("<%=txtSoTienDongKhoaHoc_Hidden.ClientID%>").value;
            let chuongtrinhgiamhocphi = document.getElementById("<%=txtChuongTrinhGiamHocPhi_Hidden.ClientID%>").value
            let sotiencoc = document.getElementById("<%=txtSoTienDaDong_Hidden.ClientID%>").value;
            if (sotienKhoaHoc != "" || sotiencoc != "" || chuongtrinhgiamhocphi != "") {
                let sotienconlai = sotienKhoaHoc - sotiencoc - chuongtrinhgiamhocphi;
                document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value = sotienconlai;
                const value = document.getElementById("<%=txtSoTienConLai_Hidden.ClientID%>").value.replace(/,/g, '');
                this.value = parseFloat(value).toLocaleString('vn', {
                    style: 'decimal',
                    maximumFractionDigits: 0,
                    minimumFractionDigits: 0
                });
                    document.getElementById("<%=txtSoTienConLai.ClientID%>").value = this.value;
           }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
            function func() {
                grvList.Refresh();
            }
            function btnluu() {
                document.getElementById('<%=btnLuu.ClientID%>').click();
            }

            function OnUnselectAllRowsLinkClick() {
                grvList.UnselectRows();
            }
    </script>
    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }
    </style>
    <div class="card card-block">
        <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">THÔNG TIN ĐÓNG HỌC PHÍ</div>
        <br />
        <div style="float: right">
            <a href="admin-dong-hoc-phi" id="btnQuayLai" class="btn btn-primary">Quay lại</a>
        </div>
        <div class="col-12 form-group">
            <label class="col-3">Tình trạng đóng học phí:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlTinhTrangDongHocPhi" runat="server" ValueType="System.Int32" TextField="tinhtrangdong_name" ValueField="tinhtrangdong_id" ClientInstanceName="ddlTinhTrangDongHocPhi" CssClass="" NullText="Chọn tình trạng"  Width="95%"></dx:ASPxComboBox>
            </div>
            <label class="col-1">Chi tiết:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlChiTiet" runat="server" ValueType="System.Int32" TextField="chitiettinhtrang_name" ValueField="chitiettinhtrang_id" ClientInstanceName="ddlChiTiet" CssClass="" NullText="Chọn tình trạng" Width="95%" OnSelectedIndexChanged="ddlChiTiet_SelectedIndexChanged" AutoPostBack="true"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-3">Cơ sở:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
            </div>

        </div>
        <asp:UpdatePanel ID="upLop" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <label class="col-3 form-control-label">Lớp Học:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddllop" runat="server" ValueType="System.Int32" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddllop" CssClass="" NullText="Chọn Lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                    <label class="col-1 form-control-label">Giờ học:</label>
                    <div class="col-4">
                        <b>
                            <asp:Label ID="lblGioHoc" runat="server"></asp:Label></b>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3 form-control-label">Học sinh:</label>
                    <div class="col-4">
                        <%-- OnSelectedIndexChanged="ddlHocSinh_SelectedIndexChanged" AutoPostBack="true"--%> 
                        <dx:ASPxComboBox ID="ddlHocSinh" runat="server" ValueType="System.Int32" TextField="account_vn" ValueField="account_id" ClientInstanceName="ddlHocSinh" CssClass="" NullText="Chọn học sinh" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckChuNhat" runat="server" Text="Chủ nhật" />
                </div>

                <%--thời gian học--%>
                <div id="div_NgayBatDau" runat="server" class="col-12 form-group">
                    <label class="col-3 form-control-label">Ngày bắt đầu học :</label>
                    <div class="col-4">
                        <input class="form-control boxed" type="date" name="name" runat="server" id="dteNgayBatDauHoc" />
                    </div>
                    <label class="col-2 form-control-label">Số buổi học :</label>
                    <div class="col-1">
                        <input class="form-control boxed" name="name" runat="server" id="txtTongSoBuoiHoc" />
                    </div>
                    <div class="col-2">
                        <a id="btnKiemtra" runat="server" class="btn btn-primary" onserverclick="btnKiemtra_ServerClick">Kiểm tra ngày kết thúc</a>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-3 form-control-label">Ngày kết thúc :</label>
                        <div class="col-4">
                            <input class="form-control boxed" type="text" name="name" runat="server" id="dteNgayKetThuc" />
                        </div>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Số tiền đóng khóa học:</label>
                    <div class="col-4">
                        <input type="text" id="txtSoTienDongKhoaHoc" runat="server" onfocus="myfocus1()" onblur="myDoiTienTe1(this.value)" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Chương trình giảm học phí:</label>
                    <div class="col-4">
                        <input type="text" id="txtChuongTrinhGiamHocPhi" runat="server" onfocus="myfocus5()" onblur="myDoiTienTe5(this.value)" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Số tiền đã đóng (đặt cọc):</label>
                    <div class="col-4">
                        <input type="text" id="txtSoTienDaDong" runat="server" onfocus="myfocus2()" onblur="myDoiTienTe2(this.value)" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Số tiền còn lại:</label>
                    <div class="col-4">
                        <input type="text" id="txtSoTienConLai" runat="server" onfocus="myfocus4()" onblur="myDoiTienTe4(this.value)" class="form-control" />
                    </div>
                    <label class="col-2">Số tiền giáo trình:</label>
                    <div class="col-3">
                        <input type="text" id="txtGiaoTrinh" runat="server" onfocus="myfocus3()" onblur="myDoiTienTe3(this.value)" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Tổng thu:</label>
                    <div class="col-4">
                        <input type="text" id="txtTongThu" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Phiếu thu quyển:</label>
                    <div class="col-4">
                        <input type="text" id="txtPhieuThu" runat="server" class="form-control" />
                    </div>
                    <label class="col-2">Mã số:</label>
                    <div class="col-3">
                        <input type="text" id="txtMaSo" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Nhân viên chiêu sinh:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlNhanVien" runat="server" ValueType="System.Int32" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlNhanVien" CssClass="" NullText="Chọn nhân viên" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Hình thức thanh toán:</label>
                    <div class="col-4">
                        <asp:DropDownList ID="ddlThanhToan" runat="server" CssClass="form-control">
                            <asp:ListItem Value="Tiền mặt">Tiền mặt</asp:ListItem>
                            <asp:ListItem Value="Chuyển khoản">Chuyển khoản</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-3">Ghi chú:</label>
                    <div class="col-9">
                        <input type="text" id="txtGhiChu" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <asp:UpdatePanel ID="udButton" runat="server">
                            <ContentTemplate>
                                <div class="mar_but button">
                                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="display:none">
            số tiền đóng khóa học<input id="txtSoTienDongKhoaHoc_Hidden" runat="server" type="text" />
            giảm học phí
            <input id="txtChuongTrinhGiamHocPhi_Hidden" runat="server" type="text" />

            số tiền cọc
        <input id="txtSoTienDaDong_Hidden" runat="server" type="text" />
            số tiền còn lại
        <input id="txtSoTienConLai_Hidden" runat="server" type="text" />
            giáo trình
        <input id="txtGiaoTrinh_Hidden" runat="server" type="text" />
            Tổng thu
         <input id="txtTongThu_Hidden" runat="server" type="text" />
            <input id="txtDanhSachChecked" runat="server" type="text" placeholder="idthu" />
            <input id="txtCountChecked" runat="server" type="text" />
        </div>
    </div>
    <script>
            function checkid(id) {
                var arrayvalue = document.getElementById("<%= txtDanhSachChecked.ClientID %>").value;
            var array = JSON.parse("[" + arrayvalue + "]");
            var index = array.indexOf(id);
            if (index > -1) {
                array.splice(index, 1);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
                document.getElementById("<%= txtCountChecked.ClientID %>").value = document.getElementById("<%= txtCountChecked.ClientID %>").value - 1;
            }
            else {
                document.getElementById("<%= txtCountChecked.ClientID %>").value = array.push(id);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
                }
            }

    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

