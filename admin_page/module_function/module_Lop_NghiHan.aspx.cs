﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Lop_NghiHan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (checkButton.groupuser_id == 6)
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = false;
                //menuNhanVien.Visible = true;
                //menuQuanTri.Visible = false;
                //btnDuyet.Visible = false;
            }
            else
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = true;
                //menuNhanVien.Visible = false;
                //menuQuanTri.Visible = true;
                //btnDuyet.Visible = false;
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getCoSo = from tt in db.tbCosos where tt.coso_hidden==false select tt;
        ddlCoSo.DataSource = getCoSo;
        ddlCoSo.DataBind();
        ddlNuocNgoai.DataSource = from u in db.admin_Users where u.groupuser_id == 4 select u;
        ddlNuocNgoai.DataBind();
        ddlVietNam.DataSource = from u in db.admin_Users where u.groupuser_id == 3 select u;
        ddlVietNam.DataBind();
        ddlTroGiang.DataSource = from u in db.admin_Users where u.groupuser_id == 5 select u;
        ddlTroGiang.DataBind();
        var getData = from nc in db.tbLops
                      join cs in db.tbCosos on nc.coso_id equals cs.coso_id
                      where nc.hidden == false && nc.lop_tinhtrang =="Lớp cũ"
                      orderby nc.lop_id descending
                      select new
                      {
                          nc.lop_NgayNghiHan,
                          cs.coso_name,
                          nc.lop_id,
                          nc.lop_name,
                          thoigian = nc.giohoc_batdau + " - " + nc.giohoc_ketthuc,
                          nc.lop_thu2,
                          nc.lop_thu3,
                          nc.lop_thu4,
                          nc.lop_thu5,
                          nc.lop_thu6,
                          nc.lop_thu7,
                          nc.lop_chunhat,
                          nc.lop_ghichu,
                          lop_siso = (from hstl in db.tbhocsinhtronglops
                                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                      where hstl.lop_id ==nc.lop_id && hstl.hstl_hidden == false && hs.hidden == false
                                      select hstl).Count()>0? (from hstl in db.tbhocsinhtronglops
                                                               join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                                               where hstl.lop_id == nc.lop_id && hstl.hstl_hidden == false && hs.hidden==false select hstl).Count():0,
                          //buoihoc = nc.lop_thu2==true?"Thứ 2 &": nc.lop_thu3 == true ? "Thứ 3 &" : nc.lop_thu4==true?"Thứ 4":"", 
                          username_idEn = (from n in db.tbLops
                                           join gv in db.admin_Users on nc.username_idEn equals gv.username_id
                                           //n.username_idEn == gv.username_id
                                           select gv.username_fullname).FirstOrDefault(),
                          username_idVn = (from n in db.tbLops
                                           join gv in db.admin_Users on nc.username_idVn equals gv.username_id
                                           // where n.lop_id == nc.lop_id && n.username_idVn == gv.username_id
                                           select gv.username_fullname).FirstOrDefault(),
                          username_trogiang_id = (from n in db.tbLops
                                           join gv in db.admin_Users on nc.username_trogiang_id equals gv.username_id
                                           select gv.username_fullname).FirstOrDefault(),
                          //siso = (from ss in db.tbhocsinhtronglops where ss.lop_id == nc.lop_id select ss).Count()
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
       // lblTongSiSo.Text = (from hstl in db.tbhocsinhtronglops where hstl.hstl_hidden==false select hstl).Count() + "";
    }
 

    private void setNULL()
    {
        txtLop.Text = "";
        ddlCoSo.Text = "";
        txtTuGio.Text = "";
        txtDenGio.Text = "";
        ddlNuocNgoai.Text = "";
        ddlVietNam.Text = "";
        ddlTroGiang.Text = "";
        txtGhiChu.Text = "";
        ckCN.Checked = false;
        ckThu2.Checked = false;
        ckThu3.Checked = false;
        ckThu4.Checked = false;
        ckThu5.Checked = false;
        ckThu6.Checked = false;
        ckThu7.Checked = false;
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lop_id" }));
        // đẩy id vào session
        Session["_id"] = _id;

        var getData = (from nc in db.tbLops
                       join cs in db.tbCosos on nc.coso_id equals cs.coso_id
                       where nc.lop_id == _id
                       select new { 
                        cs.coso_name,
                        nc.lop_name,
                        nc.giohoc_batdau,
                        nc.giohoc_ketthuc,
                        nc.lop_thu2,
                           nc.lop_thu3,
                           nc.lop_thu4,
                           nc.lop_thu5,
                           nc.lop_thu6,
                           nc.lop_thu7,
                           nc.lop_ghichu,
                           nc.lop_chunhat,
                           nc.username_idEn,
                           nc.username_idVn,
                           nc.username_trogiang_id
                       }).Single();
        ddlCoSo.Text = getData.coso_name;
        txtLop.Text = getData.lop_name;
        txtTuGio.Text = getData.giohoc_batdau;
        txtDenGio.Text = getData.giohoc_ketthuc;
        txtGhiChu.Text = getData.lop_ghichu;
        if (getData.lop_thu2 == true)
            ckThu2.Checked = true;
        else
            ckThu2.Checked = false;
        if (getData.lop_thu3 == true)
            ckThu3.Checked = true;
        else
            ckThu3.Checked = false;
        if (getData.lop_thu4 == true)
            ckThu4.Checked = true;
        else
            ckThu4.Checked = false;
        if (getData.lop_thu5 == true)
            ckThu5.Checked = true;
        else
            ckThu5.Checked = false;
        if (getData.lop_thu6 == true)
            ckThu6.Checked = true;
        else
            ckThu6.Checked = false;
        if (getData.lop_thu7 == true)
            ckThu7.Checked = true;
        else
            ckThu7.Checked = false;
        if (getData.lop_chunhat == true)
            ckCN.Checked = true;
        else
            ckCN.Checked = false;
        if (getData.username_idEn == null && getData.username_idVn == null)
        {
            txtLop.Text = getData.lop_name;
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        }
        else
        {
            ddlVietNam.Text = (from n in db.admin_Users
                            where n.username_id == getData.username_idVn
                            select n).FirstOrDefault().username_fullname;
            ddlNuocNgoai.Text = (from n in db.admin_Users
                            where n.username_id == getData.username_idEn
                            select n).FirstOrDefault().username_fullname;
            ddlTroGiang.Text = (from n in db.admin_Users
                                 where n.username_id == getData.username_trogiang_id
                                 select n).FirstOrDefault().username_fullname;
            var getGVVN = from u in db.admin_Users
                          where u.groupuser_id == 3
                          select u;
            ddlVietNam.DataSource = getGVVN;
            ddlVietNam.DataBind();
            var getGVEN = from u in db.admin_Users
                          where u.groupuser_id == 4
                          select u;
            ddlNuocNgoai.DataSource = getGVEN;
            ddlNuocNgoai.DataBind();
            var getGVTroGiang = from u in db.admin_Users
                          where u.groupuser_id == 5
                          select u;
            ddlTroGiang.DataSource = getGVTroGiang;
            ddlTroGiang.DataBind();
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_Lop cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lop_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_Lop();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    //Lưu Lịch Sử Thao Tác
                    var getData = (from lp in db.tbLops
                                   where lp.lop_id == Convert.ToInt32(item)
                                   select new
                                   {
                                       lp.lop_id,
                                       lp.lop_name
                                   }).First();
                    tbOperationHistory ins = new tbOperationHistory();
                    ins.history_day = DateTime.Now;
                    ins.username_id = logedMember.username_id;
                    ins.history_content = "Xóa lớp " + getData.lop_name;
                    db.tbOperationHistories.InsertOnSubmit(ins);
                    db.SubmitChanges();
                }
                else
                {
                    alert.alert_Error(Page, "Xóa thất bại", "");
                }
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (txtLop.Text != "" && ddlCoSo.Text!="" && txtTuGio.Text!="" && txtDenGio.Text != "" && ddlNuocNgoai.Text!="" && ddlTroGiang.Text!=""&& ddlVietNam.Text!="")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_Lop cls = new cls_Lop();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                //kiểm tra tên lớp đã có chưa
                var check = from l in db.tbLops
                            where l.lop_name == txtLop.Text && l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            select l;
                if (check.Count() > 0)
                {
                    alert.alert_Warning(Page, "Tên lớp đã tồn tại! Vui lòng kiểm tra lại", "");
                }
                else
                {
                    if (cls.Linq_Them(Convert.ToInt16(ddlCoSo.SelectedItem.Value), txtLop.Text,txtTuGio.Text,txtDenGio.Text,ckThu2.Checked, ckThu3.Checked,ckThu4.Checked,ckThu5.Checked,ckThu6.Checked,ckThu7.Checked,ckCN.Checked,Convert.ToInt16(ddlNuocNgoai.SelectedItem.Value), Convert.ToInt16(ddlVietNam.SelectedItem.Value), Convert.ToInt16(ddlTroGiang.SelectedItem.Value),txtGhiChu.Text))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.Refresh();})", true);
                        loadData();
                        //Lưu Lịch Sử Thao Tác
                        tbOperationHistory ins = new tbOperationHistory();
                        ins.history_day = DateTime.Now;
                        ins.username_id = logedMember.username_id;
                        ins.history_content = "Thêm lớp " + txtLop.Text;
                        db.tbOperationHistories.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    else alert.alert_Error(Page, "Thêm thất bại", "");
                }
            }
            else
            {
                var getData = (from nc in db.tbLops
                               where nc.lop_id == Convert.ToInt32(Session["_id"].ToString())
                               select nc).Single();
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), Convert.ToInt16(ddlCoSo.SelectedItem.Value), txtLop.Text, txtTuGio.Text, txtDenGio.Text, ckThu2.Checked, ckThu3.Checked, ckThu4.Checked, ckThu5.Checked, ckThu6.Checked, ckThu7.Checked, ckCN.Checked, Convert.ToInt16(ddlNuocNgoai.SelectedItem.Value), Convert.ToInt16(ddlVietNam.SelectedItem.Value), Convert.ToInt16(ddlTroGiang.SelectedItem.Value),txtGhiChu.Text))
                {
                    
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                    //Lưu Lịch Sử Thao Tác
                    tbOperationHistory ins = new tbOperationHistory();
                    ins.history_day = DateTime.Now;
                    ins.username_id = logedMember.username_id;
                    ins.history_content = "Sửa lớp " + getData.lop_name +" thành lớp "+txtLop.Text;
                    db.tbOperationHistories.InsertOnSubmit(ins);
                    db.SubmitChanges();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
        //popupControl.ShowOnPageLoad = false;
    }

    protected void btnLopCu_Click(object sender, EventArgs e)
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_Lop cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lop_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_Lop();
                if (cls.Linq_ChuyenLopCu(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Đã chuyễn về lớp nghỉ hẵn thành công", "");
                    //Lưu Lịch Sử Thao Tác
                    var getData = (from lp in db.tbLops
                                   where lp.lop_id == Convert.ToInt32(item)
                                   select new
                                   {
                                       lp.lop_id,
                                       lp.lop_name
                                   }).First();
                }
                else
                {
                    alert.alert_Error(Page, "Đã chuyển thất bại", "");
                }
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
}