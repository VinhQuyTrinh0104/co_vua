﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Hocsinhtronglop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();

    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        //BindDropdown();

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getData = from hstl in db.tbhocsinhtronglops
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      join mh in db.tbMonHocs on hstl.monhoc_id equals mh.monhoc_id
                      where hstl.hstl_hidden == false && hs.account_coso == logedMember.username_coso
                      orderby hstl.hstl_id descending
                      select new
                      {
                          hstl.hstl_id,
                          l.lop_id,
                          l.lop_name,
                          hs.account_id,
                          hs.account_vn,
                          hs.account_en,
                          hs.account_namsinh,
                          mh.monhoc_id,
                          mh.monhoc_name,
                          hstl_baoluu = hstl.hstl_baoluu == false ? "Đang học" : "Bảo lưu"
                      };
        grvList.DataSource = getData;
        grvList.DataBind();

        ddllop.DataSource = from l in db.tbLops
                            where l.hidden == false
                            select l;
        ddllop.DataBind();
        ddlmh.DataSource = from mh in db.tbMonHocs
                           select mh;
        ddlmh.DataBind();
        ddlchuyenlop.DataSource = from l in db.tbLops
                                  where l.hidden == false
                                  select l;
        ddlchuyenlop.DataBind();


    }
    protected void btnChuyenLop_Click(object sender, EventArgs e)
    {

        cls_Hocsinhtronglop cls = new cls_Hocsinhtronglop();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hstl_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                if (cls.Linq_Sua(Convert.ToInt32(item), Convert.ToInt32(ddlchuyenlop.Value.ToString())))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển lớp thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                    //alert.alert_Success(Page, "Chuyển lớp thành công!", "");
                    loadData();
                    //Lưu lịch sử thao tác
                    tbOperationHistory ins = new tbOperationHistory();
                    ins.history_day = DateTime.Now;
                    ins.username_id = logedMember.username_id;
                    ins.history_content = "Chuyển học sinh ";
                }
                else
                    alert.alert_Error(Page, "Chuyển lớp thất bại", "");
            }
        }

    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getData = from hstl in db.tbhocsinhtronglops
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      join mh in db.tbMonHocs on hstl.monhoc_id equals mh.monhoc_id
                      where hstl.hstl_hidden == false && hstl.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value) && hs.account_coso == logedMember.username_coso
                      orderby hstl.hstl_id descending
                      select new
                      {
                          hstl.hstl_id,
                          l.lop_id,
                          l.lop_name,
                          hs.account_id,
                          hs.account_vn,
                          hs.account_en,
                          hs.account_namsinh,
                          //hs.account_ngaybatdau,
                          //hs.account_ngayketthuc,
                          mh.monhoc_id,
                          mh.monhoc_name,
                          hstl_baoluu = hstl.hstl_baoluu == false ? "Đang học" : "Đang bảo lưu"
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void ddlmh_SelectedIndexChanged(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getData = from hstl in db.tbhocsinhtronglops
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      join mh in db.tbMonHocs on hstl.monhoc_id equals mh.monhoc_id
                      where hstl.hstl_hidden == false
                      && hstl.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value)
                      && hstl.monhoc_id == Convert.ToInt32(ddlmh.SelectedItem.Value)
                      && hs.account_coso == logedMember.username_coso
                      orderby hstl.hstl_id descending
                      select new
                      {
                          hstl.hstl_id,
                          l.lop_id,
                          l.lop_name,
                          hs.account_id,
                          hs.account_vn,
                          hs.account_en,
                          hs.account_namsinh,
                          //hs.account_ngaybatdau,
                          //hs.account_ngayketthuc,
                          mh.monhoc_id,
                          mh.monhoc_name,
                          hstl_baoluu = hstl.hstl_baoluu == false ? "Đang học" : "Đang bảo lưu"
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hstl_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from l in db.tbLops
                       join hstl in db.tbhocsinhtronglops on l.lop_id equals hstl.lop_id
                       join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                       join mh in db.tbMonHocs on hstl.monhoc_id equals mh.monhoc_id
                       where hstl.hstl_id == _id
                       select new
                       {
                           hstl.hstl_id,
                           l.lop_id,
                           l.lop_name,
                           hs.account_id,
                           hs.account_vn,
                           hs.account_en,
                           hs.account_namsinh,
                           hs.account_coso,
                           //hstl.hstl_ngaybatdauhoc,
                           //hstl.hstl_ngayketthuchoc,
                           //hstl.hstl_thoigianhoc,
                           mh.monhoc_id,
                           mh.monhoc_name,
                           hstl.hstl_ghichu
                       }).FirstOrDefault();
        txtHoten.Value = getData.account_vn;
        txtMonHoc.Value = getData.monhoc_name;
        txtNgaysinh.Value = Convert.ToString(getData.account_namsinh.Value.ToString("dd-MM-yyyy").Replace(' ', 'T'));
        //txtDateStart.Value = getData.hstl_ngaybatdauhoc.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        //txtThoiGianHoc.Value = getData.hstl_thoigianhoc;
        txtLop.Value = getData.lop_name;
        txtGhichu.Value = getData.hstl_ghichu;
        ddlcoso.Text = getData.account_coso;
        ddlcoso.DataSource = from cs in db.tbCosos
                             select cs;
        ddlcoso.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "showPopup.Show();", true);
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

    }
    protected void btnNghihoc_Click(object sender, EventArgs e)
    {
        cls_Hocsinhtronglop cls = new cls_Hocsinhtronglop();
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hstl_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                //lấy tk học sinh trong bảng hstl
                var check = (from hstl in db.tbhocsinhtronglops
                                 // join hs in db.tbHocPhis on hstl.account_id equals hs.account_id
                             where hstl.hstl_id == Convert.ToInt32(item)
                             select hstl).FirstOrDefault();
                check.hstl_hidden = true;
                try
                {
                    db.SubmitChanges();
                }
                catch { }
                //thêm dữ liệu vào bảng học phí
                tbHocPhi ins = new tbHocPhi();
                ins.hp_ghichu = "Đã nghỉ học";
                ins.account_id = check.account_id;
                db.tbHocPhis.InsertOnSubmit(ins);
                try
                {
                    db.SubmitChanges();
                }
                catch { }
                // hidden học sinh trong table account
                tbAccount update = db.tbAccounts.Where(x => x.account_id == check.account_id).FirstOrDefault();
                update.hidden = true;
                update.account_acctive = false;
                try
                {
                    db.SubmitChanges();
                }
                catch { }
                // hidden học sinh trong table bài tập tài khoản
                tbBaiTapTaiKhoan update1 = db.tbBaiTapTaiKhoans.Where(x => x.account_id == check.account_id).FirstOrDefault();
                update.hidden = true;
                try
                {
                    db.SubmitChanges();
                }
                catch { }
                if (cls.Linq_NghiHoc(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Đã cho học sinh nghỉ học!", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Thất bại", "");
            }
        }
    }

    protected void btnBaoLuu_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hstl_id" }));
        if (txtGhichu.Value == "")
        {
            alert.alert_Warning(Page, "Vui lòng nhập ghi chú bảo lưu", "");
        }
        else
        {
            tbhocsinhtronglop update = db.tbhocsinhtronglops.Where(x => x.hstl_id == _id).FirstOrDefault();
            update.hstl_baoluu = true;
            update.hstl_ghichu = txtGhichu.Value;
            try
            {
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã bảo lưu học sinh", "");
            }
            catch { }
        }
    }
}