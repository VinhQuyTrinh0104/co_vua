﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_BaoLuu_backup.aspx.cs" Inherits="admin_page_module_function_module_BaoLuu_backup" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }
    </style>
      <%--<uc1:Menu ID="Menu1" runat="server" />--%>
    <div class="card card-block">
        <div style="float: right">
            <a href="admin-ds-bao-luu" id="btnQuayLai" class="btn btn-primary">Quay lại</a>
        </div>
        <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">*Bảo lưu toàn Trung tâm</div>
        <br />
        <asp:UpdatePanel ID="upBaoLuuTrungTam" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                     <label class="col-2">Số buổi bảo lưu:</label>
                    <div class="col-2">
                        <input type="text" id="txtSoBuoiBaoLuu" runat="server" class="form-control" />
                    </div>
                    <div class="col-1 mx-1">
                        buổi
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                 <%--  <label class="col-2">Ngày bắt đầu bảo lưu:</label>
                    <div class="col-4">
                        <input type="date" id="dteNgayBatDau" runat="server" class="form-control" />
                    </div>--%>
                </div>
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckChuNhat" runat="server" Text="Chủ nhật" />
                </div>
                <div class="col-12 form-group" style="display: none">
                    <label class="col-3 form-control-label">Ngày kết thúc :</label>
                    <div class="col-4">
                        <input class="form-control boxed" type="text" name="name" runat="server" id="dteNgayKetThuc" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lý do bao lưu:</label>
                    <div class="col-10">
                        <input type="text" id="txtLyDoBaoLuu" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <div style="text-align: center"><a id="btnXacNhanToanTrungTam" runat="server" class="btn btn-primary" onserverclick="btnXacNhanToanTrungTam_ServerClick">Bảo lưu</a></div>
                    <div style="text-align: center">--------------------  ***  --------------------  </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upCaNhan" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">*Bảo lưu cá nhân học sinh</div>
                    <br />
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbCoSoCaNhan" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="cbbCoSoCaNhan" OnSelectedIndexChanged="cbbCoSoCaNhan_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbLopCaNhan" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="cbbLopCaNhan" OnSelectedIndexChanged="cbbLopCaNhan_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                    <label class="col-2">Số buổi bao lưu:</label>
                    <div class="col-2">
                        <input type="text" id="txtSoBuoiBaoLuuCaNhan" runat="server" class="form-control" />
                    </div>
                    <div class="col-1 mx-1">
                        buổi
                    </div>
                </div>
                <div class="col-12 form-group">

                    <label class="col-2 form-control-label">Học sinh:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlHocSinh" runat="server" ValueType="System.Int32" TextField="account_vn" ValueField="account_id" ClientInstanceName="ddlHocSinh" CssClass="" NullText="Chọn học sinh" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lý do bao lưu:</label>
                    <div class="col-10">
                        <input type="text" id="txtLyDoBaoLuuCaNhan" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <div style="text-align: center"><a id="btnBaoLuuCaNha" runat="server" class="btn btn-primary" onserverclick="btnBaoLuuCaNha_ServerClick">Bảo lưu</a></div>
                    <div style="text-align: center">--------------------  ***  --------------------  </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upNghiHan" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">*Học sinh nghỉ hẵn</div>
                    <br />
                </div>
                 <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbCoSoNghiHan" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="cbbCoSoNghiHan" OnSelectedIndexChanged="cbbCoSoNghiHan_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbLopNghiHan" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" OnSelectedIndexChanged="cbbLopNghiHan_SelectedIndexChanged" AutoPostBack="true" ClientInstanceName="cbbLopNghiHan" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                    <label class="col-2 form-control-label">Học sinh:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="cbbHocSinhNghiHan" runat="server" ValueType="System.Int32" TextField="account_vn" ValueField="account_id" ClientInstanceName="cbbHocSinhNghiHan" CssClass="" NullText="Chọn học sinh" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Ngày nghỉ hẵn:</label>
                    <div class="col-6">
                        <input type="date" id="dteNgayNghiHan" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lý do nghỉ hẵn:</label>
                    <div class="col-10">
                        <input type="text" id="txtLyDoNghiHan" runat="server" class="form-control" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <div style="text-align: center"><a id="btnNghiHan" runat="server" class="btn btn-primary" onserverclick="btnNghiHan_ServerClick">Nghỉ hẵn</a></div>
                    <div style="text-align: center">--------------------  ***  --------------------  </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

