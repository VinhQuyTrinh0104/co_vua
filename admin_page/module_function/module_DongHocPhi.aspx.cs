﻿using ClosedXML.Excel;
using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DongHocPhi : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public string xemThongke = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (checkButton.groupuser_id == 6)
            {
                btnChiTiet.Visible = false;
                btnDlt.Visible = false;
                
            }
            else
            {
                if (checkButton.groupuser_id == 2)
                {
                  
                    btnChiTiet.Visible = true;
                    btnDlt.Visible = true;
                }
                
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                var listNV = from cs in db.tbCosos select cs;
                ddlCoSo.Items.Clear();
                ddlCoSo.Items.Insert(0, "Tất cả");
                ddlCoSo.AppendDataBoundItems = true;
                ddlCoSo.DataTextField = "coso_name";
                ddlCoSo.DataValueField = "coso_id";
                ddlCoSo.DataSource = listNV;
                ddlCoSo.DataBind();
            }
            if (xemThongke != "")
            {
                getXemThongke();
            }
            else
            {
                loadData();
            }

        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from hp in db.tbHocPhis
                      join u in db.admin_Users on hp.username_id equals u.username_id
                      join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
                      join hs in db.tbAccounts on hpct.hocsinh_id equals hs.account_id
                      join l in db.tbLops on hpct.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby hp.hp_id descending
                      select new
                      {
                          cs.coso_name,
                          l.lop_name,
                          hp.hp_id,
                          hp.hocphi_ngaydongtien,
                          hpct.hocphichitiet_phieuthu,
                          hpct.hocphichitiet_maso,
                          u.username_fullname,
                          hs.account_vn,
                          hp.hocphi_tinhtrangdong,
                          hp.hocphi_chitiet,
                          hp.hocphi_tongtien,
                          hpct.hocphichitiet_sotiendongkhoahoc,
                          hpct.hocphichitiet_sotiendadong,
                          hpct.hocphichitiet_sotienconlai,
                          hpct.hocphichitiet_sotiengiaotrinh,
                          hpct.hocphichitiet_sobuoihoc,
                          hpct.hocphichitiet_ngaybatdau,
                          hpct.hocphichitiet_ngayketthuc,
                          hpct.hocphichitiet_chuongtrinhgiam,
                          hpct.hocphichitiet_hinhthucthanhtoan,
                          hpct.hocphichitiet_ghichu
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Response.Redirect("dang-ky-hoc-phi-0");
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hp_id" }));
        // đẩy id vào session
        Response.Redirect("dang-ky-hoc-phi-" + _id);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hp_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhi deleteHocPhi = (from hp in db.tbHocPhis where hp.hp_id == Convert.ToInt32(item.ToString()) select hp).FirstOrDefault();
                db.tbHocPhis.DeleteOnSubmit(deleteHocPhi);
                db.SubmitChanges();
                tbHocPhiChiTiet deleteHPCT = (from hpct in db.tbHocPhiChiTiets where hpct.hocphi_id == Convert.ToInt32(item.ToString()) select hpct).FirstOrDefault();
                db.tbHocPhiChiTiets.DeleteOnSubmit(deleteHPCT);
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã xóa thành công", "");
                loadData();
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        xemThongke = "xemthongke";
        getXemThongke();


    }
    protected void getXemThongke()
    {
        if (ddlCoSo.Text != "Tất cả")
        {
            var getData = from hp in db.tbHocPhis
                          join u in db.admin_Users on hp.username_id equals u.username_id
                          join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
                          join hs in db.tbAccounts on hpct.hocsinh_id equals hs.account_id
                          join l in db.tbLops on hpct.lop_id equals l.lop_id
                          join cs in db.tbCosos on l.coso_id equals cs.coso_id
                          where l.coso_id == Convert.ToInt32(ddlCoSo.SelectedItem.Value)
                          && hp.hocphi_ngaydongtien.Value.Month.ToString() == ddlThang.Text && hp.hocphi_ngaydongtien.Value.Year.ToString() == ddlNam.Text
                          orderby hp.hp_id descending
                          select new
                          {
                              cs.coso_name,
                              l.lop_name,
                              hp.hp_id,
                              hp.hocphi_ngaydongtien,
                              hpct.hocphichitiet_phieuthu,
                              hpct.hocphichitiet_maso,
                              u.username_fullname,
                              hs.account_vn,
                              hp.hocphi_tinhtrangdong,
                              hp.hocphi_chitiet,
                              hp.hocphi_tongtien,
                              hpct.hocphichitiet_sotiendongkhoahoc,
                              hpct.hocphichitiet_sotiendadong,
                              hpct.hocphichitiet_sotienconlai,
                              hpct.hocphichitiet_sotiengiaotrinh,
                              hpct.hocphichitiet_sobuoihoc,
                              hpct.hocphichitiet_ngaybatdau,
                              hpct.hocphichitiet_ngayketthuc,
                              hpct.hocphichitiet_chuongtrinhgiam,
                              hpct.hocphichitiet_hinhthucthanhtoan,
                              hpct.hocphichitiet_ghichu
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
        }
        else
        {
            var getData = from hp in db.tbHocPhis
                          join u in db.admin_Users on hp.username_id equals u.username_id
                          join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
                          join hs in db.tbAccounts on hpct.hocsinh_id equals hs.account_id
                          join l in db.tbLops on hpct.lop_id equals l.lop_id
                          join cs in db.tbCosos on l.coso_id equals cs.coso_id
                          where hp.hocphi_ngaydongtien.Value.Month.ToString() == ddlThang.Text && hp.hocphi_ngaydongtien.Value.Year.ToString() == ddlNam.Text
                          orderby hp.hp_id descending
                          select new
                          {
                              cs.coso_name,
                              l.lop_name,
                              hp.hp_id,
                              hp.hocphi_ngaydongtien,
                              hpct.hocphichitiet_phieuthu,
                              hpct.hocphichitiet_maso,
                              u.username_fullname,
                              hs.account_vn,
                              hp.hocphi_tinhtrangdong,
                              hp.hocphi_chitiet,
                              hp.hocphi_tongtien,
                              hpct.hocphichitiet_sotiendongkhoahoc,
                              hpct.hocphichitiet_sotiendadong,
                              hpct.hocphichitiet_sotienconlai,
                              hpct.hocphichitiet_sotiengiaotrinh,
                              hpct.hocphichitiet_sobuoihoc,
                              hpct.hocphichitiet_ngaybatdau,
                              hpct.hocphichitiet_ngayketthuc,
                              hpct.hocphichitiet_chuongtrinhgiam,
                              hpct.hocphichitiet_hinhthucthanhtoan,
                              hpct.hocphichitiet_ghichu
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
        }
    }
    protected void btnXuatExcel_ServerClick(object sender, EventArgs e)
    {
        string constr = ConfigurationManager.ConnectionStrings["db_vjlcConnectionString"].ConnectionString;
        if (ddlCoSo.Text != "Tất cả")
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(

                  " select hocphi_ngaydongtien as [hocphi_ngaydongtien], hocphichitiet_phieuthu as[Quyển Phiếu thu]," +
                  " hocphichitiet_maso as [Mã Phiếu thu], coso_name as [Cơ sở], lop_name as [Lớp]," +
                  " username_fullname as [Họ và tên nhân viên], account_vn as [Họ và Tên học viên]," +
                  " hocphichitiet_sobuoihoc as [Số buổi đăng kí], hocphichitiet_ngaybatdau as [Ngày bắt đầu]," +
                  " hocphichitiet_ngayketthuc as [Ngày kết thúc], hocphi_tinhtrangdong as [Tình trạng đóng học phí học sinh]," +
                  " hocphi_chitiet as [Chi Tiết đóng học phí], hocphichitiet_sotiendongkhoahoc as [Tiền khóa học]," +
                  " hocphichitiet_chuongtrinhgiam as [Chương trình giảm], hocphichitiet_sotiendadong as [Tiền đặt cọc]," +
                  " hocphichitiet_sotienconlai as [Tiền còn lại], hocphichitiet_sotiengiaotrinh as [Tiền Giáo trình]," +
                  " hocphi_tongtien as [Tổng tiền thu], hocphichitiet_hinhthucthanhtoan as [Hình thức thanh toán]," +
                  " hocphichitiet_ghichu as [Ghi chú(Thông tin đóng học phí của học sinh)]" +
                  " from tbHocPhi, admin_User, tbHocPhiChiTiet, tbAccount, tbLop, tbCoso" +
                  " where tbHocPhi.username_id = admin_User.username_id and tbHocPhi.hp_id = tbHocPhiChiTiet.hocphi_id" +
                  " and tbHocPhiChiTiet.hocsinh_id = tbAccount.account_id and tbHocPhiChiTiet.lop_id = tbLop.lop_id" +
                  " and tbLop.coso_id = tbCoso.coso_id" +
                  " and tbCoso.coso_id = '" + ddlCoSo.SelectedValue + "'" +
                  " and MONTH(tbHocPhi.hocphi_ngaydongtien) = '" + ddlThang.Text + "' and YEAR(tbHocPhi.hocphi_ngaydongtien) = '" + ddlNam.Text + "'"
                    ))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                wb.Worksheets.Add(dt, "Customers");

                                Response.Clear();
                                Response.Buffer = true;
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("content-disposition", "attachment;filename=dong-hoc-phi.xlsx");
                                using (MemoryStream MyMemoryStream = new MemoryStream())
                                {
                                    wb.SaveAs(MyMemoryStream);
                                    MyMemoryStream.WriteTo(Response.OutputStream);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(

                  " select hocphi_ngaydongtien as [hocphi_ngaydongtien], hocphichitiet_phieuthu as[Quyển Phiếu thu]," +
                  " hocphichitiet_maso as [Mã Phiếu thu], coso_name as [Cơ sở], lop_name as [Lớp]," +
                  " username_fullname as [Họ và tên nhân viên], account_vn as [Họ và Tên học viên]," +
                  " hocphichitiet_sobuoihoc as [Số buổi đăng kí], hocphichitiet_ngaybatdau as [Ngày bắt đầu]," +
                  " hocphichitiet_ngayketthuc as [Ngày kết thúc], hocphi_tinhtrangdong as [Tình trạng đóng học phí học sinh]," +
                  " hocphi_chitiet as [Chi Tiết đóng học phí], hocphichitiet_sotiendongkhoahoc as [Tiền khóa học]," +
                  " hocphichitiet_chuongtrinhgiam as [Chương trình giảm], hocphichitiet_sotiendadong as [Tiền đặt cọc]," +
                  " hocphichitiet_sotienconlai as [Tiền còn lại], hocphichitiet_sotiengiaotrinh as [Tiền Giáo trình]," +
                  " hocphi_tongtien as [Tổng tiền thu], hocphichitiet_hinhthucthanhtoan as [Hình thức thanh toán]," +
                  " hocphichitiet_ghichu as [Ghi chú(Thông tin đóng học phí của học sinh)]" +
                  " from tbHocPhi, admin_User, tbHocPhiChiTiet, tbAccount, tbLop, tbCoso" +
                  " where tbHocPhi.username_id = admin_User.username_id and tbHocPhi.hp_id = tbHocPhiChiTiet.hocphi_id" +
                  " and tbHocPhiChiTiet.hocsinh_id = tbAccount.account_id and tbHocPhiChiTiet.lop_id = tbLop.lop_id" +
                  " and tbLop.coso_id = tbCoso.coso_id" +
                  " and MONTH(tbHocPhi.hocphi_ngaydongtien) = '" + ddlThang.Text + "' and YEAR(tbHocPhi.hocphi_ngaydongtien) = '" + ddlNam.Text + "'"
                    ))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                wb.Worksheets.Add(dt, "Customers");

                                Response.Clear();
                                Response.Buffer = true;
                                Response.Charset = "";
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("content-disposition", "attachment;filename=dong-hoc-phi.xlsx");
                                using (MemoryStream MyMemoryStream = new MemoryStream())
                                {
                                    wb.SaveAs(MyMemoryStream);
                                    MyMemoryStream.WriteTo(Response.OutputStream);
                                    Response.Flush();
                                    Response.End();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}