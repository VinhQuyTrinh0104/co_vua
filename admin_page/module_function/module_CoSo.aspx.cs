﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_CoSo : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
           
            if (checkButton.groupuser_id == 6)
            {
               
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = false;
                
            }
            else
            {
                
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                btnDlt.Visible = true;
               
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getCoSo = from tt in db.tbCosos where tt.coso_hidden == false orderby tt.coso_id descending select tt;
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getCoSo;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txtName.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "coso_id" }));
        // đẩy id vào session
        Session["_id"] = _id;

        var getData = (from cs in db.tbCosos
                       where cs.coso_id == _id
                       select cs).Single();
        txtName.Text = getData.coso_name;
        txtDiaChi.Text = getData.coso_diachi;
        txtPhone.Text = getData.coso_phone;
        txtEmail.Text = getData.coso_email;
        txtGhiChu.Text = getData.coso_ghichu;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);

    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_Coso cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "coso_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_Coso();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    //Lưu Lịch Sử Thao Tác
                }
                else
                {
                    alert.alert_Error(Page, "Xóa thất bại", "");
                }
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (txtName.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_Coso cls = new cls_Coso();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {

                if (cls.Linq_Them(txtName.Text, txtDiaChi.Text, txtPhone.Text, txtEmail.Text, txtGhiChu.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                var getData = (from nc in db.tbCosos
                               where nc.coso_id == Convert.ToInt32(Session["_id"].ToString())
                               select nc).Single();
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtName.Text, txtDiaChi.Text, txtPhone.Text, txtEmail.Text, txtGhiChu.Text))
                {

                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();

                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
        popupControl.ShowOnPageLoad = false;
    }
}