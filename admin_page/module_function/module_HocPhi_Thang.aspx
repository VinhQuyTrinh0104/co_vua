﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_HocPhi_Thang.aspx.cs" Inherits="admin_page_module_function_module_HocPhi_Thang" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }

        .dxpc-content {
            height: 83vh !important;
        }

        .col-12 {
            display: flex;
            align-items: center !important;
        }
    </style>
    <div class="card card-block box-admin">
        <div id="menuQuanTri" runat="server">
      
    </div>
        <div class="form-group row">
            <div class="col-12 form-group-name">THỐNG KÊ DOANH THU</div>
            <div>
                <label class="col-2">Chọn từ ngày:</label>
            </div>
            <div class="col-3">
                <input type ="date" id="dteTuNgay" runat ="server"  class="form-control"/>
            </div>
             <div>
                <label class="col-2">Chọn đến ngày:</label>
            </div>
            <div class="col-3">
                 <input type ="date" id="dteDenNgay" runat ="server"  class="form-control"/>
            </div>
            <div class="col-2">
                &nbsp;&nbsp; <a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick" class="btn btn-primary">Xem</a>
            </div>
            <%-- <div class="col-2">
                        <a href="#" id="btnXuatExcel" runat="server" onserverclick="btnXuatExcel_ServerClick" class="btn btn-primary">Xuất Excel</a>
                    </div>--%>
        </div>
        <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="account_id" Width="100%">
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="6%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Họ và Tên học sinh" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
              <%--  <dx:GridViewDataColumn Caption="English name" FieldName="account_en" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>--%>
                <%-- <dx:GridViewDataColumn Caption="Tháng" FieldName="hocphichitiet_thang" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>--%>
                <dx:GridViewDataColumn Caption="Học phí đã đóng" FieldName="hocphichitiet_sotiendadong" HeaderStyle-HorizontalAlign="Center" Width="12%">
                    <DataItemTemplate>
                        <%#Eval("hocphichitiet_sotiendadong", "{0:N0}") %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
             <%--   <dx:GridViewDataColumn Caption="Số buổi khóa học" FieldName="sobuoikhoahoc" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Số buổi đã học" FieldName="diemdanh" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>--%>
               <%-- <dx:GridViewDataColumn Caption="Học phí từng ngày" FieldName="hocphitungngay" HeaderStyle-HorizontalAlign="Center" Width="18%">
                    <DataItemTemplate>
                        <%#Eval("hocphitungngay", "{0:N0}") %>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>--%>
                <dx:GridViewDataColumn Caption="Ngày đóng tiền" FieldName="hocphi_ngaydongtien" HeaderStyle-HorizontalAlign="Center" Width="12%">
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Ghi chú" FieldName="hocphichitiet_ghichu" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
            </Columns>
            <SettingsSearchPanel Visible="true" />
            <SettingsSearchPanel Visible="false" />
            <SettingsBehavior AllowFocusedRow="true" />
            <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
            <SettingsLoadingPanel Text="Đang tải..." />
            <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
        </dx:ASPxGridView>
    </div>
    <div style="float: right">
        Tổng doanh thu:
            <asp:Label ID="lblTongDoanhThu" runat="server"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



