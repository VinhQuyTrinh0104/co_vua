﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaoLuu_backup : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public DateTime ngaycuoicung;
    public DateTime ngaycuoi;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
                cbbCoSoCaNhan.DataSource = from cs in db.tbCosos select cs;
                cbbCoSoCaNhan.DataBind();
                cbbCoSoNghiHan.DataSource = from cs in db.tbCosos select cs;
                cbbCoSoNghiHan.DataBind();
            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getLop = from tt in db.tbLops select tt;
        ddlLop.DataSource = getLop;
        ddlLop.DataBind();
        cbbLopCaNhan.DataSource = getLop;
        cbbLopCaNhan.DataBind();
        cbbLopNghiHan.DataSource = getLop;
        cbbLopNghiHan.DataBind();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getHocSinh = from a in db.tbAccounts select a;
        ddlHocSinh.DataSource = getHocSinh;
        ddlHocSinh.DataBind();
        cbbHocSinhNghiHan.DataSource = getHocSinh;
        cbbHocSinhNghiHan.DataBind();
    }

    public void checkNgayKetThuc(DateTime dteNgayBatDauhoc, int tongsobuoi)
    {
        int i = 1;
        // cho chạy từ ngày bắt đầu cho tới khi tổng số buổi = tongssobuoi thì out
        string kiemtrathu = dteNgayBatDauhoc.DayOfWeek + "";
        for (int j = 1; j <= 5000; j++)
        {
            // ví dụ học sinh học 3 buổi thứ 2,4,6
            // ngày bắt đầu là ngày thứ 4 
            DateTime ngaytieptheo = dteNgayBatDauhoc.AddDays(j);
            kiemtrathu = ngaytieptheo.DayOfWeek + "";

            if (kiemtrathu == "Monday" && ckThu2.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Tuesday" && ckThu3.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Wednesday" && ckThu4.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Thursday" && ckThu5.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Friday" && ckThu6.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Saturday" && ckThu7.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Sunday" && ckChuNhat.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (i == tongsobuoi)
            {
                dteNgayKetThuc.Value = ngaytieptheo + "";
                // dteNgayKetThuc.Value = ngaycuoi.ToString("dd/MM/yyyy");
                break;
                // thí lấy ngày cuối cùng đó ra để lưu ngày kết thúc
            }
            else
            {
            }
        }
    }
    protected void btnXacNhanToanTrungTam_ServerClick(object sender, EventArgs e)
    {
        var dsHocSinhTrongLop = from hstl in db.tbhocsinhtronglops
                                join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                join hp5 in db.tbHocPhiChiTiets on hstl.hstl_id equals hp5.hstl_id
                                where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden==false
                                group hstl by hstl.hstl_id into item
                                select new
                                {
                                    hocphichitiet_sobuoihoc = (from hpct in db.tbHocPhiChiTiets
                                                               join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                                               where hpct.hstl_id == item.Key
                                                                && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                                               orderby hpct.hocphichitiet_id descending
                                                               select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
                                    hocphichitiet_ngaybatdau = (from hpct in db.tbHocPhiChiTiets
                                                                join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                                                where hpct.hstl_id == item.Key
                                                                 && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                                                orderby hpct.hocphichitiet_id descending select hpct).FirstOrDefault().hocphichitiet_ngaybatdau,
                                    hstl_id = (from hpct in db.tbHocPhiChiTiets
                                               where hpct.hstl_id == item.Key
                                               orderby hpct.hocphichitiet_id descending
                                               select hpct).FirstOrDefault().hstl_id,
                                };
        foreach (var item in dsHocSinhTrongLop)
        {
            var check_idhocPhiChiTiet = (from hstl in db.tbhocsinhtronglops
                                         join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                         join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                         join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                         where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                                         && hstl.hstl_id == item.hstl_id && hstl.hstl_hidden == false
                                       && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                         orderby hpct.hocphichitiet_id descending
                                         select hpct).FirstOrDefault();
            // Chạy từng học sinh cuối cùng trong bảng ghi học phí
            var checkSoLanBaoLuu_TungHocSinh = (from hstl in db.tbhocsinhtronglops
                                                join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                                join bl in db.tbHocPhiBaoLuus on hstl.hstl_id equals bl.hstl_id
                                                join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                                join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                                where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                                                && hstl.hstl_id == item.hstl_id && hstl.hstl_hidden == false
                                                && bl.hocphichitiet_id == check_idhocPhiChiTiet.hocphichitiet_id
                                                 && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                                orderby bl.baoluu_id descending
                                                select bl).FirstOrDefault();
            var checkSumNgayBaoLuu_TungHocSinh = (from hstl in db.tbhocsinhtronglops
                                                  join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                                  join bl in db.tbHocPhiBaoLuus on hstl.hstl_id equals bl.hstl_id
                                                  join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                                  join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                                  where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                                                  && hstl.hstl_id == item.hstl_id && hstl.hstl_hidden == false
                                                  && bl.hocphichitiet_id == check_idhocPhiChiTiet.hocphichitiet_id
                                                   && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                                  orderby bl.baoluu_id descending
                                                  select bl).Sum(x => x.baoluu_sobuoi);
            if (checkSumNgayBaoLuu_TungHocSinh == null)
            {
                checkSumNgayBaoLuu_TungHocSinh = 0;
            }
            int tongsobuoi = Convert.ToInt16(item.hocphichitiet_sobuoihoc) + Convert.ToInt16(checkSumNgayBaoLuu_TungHocSinh) + Convert.ToInt16(txtSoBuoiBaoLuu.Value);
            checkNgayKetThuc(item.hocphichitiet_ngaybatdau.Value, tongsobuoi);
            tbHocPhiBaoLuu insert = new tbHocPhiBaoLuu();
            insert.hstl_id = item.hstl_id;
            if (checkSoLanBaoLuu_TungHocSinh==null)
            {
                insert.baoluu_name = "1";
            }
            else
            {
                insert.baoluu_name = (Convert.ToInt16(checkSoLanBaoLuu_TungHocSinh.baoluu_name) + 1) + "";
            }
            insert.baoluu_sobuoi = Convert.ToInt16(txtSoBuoiBaoLuu.Value);
            insert.baoluu_ngaybaoluu = DateTime.Now + "";
            // sai chổ lấy ngày kết thúc, cần phải lấy theo hàm
            //insert.baoluu_ngayketthuc = dteNgayKetThuc.Value;
            insert.baoluu_ngayketthuc_hidden = Convert.ToDateTime(dteNgayKetThuc.Value);
            // lấy ngày kết thúc của từng bạn.
            insert.baoluu_loai = "Trung tâm bảo lưu";
            insert.hocphichitiet_id = check_idhocPhiChiTiet.hocphichitiet_id;
            insert.lop_id = Convert.ToInt16(ddlLop.SelectedItem.Value);
            insert.baoluu_ghichu = txtLyDoBaoLuu.Value;
            insert.baoluu_NgayNhap = DateTime.Now;
            insert.baoluu_tinhtrang = "Chưa duyệt";
            db.tbHocPhiBaoLuus.InsertOnSubmit(insert);
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã bảo lưu", "");
        }

    }
    protected void btnBaoLuuCaNha_ServerClick(object sender, EventArgs e)
    {
        var check_idhocPhiChiTiet = (from hstl in db.tbhocsinhtronglops
                                     join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                     join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                     join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                     where hstl.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                                     && hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                      && hs.hidden == false && hstl.hstl_hidden == false
                                     && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                     orderby hpct.hocphichitiet_id descending
                                     select hpct).FirstOrDefault();
        var dsHocSinhTrongLop =  (from hstl in db.tbhocsinhtronglops
                                 join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                 join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                 join hp1 in db.tbHocPhis on hpct1.hocphi_id equals hp1.hp_id
                                 where hstl.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                                 && hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                  && hs.hidden == false && hstl.hstl_hidden == false
                                 && (hp1.hocphi_chitiet != "Bổ sung học phí") && (hp1.hocphi_chitiet != "Đăng ký chương trình") && (hp1.hocphi_chitiet != "Đặt cọc")
                                 select new
                                 {
                                     hstl.hstl_id,
                                     hocphichitiet_sobuoihoc = (from hpct in db.tbHocPhiChiTiets
                                                                join hpp in db.tbHocPhis on hpct.hocphi_id equals hpp.hp_id
                                                                where hpct.hstl_id == hstl.hstl_id
                                                                && (hpp.hocphi_chitiet != "Bổ sung học phí") && (hpp.hocphi_chitiet != "Đăng ký chương trình") && (hpp.hocphi_chitiet != "Đặt cọc")
                                                                orderby hpct.hocphichitiet_id descending
                                                                select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
                                     // cần kiểm tra ngày kết thúc bên chi tiết với ngày kết thúc bên bảo lưu, ngày nào lớn hơn thì lấy ngày bắt đầu của bảng  đó
                                     hocphichitiet_ngaybatdau = (from hpct in db.tbHocPhiChiTiets
                                                                 join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                                                 where hpct.hstl_id == hstl.hstl_id
                                                                  && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                                                 orderby hpct.hocphichitiet_id descending
                                                                 select hpct).FirstOrDefault().hocphichitiet_ngaybatdau,
                                 }).FirstOrDefault();
        var checkSumNgayBaoLuu = (from hstl in db.tbhocsinhtronglops
                                  join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                  join bl in db.tbHocPhiBaoLuus on hstl.hstl_id equals bl.hstl_id
                                  join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                  join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                  where hstl.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                                  && hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                  && hstl.hstl_hidden == false && hs.hidden == false && bl.hocphichitiet_id == check_idhocPhiChiTiet.hocphichitiet_id
                                  && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                  //sợ thiếu điều kiện học phí chi tiết
                                  orderby hpct.hocphichitiet_id descending
                                  select bl).Sum(x => x.baoluu_sobuoi);
        var checkSoLanBaoLuu = (from hstl in db.tbhocsinhtronglops
                                join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                join bl in db.tbHocPhiBaoLuus on hstl.hstl_id equals bl.hstl_id
                                join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                                where hstl.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                                 && hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                 && hs.hidden == false && hstl.hstl_hidden == false
                                 && bl.hocphichitiet_id == check_idhocPhiChiTiet.hocphichitiet_id
                                 && (hp.hocphi_chitiet != "Bổ sung học phí") && (hp.hocphi_chitiet != "Đăng ký chương trình") && (hp.hocphi_chitiet != "Đặt cọc")
                                //sợ thiếu điều kiện học phí chi tiết
                                orderby bl.baoluu_id descending
                                select bl).FirstOrDefault();

        // Kiểm tra sô lần bảo lưu lúc trước, nếu chưa có bảo lưu lần nào thì chỉ cần lấy hocphichitiet_sobuoihoc + với số buổi cần bảo lưu
        int tongsobuoi = Convert.ToInt16(dsHocSinhTrongLop.hocphichitiet_sobuoihoc) + Convert.ToInt16(checkSumNgayBaoLuu) + Convert.ToInt16(txtSoBuoiBaoLuuCaNhan.Value);
        checkNgayKetThuc(dsHocSinhTrongLop.hocphichitiet_ngaybatdau.Value, tongsobuoi);
        tbHocPhiBaoLuu insert = new tbHocPhiBaoLuu();
        insert.hstl_id = dsHocSinhTrongLop.hstl_id;
        if (checkSoLanBaoLuu == null)
        {
            insert.baoluu_name = "1";
        }
        else
        {
            insert.baoluu_name = (Convert.ToInt16(checkSoLanBaoLuu.baoluu_name) + 1) + "";
        }
        insert.baoluu_sobuoi = Convert.ToInt16(txtSoBuoiBaoLuuCaNhan.Value);
        insert.lop_id = Convert.ToInt16(cbbLopCaNhan.SelectedItem.Value);
        insert.baoluu_ngaybaoluu = DateTime.Now + "";
        //insert.baoluu_ngayketthuc = dteNgayKetThuc.Value;
        insert.baoluu_ngayketthuc_hidden = Convert.ToDateTime(dteNgayKetThuc.Value);
        insert.baoluu_ghichu = txtLyDoBaoLuuCaNhan.Value;
        insert.baoluu_loai = "Cá nhân bảo lưu";
        insert.baoluu_NgayNhap = DateTime.Now;
        insert.hocphichitiet_id = check_idhocPhiChiTiet.hocphichitiet_id;
        insert.baoluu_tinhtrang = "Chưa duyệt";
        db.tbHocPhiBaoLuus.InsertOnSubmit(insert);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã bảo lưu", "");
    }
    protected void btnNghiHan_ServerClick(object sender, EventArgs e)
    {
        var dsHocSinhTrongLop = (from hstl in db.tbhocsinhtronglops
                                 join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                 where hstl.lop_id == Convert.ToInt32(cbbLopNghiHan.SelectedItem.Value)
                                 && hs.account_id == Convert.ToInt32(cbbHocSinhNghiHan.SelectedItem.Value)
                                 && hstl.hstl_hidden == false && hs.hidden == false
                                 orderby hstl.hstl_id descending
                                 select hstl).FirstOrDefault();
        tbHocPhiBaoLuu insert = new tbHocPhiBaoLuu();
        insert.hstl_id = dsHocSinhTrongLop.hstl_id;
        insert.baoluu_name = "Nghỉ hẵn";
        insert.baoluu_ngaybaoluu = DateTime.Now + "";
        insert.lop_id = Convert.ToInt16(cbbLopNghiHan.SelectedItem.Value);
        //insert.baoluu_ngayketthuc = dteNgayNghiHan.Value + "";
        insert.baoluu_ngayketthuc_hidden = Convert.ToDateTime(dteNgayNghiHan.Value);
        insert.baoluu_ghichu = txtLyDoNghiHan.Value;
        //insert.hocphichitiet_id = check_idhocPhiChiTiet.hocphichitiet_id;
        insert.baoluu_loai = "Nghỉ hẵn";
        insert.baoluu_tinhtrang = "Chưa duyệt";
        insert.baoluu_NgayNhap = DateTime.Now;
        db.tbHocPhiBaoLuus.InsertOnSubmit(insert);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã nghỉ hẵn", "");
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
        }

    }
    protected void cbbLopCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbbLopCaNhan.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                           select l).SingleOrDefault();
            ddlHocSinh.DataSource = from hs in db.tbAccounts
                                    join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                    join l in db.tbLops on hstl.lop_id equals l.lop_id
                                    where l.lop_id == Convert.ToInt16(cbbLopCaNhan.SelectedItem.Value)
                                    && hstl.hstl_hidden == false && hs.hidden == false
                                    select hs;
            ddlHocSinh.DataBind();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
        }
    }



    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                             && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }

    protected void cbbCoSoCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbbLopCaNhan.DataSource = from l in db.tbLops
                                  where l.coso_id == Convert.ToInt16(cbbCoSoCaNhan.SelectedItem.Value)
                                   && l.hidden == false && l.lop_tinhtrang == null
                                  select l;
        cbbLopCaNhan.DataBind();
    }

    protected void cbbCoSoNghiHan_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbbLopNghiHan.DataSource = from l in db.tbLops
                                   where l.coso_id == Convert.ToInt16(cbbCoSoNghiHan.SelectedItem.Value)
                                    && l.hidden == false && l.lop_tinhtrang == null
                                   select l;
        cbbLopNghiHan.DataBind();
    }

    protected void cbbLopNghiHan_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbbLopNghiHan.Text != "")
        {
            cbbHocSinhNghiHan.DataSource = from hs in db.tbAccounts
                                           join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                           join l in db.tbLops on hstl.lop_id equals l.lop_id
                                           where l.lop_id == Convert.ToInt16(cbbLopNghiHan.SelectedItem.Value) && hstl.hstl_hidden == false
                                           select hs;
            cbbHocSinhNghiHan.DataBind();
        }
    }
}