﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HistorySLL : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }

            //đối với tk admin
            if (logedMember.username_id == 2)
            {
                var getData = from nc in db.tbSoLienLacs
                              join mh in db.tbMonHocs on nc.monhoc_id equals mh.monhoc_id
                              join us in db.admin_Users on nc.username_id equals us.username_id
                              where nc.sll_hidden == false
                              orderby nc.sll_id descending
                              //where nc.htr_date.Value.Date == Convert.ToDateTime(txtNgay.Value).Date
                              select new
                              {
                                  nc.sll_id,
                                  nc.sll_date,
                                  nc.account_vn,
                                  nc.sll_danhgiagv,
                                  nc.sll_kqgiuaky,
                                  nc.sll_kqcuoiky,
                                  us.username_id,
                                  us.username_fullname,
                                  mh.monhoc_id,
                                  mh.monhoc_name
                              };
                // đẩy dữ liệu vào gridivew
                grvList.DataSource = getData;
                grvList.DataBind();

            }
            else
            {
                loadData();
            }
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        // load data đổ vào var danh sách
        var getData = from nc in db.tbSoLienLacs
                      join us in db.admin_Users on nc.username_id equals us.username_id
                      join mh in db.tbMonHocs on nc.monhoc_id equals mh.monhoc_id
                      where nc.sll_hidden == false && us.username_id == logedMember.username_id
                      orderby nc.sll_id descending
                      //where nc.htr_date.Value.Date == Convert.ToDateTime(txtNgay.Value).Date
                      select new
                      {
                          nc.sll_id,
                          nc.sll_date,
                          nc.account_vn,
                          nc.sll_danhgiagv,
                          nc.sll_kqgiuaky,
                          nc.sll_kqcuoiky,
                          mh.monhoc_id,
                          mh.monhoc_name,
                          us.username_id,
                          us.username_fullname
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txtgiaovien.Value = "";
        //txthocsinh.Value = "";
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "sll_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbSoLienLacs
                       where nc.sll_id == _id
                       select nc).First();
        txtgiaovien.Value = getData.sll_danhgiagv;
        //txthocsinh.Value = getData.htr_danhgiahs;
        txtKqgiuaky.Value = getData.sll_kqgiuaky;
        txtKqcuoiky.Value = getData.sll_kqcuoiky;
        //txtTinhtrang.InnerText = getData.htr_tinhtrang.ToString();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    public bool checknull()
    {
        if (txtgiaovien.Value != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
       admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
             if (logedMember.username_id == 2)
        {
            popupControl.ShowOnPageLoad = false;
        }
        else
        {
            cls_HistorySLL cls = new cls_HistorySLL();
            int id = Convert.ToInt32(Session["_id"].ToString());
            if (checknull() == false)
                alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
            else
            {
                if (id.ToString() != "0")
                {
                    if (cls.Linq_Sua(id, txtgiaovien.Value))
                    {
                        alert.alert_Success(Page, "Lưu thành công", "");

                        var getsll = (from n in db.tbSoLienLacs
                                      where n.sll_id == id
                                      select n).FirstOrDefault();
                        //cập nhật lại sổ liên lạc
                        getsll.sll_date = DateTime.Now;
                        getsll.sll_danhgiagv = txtgiaovien.Value;
                        getsll.sll_kqcuoiky = txtKqcuoiky.Value;
                        getsll.sll_kqgiuaky = txtKqgiuaky.Value;
                        getsll.username_id = logedMember.username_id;
                        getsll.sll_trangthai = false;
                        try
                        {
                            db.SubmitChanges();
                        }
                        catch { }
                        //thêm lại vào lịch sử đánh giá
                        tbSoLienLacHistory insert = new tbSoLienLacHistory();
                        insert.htr_date = getsll.sll_date;
                        insert.htr_danhgiagv = txtgiaovien.Value;
                        insert.account_id = getsll.account_id;
                        insert.account_vn = getsll.account_vn;
                        insert.htr_kqcuoiky = txtKqcuoiky.Value;
                        insert.htr_kqgiuaky = txtKqgiuaky.Value;
                        insert.username_id = logedMember.username_id;
                        insert.htr_trangthai = true;
                        insert.htr_hidden = false;
                        db.tbSoLienLacHistories.InsertOnSubmit(insert);
                        try
                        {
                            db.SubmitChanges();
                        }
                        catch { }
                        loadData();
                    }
                    else alert.alert_Error(Page, "Lưu thất bại", "");
                }
            }
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (logedMember.username_id == 2)
        {
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Bạn không thể xóa dữ liệu!','','error').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            cls_HistorySLL cls;
            List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "sll_id" });
            if (selectedKey.Count > 0)
            {
                foreach (var item in selectedKey)
                {
                    cls = new cls_HistorySLL();
                    if (cls.Linq_Xoa(Convert.ToInt32(item)))
                        alert.alert_Success(Page, "Xóa thành công", "");
                    else
                        alert.alert_Error(Page, "Xóa thất bại", "");
                }
            }
            else
                alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        }
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        var list = from nc in db.tbSoLienLacs
                   join us in db.admin_Users on nc.username_id equals us.username_id
                   where nc.sll_date.Value.Date == Convert.ToDateTime(txtNgay.Value).Date && nc.sll_date.Value.Month == Convert.ToDateTime(txtNgay.Value).Month
                   orderby nc.sll_id descending
                   select new
                   {
                       nc.sll_id,
                       nc.sll_date,
                       nc.account_vn,
                       nc.sll_danhgiagv,
                       nc.sll_kqcuoiky,
                       nc.sll_kqgiuaky,
                       us.username_id,
                       us.username_fullname
                   };
        grvList.DataSource = list;
        grvList.DataBind();
    }
}