﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaoLuu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public DateTime ngaycuoicung;
    public DateTime ngaycuoi;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
                cbbCoSoCaNhan.DataSource = from cs in db.tbCosos select cs;
                cbbCoSoCaNhan.DataBind();
                cbbCoSoNghiHan.DataSource = from cs in db.tbCosos select cs;
                cbbCoSoNghiHan.DataBind();
            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getLop = from tt in db.tbLops select tt;
        ddlLop.DataSource = getLop;
        ddlLop.DataBind();
        cbbLopCaNhan.DataSource = getLop;
        cbbLopCaNhan.DataBind();
        cbbLopNghiHan.DataSource = getLop;
        cbbLopNghiHan.DataBind();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getHocSinh = from a in db.tbAccounts select a;
        ddlHocSinh.DataSource = getHocSinh;
        ddlHocSinh.DataBind();
        cbbHocSinhNghiHan.DataSource = getHocSinh;
        cbbHocSinhNghiHan.DataBind();
    }

    public void checkNgayKetThuc(DateTime dteNgayKetThucCu, int tongsobuoi)
    {
        int i = 1;
        // cho chạy từ ngày bắt đầu cho tới khi tổng số buổi = tongssobuoi thì out
        string kiemtrathu = dteNgayKetThucCu.DayOfWeek + "";
        for (int j = 1; j <= 5000; j++)
        {
            // ví dụ học sinh học 3 buổi thứ 2,4,6
            // ngày bắt đầu là ngày thứ 4 
            DateTime ngaytieptheo = dteNgayKetThucCu.AddDays(j);
            kiemtrathu = ngaytieptheo.DayOfWeek + "";

            if (kiemtrathu == "Monday" && ckThu2.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Tuesday" && ckThu3.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Wednesday" && ckThu4.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Thursday" && ckThu5.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Friday" && ckThu6.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Saturday" && ckThu7.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Sunday" && ckChuNhat.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (i > tongsobuoi)
            {
                dteNgayKetThuc.Value = ngaytieptheo + "";
                // dteNgayKetThuc.Value = ngaycuoi.ToString("dd/MM/yyyy");
                break;
                // thí lấy ngày cuối cùng đó ra để lưu ngày kết thúc
            }
            else
            {
            }
        }
    }
    protected void btnXacNhanToanTrungTam_ServerClick(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        // Chỉ cần lấy ngày kết thúc sau đó tính sổ buổi bảo lưu để lấy ngày kết thúc cuối cùng của học sinh đó
        var dsHocSinhTrongLop = from hstl in db.tbhocsinhtronglops
                                join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value) 
                                && hstl.hstl_hidden == false && hs.hidden == false && hpct.hocphichitiet_ngayketthuc!=null
                                group hstl by hstl.hstl_id into g
                                select new
                                {
                                    hstl_id = g.Key
                                };
        foreach (var item in dsHocSinhTrongLop)
        {
            var check_idhocPhiChiTiet = (from hstl in db.tbhocsinhtronglops
                                         join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                         join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                                         where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                                         && hstl.hstl_id == item.hstl_id
                                         && hstl.hstl_hidden == false && hs.hidden==false
                                         && hpct.hocphichitiet_ngayketthuc!=null
                                         orderby hpct.hocphichitiet_id descending
                                         select new {
                                             hs.account_id,
                                             hs.account_vn,
                                             hstl.hstl_id,
                                             hpct.hocphichitiet_ngayketthuc,
                                         }).FirstOrDefault();
            int tongsobuoi =  Convert.ToInt16(txtSoBuoiBaoLuu.Value);
            checkNgayKetThuc(check_idhocPhiChiTiet.hocphichitiet_ngayketthuc.Value, tongsobuoi);
            tbHocPhiChiTiet insert = new tbHocPhiChiTiet();
            insert.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayKetThuc.Value);
            insert.baoluu_loai = "Trung tâm bảo lưu";
            insert.hocphichitiet_ghichu = txtLyDoBaoLuu.Value;
            insert.baoluu_sobuoi = Convert.ToInt16(txtSoBuoiBaoLuu.Value);
            insert.hocsinh_id = check_idhocPhiChiTiet.account_id;
            insert.hstl_id = check_idhocPhiChiTiet.hstl_id;
            insert.lop_id = Convert.ToInt16(ddlLop.SelectedItem.Value);
            insert.hocsinh_fullname = check_idhocPhiChiTiet.account_vn;
            insert.hocphichitiet_nhanvien = logedMember.username_fullname;
            insert.hocphichitiet_createdate = DateTime.Now;
            db.tbHocPhiChiTiets.InsertOnSubmit(insert);
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã bảo lưu", "");
        }

    }
    protected void btnBaoLuuCaNhan_ServerClick(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var dsHocSinhTrongLop = (from hstl in db.tbhocsinhtronglops
                                 join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                 join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                 where hstl.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                                 && hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                 && hs.hidden == false && hstl.hstl_hidden == false && hpct1.hocphichitiet_ngayketthuc!=null
                                 orderby hpct1.hocphichitiet_id descending
                                 select new
                                 {
                                     hs.account_id,
                                     hs.account_vn,
                                     hstl.hstl_id,
                                     hpct1.hocphichitiet_ngayketthuc,
                                 }).FirstOrDefault();
        int tongsobuoi = Convert.ToInt16(txtSoBuoiBaoLuuCaNhan.Value);
        // lấy ngày kết thúc cuối cùng + số buổi bảo lưu sẽ ra ngày kết thúc tiếp theo
        checkNgayKetThuc(dsHocSinhTrongLop.hocphichitiet_ngayketthuc.Value, tongsobuoi);
        dteNgayKetThucCu.Value = dsHocSinhTrongLop.hocphichitiet_ngayketthuc.Value.ToString();
        tbHocPhiChiTiet insert = new tbHocPhiChiTiet();
        insert.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayKetThuc.Value);
        insert.baoluu_loai = "Cá nhân bảo lưu";
        insert.hocphichitiet_ghichu = txtLyDoBaoLuuCaNhan.Value;
        insert.baoluu_sobuoi = Convert.ToInt16(txtSoBuoiBaoLuuCaNhan.Value);
        insert.hocsinh_id = dsHocSinhTrongLop.account_id;
        insert.hstl_id = dsHocSinhTrongLop.hstl_id;
        insert.lop_id = Convert.ToInt16(cbbLopCaNhan.SelectedItem.Value);
        insert.hocsinh_fullname = dsHocSinhTrongLop.account_vn;
        insert.hocphichitiet_nhanvien = logedMember.username_fullname;
        insert.hocphichitiet_createdate = DateTime.Now;
        db.tbHocPhiChiTiets.InsertOnSubmit(insert);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã bảo lưu", "");
    }
    protected void btnNghiHan_ServerClick(object sender, EventArgs e)
    {
        var dsHocSinhTrongLop = (from hstl in db.tbhocsinhtronglops
                                 join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                 where hstl.lop_id == Convert.ToInt32(cbbLopNghiHan.SelectedItem.Value)
                                 && hs.account_id == Convert.ToInt32(cbbHocSinhNghiHan.SelectedItem.Value)
                                 && hstl.hstl_hidden == false && hs.hidden == false
                                 orderby hstl.hstl_id descending
                                 select hstl).FirstOrDefault();
        tbHocPhiChiTiet insert = new tbHocPhiChiTiet();
        insert.hstl_id = dsHocSinhTrongLop.hstl_id;
        insert.lop_id = Convert.ToInt16(cbbLopNghiHan.SelectedItem.Value);
        //insert.baoluu_ngayketthuc = dteNgayNghiHan.Value + "";
        insert.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayNghiHan.Value);
        insert.hocphichitiet_ghichu = txtLyDoNghiHan.Value;
        //insert.hocphichitiet_id = check_idhocPhiChiTiet.hocphichitiet_id;
        insert.baoluu_loai = "Nghỉ hẵn";
        insert.baoluu_tinhtrang = "Chưa duyệt";
        db.tbHocPhiChiTiets.InsertOnSubmit(insert);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã nghỉ hẵn", "");
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
        }

    }
    protected void cbbLopCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbbLopCaNhan.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(cbbLopCaNhan.SelectedItem.Value)
                           select l).SingleOrDefault();
            ddlHocSinh.DataSource = from hs in db.tbAccounts
                                    join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                    join l in db.tbLops on hstl.lop_id equals l.lop_id
                                    where l.lop_id == Convert.ToInt16(cbbLopCaNhan.SelectedItem.Value)
                                    && hstl.hstl_hidden == false && hs.hidden == false
                                    select hs;
            ddlHocSinh.DataBind();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
        }
    }



    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                             && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }

    protected void cbbCoSoCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbbLopCaNhan.DataSource = from l in db.tbLops
                                  where l.coso_id == Convert.ToInt16(cbbCoSoCaNhan.SelectedItem.Value)
                                   && l.hidden == false && l.lop_tinhtrang == null
                                  select l;
        cbbLopCaNhan.DataBind();
    }

    protected void cbbCoSoNghiHan_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbbLopNghiHan.DataSource = from l in db.tbLops
                                   where l.coso_id == Convert.ToInt16(cbbCoSoNghiHan.SelectedItem.Value)
                                    && l.hidden == false && l.lop_tinhtrang == null
                                   select l;
        cbbLopNghiHan.DataBind();
    }

    protected void cbbLopNghiHan_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbbLopNghiHan.Text != "")
        {
            cbbHocSinhNghiHan.DataSource = from hs in db.tbAccounts
                                           join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                           join l in db.tbLops on hstl.lop_id equals l.lop_id
                                           where l.lop_id == Convert.ToInt16(cbbLopNghiHan.SelectedItem.Value) && hstl.hstl_hidden == false
                                           select hs;
            cbbHocSinhNghiHan.DataBind();
        }
    }
}