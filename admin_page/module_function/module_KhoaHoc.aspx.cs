﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_KhoaHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getData = from n in db.tbKhoaHocs
                      join tb in db.tbLoaiKhoaHocs on n.loaikhoahoc_id equals tb.loaikhoahoc_id
                      select new
                      {
                          n.khoahoc_id,
                          n.khoahoc_name,
                          n.khoahoc_summary,
                          n.link_seo,
                          tb.loaikhoahoc_name,
                          tb.loaikhoahoc_id
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlloaisanpham.DataSource = from tb in db.tbLoaiKhoaHocs
                                    select tb;
        ddlloaisanpham.DataBind();

    }
    private void setNULL()
    {
        txttensanpham.Text = "";
        txtlink.Text = "";
        txttomtat.Text = "";
        ddlloaisanpham.Text = "";

    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "khoahoc_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbKhoaHocs
                       join tb in db.tbLoaiKhoaHocs on n.loaikhoahoc_id equals tb.loaikhoahoc_id
                       where n.khoahoc_id == _id
                       select n).Single();
        txttensanpham.Text = getData.khoahoc_name;
        txttomtat.Text = getData.khoahoc_summary;
        txtlink.Text = getData.link_seo;
        ddlloaisanpham.Value = getData.loaikhoahoc_id;

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_KhoaHoc cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "khoahoc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_KhoaHoc();

                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {

        cls_KhoaHoc cls = new cls_KhoaHoc();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {


        }
        if (Session["_id"].ToString() == "0")
        {


            if (cls.Linq_Them(txttensanpham.Text, txttomtat.Text, Convert.ToInt32(ddlloaisanpham.Value.ToString()), txtlink.Text))
            {
                alert.alert_Success(Page, "Thêm thành công", "");
                loadData();
            }
            else alert.alert_Error(Page, "Thêm thất bại", "");

        }
        else
        {

            if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text, txttomtat.Text, Convert.ToInt32(ddlloaisanpham.Value.ToString()), txtlink.Text))
            {
                alert.alert_Success(Page, "Cập nhật thành công", "");
                loadData();
            }
            else alert.alert_Error(Page, "Cập nhật thất bại", "");
        }
        popupControl.ShowOnPageLoad = false;
    }
    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }
}