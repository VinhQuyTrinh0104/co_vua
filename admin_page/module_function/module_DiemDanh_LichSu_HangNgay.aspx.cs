﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_LichSu_HangNgay : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
            }
            var getLop = from tt in db.tbLops select tt;
            ddlLop.DataSource = getLop;
            ddlLop.DataBind();
            //loadData();

        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        tbDiemDanh update = (from dd in db.tbDiemDanhs
                             where dd.hstl_id == Convert.ToInt32(txtHocSinhTrongLop.Value)
                              && dd.diemdanh_ngay.Value.Date == DateTime.Now.Date
                                     && dd.diemdanh_ngay.Value.Month == DateTime.Now.Month
                                      && dd.diemdanh_ngay.Value.Year == DateTime.Now.Year
                             select dd).FirstOrDefault();
        update.diemdanh_vang = "Đúng giờ";
        update.diemdanh_ghichu = "";
        db.SubmitChanges();
        alert.alert_Update(Page, "Đã xác nhận lại, vui lòng kiểm tra form điểm danh", "");
        //loadData();
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                             && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (ddlLop.Text != "")
        {
            var getDiemDanhLop = from dd in db.tbDiemDanhs
                                 join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                                 join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                 join l in db.tbLops on hstl.lop_id equals l.lop_id
                                 join u in db.admin_Users on dd.username_id equals u.username_id
                                 where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                                 && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                                  && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                                   && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                 select new
                                 {
                                     dd.diemdanh_ngay,
                                     dd.diemdanh_vang,
                                     dd.diemdanh_ghichu,
                                     u.username_fullname,
                                     l.lop_name,
                                     ac.account_vn,
                                     ac.account_en
                                 };
            rpData.DataSource = getDiemDanhLop;
            rpData.DataBind();
        }
        else
        {
            alert.alert_Warning(Page, "Vui lòng chọn lớp để xem", "");
        }
    }


}