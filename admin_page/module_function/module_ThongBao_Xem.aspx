﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongBao_Xem.aspx.cs" Inherits="admin_page_module_function_module_ThongBao_Xem" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>--%>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">


    <div class="card card-block box-admin">
        <div style="float:right">
            <a href="admin-home" class="btn btn-primary">Quay lại</a>
        </div>
        <asp:Repeater ID="rpContentThongBao" runat="server">
            <ItemTemplate>
                <div class="form-group-name">
                    <%#Eval("thongbao_title") %>
                </div>
                <div class="card card-block">
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <%#Eval("thongbao_content") %>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <%-- popup chi tiết --%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



