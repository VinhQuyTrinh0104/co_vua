﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DiemDanh_LichSu_HangNgay.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_LichSu_HangNgay" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>
<%@ Register Src="~/web_usercontrol/MenuGiaoVien.ascx" TagPrefix="uc1" TagName="MenuGiaoVien" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        function myDiemDanh(id) {
            document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value = id;
            document.getElementById("<%=btnSave.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <%--<div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
     <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN LỊCH SỬ ĐIỂM DANH HẰNG NGÀY
        </div>
        <%-- <asp:UpdatePanel ID="upDiemDanh" runat="server">
            <ContentTemplate>--%>
        <div class="col-12 form-group">
            <div class="col-3 form-group">
                <label class="col-3">Cơ sở:</label>
                <div class="col-9">
                    <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-3 form-group">
                <label class="col-2">Lớp:</label>
                <div class="col-10">
                    <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-3 form-group">
                <label class="col-2">Ngày:</label>
                <div class="col-10">
                    <input id="dteNgay" runat="server" type="date" />
                </div>
            </div>
            <div class="col-2 form-group">
                <a href="javascript:void(0)" class="btn btn-primary" id="btnXem" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
            </div>
        </div>
        <div>
            <b></b>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="text-align: center">STT</th>
                    <th>Họ tên</th>
                     <th>English name</th>
                    <th>Tình trạng</th>
                    <th>Lý do</th>
                    <th>Giáo viên điểm danh</th>
                </tr>
                <asp:Repeater ID="rpData" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><%=STT++ %></td>
                            <td><%#Eval("account_vn") %></td>
                            <td><%#Eval("account_en") %></td>
                            <td><%#Eval("diemdanh_vang") %></td>
                            <td><%#Eval("diemdanh_ghichu") %></td>
                            <td><%#Eval("username_fullname") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </thead>
        </table>
        <div style="display: none">
            <input type="text" id="txtHocSinhTrongLop" runat="server" />
            <a id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
        </div>
        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



