﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSach_HocSinh_ThongKeChiTiet_backup.aspx.cs" Inherits="admin_page_module_function_module_DanhSach_HocSinh_ThongKeChiTiet_backup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div style="float:right">
            <a href="admin-ds-tong" class="btn btn-primary" >Quay lại</a>
        </div>
        <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">
            <label class="col-12">THÔNG TIN HỌC SINH</label>
        </div>
        <div class="div_content col-12">
            <div class="row">
                <div class="col-6" style="margin: 0">
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Cơ sở:*</label>
                        <div class="col-10">
                            <asp:Label ID="txtCoSo" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Họ Và Tên:*</label>
                        <div class="col-10">
                            <asp:Label ID="txtFullName" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Ngày Sinh:*</label>
                        <div class="col-10">
                            <asp:Label ID="txtNgaySinh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">SĐT:</label>
                        <div class="col-10">
                            <asp:Label ID="txtSDTHocSinh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Trường học:</label>
                        <div class="col-10">
                            <asp:Label ID="txtTruongHoc" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Lớp: *</label>
                        <div class="col-10">
                            <asp:Label ID="txtLop" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">English name:</label>
                        <div class="col-10">
                            <asp:Label ID="txtBietDanh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Giới tính:</label>
                        <div class="col-10">
                            <asp:Label ID="txtGioiTinh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Email:</label>
                        <div class="col-10">
                            <asp:Label ID="txtEmailHocSinh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Nơi sinh:</label>
                        <div class="col-10">
                            <asp:Label ID="txtNoiSinh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Chổ ở hiện tại:</label>
                    <div class="col-10">
                        <asp:Label ID="txtChoOHienTai" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Thông tin phụ huynh:</label>
                </div>
                <div class="col-6" style="margin: 0">
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Tên ba:</label>
                        <div class="col-10">
                            <asp:Label ID="txtTenBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">SĐT ba:</label>
                        <div class="col-10">
                            <asp:Label ID="txtSDTBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Nghề nghiệp:</label>
                        <div class="col-10">
                            <asp:Label ID="txtNgheNghiep" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Email:</label>
                        <div class="col-10">
                            <asp:Label ID="txtEmaiBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Tên mẹ:</label>
                        <div class="col-10">
                            <asp:Label ID="txtTenMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">SĐT mẹ:</label>
                        <div class="col-10">
                            <asp:Label ID="txtSDTMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Nghề nghiệp mẹ:</label>
                        <div class="col-10">
                            <asp:Label ID="txtNgheNghiepMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Email mẹ:</label>
                        <div class="col-10">
                            <asp:Label ID="txtEmailMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Mong muốn nhu cầu của ba mẹ khi cho con học tại VJLC:</label>
                    <div class="col-10">
                        <asp:Label ID="txtMongMuon" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="div_content col-12" style="text-align: center">
            ------------------------------------ *** -----------------------------------
        </div>
        <div class="div_content col-12">
            <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">
                <label class="col-12">ĐĂNG KÝ HỌC PHÍ</label>
            </div>
        </div>
        <div class="col-12 form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hp_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Ngày đăng kí" FieldName="hocphi_ngaydongtien" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Quyển phiếu thu" FieldName="hocphichitiet_phieuthu" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Mã phiếu thu" FieldName="hocphichitiet_maso" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Nhân viên" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tên hs" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số buổi ĐK" FieldName="hocphichitiet_sobuoihoc" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày bắt đầu" FieldName="hocphichitiet_ngaybatdau" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày kết thúc" FieldName="ngayketthuc" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tình trạng đóng" FieldName="hocphi_tinhtrangdong" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Chi tiết" FieldName="hocphi_chitiet" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền khóa học" FieldName="hocphichitiet_sotiendongkhoahoc" HeaderStyle-HorizontalAlign="Center" Width="20%" CellStyle-VerticalAlign="Middle">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiendongkhoahoc", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Chương trình giảm" FieldName="hocphichitiet_chuongtrinhgiam" HeaderStyle-HorizontalAlign="Center" Width="20%" CellStyle-VerticalAlign="Middle">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_chuongtrinhgiam", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền đặt cọc" FieldName="hocphichitiet_sotiendadong" HeaderStyle-HorizontalAlign="Center" Width="35%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiendadong", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền còn lại" FieldName="hocphichitiet_sotienconlai" HeaderStyle-HorizontalAlign="Center" Width="35%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotienconlai", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền giáo trình" FieldName="hocphichitiet_sotiengiaotrinh" HeaderStyle-HorizontalAlign="Center" Width="35%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiengiaotrinh", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tổng tiền thu" FieldName="hocphi_tongtien" HeaderStyle-HorizontalAlign="Center" Width="35%">
                        <DataItemTemplate>
                            <%#Eval("hocphi_tongtien", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Hình thức thanh toán" FieldName="hocphichitiet_hinhthucthanhtoan" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                  
                     <dx:GridViewDataColumn Caption="Ghi chú" FieldName="hocphichitiet_ghichu" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>
                  
                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
         <div class="div_content col-12" style="text-align: center">
            ------------------------------------ *** -----------------------------------
        </div>
        <div class="div_content col-12">
            <div style="text-align: center; color: blue; font-size: 28px; font-weight: bold">
                <label class="col-12">BẢO LƯU</label>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvListBaoLuu" runat="server" ClientInstanceName="grvListBaoLuu" KeyFieldName="baoluu_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Ngày nhập" FieldName="baoluu_NgayNhap" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Học sinh" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số lần BL" FieldName="baoluu_name" HeaderStyle-HorizontalAlign="Center" Width="7%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số buổi BL" FieldName="baoluu_sobuoi" HeaderStyle-HorizontalAlign="Center" Width="7%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tình trạng" FieldName="baoluu_loai" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Lý do" FieldName="baoluu_ghichu" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

