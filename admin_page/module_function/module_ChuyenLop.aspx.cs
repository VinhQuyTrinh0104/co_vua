﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ChuyenLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public DateTime ngaycuoicung;
    public DateTime ngaycuoi;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
                ddlCoSoMoi.DataSource = from cs in db.tbCosos select cs;
                ddlCoSoMoi.DataBind();
            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getLop = from tt in db.tbLops select tt;
        //admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var getHocSinh = from a in db.tbAccounts select a;

    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

    }
    public void checkNgayKetThuc(DateTime dteNgayBatDauhoc, int tongsobuoi)
    {
        int i = 1;
        // cho chạy từ ngày bắt đầu cho tới khi tổng số buổi = tongssobuoi thì out
        string kiemtrathu = dteNgayBatDauhoc.DayOfWeek + "";
        for (int j = 1; j <= 3000; j++)
        {
            // ví dụ học sinh học 3 buổi thứ 2,4,6
            // ngày bắt đầu là ngày thứ 4 
            DateTime ngaytieptheo = dteNgayBatDauhoc.AddDays(j);
            kiemtrathu = ngaytieptheo.DayOfWeek + "";

            if (kiemtrathu == "Monday" && ckMoiThu2.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Tuesday" && ckMoiThu3.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Wednesday" && ckMoiThu4.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Thursday" && ckMoiThu5.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Friday" && ckMoiThu6.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Saturday" && ckMoiThu7.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (kiemtrathu == "Sunday" && ckMoiChuNhat.Checked == true)
            {
                i = i + 1;
                ngaycuoi = ngaytieptheo;
            }
            if (i == tongsobuoi)
            {
                //dteNgayKetThuc.Value = ngaytieptheo + "";
                dteNgayKetThuc.Value = ngaytieptheo.ToString("dd/MM/yyyy");
                // dteNgayKetThuc.Value = ngaycuoi.ToString("dd/MM/yyyy");
                break;
                // thí lấy ngày cuối cùng đó ra để lưu ngày kết thúc
            }
            else
            {
            }
        }
    }
    protected void btnKiemtra_ServerClick(object sender, EventArgs e)
    {
        checkNgayKetThuc(Convert.ToDateTime(dteNgayBatDau.Value), Convert.ToInt32(txtSoBuoiConLai.Value));
    }
    protected void btnXacNhanToanTrungTam_ServerClick(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //Bước 1 loại bỏ hs cũ cho về tình trạng true
        tbhocsinhtronglop updateHS = (from hs in db.tbhocsinhtronglops
                                      where hs.account_id == Convert.ToInt32(cbbHocSinh.SelectedItem.Value)
                                      orderby hs.hstl_id descending
                                      select hs).FirstOrDefault();
        updateHS.hstl_hidden = true;
        db.SubmitChanges();
        // Bước 2: thêm vào bảng học sinh trong lớp
        tbhocsinhtronglop inserthstl = new tbhocsinhtronglop();
        inserthstl.lop_id = Convert.ToInt16(ddlLopMoi.SelectedItem.Value);
        inserthstl.account_id = Convert.ToInt32(cbbHocSinh.SelectedItem.Value);
        inserthstl.hstl_hidden = false;
        db.tbhocsinhtronglops.InsertOnSubmit(inserthstl);
        db.SubmitChanges();
        // Bước 3 Thêm vào bảng học phí và bảng học phí chi tiết với ghi chú là chuyển lớp.
        tbHocPhi insertHocPhi = new tbHocPhi();
        insertHocPhi.hp_ghichu = "Chuyển lớp";
        insertHocPhi.hocphi_chitiet = "Chuyển lớp";
        insertHocPhi.account_id = inserthstl.account_id;
        insertHocPhi.username_id = logedMember.username_id;
        insertHocPhi.hstl_id = inserthstl.hstl_id;
        db.tbHocPhis.InsertOnSubmit(insertHocPhi);
        db.SubmitChanges();

        tbHocPhiChiTiet insertHocPhiChiTiet = new tbHocPhiChiTiet();
        insertHocPhiChiTiet.hocphi_id = insertHocPhi.hp_id;
        insertHocPhiChiTiet.hocsinh_id = inserthstl.account_id;
        insertHocPhiChiTiet.hstl_id = inserthstl.hstl_id;
        tbAccount hsvn = (from hs in db.tbAccounts where hs.account_id == inserthstl.account_id select hs).FirstOrDefault();
        insertHocPhiChiTiet.hocsinh_fullname = hsvn.account_vn;
        insertHocPhiChiTiet.lop_id = inserthstl.lop_id;
        insertHocPhiChiTiet.hocphichitiet_createdate = DateTime.Now;
        insertHocPhiChiTiet.hocphichitiet_sobuoihoc = Convert.ToInt16(txtSoBuoiConLai.Value);
        insertHocPhiChiTiet.hocphichitiet_ngaybatdau = Convert.ToDateTime(dteNgayBatDau.Value);
        insertHocPhiChiTiet.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayKetThuc.Value);
        insertHocPhiChiTiet.hocphichitiet_nhanvien = logedMember.username_fullname;
        insertHocPhiChiTiet.hocphichitiet_ghichu = "Chuyển lớp";
        insertHocPhiChiTiet.baoluu_loai = "Chuyển lớp";
        db.tbHocPhiChiTiets.InsertOnSubmit(insertHocPhiChiTiet);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã chuyển lớp", "");
    }

    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getHocSinh = from hs in db.tbAccounts
                         join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                         where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden == false
                         select hs;
        cbbHocSinh.DataSource = getHocSinh;
        cbbHocSinh.DataBind();
        if (ddlLop.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
        }
    }

    protected void ddlLopMoi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLopMoi.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLopMoi.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
                ckMoiThu2.Checked = true;
            else
                ckMoiThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckMoiThu3.Checked = true;
            else
                ckMoiThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckMoiThu4.Checked = true;
            else
                ckMoiThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckMoiThu5.Checked = true;
            else
                ckMoiThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckMoiThu6.Checked = true;
            else
                ckMoiThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckMoiThu7.Checked = true;
            else
                ckMoiThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckMoiChuNhat.Checked = true;
            else
                ckMoiChuNhat.Checked = false;
        }
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                             && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
    protected void ddlCoSoMoi_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLopMoi.DataSource = from l in db.tbLops
                               where l.coso_id == Convert.ToInt16(ddlCoSoMoi.SelectedItem.Value)
                                && l.hidden == false && l.lop_tinhtrang == null
                               select l;
        ddlLopMoi.DataBind();
    }
}