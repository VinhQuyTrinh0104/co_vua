﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_HistorySLL.aspx.cs" Inherits="admin_page_module_function_module_HistorySLL" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
       <%-- function btnChiTiet() {
            document.getElementById('<%=btnChuyenLop.ClientID%>').click();
        }--%>
        //function popupHide() {
        //    document.getElementById('btnClosePopup').click();
        //}
       <%-- function checkNULL() {
            var CityName = document.getElementById('<%= btnLuu.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }--%>
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtgiaovien.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Nội dung đánh giá không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
        //$(document).ready(function () {
        //    var date = new Date();

        //    var day = date.getDate();
        //    var month = date.getMonth() + 1;
        //    var year = date.getFullYear();

        //    if (month < 10) month = "0" + month;
        //    if (day < 10) day = "0" + day;

        //    var today = year + "-" + month + "-" + day;
        //    $("#txtNgay").attr("value", today);
        //});
        //js date
        function getDate() {
            var date = new Date();

            var day = date.getDate();
            var month = date.getMonth() + 1;
            var year = date.getFullYear();

            if (month < 10) month = "0" + month;
            if (day < 10) day = "0" + day;

            var today = year + "-" + day + "-" + month;
            //document.getElementById('txtNgay').day = today;
            var dateControl = document.querySelector('input[type="date"]');
            dateControl.value = today;
        }
    </script>

    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive" onload="getDate()">
            <div class="row">
                <div class="col-12 form-group">
                    <label class="col-1 col-sm-1 form-control-label mb-2">Ngày:</label>
                    <input type="date" id="txtNgay" class="" runat="server" value="" />
                    <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm Kiếm" OnClick="btnTimKiem_Click" />
                </div>

            </div>
            <div class="row form-group table-responsive">
                <div class="col-sm-12 ">
                    <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="sll_id" Width="100%">
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="6%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn Caption="Ngày đánh giá" FieldName="sll_date" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Họ và tên" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Nội dung thông báo" Settings-AllowEllipsisInText="true" FieldName="sll_danhgiagv" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Môn Học" Settings-AllowEllipsisInText="true" FieldName="monhoc_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="KQ giữa kỳ" Settings-AllowEllipsisInText="true" FieldName="sll_kqgiuaky" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="KQ cuối kỳ" Settings-AllowEllipsisInText="true" FieldName="sll_kqcuoiky" HeaderStyle-HorizontalAlign="Center" Width="12%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Giáo viên đánh giá" Settings-AllowEllipsisInText="true" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Empty" />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>

    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="800px" Height="400px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Lịch sử sổ liên lạc" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh(); grvList.UnselectRows();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Nội dung thông báo:</label>
                                        <div class="col-10">
                                            <textarea id="txtgiaovien" runat="server" rows="8" class="form-control boxed" style="width: 95%"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Điểm giữa kì:</label>
                                            <div class="col-8">
                                                <textarea id="txtKqgiuaky" runat="server" rows="3" style="width: 90%"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Điểm cuối kì:</label>
                                            <div class="col-8">
                                                <textarea id="txtKqcuoiky" runat="server" rows="3" style="width: 90%"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
