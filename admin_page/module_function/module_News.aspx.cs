﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_News : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    int vitri;
    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddlloaisanpham.DataSource = from tb in db.tbNewCates where tb.hidden == null select tb;
        ddlloaisanpham.DataBind();
        var getData = from n in db.tbNews
                      join tb in db.tbNewCates on n.newcate_id equals tb.newcate_id
                      orderby n.news_id descending
                      select new
                      {
                          n.news_id,
                          n.news_title,
                          n.news_content,
                          n.news_image,
                          n.news_summary,
                          tb.newcate_title,
                      };
        grvList.DataSource = getData;
        grvList.DataBind();


    }
    private void setNULL()
    {
        txttensanpham.Text = "";
        edtnoidung.Html = "";
        txttomtat.Text = "";
        ddlloaisanpham.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
        loadData();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "news_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbNews
                       join tb in db.tbNewCates on n.newcate_id equals tb.newcate_id
                       where n.news_id == _id
                       select new
                       {
                           n.news_id,
                           n.news_title,
                           n.news_content,
                           n.news_image,
                           n.news_summary,
                           tb.newcate_title,
                           n.newcate_id,
                       }).Single();
        txttensanpham.Text = getData.news_title;
        edtnoidung.Html = getData.news_content;
        txttomtat.Text = getData.news_summary;
        ddlloaisanpham.Text = getData.newcate_title;
        dteDate.Value = dteDate.Value;
       
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.news_image + "'); ", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_News cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_News();
                tbNew checkImage = (from i in db.tbNews where i.news_id == Convert.ToInt32(item) select i).SingleOrDefault();
                string pathToFiles = Server.MapPath(checkImage.news_image);
                delete(pathToFiles);
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "" && edtnoidung.Html != "" && txttomtat.Text!="")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/uploadimages/anh_slide/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/uploadimages/anh_slide/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/anh_slide"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        cls_News cls = new cls_News();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                if (dteDate.Value == "")
                {
                    dteDate.Value = DateTime.Now.ToString();
                }
                
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                else
                {
                }
                if (cls.Linq_Them(txttensanpham.Text, txttomtat.Text, image, edtnoidung.Html, Convert.ToInt32(ddlloaisanpham.Value.ToString()), Convert.ToDateTime(dteDate.Value)))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();

                }
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (dteDate.Value == "")
                {
                    dteDate.Value = DateTime.Now.ToString();
                }
               
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text, txttomtat.Text, image, edtnoidung.Html, Convert.ToDateTime(dteDate.Value)))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
        }
    }
    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }
    
}