﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichSuThaoTac : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {// Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void loadData()
    {
        var getData = from ls in db.tbOperationHistories
                      join user in db.admin_Users on ls.username_id equals user.username_id
                      orderby ls.history_id descending
                      select new
                      {
                          ls.history_id,
                          ls.history_day,
                          ls.history_content,
                          user.username_id,
                          user.username_fullname
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    //protected void btnThem_Click(object sender, EventArgs e)
    //{

    //}

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {

    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {

    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

    }
}