﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSach_Tong_backup.aspx.cs" Inherits="admin_page_module_function_module_DanhSach_Tong_backup" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        var arr = [];
        var arr_lop = [];
        function myCoSo(id) {

            let index = arr.indexOf(id); // tìm vị trí của id trong mảng
            if (index == -1) { // nếu không tìm thấy
                arr.push(id);
            } else {
                arr.splice(index, 1);
            }
            //console.log('id: ', id);
            //console.log('arr: ', arr);
            let txt = arr.toString();
            console.log('txt: ', txt);
            document.getElementById("<%=txtCoSo.ClientID%>").value = arr.toString();
        }
        function myLop(id) {
            let index = arr_lop.indexOf(id); // tìm vị trí của id trong mảng
            if (index == -1) { // nếu không tìm thấy
                arr_lop.push(id);
            } else {
                arr_lop.splice(index, 1);
            }
            //console.log('id: ', id);
            //console.log('arr: ', arr);
            let txt = arr_lop.toString();
            console.log('txt: ', txt);
            document.getElementById("txtlanclick").value = "0";
            document.getElementById("<%=txtLop.ClientID%>").value = arr_lop.toString();
        }
        function chonTatCaLop() {
            document.getElementById("txtlanclick").value = Number(document.getElementById("txtlanclick").value) + 1;
            var inputs = document.querySelectorAll('.pl');
            if (document.getElementById("txtlanclick").value % 2 == 0) {
                for (var i = 0; i < inputs.length; i++) {
                    inputs[i].checked = false;
                }
                arr_lop = [];
            }
            else {
                for (var i = 0; i < inputs.length; i++) {
                    inputs[i].checked = true;
                    //console.log(Number(inputs[i].value))
                    if (!arr_lop.includes(Number(inputs[i].value)))
                        arr_lop.push(Number(inputs[i].value));
                }
            }
            document.getElementById("<%=txtLop.ClientID%>").value = arr_lop.toString();
        }
        function setChecked() {
            var _arr = document.getElementById("<%=txtLop.ClientID%>").value.split(',');
            for (var i = 0; i < _arr.length; i++) {
                document.getElementById("ckl" + _arr[i]).checked = true;
            }
        }
    </script>
    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }

        .dxpc-content {
            height: 83vh !important;
        }

        .col-12 {
            display: flex;
            align-items: center !important;
        }

        .check-box {
            margin: 1% 0;
            color: #007057;
            font-weight: 700;
        }
    </style>
    <%-- <div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
    <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block box-admin">
        <div class="col-12 form-group-name">
            QUẢN LÝ THÔNG TIN TỔNG
        </div>
        <div class="check-box">
            Cơ sở: &nbsp;&nbsp;
            <asp:Repeater ID="rpDanhSachCoSo" runat="server">
                <ItemTemplate>
                    <input type="checkbox" id="ck<%#Eval("coso_id") %>" value="<%#Eval("coso_name") %>" name="check-danhsach" onclick="myCoSo(<%#Eval("coso_id") %>)" style="margin: 0 1%"><%#Eval("coso_name") %></label>&nbsp;&nbsp;
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div>
                    <input id="txtCoSo" runat="server" type="text" hidden="hidden" />
                    <a id="btnCoSo" runat="server" onserverclick="btnCoSo_ServerClick" class="btn btn-primary">Xem lớp</a>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="udButton" runat="server">
            <ContentTemplate>
                <div>
                    <%-- <div class="check-box">Lớp</div>--%>
                    <asp:Repeater ID="rpDanhSachLop" runat="server">
                        <ItemTemplate>
                            <b>
                                <input type="checkbox" class="pl" id="ckl<%#Eval("lop_id") %>" value="<%#Eval("lop_id") %>" onclick="myLop(<%#Eval("lop_id") %>)"><%#Eval("lop_name") %>&nbsp;&nbsp;</b>
                        </ItemTemplate>
                    </asp:Repeater>
                    <input type="text" id="txtlanclick" value="0" hidden="hidden" />
                    <a href="javascript:void(0)" class="btn btn-primary" onclick="chonTatCaLop()">Tất cả</a>
                    <%--  <input type="checkbox" id="cklTatCa" onclick="chonTatCaLop()" value="tatca">Tất cả&nbsp;&nbsp;</b>--%>
                </div>
                <input id="txtLop" runat="server" type="text" hidden="hidden" />
                <a id="btnLop" runat="server" onserverclick="btnLop_ServerClick" class="btn btn-primary">Xem danh sách học sinh</a>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="form-group row" style="margin: 1%;">
            <div class="form-group" style="justify-content: space-between;">
                <div class="row col-3" style="display: flex">
                    <label style="display: flex; align-items: center">cơ sở:</label>
                    <asp:DropDownList ID="ddlCoSo" CssClass="form-control" runat="server" Width="50%">
                    </asp:DropDownList>
                </div>
                <div class="row col-3" style="display: flex">
                    <label style="display: flex; align-items: center">Tháng:</label>
                    <asp:DropDownList ID="ddlThang" CssClass="form-control" runat="server" Width="30%">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="row col-4" style="display: flex">
                    <label style="display: flex; align-items: center">Năm:</label>
                    <asp:DropDownList ID="ddlNam" CssClass="form-control" runat="server" Width="30%">
                        <asp:ListItem Value="2022">2022</asp:ListItem>
                        <asp:ListItem Value="2023">2023</asp:ListItem>
                        <asp:ListItem Value="2024">2024</asp:ListItem>
                        <asp:ListItem Value="2025">2025</asp:ListItem>
                        <asp:ListItem Value="2026">2026</asp:ListItem>
                        <asp:ListItem Value="2027">2027</asp:ListItem>
                        <asp:ListItem Value="2028">2028</asp:ListItem>
                        <asp:ListItem Value="2029">2029</asp:ListItem>
                        <asp:ListItem Value="2030">2030</asp:ListItem>
                    </asp:DropDownList>

                    <div>
                        <a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                    <div class="col-2">

                        <a href="#" id="btnXuatExcelLop" runat="server" onserverclick="btnXuatExcelLop_ServerClick" class="btn btn-primary">Xuất Excel</a>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="form-group table-responsive">
                    <div class="col-sm-10" style="padding: 0">
                        <a href="#" id="btnXemthongTinHocSinh" runat="server" onserverclick="btnXemthongTinHocSinh_ServerClick" class="btn btn-primary">Xem thông tin chi tiết</a>
                    </div>
                    <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="account_id" Width="100%">
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn Caption="Ngày nhập" FieldName="account_ngaydangky" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Họ và Tên nhân viên" FieldName="hocphichitiet_nhanvien" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Họ và Tên học sinh" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="English name" FieldName="account_en" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Ngày sinh" FieldName="account_ngaysinh" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Giới tính" FieldName="account_gioitinh" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số phiếu thu" FieldName="hocphichitiet_phieuthu" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Mã phiếu thu" FieldName="hocphichitiet_maso" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số buổi đăng kí" FieldName="hocphichitiet_sobuoihoc" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Ngày bắt đầu" FieldName="hocphichitiet_ngaybatdau" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số buổi bảo lưu" FieldName="baoluu_sobuoi" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Ngày kết thúc" FieldName="hocphichitiet_ngayketthuc" HeaderStyle-HorizontalAlign="Center" Width="18%">
                                <DataItemTemplate>
                                    <%#Convert.ToDateTime(Eval("hocphichitiet_ngayketthuc")).ToString("dd/MM/yyyy")%>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tháng kết thúc" FieldName="hocphichitiet_thangketthuc" HeaderStyle-HorizontalAlign="Center" Width="8%">
                                <DataItemTemplate>
                                    <%#Convert.ToDateTime(Eval("hocphichitiet_thangketthuc")).ToString("MM/yyyy")%>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số điện thoại học sinh" FieldName="account_phone" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Email học sinh" FieldName="account_email" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Họ Tên ba" FieldName="account_tenba" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số điện thoại ba" FieldName="account_sdtba" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Họ Tên mẹ" FieldName="account_tenme" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số điện thoại mẹ" FieldName="account_sdtme" HeaderStyle-HorizontalAlign="Center" Width="18%"></dx:GridViewDataColumn>

                        </Columns>
                        <SettingsSearchPanel Visible="true" />
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



