﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DongHocPhi.aspx.cs" Inherits="admin_page_module_function_module_DongHocPhi" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
    </script>
    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN ĐĂNG KÍ HỌC PHÍ
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnThem" runat="server" Text="Đăng ký mới" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                        <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        <input type="submit" id="btnDlt" runat="server" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <br />
        <div class="form-group row">
            <div class="col-12 form-group">
                <label class="col-1">Cơ sở:</label>
                <div class="col-2">
                    <asp:DropDownList CssClass="form-control" ID="ddlCoSo" runat="server"></asp:DropDownList>
                    <%--<dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" CssClass="" NullText="Chọn cơ sở"></dx:ASPxComboBox>--%>
                </div>
                <label class="col-1">Chọn tháng:</label>
                <div class="col-2">
                    <asp:DropDownList ID="ddlThang" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="6">6</asp:ListItem>
                        <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                        <asp:ListItem Value="9">9</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <label class="col-1">Chọn năm:</label>
                <div class="col-2">
                    <asp:DropDownList ID="ddlNam" runat="server" CssClass="form-control">
                        <asp:ListItem Value="2022">2022</asp:ListItem>
                        <asp:ListItem Value="2023">2023</asp:ListItem>
                        <asp:ListItem Value="2024">2024</asp:ListItem>
                        <asp:ListItem Value="2025">2025</asp:ListItem>
                        <asp:ListItem Value="2026">2026</asp:ListItem>
                        <asp:ListItem Value="2027">2027</asp:ListItem>
                        <asp:ListItem Value="2028">2028</asp:ListItem>
                        <asp:ListItem Value="2029">2029</asp:ListItem>
                        <asp:ListItem Value="2030">2030</asp:ListItem>
                    </asp:DropDownList>
                </div>
               
                <div class="col-2">
                    &nbsp;&nbsp; &nbsp;&nbsp;<a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick" class="btn btn-primary">Xem</a>
                </div>
                  <div class="col-1">
                 <a href="#" id="btnXuatExcel" runat="server" onserverclick="btnXuatExcel_ServerClick" class="btn btn-primary">Xuất Excel</a>
                </div>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hp_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Ngày Đăng kí" FieldName="hocphi_ngaydongtien" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Quyển Phiếu thu" FieldName="hocphichitiet_phieuthu" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Mã Phiếu thu" FieldName="hocphichitiet_maso" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption=" Họ và tên nhân viên (fullname)" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ và Tên học viên (Việt Nam)" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số buổi đăng kí" FieldName="hocphichitiet_sobuoihoc" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày bắt đầu" FieldName="hocphichitiet_ngaybatdau" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày kết thúc" FieldName="hocphichitiet_ngayketthuc" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>

                    <dx:GridViewDataColumn Caption="Tình trạng đóng học phí học sinh" FieldName="hocphi_tinhtrangdong" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Chi Tiết đóng học phí" FieldName="hocphi_chitiet" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền khóa học" FieldName="hocphichitiet_sotiendongkhoahoc" HeaderStyle-HorizontalAlign="Center" Width="10%" CellStyle-VerticalAlign="Middle">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiendongkhoahoc", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Chương trình giảm" FieldName="hocphichitiet_chuongtrinhgiam" HeaderStyle-HorizontalAlign="Center" Width="10%" CellStyle-VerticalAlign="Middle">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_chuongtrinhgiam", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền đặt cọc" FieldName="hocphichitiet_sotiendadong" HeaderStyle-HorizontalAlign="Center" Width="10%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiendadong", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền còn lại" FieldName="hocphichitiet_sotienconlai" HeaderStyle-HorizontalAlign="Center" Width="10%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotienconlai", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền Giáo trình" FieldName="hocphichitiet_sotiengiaotrinh" HeaderStyle-HorizontalAlign="Center" Width="10%">
                        <DataItemTemplate>
                            <%#Eval("hocphichitiet_sotiengiaotrinh", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tổng tiền thu" FieldName="hocphi_tongtien" HeaderStyle-HorizontalAlign="Center" Width="10%">
                        <DataItemTemplate>
                            <%#Eval("hocphi_tongtien", "{0:N0}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Hình thức thanh toán" FieldName="hocphichitiet_hinhthucthanhtoan" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>

                    <dx:GridViewDataColumn Caption="Ghi chú (Thông tin đóng học phí của học sinh)" FieldName="hocphichitiet_ghichu" HeaderStyle-HorizontalAlign="Center" Width="35%"></dx:GridViewDataColumn>

                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



