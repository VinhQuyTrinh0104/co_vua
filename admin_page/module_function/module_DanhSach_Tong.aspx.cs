﻿using ClosedXML.Excel;
using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using OfficeOpenXml;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSach_Tong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    DateTime ngayketthuc;
    DataTable dt;
    bool gioitinh = true;

    private static string sukien = "";
    private static string sukiensearch = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                sukiensearch = "";
                sukien = "";
                Session["_id"] = 0;
                loadCoSo();
                // loadData();
            }
            rpDanhSachCoSo.DataSource = from cs in db.tbCosos where cs.coso_hidden == false select cs;
            rpDanhSachCoSo.DataBind();
            if (sukiensearch == "sukiensearch")
            {
                loadSearch();
            }
            else
            {
                if (sukien == "dslop")
                {
                    getdsHocSinhTheoLop();
                }
                else
                {
                    loadData();
                }
            }
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadCoSo()
    {
        var listNV = from nv in db.tbCosos select nv;
        ddlCoSo.Items.Clear();
        ddlCoSo.Items.Insert(0, "Tất cả");
        ddlCoSo.AppendDataBoundItems = true;
        ddlCoSo.DataTextField = "coso_name";
        ddlCoSo.DataValueField = "coso_id";
        ddlCoSo.DataSource = listNV;
        ddlCoSo.DataBind();
    }
    private void loadData()
    {
        var getLop = from tt in db.tbLops where tt.hidden == false select tt;

        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //admin_User là ROOT or ADMIN thì xem được hết danh sách account
        var getData = from nc in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby hstl.hstl_id descending
                      where nc.hidden == false && hstl.hstl_hidden == false
                      group new { nc, cs, l, hstl } by new { hstl.hstl_id, nc.username_username, nc.account_email, nc.account_ngaysinh, nc.account_vn, nc.account_phone, nc.account_tenba, nc.account_tenme, nc.account_sdtba, nc.account_sdtme, nc.account_id, nc.account_ngaydangky, nc.account_en, nc.account_gioitinh, cs.coso_name, l.lop_name } into g
                      select new
                      {
                          //nc.account_ngaydangky,
                          account_id = g.Key.account_id,
                          account_ngaydangky = g.Key.account_ngaydangky,
                          account_en = g.Key.account_en,
                          //account_gioitinh = nc.account_gioitinh==true?"Nam":"Nữ",
                          account_gioitinh = g.Key.account_gioitinh == true ? "Nam" : "Nữ",
                          //cs.coso_name,
                          coso_name = g.Key.coso_name,
                          lop_name = g.Key.lop_name,

                          //nc.account_vn,
                          account_vn = g.Key.account_vn,
                          //nc.account_phone,
                          account_phone = g.Key.account_phone,
                          //nc.account_tenba,
                          account_tenba = g.Key.account_tenba,
                          //nc.account_sdtba,
                          account_sdtba = g.Key.account_sdtba,
                          //nc.account_sdtme,
                          account_sdtme = g.Key.account_sdtme,
                          //nc.account_tenme,
                          account_tenme = g.Key.account_tenme,
                          //nc.username_username,
                          username_username = g.Key.username_username,
                          //nc.account_ngaysinh,
                          account_ngaysinh = g.Key.account_ngaysinh,
                          //hpct.hocphichitiet_nhanvien,
                          hocphichitiet_nhanvien = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_nhanvien,
                          //nc.account_email,
                          account_email = g.Key.account_email,
                          hocphichitiet_phieuthu = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_phieuthu,
                          hocphichitiet_maso = (from hpct1 in db.tbHocPhiChiTiets
                                                where hpct1.hstl_id == g.Key.hstl_id
                                                orderby hpct1.hocphichitiet_id descending
                                                select hpct1).FirstOrDefault().hocphichitiet_maso,
                          hocphichitiet_sobuoihoc = (from hpct1 in db.tbHocPhiChiTiets
                                                     join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                     join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                                                     where hpct1.hstl_id == g.Key.hstl_id && acc.hidden == false
                                                     && hstl.hstl_hidden == false && hpct1.hocphichitiet_sobuoihoc != null
                                                     orderby hpct1.hocphichitiet_id descending
                                                     select hpct1).FirstOrDefault().hocphichitiet_sobuoihoc,

                          // Ngày bắt đầu sẽ luôn lấy ở bên bảng học phí chi tiết trừ 3 trường hợp đặc biệt
                          hocphichitiet_ngaybatdau = (from hpct1 in db.tbHocPhiChiTiets
                                                      join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                      join hp in db.tbHocPhis on hpct1.hocphi_id equals hp.hp_id
                                                      join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                      where hstl.hstl_id == g.Key.hstl_id
                                                      && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngaybatdau != null
                                                      orderby hpct1.hocphichitiet_id descending
                                                      select hpct1).FirstOrDefault().hocphichitiet_ngaybatdau,

                          // lấy hstl id cuối cùng ra kiểm tra có trong học phí bảo lưu ko
                          // Bước 1: Phải kiểm tra trường hợp chuyển lớp đầu tiên vì trường hợp chuyển lớp sẽ ko nhập lại học phí chi tiết 
                          // Bước 2: Kiểm tra giá trị null của form bảo lưu khi có cột học phí chi tiết để loại ra 3 trường hợp. 
                          // Bước 3: Kiểm tra ngày kết thúc của bảng chi tiết với bảng bao lưu bảng nào có ngày kết thúc lớn hơn thì lấy
                          hocphichitiet_ngayketthucgannhat = (from bl in db.tbHocPhiChiTiets
                                                              join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                                              //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                                              where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                                              orderby bl.hocphichitiet_id descending
                                                              select bl).FirstOrDefault().baoluu_sobuoi == null ? (from hstl in db.tbhocsinhtronglops
                                                                                                                   join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                                                                                   join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                                                                                   where hstl.hstl_id == g.Key.hstl_id
                                                                                                                   && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                                                                                   orderby hpct1.hocphichitiet_id descending
                                                                                                                   select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc :
                          (from hstl in db.tbhocsinhtronglops
                           join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                           join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                           where hstl.hstl_id == g.Key.hstl_id
                           && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                           orderby hpct1.hocphichitiet_id descending
                           select hpct1).Skip(1).Take(1).SingleOrDefault().hocphichitiet_ngayketthuc,
                          hocphichitiet_ngayketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                          hocphichitiet_thangketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                          baoluu_sobuoi = (from bl in db.tbHocPhiChiTiets
                                           join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                           //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                           where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                           orderby bl.hocphichitiet_id descending
                                           select bl.baoluu_sobuoi).ToString(),
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    protected void btnCoSo_ServerClick(object sender, EventArgs e)
    {
        string[] arrCoSo = txtCoSo.Value.Split(',');
        var getLopTong = from x in db.tbLops
                         where arrCoSo.Contains(x.coso_id.ToString()) && x.lop_tinhtrang == null && x.hidden == false
                         select x;
        rpDanhSachLop.DataSource = getLopTong;
        rpDanhSachLop.DataBind();
    }

    protected void btnLop_ServerClick(object sender, EventArgs e)
    {
        sukien = "dslop";
        sukiensearch = "";
        getdsHocSinhTheoLop();
    }

    protected void btnXemthongTinHocSinh_ServerClick(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "account_id" }));
        Response.Redirect("admin-xem-chi-tiet-hoc-sinh-" + id);
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {

        sukiensearch = "sukiensearch";
        sukien = "";
        loadSearch();
        //setChecked()
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "setChecked()", true);
    }
    protected void loadSearch()
    {
        if (ddlCoSo.Text != "Tất cả")
        {
            var getData = from nc in db.tbAccounts
                          join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                          join l in db.tbLops on hstl.lop_id equals l.lop_id
                          join cs in db.tbCosos on l.coso_id equals cs.coso_id
                          orderby hstl.hstl_id descending
                          where nc.hidden == false && hstl.hstl_hidden == false
                          && cs.coso_id == Convert.ToInt16(ddlCoSo.SelectedValue)
                          group new { nc, cs, l, hstl } by new { hstl.hstl_id, nc.username_username, nc.account_email, nc.account_ngaysinh, nc.account_vn, nc.account_phone, nc.account_tenba, nc.account_tenme, nc.account_sdtba, nc.account_sdtme, nc.account_id, nc.account_ngaydangky, nc.account_en, nc.account_gioitinh, cs.coso_name, l.lop_name } into g
                          select new
                          {
                              //nc.account_ngaydangky,
                              account_id = g.Key.account_id,
                              account_ngaydangky = g.Key.account_ngaydangky,
                              account_en = g.Key.account_en,
                              //account_gioitinh = nc.account_gioitinh==true?"Nam":"Nữ",
                              account_gioitinh = g.Key.account_gioitinh == true ? "Nam" : "Nữ",
                              //cs.coso_name,
                              coso_name = g.Key.coso_name,
                              lop_name = g.Key.lop_name,

                              //nc.account_vn,
                              account_vn = g.Key.account_vn,
                              //nc.account_phone,
                              account_phone = g.Key.account_phone,
                              //nc.account_tenba,
                              account_tenba = g.Key.account_tenba,
                              //nc.account_sdtba,
                              account_sdtba = g.Key.account_sdtba,
                              //nc.account_sdtme,
                              account_sdtme = g.Key.account_sdtme,
                              //nc.account_tenme,
                              account_tenme = g.Key.account_tenme,
                              //nc.username_username,
                              username_username = g.Key.username_username,
                              //nc.account_ngaysinh,
                              account_ngaysinh = g.Key.account_ngaysinh,
                              //hpct.hocphichitiet_nhanvien,
                              hocphichitiet_nhanvien = (from hpct1 in db.tbHocPhiChiTiets
                                                        where hpct1.hstl_id == g.Key.hstl_id
                                                        orderby hpct1.hocphichitiet_id descending
                                                        select hpct1).FirstOrDefault().hocphichitiet_nhanvien,
                              //nc.account_email,
                              account_email = g.Key.account_email,
                              hocphichitiet_phieuthu = (from hpct1 in db.tbHocPhiChiTiets
                                                        where hpct1.hstl_id == g.Key.hstl_id
                                                        orderby hpct1.hocphichitiet_id descending
                                                        select hpct1).FirstOrDefault().hocphichitiet_phieuthu,
                              hocphichitiet_maso = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_maso,
                              hocphichitiet_sobuoihoc = (from hpct1 in db.tbHocPhiChiTiets
                                                         join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                         join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                                                         where hpct1.hstl_id == g.Key.hstl_id && acc.hidden == false
                                                         && hstl.hstl_hidden == false && hpct1.hocphichitiet_sobuoihoc != null
                                                         orderby hpct1.hocphichitiet_id descending
                                                         select hpct1).FirstOrDefault().hocphichitiet_sobuoihoc,

                              // Ngày bắt đầu sẽ luôn lấy ở bên bảng học phí chi tiết trừ 3 trường hợp đặc biệt
                              hocphichitiet_ngaybatdau = (from hpct1 in db.tbHocPhiChiTiets
                                                          join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                          join hp in db.tbHocPhis on hpct1.hocphi_id equals hp.hp_id
                                                          join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                          where hstl.hstl_id == g.Key.hstl_id
                                                          && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngaybatdau != null
                                                          orderby hpct1.hocphichitiet_id descending
                                                          select hpct1).FirstOrDefault().hocphichitiet_ngaybatdau,

                              // lấy hstl id cuối cùng ra kiểm tra có trong học phí bảo lưu ko
                              // Bước 1: Phải kiểm tra trường hợp chuyển lớp đầu tiên vì trường hợp chuyển lớp sẽ ko nhập lại học phí chi tiết 
                              // Bước 2: Kiểm tra giá trị null của form bảo lưu khi có cột học phí chi tiết để loại ra 3 trường hợp. 
                              // Bước 3: Kiểm tra ngày kết thúc của bảng chi tiết với bảng bao lưu bảng nào có ngày kết thúc lớn hơn thì lấy
                              hocphichitiet_ngayketthucgannhat = (from bl in db.tbHocPhiChiTiets
                                                                  join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                                                  //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                                                  where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                                                  orderby bl.hocphichitiet_id descending
                                                                  select bl).FirstOrDefault().baoluu_sobuoi == null ? (from hstl in db.tbhocsinhtronglops
                                                                                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                                                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                                                                                       where hstl.hstl_id == g.Key.hstl_id
                                                                                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                                                                                       orderby hpct1.hocphichitiet_id descending
                                                                                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc :
                          (from hstl in db.tbhocsinhtronglops
                           join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                           join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                           where hstl.hstl_id == g.Key.hstl_id
                           && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                           orderby hpct1.hocphichitiet_id descending
                           select hpct1).Skip(1).Take(1).SingleOrDefault().hocphichitiet_ngayketthuc,
                              hocphichitiet_ngayketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                              hocphichitiet_thangketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                              baoluu_sobuoi = (from bl in db.tbHocPhiChiTiets
                                               join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                               //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                               where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                               orderby bl.hocphichitiet_id descending
                                               select bl.baoluu_sobuoi).ToString(),
                          };
            var loadSearch = from ck in getData
                             where Convert.ToDateTime(ck.hocphichitiet_ngayketthuc).Month == Convert.ToInt16(ddlThang.Text)
                             && Convert.ToDateTime(ck.hocphichitiet_ngayketthuc).Year == Convert.ToInt16(ddlNam.Text)
                             select ck;
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = loadSearch;
            grvList.DataBind();
        }
        else
        {
            var getData = from nc in db.tbAccounts
                          join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                          join l in db.tbLops on hstl.lop_id equals l.lop_id
                          join cs in db.tbCosos on l.coso_id equals cs.coso_id
                          orderby hstl.hstl_id descending
                          where nc.hidden == false && hstl.hstl_hidden == false
                          group new { nc, cs, l, hstl } by new { hstl.hstl_id, nc.username_username, nc.account_email, nc.account_ngaysinh, nc.account_vn, nc.account_phone, nc.account_tenba, nc.account_tenme, nc.account_sdtba, nc.account_sdtme, nc.account_id, nc.account_ngaydangky, nc.account_en, nc.account_gioitinh, cs.coso_name, l.lop_name } into g
                          select new
                          {
                              //nc.account_ngaydangky,
                              account_id = g.Key.account_id,
                              account_ngaydangky = g.Key.account_ngaydangky,
                              account_en = g.Key.account_en,
                              //account_gioitinh = nc.account_gioitinh==true?"Nam":"Nữ",
                              account_gioitinh = g.Key.account_gioitinh == true ? "Nam" : "Nữ",
                              //cs.coso_name,
                              coso_name = g.Key.coso_name,
                              lop_name = g.Key.lop_name,

                              //nc.account_vn,
                              account_vn = g.Key.account_vn,
                              //nc.account_phone,
                              account_phone = g.Key.account_phone,
                              //nc.account_tenba,
                              account_tenba = g.Key.account_tenba,
                              //nc.account_sdtba,
                              account_sdtba = g.Key.account_sdtba,
                              //nc.account_sdtme,
                              account_sdtme = g.Key.account_sdtme,
                              //nc.account_tenme,
                              account_tenme = g.Key.account_tenme,
                              //nc.username_username,
                              username_username = g.Key.username_username,
                              //nc.account_ngaysinh,
                              account_ngaysinh = g.Key.account_ngaysinh,
                              //hpct.hocphichitiet_nhanvien,
                              hocphichitiet_nhanvien = (from hpct1 in db.tbHocPhiChiTiets
                                                        where hpct1.hstl_id == g.Key.hstl_id
                                                        orderby hpct1.hocphichitiet_id descending
                                                        select hpct1).FirstOrDefault().hocphichitiet_nhanvien,
                              //nc.account_email,
                              account_email = g.Key.account_email,
                              hocphichitiet_phieuthu = (from hpct1 in db.tbHocPhiChiTiets
                                                        where hpct1.hstl_id == g.Key.hstl_id
                                                        orderby hpct1.hocphichitiet_id descending
                                                        select hpct1).FirstOrDefault().hocphichitiet_phieuthu,
                              hocphichitiet_maso = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_maso,
                              hocphichitiet_sobuoihoc = (from hpct1 in db.tbHocPhiChiTiets
                                                         join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                         join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                                                         where hpct1.hstl_id == g.Key.hstl_id && acc.hidden == false
                                                         && hstl.hstl_hidden == false && hpct1.hocphichitiet_sobuoihoc != null
                                                         orderby hpct1.hocphichitiet_id descending
                                                         select hpct1).FirstOrDefault().hocphichitiet_sobuoihoc,

                              // Ngày bắt đầu sẽ luôn lấy ở bên bảng học phí chi tiết trừ 3 trường hợp đặc biệt
                              hocphichitiet_ngaybatdau = (from hpct1 in db.tbHocPhiChiTiets
                                                          join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                          join hp in db.tbHocPhis on hpct1.hocphi_id equals hp.hp_id
                                                          join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                          where hstl.hstl_id == g.Key.hstl_id
                                                          && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngaybatdau != null
                                                          orderby hpct1.hocphichitiet_id descending
                                                          select hpct1).FirstOrDefault().hocphichitiet_ngaybatdau,

                              // lấy hstl id cuối cùng ra kiểm tra có trong học phí bảo lưu ko
                              // Bước 1: Phải kiểm tra trường hợp chuyển lớp đầu tiên vì trường hợp chuyển lớp sẽ ko nhập lại học phí chi tiết 
                              // Bước 2: Kiểm tra giá trị null của form bảo lưu khi có cột học phí chi tiết để loại ra 3 trường hợp. 
                              // Bước 3: Kiểm tra ngày kết thúc của bảng chi tiết với bảng bao lưu bảng nào có ngày kết thúc lớn hơn thì lấy
                              hocphichitiet_ngayketthucgannhat = (from bl in db.tbHocPhiChiTiets
                                                                  join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                                                  //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                                                  where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                                                  orderby bl.hocphichitiet_id descending
                                                                  select bl).FirstOrDefault().baoluu_sobuoi == null ? (from hstl in db.tbhocsinhtronglops
                                                                                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                                                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                                                                                       where hstl.hstl_id == g.Key.hstl_id
                                                                                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                                                                                       orderby hpct1.hocphichitiet_id descending
                                                                                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc :
                          (from hstl in db.tbhocsinhtronglops
                           join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                           join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                           where hstl.hstl_id == g.Key.hstl_id
                           && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                           orderby hpct1.hocphichitiet_id descending
                           select hpct1).Skip(1).Take(1).SingleOrDefault().hocphichitiet_ngayketthuc,
                              hocphichitiet_ngayketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                              hocphichitiet_thangketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                              baoluu_sobuoi = (from bl in db.tbHocPhiChiTiets
                                               join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                               //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                               where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                               orderby bl.hocphichitiet_id descending
                                               select bl.baoluu_sobuoi).ToString(),
                          };
            var loadSearch = from ck in getData
                             where Convert.ToDateTime(ck.hocphichitiet_ngayketthuc).Month == Convert.ToInt16(ddlThang.Text)
                             && Convert.ToDateTime(ck.hocphichitiet_ngayketthuc).Year == Convert.ToInt16(ddlNam.Text)
                             select ck;
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = loadSearch;
            grvList.DataBind();
        }
    }

    protected void btnXuatExcelLop_ServerClick(object sender, EventArgs e)
    {
        Random rd = new Random();
        string file_excel = rd.Next(1, 9999) + "thongketong.xlsx";

        var newFile = new FileInfo(Server.MapPath("~/uploadimages/thongketong/" + file_excel));
        using (var package = new ExcelPackage(newFile))
        {
            // Thêm một trang tính mới vào file Excel
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
            // Điền dữ liệu vào ô A1 của trang tính
            worksheet.Cells["A1"].Value = "Ngày nhập";
            worksheet.Cells["B1"].Value = "Cơ sở";
            worksheet.Cells["C1"].Value = "Lớp";
            worksheet.Cells["D1"].Value = "Họ và tên nhân viên";
            worksheet.Cells["E1"].Value = "Họ và tên học sinh";
            worksheet.Cells["F1"].Value = "English name";
            worksheet.Cells["G1"].Value = "Ngày sinh";
            worksheet.Cells["H1"].Value = "Giới tính";
            worksheet.Cells["I1"].Value = "Số điện thoại học sinh";
            worksheet.Cells["J1"].Value = "Email học sinh";
            worksheet.Cells["K1"].Value = "Họ tên ba";
            worksheet.Cells["L1"].Value = "Số điện thoại ba";
            worksheet.Cells["M1"].Value = "Họ tên mẹ";
            worksheet.Cells["N1"].Value = "Số điện thoại mẹ";
            worksheet.Cells["O1"].Value = "Phiếu thu";
            worksheet.Cells["P1"].Value = "Mã phiếu";
            worksheet.Cells["Q1"].Value = "Số buổi đăng ký";
            worksheet.Cells["R1"].Value = "Ngày bắt đầu";
            worksheet.Cells["S1"].Value = "Số buổi bảo lưu";
            worksheet.Cells["T1"].Value = "Ngày kết kết";
            worksheet.Cells["U1"].Value = "Tháng kết thúc";
            int i = 1;
            dt = (DataTable)Session["dt"];
            foreach (DataRow item in dt.Rows)
            {
                i++;
                worksheet.Cells["A" + i].Value = item[0];
                worksheet.Cells["B" + i].Value = item[1];
                worksheet.Cells["C" + i].Value = item[2];
                worksheet.Cells["D" + i].Value = item[3];
                worksheet.Cells["E" + i].Value = item[4];
                worksheet.Cells["F" + i].Value = item[5];
                worksheet.Cells["G" + i].Value = item[6];
                worksheet.Cells["H" + i].Value = item[7];
                worksheet.Cells["I" + i].Value = item[8];
                worksheet.Cells["J" + i].Value = item[9];
                worksheet.Cells["K" + i].Value = item[10];
                worksheet.Cells["L" + i].Value = item[11];
                worksheet.Cells["M" + i].Value = item[12];
                worksheet.Cells["N" + i].Value = item[13];
                worksheet.Cells["O" + i].Value = item[14];
                worksheet.Cells["P" + i].Value = item[15];
                worksheet.Cells["Q" + i].Value = item[16];
                worksheet.Cells["R" + i].Value = item[17];
                worksheet.Cells["S" + i].Value = item[18];
                worksheet.Cells["T" + i].Value = item[19];
                worksheet.Cells["U" + i].Value = item[19];
            }
            // Lưu file Excel
            package.Save();
            string filePath = Server.MapPath("~/uploadimages/thongketong/" + file_excel);
            // Gửi file Excel về trình duyệt của người dùng
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
            Response.TransmitFile(filePath);
            Response.End();
            //DeleteExcelFile();
        }
    }
    protected void DeleteExcelFile()
    {
        string filePath = Server.MapPath("D:/2023/toantuduy/admin_page/module_function/newfile.xlsx");
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }
    protected void getdsHocSinhTheoLop()
    {

        string[] arrLop = txtLop.Value.Split(',');
        var getData = from nc in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on nc.account_id equals hstl.account_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby hstl.hstl_id descending
                      where nc.hidden == false && arrLop.Contains(l.lop_id.ToString())
                      && hstl.hstl_hidden == false
                      group new { nc, cs, l, hstl } by new { hstl.hstl_id, nc.username_username, nc.account_email, nc.account_ngaysinh, nc.account_vn, nc.account_phone, nc.account_tenba, nc.account_tenme, nc.account_sdtba, nc.account_sdtme, nc.account_id, nc.account_ngaydangky, nc.account_en, nc.account_gioitinh, cs.coso_name, l.lop_name } into g
                      select new
                      {
                          //nc.account_ngaydangky,
                          account_id = g.Key.account_id,
                          account_ngaydangky = g.Key.account_ngaydangky,
                          account_en = g.Key.account_en,
                          //account_gioitinh = nc.account_gioitinh==true?"Nam":"Nữ",
                          account_gioitinh = g.Key.account_gioitinh == true ? "Nam" : "Nữ",
                          //cs.coso_name,
                          coso_name = g.Key.coso_name,
                          lop_name = g.Key.lop_name,

                          //nc.account_vn,
                          account_vn = g.Key.account_vn,
                          //nc.account_phone,
                          account_phone = g.Key.account_phone,
                          //nc.account_tenba,
                          account_tenba = g.Key.account_tenba,
                          //nc.account_sdtba,
                          account_sdtba = g.Key.account_sdtba,
                          //nc.account_sdtme,
                          account_sdtme = g.Key.account_sdtme,
                          //nc.account_tenme,
                          account_tenme = g.Key.account_tenme,
                          //nc.username_username,
                          username_username = g.Key.username_username,
                          //nc.account_ngaysinh,
                          account_ngaysinh = g.Key.account_ngaysinh,
                          //hpct.hocphichitiet_nhanvien,
                          hocphichitiet_nhanvien = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_nhanvien,
                          //nc.account_email,
                          account_email = g.Key.account_email,
                          hocphichitiet_phieuthu = (from hpct1 in db.tbHocPhiChiTiets
                                                    where hpct1.hstl_id == g.Key.hstl_id
                                                    orderby hpct1.hocphichitiet_id descending
                                                    select hpct1).FirstOrDefault().hocphichitiet_phieuthu,
                          hocphichitiet_maso = (from hpct1 in db.tbHocPhiChiTiets
                                                where hpct1.hstl_id == g.Key.hstl_id
                                                orderby hpct1.hocphichitiet_id descending
                                                select hpct1).FirstOrDefault().hocphichitiet_maso,
                          hocphichitiet_sobuoihoc = (from hpct1 in db.tbHocPhiChiTiets
                                                     join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                     join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                                                     where hpct1.hstl_id == g.Key.hstl_id && acc.hidden == false
                                                     && hstl.hstl_hidden == false && hpct1.hocphichitiet_sobuoihoc != null
                                                     orderby hpct1.hocphichitiet_id descending
                                                     select hpct1).FirstOrDefault().hocphichitiet_sobuoihoc,

                          // Ngày bắt đầu sẽ luôn lấy ở bên bảng học phí chi tiết trừ 3 trường hợp đặc biệt
                          hocphichitiet_ngaybatdau = (from hpct1 in db.tbHocPhiChiTiets
                                                      join hstl in db.tbhocsinhtronglops on hpct1.hstl_id equals hstl.hstl_id
                                                      join hp in db.tbHocPhis on hpct1.hocphi_id equals hp.hp_id
                                                      join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                      where hstl.hstl_id == g.Key.hstl_id
                                                      && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngaybatdau != null
                                                      orderby hpct1.hocphichitiet_id descending
                                                      select hpct1).FirstOrDefault().hocphichitiet_ngaybatdau,

                          // lấy hstl id cuối cùng ra kiểm tra có trong học phí bảo lưu ko
                          // Bước 1: Phải kiểm tra trường hợp chuyển lớp đầu tiên vì trường hợp chuyển lớp sẽ ko nhập lại học phí chi tiết 
                          // Bước 2: Kiểm tra giá trị null của form bảo lưu khi có cột học phí chi tiết để loại ra 3 trường hợp. 
                          // Bước 3: Kiểm tra ngày kết thúc của bảng chi tiết với bảng bao lưu bảng nào có ngày kết thúc lớn hơn thì lấy
                          hocphichitiet_ngayketthucgannhat = (from bl in db.tbHocPhiChiTiets
                                                              join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                                              //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                                              where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                                              orderby bl.hocphichitiet_id descending
                                                              select bl).FirstOrDefault().baoluu_sobuoi == null ? (from hstl in db.tbhocsinhtronglops
                                                                                                                   join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                                                                                   join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                                                                                   where hstl.hstl_id == g.Key.hstl_id
                                                                                                                   && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                                                                                   orderby hpct1.hocphichitiet_id descending
                                                                                                                   select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc :
                          (from hstl in db.tbhocsinhtronglops
                           join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                           join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                           where hstl.hstl_id == g.Key.hstl_id
                           && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                           orderby hpct1.hocphichitiet_id descending
                           select hpct1).Skip(1).Take(1).SingleOrDefault().hocphichitiet_ngayketthuc,
                          hocphichitiet_ngayketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                          hocphichitiet_thangketthuc =  // Kiểm tra bảng học phí bảo lưu kết hợp với bảng học phí chi tiết để loại ra 3 trường hợp
                                                      (from hstl in db.tbhocsinhtronglops
                                                       join hpct1 in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct1.hstl_id
                                                       join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                                       where hstl.hstl_id == g.Key.hstl_id
                                                       && hstl.hstl_hidden == false && ac.hidden == false && hpct1.hocphichitiet_ngayketthuc != null
                                                       orderby hpct1.hocphichitiet_id descending
                                                       select hpct1).FirstOrDefault().hocphichitiet_ngayketthuc,

                          baoluu_sobuoi = (from bl in db.tbHocPhiChiTiets
                                           join hstl1 in db.tbhocsinhtronglops on bl.hstl_id equals hstl1.hstl_id
                                           //join hpct in db.tbHocPhiChiTiets on bl.hocphichitiet_id equals hpct.hocphichitiet_id
                                           where hstl1.hstl_id == g.Key.hstl_id && hstl1.hstl_hidden == false && bl.baoluu_sobuoi != null
                                           orderby bl.hocphichitiet_id descending
                                           select bl).FirstOrDefault().baoluu_sobuoi,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        Session["dt"] = getData;


        dt = new DataTable();
        dt.Columns.Add("account_ngaydangky", typeof(string));
        dt.Columns.Add("coso_name", typeof(string));
        dt.Columns.Add("lop_name", typeof(string));
        dt.Columns.Add("hocphichitiet_nhanvien", typeof(string));
        dt.Columns.Add("account_vn", typeof(string));
        dt.Columns.Add("account_en", typeof(string));
        dt.Columns.Add("account_ngaysinh", typeof(string));
        dt.Columns.Add("account_gioitinh", typeof(string));
        dt.Columns.Add("account_phone", typeof(string));
        dt.Columns.Add("account_email", typeof(string));
        dt.Columns.Add("account_tenba", typeof(string));
        dt.Columns.Add("account_sdtba", typeof(string));
        dt.Columns.Add("account_tenme", typeof(string));
        dt.Columns.Add("account_sdtme", typeof(string));
        dt.Columns.Add("hocphichitiet_phieuthu", typeof(string));
        dt.Columns.Add("hocphichitiet_maso", typeof(string));
        dt.Columns.Add("hocphichitiet_sobuoihoc", typeof(string));
        dt.Columns.Add("hocphichitiet_ngaybatdau", typeof(string));
        dt.Columns.Add("baoluu_sobuoi", typeof(string));
        dt.Columns.Add("hocphichitiet_ngayketthuc", typeof(string));
        dt.Columns.Add("hocphichitiet_thangketthuc", typeof(string));

        foreach (var item in getData)
        {
            DataRow dtrow = dt.NewRow();
            dtrow[0] = item.account_ngaydangky;
            dtrow[1] = item.coso_name;
            dtrow[2] = item.lop_name;
            dtrow[3] = item.hocphichitiet_nhanvien;
            dtrow[4] = item.account_vn;
            dtrow[5] = item.account_en;
            dtrow[6] = item.account_ngaysinh;
            dtrow[7] = item.account_gioitinh;
            dtrow[8] = item.account_phone;
            dtrow[9] = item.account_email;
            dtrow[10] = item.account_tenba;
            dtrow[11] = item.account_sdtba;
            dtrow[12] = item.account_tenme;
            dtrow[13] = item.account_sdtme;
            dtrow[14] = item.hocphichitiet_phieuthu;
            dtrow[15] = item.hocphichitiet_maso;
            dtrow[16] = item.hocphichitiet_sobuoihoc;
            dtrow[17] = item.hocphichitiet_ngaybatdau;
            dtrow[18] = item.baoluu_sobuoi;
            dtrow[19] = item.hocphichitiet_ngayketthuc;
            dtrow[20] = item.hocphichitiet_thangketthuc;
            dt.Rows.Add(dtrow);
        }
        Session["dt"] = dt;
        // dt = (DataTable)getData;
        //setChecked()
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "setChecked()", true);
    }

}