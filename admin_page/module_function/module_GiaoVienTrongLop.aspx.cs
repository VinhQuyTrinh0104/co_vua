﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoVienTrongLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddllop.DataSource = from l in db.tbLops
                            where l.username_idEn==null && l.hidden ==false
                            select l;
        ddllop.DataBind();
        var loadGV = from gv in db.admin_Users
                     orderby gv.username_id descending
                     join gr in db.admin_GroupUsers on gv.groupuser_id equals gr.groupuser_id
                     where gv.groupuser_id == 3 || gv.groupuser_id == 4
                     select new
                     {
                         gv.username_id,
                         gv.username_fullname,
                         gv.username_phone,
                         gv.username_email,
                         gr.groupuser_name
                     };
        grvList.DataSource = loadGV;
        grvList.DataBind();


    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_TaoGiaoVienTrongLop cls = new cls_TaoGiaoVienTrongLop();
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (ddllop.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Hãy chọn lớp để thêm giáo viên','','error').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "username_id", "username_fullname" });
            if (selectedKey.Count > 1)
            {
                foreach (object[] item in selectedKey)
                {
                    //insert id giáo viên vào 2 cột username_idVn & username_idEn trong table tbLop
                    // kiểm tra giáo viên đó là người VN hay người Nước ngoài
                    var kiemtra = (from user in db.admin_Users
                                   where user.username_id == Convert.ToInt32(item[0])
                                   select new
                                   {
                                       user.username_id,
                                       user.groupuser_id
                                   }).FirstOrDefault();
                    if (kiemtra != null)
                    {
                        var getlop = (from l in db.tbLops
                                      where l.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value)
                                      select l).FirstOrDefault();
                        //tbLop update = getlop.FirstOrDefault();
                        //Người VN có group.._id = 3
                        if (kiemtra.groupuser_id == 3)
                        {
                            if (getlop.username_idVn == null)
                            {
                                getlop.username_idVn = Convert.ToInt32(item[0]);
                                try
                                {
                                    db.SubmitChanges();
                                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                                }
                                catch
                                {
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã có giáo viên này trong lớp!','','error').then(function(){grvList.UnselectRows();})", true);
                            }
                        }
                        //Người NN có group.._id = 4
                        if (kiemtra.groupuser_id == 4)
                        {
                            if (getlop.username_idEn == null)
                            {
                                getlop.username_idEn = Convert.ToInt32(item[0]);
                                try
                                {
                                    db.SubmitChanges();
                                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                                }
                                catch
                                {
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã có giáo viên này trong lớp!','','error').then(function(){grvList.UnselectRows();})", true);
                            }
                        }
                    }
                    //Lưu lịch sử thao tác
                    tbOperationHistory ins = new tbOperationHistory();
                    ins.history_day = DateTime.Now;
                    ins.username_id = logedMember.username_id;
                    ins.history_content = "Thêm giáo viên "+item[1] + " vào lớp "+ ddllop.Text;
                    db.tbOperationHistories.InsertOnSubmit(ins);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Vui lòng chọn tối thiểu 1 giáo viên Nước ngoài và 1 giáo viên Việt Nam','','warning').then(function(){grvList.UnselectRows();})", true);
                loadData();
            }
        }
    }
}