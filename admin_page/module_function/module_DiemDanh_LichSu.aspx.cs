﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_LichSu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
            }
            var getLop = from tt in db.tbLops where tt.hidden == false && tt.lop_tinhtrang == null select tt;
            ddlLop.DataSource = getLop;
            ddlLop.DataBind();
            //loadData();

        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        tbDiemDanh update = (from dd in db.tbDiemDanhs
                             where dd.hstl_id == Convert.ToInt32(txtHocSinhTrongLop.Value)
                              && dd.diemdanh_ngay.Value.Date == DateTime.Now.Date
                                     && dd.diemdanh_ngay.Value.Month == DateTime.Now.Month
                                      && dd.diemdanh_ngay.Value.Year == DateTime.Now.Year
                             select dd).FirstOrDefault();
        update.diemdanh_vang = "Đúng giờ";
        update.diemdanh_ghichu = "";
        db.SubmitChanges();
        alert.alert_Update(Page, "Đã xác nhận lại, vui lòng kiểm tra form điểm danh", "");
        //loadData();
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
        try
        {
            if (ddlLop.Text != "")
            {

                var listParent = from ng in db.tbDiemDanhs
                                 join hstl in db.tbhocsinhtronglops on ng.hstl_id equals hstl.hstl_id
                                 join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                 join l in db.tbLops on hstl.lop_id equals l.lop_id
                                 where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                                 && ac.hidden == false && hstl.hstl_hidden == false
                                 && ng.diemdanh_ngay.Value.Month == Convert.ToInt32(ddlThang.SelectedValue)
                                 && ng.diemdanh_ngay.Value.Year == Convert.ToInt32(ddlNam.SelectedValue)
                                 group ng by ng.diemdanh_ngay.Value.Day into g
                                 select new
                                 {
                                     ngay = g.First().diemdanh_ngay.Value.Day
                                 };
                rpParent.DataSource = listParent;
                rpParent.DataBind();
                txtNgay.Value = string.Join(",", listParent.Select(x => x.ngay));
                var dsHocSinh = from hstl in db.tbhocsinhtronglops
                                join l in db.tbLops on hstl.lop_id equals l.lop_id
                                join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                                && hs.hidden == false && hstl.hstl_hidden == false
                                select new
                                {
                                    hstl.hstl_id,
                                    hs.account_vn,
                                };
                rpDanhSach.DataSource = dsHocSinh;
                rpDanhSach.DataBind();

            }
            else
            {
                alert.alert_Warning(Page, "Vui lòng chọn lớp để xem", "");
            }
        }
        catch (Exception ex)
        {
            //alert.alert_Error(Page, "Lớp hiện tại chưa có điểm danh", "");
        }
    }
    public class diemdanh
    {
        public string style { get; set; }
        public string tinhtrang { get; set; }
        public int ngay { get; set; }
    }
    protected void rpDanhSach_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpDanhSachChild = e.Item.FindControl("rpDanhSachChild") as Repeater;
        int hstl_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "hstl_id").ToString());

        string[] arr_ngay = txtNgay.Value.Split(',');
        var check_diemdanh_HocSinh = from sa in db.tbDiemDanhs
                                     where sa.hstl_id == hstl_id
                                     && sa.diemdanh_ngay.Value.Month == Convert.ToInt32(ddlThang.SelectedValue)
                                     && sa.diemdanh_ngay.Value.Year == Convert.ToInt32(ddlNam.SelectedValue)
                                     orderby sa.diemdanh_ngay.Value.Date ascending
                                     select new
                                     {
                                         style = sa.diemdanh_vang == "Vắng có phép" ? "vang" : sa.diemdanh_vang == "Vắng không phép" ? "vang" : sa.diemdanh_vang == "Bảo lưu" ? "baoluu" : "",
                                         tinhtrang = sa.diemdanh_vang == "Vắng có phép" ? "X" : sa.diemdanh_vang == "Vắng không phép" ? "X" : sa.diemdanh_vang == "Bảo lưu" ? "B" : "C",
                                         ngay = sa.diemdanh_ngay.Value.Day,
                                     };
        if (arr_ngay.Length == check_diemdanh_HocSinh.Count())
        {
            rpDanhSachChild.DataSource = check_diemdanh_HocSinh;
            rpDanhSachChild.DataBind();
        }

        else
        {
            List<diemdanh> diemdanh = new List<diemdanh>();
            int dem = 0;
            for (int i = 0; i < arr_ngay.Length; i++)
            {
                if (Convert.ToInt32(arr_ngay[i]) == check_diemdanh_HocSinh.Skip(dem).Take(1).First().ngay)
                {
                    diemdanh.Add(new diemdanh()
                    {
                        style= check_diemdanh_HocSinh.Skip(dem).Take(1).First().style,
                        tinhtrang = check_diemdanh_HocSinh.Skip(dem).Take(1).First().tinhtrang,
                        ngay = check_diemdanh_HocSinh.Skip(dem).Take(1).First().ngay,
                    });
                    dem++;
                }
                else
                {
                    diemdanh.Add(new diemdanh()
                    {
                        style="",
                        tinhtrang = "--",
                        ngay = Convert.ToInt32(arr_ngay[i]),
                    });
                }
            }
            //diemdanh.Add(new diemdanh()
            //{
            //    tinhtrang = "-",
            //});
            rpDanhSachChild.DataSource = diemdanh;
            rpDanhSachChild.DataBind();
        }

    }
}