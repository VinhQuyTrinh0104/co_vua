﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DangKyTuyenSinh.aspx.cs" Inherits="admin_page_module_function_module_DangKyTuyenSinh" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
       
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnDuyet" runat="server" Text="Duyệt" CssClass="btn btn-primary" OnClick="btnDuyet_Click" />
                        </div>
                      
                      
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="dangkytuyensinh_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Họ tên học sinh" FieldName="dangkytuyensinh_hotenhocsinh" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số điện thoại" FieldName="dangkytuyensinh_phone" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Email" FieldName="dangkytuyensinh_email" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Địa chỉ" FieldName="dangkytuyensinh_diachi" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Khóa học" FieldName="dangkytuyensinh_khoahoc" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ghi chú" FieldName="dangkytuyensinh_ghichu" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tinhf trạng" FieldName="dangkytuyensinh_tinhtrang" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>

                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

