﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LoaiThongBao : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getData = from n in db.tbLoaiThongBaos
                      join tb in db.admin_GroupUsers on n.groupuser_id equals tb.groupuser_id
                      select new
                      {
                          n.loaithongbao_id,
                          n.loaithongbao_title,
                          tb.groupuser_id,
                          tb.groupuser_name
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlloaisanpham.DataSource = from tb in db.admin_GroupUsers
                                    select tb;
        ddlloaisanpham.DataBind();

    }
    private void setNULL()
    {
        txttensanpham.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "loaithongbao_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbLoaiThongBaos
                       join tb in db.admin_GroupUsers on n.groupuser_id equals tb.groupuser_id
                       where n.loaithongbao_id == _id
                       select n).Single();
        txttensanpham.Text = getData.loaithongbao_title;
        ddlloaisanpham.Value = getData.groupuser_id;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_LoaiThongBao cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "loaithongbao_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_LoaiThongBao();

                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "" || ddlloaisanpham.ToString() != "")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {

        cls_LoaiThongBao cls = new cls_LoaiThongBao();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {


        }
        if (Session["_id"].ToString() == "0")
        {

            if (cls.Linq_Them(txttensanpham.Text, Convert.ToInt32(ddlloaisanpham.Value.ToString())))
            {
                alert.alert_Success(Page, "Thêm thành công", "");
                loadData();
            }
            else alert.alert_Error(Page, "Thêm thất bại", "");

        }
        else
        {
            if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text, Convert.ToInt32(ddlloaisanpham.Value.ToString())))
            {
                alert.alert_Success(Page, "Cập nhật thành công", "");
                loadData();
            }
            else alert.alert_Error(Page, "Cập nhật thất bại", "");
        }
        popupControl.ShowOnPageLoad = false;
    }


    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }
    //protected void btnTop_Click(object sender, EventArgs e)
    //{
    //    var updateToanBo = from n in db.tbNews
    //                       where n.news_top == true
    //                       select n;
    //    if (updateToanBo.Count() > 0)
    //    {
    //        foreach (var item in updateToanBo)
    //        {
    //            tbNew up = (from u in db.tbNews where u.news_id == item.news_id select u).SingleOrDefault();
    //            up.news_top = false;
    //            db.SubmitChanges();
    //        }
    //    }
    //    List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
    //    if (selectedKey.Count > 0)
    //    {
    //        foreach (var item in selectedKey)
    //        {
    //            tbNew check = (from c in db.tbNews where c.news_id == Convert.ToInt32(item) select c).SingleOrDefault();
    //            if (check != null)
    //            {
    //                check.news_top = true;
    //                db.SubmitChanges();
    //                alert.alert_Success(Page, "Đã lên top", "");
    //            }

    //            else
    //                alert.alert_Error(Page, "Thất bại", "");
    //        }
    //    }
    //    else
    //        alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    //}

}