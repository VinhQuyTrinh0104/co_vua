﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DiemDanh_LichSu.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_LichSu" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>
<%@ Register Src="~/web_usercontrol/MenuGiaoVien.ascx" TagPrefix="uc1" TagName="MenuGiaoVien" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        function myDiemDanh(id) {
            document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value = id;
            document.getElementById("<%=btnSave.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <%--<div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
     <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <style>
        td.vang {
            color: red;
            font-weight: bold
        }

        td.baoluu {
            color: blue;
            font-weight: bold
        }
    </style>
    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN LỊCH SỬ ĐIỂM DANH
        </div>
        <%-- <asp:UpdatePanel ID="upDiemDanh" runat="server">
            <ContentTemplate>--%>
        <div class="col-12 form-group">
            <div class="col-2 form-group">
                <label>Cơ sở:</label>
                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
            </div>

            <div class="col-2 form-group">
                <label>Lớp:</label>
                <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
            </div>
           
            <div class="col-2 form-group">
                <label>Tháng:</label>
                <asp:DropDownList runat="server" ID="ddlThang" CssClass="form-control"  Width="90%">
                    <asp:ListItem Value="1" Text="Tháng 1" />
                    <asp:ListItem Value="2" Text="Tháng 2" />
                    <asp:ListItem Value="3" Text="Tháng 3" />
                    <asp:ListItem Value="4" Text="Tháng 4" />
                    <asp:ListItem Value="5" Text="Tháng 5" />
                    <asp:ListItem Value="6" Text="Tháng 6" />
                    <asp:ListItem Value="7" Text="Tháng 7" />
                    <asp:ListItem Value="8" Text="Tháng 8" />
                    <asp:ListItem Value="9" Text="Tháng 9" />
                    <asp:ListItem Value="10" Text="Tháng 10" />
                    <asp:ListItem Value="11" Text="Tháng 11" />
                    <asp:ListItem Value="12" Text="Tháng 12" />
                </asp:DropDownList>
            </div>
              
            <div class="col-1 form-group">
                <label>Năm:</label>
                <asp:DropDownList runat="server" ID="ddlNam" CssClass="form-control"  Width="90%">
                    <asp:ListItem Value="2023" Text="2023" />
                    <asp:ListItem Value="2022" Text="2022" />
                </asp:DropDownList>
            </div>
            
            <div class="col-2 form-group">
                <label>  &nbsp; </label>
                <div>
                    <a href="javascript:void(0)" class="btn btn-primary" id="btnXem" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
        </div>
        <div>
            <b>Lưu ý: "C": là có đi học, "X": là vắng học, "B": là học sinh bảo lưu, "--" Học sinh ko có điểm danh trong ngày đó</b>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="text-align: center">STT</th>
                    <th>Họ tên</th>
                    <asp:Repeater ID="rpParent" runat="server">
                        <ItemTemplate>
                            <th style="text-align: center"><%# Eval("ngay") %></th>
                        </ItemTemplate>
                    </asp:Repeater>
                    <input style="display: none" type="text" id="txtNgay" runat="server" />
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpDanhSach" runat="server" OnItemDataBound="rpDanhSach_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <th style="text-align: center"><%#Container.ItemIndex+1 %></th>
                            <td><%#Eval("account_vn") %></td>
                            <asp:Repeater ID="rpDanhSachChild" runat="server">
                                <ItemTemplate>
                                    <td style="text-align: center" class="<%#Eval("style") %>"><%#Eval("tinhtrang") %></td>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <div style="display: none">
            <input type="text" id="txtHocSinhTrongLop" runat="server" />
            <a id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
        </div>
        <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



