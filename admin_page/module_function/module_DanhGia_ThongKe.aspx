﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhGia_ThongKe.aspx.cs" Inherits="admin_page_module_function_module_DanhGia_ThongKe" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>
<%@ Register Src="~/web_usercontrol/MenuGiaoVien.ascx" TagPrefix="uc1" TagName="MenuGiaoVien" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <%-- <div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
    <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block box-admin" >
        <div class="col-12 form-group-name">
            QUẢN LÝ THỐNG KÊ ĐÁNH GIÁ CỦA GIÁO VIÊN
            </div>
        <asp:UpdatePanel ID="upDanhGia" runat="server">
            <ContentTemplate>
                <div class="col-12 form-group">
                    <label class="col-2">Cơ sở:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2">Lớp:</label>
                    <div class="col-4">
                        <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                    </div>
                </div>
                <div class="col-12 form-group" style="text-align: center">
                    <b>THỐNG KÊ ĐÁNH GIÁ</b>
                </div>
                <div class="col-12 form-group" style="overflow-x: auto">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Họ tên</th>
                                <th scope="col">English name</th>
                                <th scope="col">Giáo viên</th>
                                <th scope="col" colspan="7">ATTITUDE</th>
                                <th scope="col" colspan="3">HOMEWORK</th>
                                <th scope="col" colspan="5">SKILLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row"></th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Confident to communicate with teachers</td>
                                <td>"Focus & follow T's Guide"</td>
                                <td>"Good Concentration"</td>
                                <td>"Active in Activties"</td>
                                <td>"Less active in activities"</td>
                                <td>"Easy to get distracted & talkative in class "</td>
                                <td>"Need to be more active"</td>
                                <td>Finished</td>
                                <td>Unfinished</td>
                                <td>Missing HW</td>
                                <td>"Good at Listening"</td>
                                <td>"Good at Speaking"</td>
                                <td>"Good Pronunciation Inotnation"</td>
                                <td>"Good Grammar"</td>
                                <td>"Need to be more Practiced"</td>
                            </tr>
                            <asp:Repeater ID="rpList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%=STT++ %></td>
                                        <td><%#Eval("account_vn") %></td>
                                        <td><%#Eval("account_en") %></td>
                                        <td><%#Eval("username_fullname") %></td>
                                        <td><%#Eval("attitude_confident") %></td>
                                        <td><%#Eval("attitude_focus") %></td>
                                        <td><%#Eval("attitude_good") %></td>
                                        <td><%#Eval("attitude_active") %></td>
                                        <td><%#Eval("attitude_less") %></td>
                                        <td><%#Eval("attitude_easy") %></td>
                                        <td><%#Eval("attitude_need") %></td>
                                        <td><%#Eval("homework_finished") %></td>
                                        <td><%#Eval("homework_unfinished") %></td>
                                        <td><%#Eval("homework_missing") %></td>
                                        <td><%#Eval("skill_good_listening") %></td>
                                        <td><%#Eval("skill_good_speaking") %></td>
                                        <td><%#Eval("skill_good_pron") %></td>
                                        <td><%#Eval("skill_good_gram") %></td>
                                        <td><%#Eval("skill_need") %></td>
                                        <td><%#Eval("danhgia_remark") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

