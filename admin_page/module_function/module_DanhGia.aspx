﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhGia.aspx.cs" Inherits="admin_page_module_function_module_DanhGia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .dadanhgia {
            background-color: red !important;
            border-color: red !important;
        }

        .btn-active {
            background-color: #343af5 !important;
            border-color: #343af5 !important;
        }
    </style>
    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN ĐÁNH GIÁ
        </div>
        <div class="card card-block">
            <asp:UpdatePanel ID="upDanhGia" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-3 form-group">
                            <label class="col-3">Month:</label>
                            <div class="col-8">
                                <asp:DropDownList ID="ddlThang" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="1">Month 1</asp:ListItem>
                                    <asp:ListItem Value="2">Month 2</asp:ListItem>
                                    <asp:ListItem Value="3">Month 3</asp:ListItem>
                                    <asp:ListItem Value="4">Month 4</asp:ListItem>
                                    <asp:ListItem Value="5">Month 5</asp:ListItem>
                                    <asp:ListItem Value="6">Month 6</asp:ListItem>
                                    <asp:ListItem Value="7">Month 7</asp:ListItem>
                                    <asp:ListItem Value="8">Month 8</asp:ListItem>
                                    <asp:ListItem Value="9">Month 9</asp:ListItem>
                                    <asp:ListItem Value="10">Month 10</asp:ListItem>
                                    <asp:ListItem Value="11">Month 11</asp:ListItem>
                                    <asp:ListItem Value="12">Month 12</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-3 form-group">
                            <label class="col-2">Cơ sở:</label>
                            <div class="col-10">
                                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                            </div>
                        </div>
                        <div class="col-3 form-group">
                            <label class="col-2">Lớp:</label>
                            <div class="col-10">
                                <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                            </div>
                        </div>
                        <div class="col-3 form-group">
                            <a href="#" class="btn btn-success" id="btnXemHocSinh" runat="server" onserverclick="btnXemHocSinh_ServerClick">Xem</a>
                        </div>
                    </div>
                    <div class="col-12 form-group">

                        <label class="col-2 form-control-label">Học sinh:</label>
                        <%--  <div class="col-4">
                            <dx:ASPxComboBox ID="ddlHocSinh" runat="server" ValueType="System.Int32" TextField="account_vn" ValueField="account_id" ClientInstanceName="ddlHocSinh" CssClass="" NullText="Chọn học sinh" Width="95%"></dx:ASPxComboBox>
                        </div>--%>
                        <asp:Repeater runat="server" ID="rpHocSinh">
                            <ItemTemplate>
                                <a href="javascript:void(0)" class="btn btn-success <%#Eval("mystyle") %>" id="btn_<%#Eval("account_id") %>" onclick="chonHocSinh(<%#Eval("account_id") %>)"><%#Eval("account_vn") %></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>

                    <div style="display: none">
                        <input type="text" id="txtHocSinh" runat="server" />
                        <a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                    </div>
                    <div id="div_DanhGia" runat="server">
                        <table class="table table-bordered">
                            <tr>
                                <th>Understanding</th>
                                <td>
                                    <input type="radio" id="rd1_1" runat="server" name="understanding" value="Excellent" />
                                    <label for="hibodywrapper_rd1_1">Excellent</label>
                                    <input type="radio" id="rd1_2" runat="server" name="understanding" value="Good" />
                                    <label for="hibodywrapper_rd1_2">Good</label>
                                    <input type="radio" id="rd1_3" runat="server" name="understanding" value="Quite Good" />
                                    <label for="hibodywrapper_rd1_3">Quite Good</label>
                                    <input type="radio" id="rd1_4" runat="server" name="understanding" value="Not Good" />
                                    <label for="hibodywrapper_rd1_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_1" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Vocabulary ability</th>
                                <td>
                                    <input type="radio" id="rd2_1" runat="server" name="vocabulary_ability" value="Excellent" />
                                    <label for="hibodywrapper_rd2_1">Excellent</label>
                                    <input type="radio" id="rd2_2" runat="server" name="vocabulary_ability" value="Good" />
                                    <label for="hibodywrapper_rd2_2">Good</label>
                                    <input type="radio" id="rd2_3" runat="server" name="vocabulary_ability" value="Quite Good" />
                                    <label for="hibodywrapper_rd2_3">Quite Good</label>
                                    <input type="radio" id="rd2_4" runat="server" name="vocabulary_ability" value="Not Good" />
                                    <label for="hibodywrapper_rd2_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_2" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Grammar</th>
                                <td>
                                    <input type="radio" id="rd3_1" runat="server" name="grammar" value="Excellent" />
                                    <label for="hibodywrapper_rd3_1">Excellent</label>
                                    <input type="radio" id="rd3_2" runat="server" name="grammar" value="Good" />
                                    <label for="hibodywrapper_rd3_2">Good</label>
                                    <input type="radio" id="rd3_3" runat="server" name="grammar" value="Quite Good" />
                                    <label for="hibodywrapper_rd3_3">Quite Good</label>
                                    <input type="radio" id="rd3_4" runat="server" name="grammar" value="Not Good" />
                                    <label for="hibodywrapper_rd3_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_3" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Interaction</th>
                                <td>
                                    <input type="radio" id="rd4_1" runat="server" name="interaction" value="Excellent" />
                                    <label for="hibodywrapper_rd4_1">Excellent</label>
                                    <input type="radio" id="rd4_2" runat="server" name="interaction" value="Good" />
                                    <label for="hibodywrapper_rd4_2">Good</label>
                                    <input type="radio" id="rd4_3" runat="server" name="interaction" value="Quite Good" />
                                    <label for="hibodywrapper_rd4_3">Quite Good</label>
                                    <input type="radio" id="rd4_4" runat="server" name="interaction" value="Not Good" />
                                    <label for="hibodywrapper_rd4_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_4" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Concentration</th>
                                <td>
                                    <input type="radio" id="rd5_1" runat="server" name="concentration" value="Excellent" />
                                    <label for="hibodywrapper_rd5_1">Excellent</label>
                                    <input type="radio" id="rd5_2" runat="server" name="concentration" value="Good" />
                                    <label for="hibodywrapper_rd5_2">Good</label>
                                    <input type="radio" id="rd5_3" runat="server" name="concentration" value="Quite Good" />
                                    <label for="hibodywrapper_rd5_3">Quite Good</label>
                                    <input type="radio" id="rd5_4" runat="server" name="concentration" value="Not Good" />
                                    <label for="hibodywrapper_rd5_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_5" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Homework</th>
                                <td>
                                    <input type="radio" id="rd6_1" runat="server" name="homework" value="Excellent" />
                                    <label for="hibodywrapper_rd6_1">Excellent</label>
                                    <input type="radio" id="rd6_2" runat="server" name="homework" value="Good" />
                                    <label for="hibodywrapper_rd6_2">Good</label>
                                    <input type="radio" id="rd6_3" runat="server" name="homework" value="Quite Good" />
                                    <label for="hibodywrapper_rd6_3">Quite Good</label>
                                    <input type="radio" id="rd6_4" runat="server" name="homework" value="Not Good" />
                                    <label for="hibodywrapper_rd6_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_6" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Rules in class</th>
                                <td>
                                    <input type="radio" id="rd7_1" runat="server" name="rules_in_class" value="Excellent" />
                                    <label for="hibodywrapper_rd7_1">Excellent</label>
                                    <input type="radio" id="rd7_2" runat="server" name="rules_in_class" value="Good" />
                                    <label for="hibodywrapper_rd7_2">Good</label>
                                    <input type="radio" id="rd7_3" runat="server" name="rules_in_class" value="Quite Good" />
                                    <label for="hibodywrapper_rd7_3">Quite Good</label>
                                    <input type="radio" id="rd7_4" runat="server" name="rules_in_class" value="Not Good" />
                                    <label for="hibodywrapper_rd7_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_7" runat="server" style="display: none" />
                                </td>
                            </tr>
                            <tr>
                                <th>Cooperation</th>
                                <td>
                                    <input type="radio" id="rd8_1" runat="server" name="cooperation" value="Excellent" />
                                    <label for="hibodywrapper_rd8_1">Excellent</label>
                                    <input type="radio" id="rd8_2" runat="server" name="cooperation" value="Good" />
                                    <label for="hibodywrapper_rd8_2">Good</label>
                                    <input type="radio" id="rd8_3" runat="server" name="cooperation" value="Quite Good" />
                                    <label for="hibodywrapper_rd8_3">Quite Good</label>
                                    <input type="radio" id="rd8_4" runat="server" name="cooperation" value="Not Good" />
                                    <label for="hibodywrapper_rd8_4">Not Good</label>
                                    <input type="text" id="txtdanhgia_8" runat="server" style="display: none" />
                                </td>
                            </tr>
                        </table>
                        <div class="col-sm-12">
                            <b>Ghi chú:</b>
                        </div>
                        <div class="col-sm-12">
                            <textarea id="txtGhiChu" runat="server" rows="4" style="width: 100%"></textarea>
                        </div>
                        <a href="#" id="btnLuu" runat="server" class="btn btn-primary" onserverclick="btnLuu_ServerClick">Save</a>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script>
        function chonHocSinh(id) {
            document.getElementById("<%=txtHocSinh.ClientID%>").value = id;
            document.getElementById("<%=btnXem.ClientID%>").click();
        }
        function active(id) {
            document.getElementById("btn_" + id).classList.add("btn-active");
        }
        function setChecked() {
            let dg1 = document.getElementById("<%=txtdanhgia_1.ClientID%>").value;
            let dg2 = document.getElementById("<%=txtdanhgia_2.ClientID%>").value;
            let dg3 = document.getElementById("<%=txtdanhgia_3.ClientID%>").value;
            let dg4 = document.getElementById("<%=txtdanhgia_4.ClientID%>").value;
            let dg5 = document.getElementById("<%=txtdanhgia_5.ClientID%>").value;
            let dg6 = document.getElementById("<%=txtdanhgia_6.ClientID%>").value;
            let dg7 = document.getElementById("<%=txtdanhgia_7.ClientID%>").value;
            let dg8 = document.getElementById("<%=txtdanhgia_8.ClientID%>").value;
            $('input[name="ctl00$hibodywrapper$understanding"][value="' + dg1 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$vocabulary_ability"][value="' + dg2 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$grammar"][value="' + dg3 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$interaction"][value="' + dg4 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$concentration"][value="' + dg5 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$homework"][value="' + dg6 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$rules_in_class"][value="' + dg7 + '"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$cooperation"][value="' + dg8 + '"]').attr('checked', true);
        }
        function setFormChecked() {
            $('input[name="ctl00$hibodywrapper$understanding"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$vocabulary_ability"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$grammar"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$interaction"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$concentration"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$homework"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$rules_in_class"][value="Excellent"]').attr('checked', true);
            $('input[name="ctl00$hibodywrapper$cooperation"][value="Excellent"]').attr('checked', true);
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

