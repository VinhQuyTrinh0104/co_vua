﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos select cs;
                ddlCoSo.DataBind();
            }
            var getLop = from tt in db.tbLops select tt;
            ddlLop.DataSource = getLop;
            ddlLop.DataBind();
            //loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getDiemDanh = from hs in db.tbAccounts
                          join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                          join dd in db.tbDiemDanhs on hstl.hstl_id equals dd.hstl_id
                          where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                          && hstl.hstl_hidden == false
                              //&& dd.diemdanh_ngay.Value.Date == DateTime.Now.Date
                              //         && dd.diemdanh_ngay.Value.Month == DateTime.Now.Month
                              //          && dd.diemdanh_ngay.Value.Year == DateTime.Now.Year
                              && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                              && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                               && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                      && dd.diemdanh_vang == "Đúng giờ"
                          select new
                          {
                              hs.account_vn,
                              hstl.hstl_id
                          };
        rpList.DataSource = getDiemDanh;
        rpList.DataBind();

    }
    private void setNULL()
    {

    }




    protected void btnSave_ServerClick(object sender, EventArgs e)
    {

        tbDiemDanh update = (from dd in db.tbDiemDanhs
                             where dd.hstl_id == Convert.ToInt32(txtHocSinhTrongLop.Value)
                             && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                              && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                               && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                             //&& dd.diemdanh_ngay.Value.Date == DateTime.Now.Date
                             //       && dd.diemdanh_ngay.Value.Month == DateTime.Now.Month
                             //        && dd.diemdanh_ngay.Value.Year == DateTime.Now.Year
                             select dd).FirstOrDefault();
        update.diemdanh_vang = txtDiemDanhVangHidden.Value;
        update.diemdanh_ghichu = txtGhiChuHidden.Value;
        db.SubmitChanges();
        alert.alert_Update(Page, "Đã xác nhận", "");
        loadData();
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value) select l;
        ddlLop.DataBind();
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.Text != "")
        {
            var getData = (from l in db.tbLops
                           where l.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value)
                           select l).SingleOrDefault();
            if (getData.lop_thu2 == true)
                ckThu2.Checked = true;
            else
                ckThu2.Checked = false;
            if (getData.lop_thu3 == true)
                ckThu3.Checked = true;
            else
                ckThu3.Checked = false;
            if (getData.lop_thu4 == true)
                ckThu4.Checked = true;
            else
                ckThu4.Checked = false;
            if (getData.lop_thu5 == true)
                ckThu5.Checked = true;
            else
                ckThu5.Checked = false;
            if (getData.lop_thu6 == true)
                ckThu6.Checked = true;
            else
                ckThu6.Checked = false;
            if (getData.lop_thu7 == true)
                ckThu7.Checked = true;
            else
                ckThu7.Checked = false;
            if (getData.lop_chunhat == true)
                ckChuNhat.Checked = true;
            else
                ckChuNhat.Checked = false;
            lblGioHoc.Text = getData.giohoc_batdau + "-" + getData.giohoc_ketthuc;
            var getDiemDanh = from hs in db.tbAccounts
                              join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                              where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                              select new
                              {
                                  hs.account_vn,
                                  hstl.hstl_id
                              };
            rpList.DataSource = getDiemDanh;
            rpList.DataBind();
            var checkNgayDiemDanh = from dd in db.tbDiemDanhs
                                    join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                                    join l in db.tbLops on hstl.lop_id equals l.lop_id
                                    where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                                       && hstl.hstl_hidden == false
                                         && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                              && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                               && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                    //&& dd.diemdanh_ngay.Value.Date == DateTime.Now.Date
                                    //&& dd.diemdanh_ngay.Value.Month == DateTime.Now.Month
                                    // && dd.diemdanh_ngay.Value.Year == DateTime.Now.Year
                                    select dd;
            if (checkNgayDiemDanh.Count() != 0)
            {
                loadData();
            }
            else
            {
                foreach (var item in getDiemDanh)
                {
                    tbDiemDanh insert = new tbDiemDanh();
                    insert.diemdanh_ngay = Convert.ToDateTime(dteNgay.Value);
                    insert.diemdanh_vang = "Đúng giờ";
                    insert.diemdanh_ghichu = "";
                    insert.hstl_id = item.hstl_id;
                    db.tbDiemDanhs.InsertOnSubmit(insert);
                    db.SubmitChanges();
                }
            }
        }
    }
}