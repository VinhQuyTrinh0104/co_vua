﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThongBao : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        if (Request.Cookies["UserName"] != null)
        {

                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getData = from n in db.tbThongBaos
                      join tb in db.tbLoaiThongBaos on n.loaithongbao_id equals tb.loaithongbao_id
                      select new
                      {
                          n.thongbao_id,
                          n.thongbao_title,
                          n.thongbao_content,
                          n.thongbao_createdate,
                          tb.loaithongbao_id,
                          tb.loaithongbao_title
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlloaisanpham.DataSource = from tb in db.tbLoaiThongBaos
                                    select tb;
        ddlloaisanpham.DataBind();
    }
    private void setNULL()
    {
        txttensanpham.Text = "";
        edtnoidung.Html = "";
        ddlloaisanpham.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thongbao_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
         var getData = (from n in db.tbThongBaos
                         join tb in db.tbLoaiThongBaos on n.loaithongbao_id equals tb.loaithongbao_id
                         where n.thongbao_id == _id
                         select n).Single();
        txttensanpham.Text = getData.thongbao_title;
        edtnoidung.Html = getData.thongbao_content;
        ddlloaisanpham.Value = getData.loaithongbao_id;
        dteDate.Value = getData.thongbao_createdate.ToString()  ;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_ThongBao cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongbao_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_ThongBao();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "" || edtnoidung.Html !="")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_ThongBao cls = new cls_ThongBao();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                if (cls.Linq_Them(txttensanpham.Text, Convert.ToInt32(ddlloaisanpham.Value.ToString()), Convert.ToDateTime(dteDate.Value), edtnoidung.Html))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text , Convert.ToInt32(ddlloaisanpham.Value.ToString()), Convert.ToDateTime(dteDate.Value), edtnoidung.Html))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
    }
}