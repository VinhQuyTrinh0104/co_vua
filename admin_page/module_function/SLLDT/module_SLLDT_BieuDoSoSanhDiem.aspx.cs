﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class admin_page_module_function_SLLDT_module_SLLDT_BieuDoSoSanhDiem : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();

    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            System.Net.WebClient wc = new System.Net.WebClient();
            wc.Encoding = Encoding.UTF8;
            string str = wc.DownloadString("http://api.cncstone.vn/api/Controller_API010_TruongTrinhHoc_/getDataChuongTrinhHoc");

            jsonitem.Value = str;

            var coso = from cs in db.tbCosos
                       select cs;
            ddlCoSo.DataSource = coso;
            ddlCoSo.DataTextField = "coso_name";
            ddlCoSo.DataValueField = "coso_id";
            ddlCoSo.DataBind();
            ddlCoSo.Items.Insert(0, "--Vui lòng chọn cơ sở--");
            ddlCoSo.Items[0].Selected = true;
            blLop.Visible = false;
        }
    }

    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selected = Int32.Parse(ddlCoSo.SelectedValue.ToString());
        var lop = from lp in db.tbLops
                  where lp.coso_id == selected && lp.lop_tinhtrang == null
                  select new
                  {
                      lp.lop_name,
                      lp.lop_id
                  };
        rpLop.DataSource = lop;
        rpLop.DataBind();
        blLop.Visible = true;
    }
}