﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_SLLDT_NoiDungBaiHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (checkButton.groupuser_id == 6)
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                //btnDlt.Visible = false;
            }
            else
            {
                btnThem.Visible = true;
                btnChiTiet.Visible = true;
                //btnDlt.Visible = true;
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        // load data đổ vào var danh sách
        var getCoSo = from tt in db.tbCosos where tt.coso_hidden == false select tt;
        ddlCoSo.DataSource = getCoSo;
        ddlCoSo.DataBind();
        ddlLop.DataSource = from l in db.tbLops
                            where l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
        var getData = from nd in db.tbSLLDT_NoiDungBaiHocs
                      join l in db.tbLops on nd.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      join u in db.admin_Users on nd.username_id equals u.username_id
                      where u.username_id == logedMember.username_id
                      orderby nd.noidungbaihoc_tinhtrang
                      select new
                      {
                          nd.noidungbaihoc_id,
                          cs.coso_name,
                          l.lop_name,
                          nd.noidungbaihoc_tinhtrang,
                          u.username_fullname
                      };

        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlLop.DataSource = from l in db.tbLops
        //                    where l.hidden == false && l.lop_tinhtrang == null
        //                    select l;
        //ddlLop.DataBind();
    }


    private void setNULL()
    {
        ddlLop.Text = "";
        ddlCoSo.Text = "";
        edtnoidung.Html = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "noidungbaihoc_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nd in db.tbSLLDT_NoiDungBaiHocs
                       join l in db.tbLops on nd.lop_id equals l.lop_id
                       join cs in db.tbCosos on l.coso_id equals cs.coso_id
                       join u in db.admin_Users on nd.username_id equals u.username_id
                       where nd.noidungbaihoc_id == _id
                       select new
                       {
                           nd.noidungbaihoc_id,
                           cs.coso_id,
                           cs.coso_name,
                           l.lop_name,
                           l.lop_id,
                           nd.noidungbaihoc_content,
                       }).Single();
        ddlCoSo.Value = getData.coso_id;
        ddlLop.Value = getData.lop_id;
        edtnoidung.Html = getData.noidungbaihoc_content;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_SLLDT_NoiDungBaiHoc cls = new cls_SLLDT_NoiDungBaiHoc(); ;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "noidungbaihoc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                var check = (from nd in db.tbSLLDT_NoiDungBaiHocs
                             where nd.noidungbaihoc_id == Convert.ToInt32(item)
                             select nd).Single();
                if (check.noidungbaihoc_tinhtrang == "Đã duyệt")
                {
                    alert.alert_Error(Page, "Không thể xóa dữ liệu đã duyệt", "");
                }
                else
                {
                    if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    {
                        alert.alert_Success(Page, "Xóa thành công", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xóa thất bại", "");
                    }
                }
            }
            loadData();
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (ddlCoSo.Text != "" && ddlLop.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        cls_SLLDT_NoiDungBaiHoc cls = new cls_SLLDT_NoiDungBaiHoc();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                //kiểm tra tên lớp đã có chưa
                if (cls.Linq_Them(Convert.ToInt16(ddlLop.SelectedItem.Value), edtnoidung.Html, logedMember.username_id))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                    popupControl.ShowOnPageLoad = false;
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                _id = Convert.ToInt16(Session["_id"].ToString());
                if (cls.Linq_Sua(_id, Convert.ToInt16(ddlLop.SelectedItem.Value), edtnoidung.Html, logedMember.username_id))
                {

                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                    popupControl.ShowOnPageLoad = false;
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
        //popupControl.ShowOnPageLoad = false;
    }

    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }
}