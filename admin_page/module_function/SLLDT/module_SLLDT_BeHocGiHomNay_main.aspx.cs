﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_SLLDT_module_SLLDT_BeHocGiHomNay_main : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert;
    private int userID;
    public int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {

            userID = (from ad in db.admin_Users
                      where ad.username_username == Request.Cookies["UserName"].Value
                      select ad.username_id).FirstOrDefault();
            var getUserClass = from nx in db.tbSLLDT_BeHocGiHomNays
                                   //join hstl in db.tbhocsinhtronglops on nx.hstl_id equals hstl.hstl_id
                                   //join l in db.tbLops on hstl.lop_id equals l.lop_id
                                   //join cs in db.tbCosos on l.coso_id equals cs.coso_id
                                   //where nx.username_id == userID
                               group nx by new { nx.nhanxet_group } into gr
                               orderby gr.Key.nhanxet_group descending
                               select new
                               {
                                   nhanxet_group = Convert.ToInt32(gr.Key.nhanxet_group),
                                   coso_name = (from cs in db.tbCosos
                                                join l in db.tbLops on cs.coso_id equals l.coso_id
                                                join bh in db.tbSLLDT_BeHocGiHomNays on l.lop_id equals bh.lop_id
                                                where bh.nhanxet_group == Convert.ToInt32(gr.Key.nhanxet_group)
                                                select cs).FirstOrDefault().coso_name,
                                   lop_name = (from l in db.tbLops
                                               join bh in db.tbSLLDT_BeHocGiHomNays on l.lop_id equals bh.lop_id
                                               where bh.nhanxet_group == Convert.ToInt32(gr.Key.nhanxet_group)
                                               select l).FirstOrDefault().lop_name,
                                   username_fullname = (from u in db.admin_Users
                                                        join nd in db.tbSLLDT_BeHocGiHomNays on u.username_id equals nd.username_id
                                                        where nd.nhanxet_group == Convert.ToInt32(gr.Key.nhanxet_group)
                                                        select u).FirstOrDefault().username_fullname,
                                   ngay = gr.First().behocgihomnay_createdate,
                                   behocgihomnay_tinhtrang = gr.First().behocgihomnay_tinhtrang
                               };


            grvList.DataSource = getUserClass;
            grvList.DataBind();

        }
        else
        {
            Response.Redirect("/admin-login");
        }

    }



    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "nhanxet_group" }));
        Session["id"] = id;
        Response.Redirect("/admin-be-hoc-gi-hom-nay-" + id);
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {

    }

}