﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_SLLDT_BeHocGiHomNay.aspx.cs" Inherits="admin_page_module_function_SLLDT_module_SLLDT_BeHocGiHomNay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .table th, .table td {
            padding: 0.75rem;
            vertical-align: middle;
            /* border-top: 1px solid #eceeef; */
            text-align: center;
        }

        .container {
            box-shadow: 0px 6px 12px 7px #8080802e;
            border-radius: 10px;
        }

        .textArea_content {
            padding: 7px;
            display: block;
            width: 100%;
            height: 40px;
            /* padding: 10px; */
            border: 1px solid gray;
            background-color: #fff;
            color: #333;
            font-size: 13px;
            width: 95%;
            background-color: #fafafa;
            border: 1px Solid #ffffff;
            border-collapse: separate;
            border-spacing: 0px;
            /* -webkit-box-shadow: inset 0px 2px 3px 0px rgb(0 0 0 / 5%); */
            /* box-shadow: inset 0px 2px 3px 0px rgb(0 0 0 / 5%); */
            font: 14px 'Segoe UI','Helvetica Neue','Droid Sans',Arial,Tahoma,Geneva,Sans-serif;
            box-shadow: 3px 3px 4px 0px #8080802e;
        }

        .table th, .table td {
            padding: 0.75rem;
            vertical-align: middle;
            /* border-top: 1px solid #eceeef; */
        }

        .td__name {
            width: 25%;
        }
        /*Thuộc tính css div bảng*/
        .block-main {
            box-shadow: 0px 6px 12px 7px #8080802e;
            border-radius: 10px;
            background-color: #ffffff;
        }

        tr {
            background-color: transparent !important;
        }

        .btn {
            margin: 0.5rem
        }

        .textArea_content:focus {
            border-color: #007bff;
            outline: none;
        }
    </style>

    <div class="card card-block box-admin">
        <div class="form-group row">

            <div class="col-3 form-group">
                <label class="col-3">Cơ sở:</label>
                <div class="col-9">
                    <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-3 form-group">
                <label class="col-2">Lớp:</label>
                <div class="col-10">
                    <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                </div>
            </div>
            <div class="col-3 form-group">
                <label class="col-2">Ngày:</label>
                <div class="col-10">
                    <input type="date" runat="server" id="dteNgay" class="form-control" max="<?php echo date('Y-m-d'); ?>" />
                </div>
            </div>
            <div class="col-3 form-group">

                <div class="col-10">
                    <a id="btnQuayLai" href="admin-be-hoc-gi-hom-nay" class="btn btn-primary" style="float: right">Quay lại</a>
                </div>
            </div>
        </div>
        <div class="form-group table-responsive">
        </div>
    </div>
    <div class="block-main">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">Nội dung nhận xét hàng tuần</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%=STT++ %></th>
                            <td class="td__name"><%#Eval("account_vn") %></td>
                            <td>
                                <textarea class="textArea_content" id="txtGhiChu_<%#Eval("hstl_id") %>" value="<%#Eval("noiDungNhanXet")%>" rows="4" cols="50" /><%#Eval("noiDungNhanXet")%></textarea>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <a href="javascript:void(0)" class="btn btn-primary" id="btnLuu" runat="server" onclick="return checkNullForComment()" >Lưu</a>
        <div style="display: none">
            <%--    <a href="javascript:void(0)" id="btnLuuNhanXet" runat="server" class="btn btn-primary" onserverclick="btnLuuNhanXet_ServerClick"></a>--%>
            <textarea id="txtCommentHiden" runat="server"> </textarea>
            <input type="text" id="txtLop" runat="server" />
            <input type="text" id="txtHocSinhTrongLop" runat="server" />
            <input type="text" id="txtDanhSachHocSinhID" runat="server" />
            <%--<a id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>--%>
            <a href="javascript:void(0)" id="btnLuuNhanXet" runat="server" class="btn btn-primary" onserverclick="btnSave_ServerClick"></a>
        </div>
        <div style="display: none">
            <input id="txtGhiChu" runat="server" />
        </div>
    </div>
    <script>
        //chặn ngày tương lai

        var dateInput = document.getElementById("<%=dteNgay.ClientID%>");
        var today = new Date().toISOString().split("T")[0];
        dateInput.setAttribute("max", today);
        function checkNullForComment() {
            //debugger;
            if (document.getElementById("<%=dteNgay.ClientID%>").value == "") {
                swal("Vui lòng chọn ngày nhập!");
                return false
            }
            let str_danhsachID = document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value.split(",");
            var ghichu = [];
            for (let i = 0; i < str_danhsachID.length; i++) {
                var _ghichu = document.getElementById("txtGhiChu_" + str_danhsachID[i]).value;
                ghichu.push(_ghichu);
            }
            document.getElementById("<%=txtGhiChu.ClientID%>").value = ghichu.join("|");
            if (document.getElementById("<%=txtGhiChu.ClientID%>").value.replace(/\|/g, "") == "") {
                swal("Chưa có mục nhận xét nào được nhập!");
                return false
            }

            document.getElementById("<%=btnLuuNhanXet.ClientID%>").click();
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

