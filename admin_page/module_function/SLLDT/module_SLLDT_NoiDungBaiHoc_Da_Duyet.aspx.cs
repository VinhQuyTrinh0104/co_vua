﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_SLLDT_NoiDungBaiHoc_Da_Duyet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from nd in db.tbSLLDT_NoiDungBaiHocs
                      join l in db.tbLops on nd.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      join u in db.admin_Users on nd.username_id equals u.username_id
                      where nd.noidungbaihoc_tinhtrang == "Đã duyệt"
                      orderby l.lop_tinhtrang, nd.noidungbaihoc_id descending
                      select new
                      {
                          nd.noidungbaihoc_id,
                          cs.coso_name,
                          l.lop_name,
                          nd.noidungbaihoc_tinhtrang,
                          u.username_fullname,
                          nd.noidungbaihoc_createdate
                      };

        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "noidungbaihoc_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nd in db.tbSLLDT_NoiDungBaiHocs
                       join l in db.tbLops on nd.lop_id equals l.lop_id
                       join cs in db.tbCosos on l.coso_id equals cs.coso_id
                       join u in db.admin_Users on nd.username_id equals u.username_id
                       where nd.noidungbaihoc_id == _id
                       select new
                       {
                           nd.noidungbaihoc_id,
                           cs.coso_name,
                           l.lop_name,
                           nd.noidungbaihoc_content,
                       }).Single();
        ddlCoSo.Text = getData.coso_name;
        ddlLop.Text = getData.lop_name;
        edtnoidung.Html = getData.noidungbaihoc_content;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "noidungbaihoc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbSLLDT_NoiDungBaiHoc delete = (from ndbh in db.tbSLLDT_NoiDungBaiHocs where ndbh.noidungbaihoc_id == Convert.ToInt32(item) select ndbh).FirstOrDefault();
                db.tbSLLDT_NoiDungBaiHocs.DeleteOnSubmit(delete);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
                loadData();
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        //admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        tbSLLDT_NoiDungBaiHoc update = (from nd in db.tbSLLDT_NoiDungBaiHocs where nd.noidungbaihoc_id == Convert.ToInt32(Session["_id"].ToString()) select nd).FirstOrDefault();
        update.noidungbaihoc_tinhtrang = "Đã duyệt";
        update.noidungbaihoc_content = edtnoidung.Html;
        //update.noidungbaihoc_tinhtrangxem = "Chưa xem";
        db.SubmitChanges();
        loadData();
        popupControl.ShowOnPageLoad = false;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Hoàn thành duyệt','','success').then(function(){grvList.Refresh();})", true);

    }

}