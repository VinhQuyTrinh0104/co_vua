﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_SLLDT_module_SLLDT_BeHocGiHomNay : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    private int lopId;
    protected void Page_Load(object sender, EventArgs e)
    {
        admin_User user = (from ad in db.admin_Users
                           where ad.username_username == Request.Cookies["UserName"].Value
                           select ad).FirstOrDefault();
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                if (RouteData.Values["id"].ToString() != "0")
                {
                    var getDaTa = (from nx in db.tbSLLDT_BeHocGiHomNays
                                   join l in db.tbLops on nx.lop_id equals l.lop_id
                                   join cs in db.tbCosos on l.coso_id equals cs.coso_id
                                   where nx.nhanxet_group == Convert.ToInt32(RouteData.Values["id"].ToString())
                                   select new
                                   {
                                       cs.coso_name,
                                       l.lop_name,
                                       nx.behocgihomnay_createdate,
                                       l.lop_id,
                                       cs.coso_id,
                                       nx.behocgihomnay_tinhtrang,
                                   }).FirstOrDefault();
                    lopId = getDaTa.lop_id;
                    txtLop.Value = getDaTa.lop_id + "";
                    ddlCoSo.Text = getDaTa.coso_name;
                    ddlLop.Value = Convert.ToInt32(getDaTa.lop_id);
                    ddlLop.Text = getDaTa.lop_name;
                    dteNgay.Value = getDaTa.behocgihomnay_createdate.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                    ddlLop.Enabled = false;
                    ddlCoSo.Enabled = false;
                    dteNgay.Disabled = true;
                    if (getDaTa.behocgihomnay_tinhtrang == "Đã duyệt")
                        btnLuu.Visible = false;
                    var getListAcounts = from ac in db.tbAccounts
                                         join hstl in db.tbhocsinhtronglops on ac.account_id equals hstl.account_id
                                         join l in db.tbLops on hstl.lop_id equals l.lop_id
                                         where l.lop_id == lopId && hstl.hstl_hidden == false && ac.hidden == false
                                         select new
                                         {
                                             hstl.hstl_id,
                                             ac.account_vn,
                                             noiDungNhanXet = (from nx in db.tbSLLDT_BeHocGiHomNays
                                                               where nx.nhanxet_group == Convert.ToInt32(RouteData.Values["id"].ToString())
                                                               && nx.hstl_id == hstl.hstl_id
                                                               select nx.behocgihomnay_nhanxet).SingleOrDefault() ?? "",

                                         };

                    rpList.DataSource = getListAcounts;
                    rpList.DataBind();
                    txtHocSinhTrongLop.Value = String.Join(",", getListAcounts.Select(x => x.hstl_id));
                }
                else
                {
                    var getCoSo = from cs in db.tbCosos
                                  select cs;

                    ddlCoSo.DataSource = getCoSo;
                    ddlCoSo.DataBind();
                    var getLop = from l in db.tbLops
                                 select l;
                    ddlLop.DataSource = getLop;
                    ddlLop.DataBind();
                }
            }


        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }

    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value) && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddlLop.DataBind();
    }

    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {

        var getListAcounts = from ac in db.tbAccounts
                             join hstl in db.tbhocsinhtronglops on ac.account_id equals hstl.account_id
                             join l in db.tbLops on hstl.lop_id equals l.lop_id
                             where l.lop_id == Convert.ToInt16(ddlLop.SelectedItem.Value)
                                && hstl.hstl_hidden == false && ac.hidden == false
                             select new
                             {
                                 hstl.hstl_id,
                                 ac.account_vn,
                                 noiDungNhanXet = ""
                             };
        rpList.DataSource = getListAcounts;
        rpList.DataBind();
        txtHocSinhTrongLop.Value = String.Join(",", getListAcounts.Select(x => x.hstl_id));

    }


    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        admin_User user = (from ad in db.admin_Users
                           where ad.username_username == Request.Cookies["UserName"].Value
                           select ad).FirstOrDefault();
        cls_Alert alert = new cls_Alert();
        string[] arrListGhiChu = txtGhiChu.Value.Split('|');
        string[] arrHocSinh = txtHocSinhTrongLop.Value.Split(',');
        int group_id = 1;
        var lastNhanXet = from nx in db.tbSLLDT_BeHocGiHomNays
                          orderby nx.behocgihomnay_id descending
                          select nx;
        if (lastNhanXet.Count() > 0)
            group_id = Convert.ToInt32(lastNhanXet.First().nhanxet_group) + 1;
        //DateTime inputDate = Convert.ToDateTime(dteNgay.Value);
        //DateTime inputDate = DateTime.ParseExact(dteNgay.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        for (int i = 0; i < arrHocSinh.Length; i++)
        {
            var acount_id = from hstl in db.tbhocsinhtronglops
                            where hstl.hstl_id == Convert.ToInt32(arrHocSinh[i])
                            select hstl.account_id;
            //thêm mới
            if (Convert.ToInt32(RouteData.Values["id"]) == 0)
            {
                if (arrListGhiChu[i] != "")
                {
                    tbSLLDT_BeHocGiHomNay insert = new tbSLLDT_BeHocGiHomNay();
                    insert.behocgihomnay_ngaynhanxet = Convert.ToDateTime(dteNgay.Value);
                    insert.behocgihomnay_createdate = DateTime.Now;
                    insert.behocgihomnay_nhanxet = arrListGhiChu[i].ToString();
                    insert.hstl_id = Convert.ToInt32(arrHocSinh[i]);
                    insert.username_id = user.username_id;
                    insert.behocgihomnay_tinhtrang = "Chưa duyệt";
                    insert.account_id = acount_id.FirstOrDefault();
                    insert.lop_id = Convert.ToInt32(ddlLop.Value);
                    insert.nhanxet_group = group_id;
                    db.tbSLLDT_BeHocGiHomNays.InsertOnSubmit(insert);
                    db.SubmitChanges();
                }
            }
            else
            {
                var checkNhanXet = (from nx in db.tbSLLDT_BeHocGiHomNays
                                    where nx.hstl_id == Convert.ToInt32(arrHocSinh[i]) && nx.nhanxet_group == Convert.ToInt32(RouteData.Values["id"])
                                    orderby nx.behocgihomnay_id descending
                                    select nx);
                if (checkNhanXet.Count() > 0)
                {
                    checkNhanXet.First().behocgihomnay_nhanxet = arrListGhiChu[i].ToString();
                    checkNhanXet.First().username_id = user.username_id;
                    db.SubmitChanges();
                }
                else
                {
                    if (arrListGhiChu[i] != "")
                    {
                        tbSLLDT_BeHocGiHomNay insert = new tbSLLDT_BeHocGiHomNay();
                        insert.behocgihomnay_ngaynhanxet = Convert.ToDateTime(dteNgay.Value);
                        insert.behocgihomnay_createdate = DateTime.Now;
                        insert.behocgihomnay_nhanxet = arrListGhiChu[i].ToString();
                        insert.hstl_id = Convert.ToInt32(arrHocSinh[i]);
                        insert.username_id = user.username_id;
                        insert.behocgihomnay_tinhtrang = "Chưa duyệt";
                        insert.account_id = acount_id.FirstOrDefault();
                        insert.lop_id = Convert.ToInt32(txtLop.Value);
                        insert.nhanxet_group = Convert.ToInt32(RouteData.Values["id"]);
                        db.tbSLLDT_BeHocGiHomNays.InsertOnSubmit(insert);
                        db.SubmitChanges();
                    }
                }
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-be-hoc-gi-hom-nay';})", true);
        }
    }

}