﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_NhapDiem : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int stt = 1;
    private int userId;
    protected void Page_Load(object sender, EventArgs e)
    {
        btn_luu.Visible = true;
        if (Request.Cookies["UserName"] != null)
        {
            userId = (from ad in db.admin_Users
                      where ad.username_username == Request.Cookies["UserName"].Value
                      select ad.username_id).FirstOrDefault();


            if (!IsPostBack)
            {
                var coso = from cs in db.tbCosos
                           select cs;
                ddlCoSo.DataSource = coso;
                ddlCoSo.DataTextField = "coso_name";
                ddlCoSo.DataValueField = "coso_id";
                ddlCoSo.DataBind();
                ddlCoSo.Items.Insert(0, "--Vui lòng chọn cơ sở--");
                ddlCoSo.Items[0].Selected = true;
                txtAccountid.Value = "";
                txtDiemHS.Value = "";

                var gv = from tk in db.admin_Users
                         where tk.username_id == userId
                         select new
                         {
                             tk.username_fullname,
                             tk.username_id
                         };
                ddlGiaoVien.DataSource = gv;
                ddlGiaoVien.DataTextField = "username_fullname";
                ddlGiaoVien.DataValueField = "username_id";
                ddlGiaoVien.DataBind();
                ddlGiaoVien.Enabled = false;
                bl_lop.Visible = false;
                tb_diem.Visible = false;
            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }


    protected void ddlGiaoVien_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selected = Int32.Parse(ddlLop.SelectedValue.ToString());
        var check = from bd in db.tbSLLDT_BangDiems
                    where bd.lop_id == selected
                    select bd;
        if (check.Count() == 0)
        {
            var lisths = (from hstl in db.tbhocsinhtronglops
                          join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                          where hstl.lop_id == selected && hstl.hstl_hidden == false && acc.hidden == false
                          orderby hstl.hstl_id descending
                          select new
                          {
                              acc.account_id,
                              hstl.hstl_id,
                          });
            foreach (var item in lisths)
            {
                tbSLLDT_BangDiem insertBD = new tbSLLDT_BangDiem();
                insertBD.lop_id = selected;
                insertBD.account_id = item.account_id;
                insertBD.hstl_id = item.hstl_id;
                insertBD.username_id = userId;
                db.tbSLLDT_BangDiems.InsertOnSubmit(insertBD);
                db.SubmitChanges();
                tbSLLDT_BangDiemChiTiet insertCT = new tbSLLDT_BangDiemChiTiet();
                insertCT.bangdiem_id = insertBD.bangdiem_id;
                db.tbSLLDT_BangDiemChiTiets.InsertOnSubmit(insertCT);
                db.SubmitChanges();
                //insertBD.bangdiemchitiet_id = insertCT.bangdiemchitiet_id;
            }
        }
        var listHS = from hstl in db.tbhocsinhtronglops
                     join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                     join bd in db.tbSLLDT_BangDiems on acc.account_id equals bd.account_id
                     join bdct in db.tbSLLDT_BangDiemChiTiets on bd.bangdiem_id equals bdct.bangdiem_id
                     where hstl.lop_id == selected && hstl.hstl_hidden == false && acc.hidden == false
                     select new
                     {
                         acc.account_id,
                         acc.account_vn,
                         bangdiemchitiet_unit1_sp = bdct.bangdiemchitiet_unit1_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_unit1_other = bdct.bangdiemchitiet_unit1_other.ToString().Replace(",", "."),
                         bangdiemchitiet_unit2_sp = bdct.bangdiemchitiet_unit2_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_unit2_other = bdct.bangdiemchitiet_unit2_other.ToString().Replace(",", "."),
                         bangdiemchitiet_unit3_sp = bdct.bangdiemchitiet_unit3_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_unit3_other = bdct.bangdiemchitiet_unit3_other.ToString().Replace(",", "."),
                         bangdiemchitiet_unit4_sp = bdct.bangdiemchitiet_unit4_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_unit4_other = bdct.bangdiemchitiet_unit4_other.ToString().Replace(",", "."),
                         bangdiemchitiet_unit5_sp = bdct.bangdiemchitiet_unit5_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_unit5_other = bdct.bangdiemchitiet_unit5_other.ToString().Replace(",", "."),
                         bangdiemchitiet_giuaki_sp = bdct.bangdiemchitiet_giuaki_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_giuaki_other = bdct.bangdiemchitiet_giuaki_other.ToString().Replace(",", "."),
                         bangdiemchitiet_cuoiki_sp = bdct.bangdiemchitiet_cuoiki_sp.ToString().Replace(",", "."),
                         bangdiemchitiet_cuoiki_other = bdct.bangdiemchitiet_cuoiki_other.ToString().Replace(",", ".")
                     };

        rpListHocSinh.DataSource = listHS;
        rpListHocSinh.DataBind();
        txtAccountid.Value = String.Join(",", listHS.Select(x => x.account_id));

        tb_diem.Visible = true;
    }

    protected double? validate_value(String numeric)
    {
        string strDiem = numeric;
        double? diem = null;

        double result;
        if (!string.IsNullOrEmpty(strDiem) && double.TryParse(strDiem, out result))
        {
            diem = result;
        }
        else
        {
            diem = null;
        }

        return diem;
    }

    protected void btn_luu_ServerClick(object sender, EventArgs e)
    {

        int selected_gv = Int32.Parse(ddlGiaoVien.SelectedValue.ToString());
        int selected_lop = Int32.Parse(ddlLop.SelectedValue.ToString());
        cls_Alert alert = new cls_Alert();
        string[] accountid = txtAccountid.Value.Split(',');
        string[] listdiem = txtDiemHS.Value.Split('|');

        for (int i = 0; i < accountid.Length; i++)
        {
            var diemcuahs = listdiem[i].Split(',');
            var check = from bd in db.tbSLLDT_BangDiems
                        join bdct in db.tbSLLDT_BangDiemChiTiets on bd.bangdiem_id equals bdct.bangdiem_id
                        where bd.account_id == Int32.Parse(accountid[i])
                        select bdct;
            if (check.Count() == 0)
            {
                tbSLLDT_BangDiem bd = new tbSLLDT_BangDiem();
                bd.account_id = Int32.Parse(accountid[i]);
                //bd.bangdiemchitiet_id = bdct.bangdiemchitiet_id;
                bd.lop_id = selected_lop;
                bd.username_id = selected_gv;
                bd.bangdiem_tinhtrang = "Chưa duyệt";
                bd.bangdiem_new = "Chưa xem";
                db.tbSLLDT_BangDiems.InsertOnSubmit(bd);
                db.SubmitChanges();
                tbSLLDT_BangDiemChiTiet bdct = new tbSLLDT_BangDiemChiTiet();
                bdct.bangdiemchitiet_unit1_sp = diemcuahs[0];
                bdct.bangdiemchitiet_unit1_other = diemcuahs[1];
                bdct.bangdiemchitiet_unit2_sp = diemcuahs[2];
                bdct.bangdiemchitiet_unit2_other = diemcuahs[3];
                bdct.bangdiemchitiet_unit3_sp = diemcuahs[4];
                bdct.bangdiemchitiet_unit3_other = diemcuahs[5];
                bdct.bangdiemchitiet_unit4_sp = diemcuahs[6];
                bdct.bangdiemchitiet_unit4_other = diemcuahs[7];
                bdct.bangdiemchitiet_unit5_sp = diemcuahs[8];
                bdct.bangdiemchitiet_unit5_other = diemcuahs[9];
                bdct.bangdiemchitiet_giuaki_sp = diemcuahs[10];
                bdct.bangdiemchitiet_giuaki_other = diemcuahs[11];
                bdct.bangdiemchitiet_cuoiki_sp = diemcuahs[12];
                bdct.bangdiemchitiet_cuoiki_other = diemcuahs[13];
                bdct.bangdiem_id = bd.bangdiem_id;
                db.tbSLLDT_BangDiemChiTiets.InsertOnSubmit(bdct);
                db.SubmitChanges();
            }
            else
            {
                tbSLLDT_BangDiem updateTinhTrang = (from bd in db.tbSLLDT_BangDiems
                                                    where bd.account_id == Int32.Parse(accountid[i])
                                                    orderby bd.bangdiem_id descending
                                                    select bd).FirstOrDefault();
                updateTinhTrang.bangdiem_tinhtrang = "Chưa duyệt";
                updateTinhTrang.bangdiem_new = "Chưa xem";
                tbSLLDT_BangDiemChiTiet bdct = (from bd in db.tbSLLDT_BangDiems
                                                join ct in db.tbSLLDT_BangDiemChiTiets on bd.bangdiem_id equals ct.bangdiem_id
                                                where bd.account_id == Int32.Parse(accountid[i])
                                                orderby ct.bangdiem_id descending
                                                select ct).FirstOrDefault();
                bdct.bangdiemchitiet_unit1_sp = diemcuahs[0];
                bdct.bangdiemchitiet_unit1_other = diemcuahs[1];
                bdct.bangdiemchitiet_unit2_sp = diemcuahs[2];
                bdct.bangdiemchitiet_unit2_other = diemcuahs[3];
                bdct.bangdiemchitiet_unit3_sp = diemcuahs[4];
                bdct.bangdiemchitiet_unit3_other = diemcuahs[5];
                bdct.bangdiemchitiet_unit4_sp = diemcuahs[6];
                bdct.bangdiemchitiet_unit4_other = diemcuahs[7];
                bdct.bangdiemchitiet_unit5_sp = diemcuahs[8];
                bdct.bangdiemchitiet_unit5_other = diemcuahs[9];
                bdct.bangdiemchitiet_giuaki_sp = diemcuahs[10];
                bdct.bangdiemchitiet_giuaki_other = diemcuahs[11];
                bdct.bangdiemchitiet_cuoiki_sp = diemcuahs[12];
                bdct.bangdiemchitiet_cuoiki_other = diemcuahs[13];
                db.SubmitChanges();
            }
        }

        alert.alert_Success(Page, "Lưu thành công", "");
        loadData();
    }
    protected void loadData()
    {
        int selected = Int32.Parse(ddlLop.SelectedValue.ToString());
        var listHS = from hstl in db.tbhocsinhtronglops
                     join acc in db.tbAccounts on hstl.account_id equals acc.account_id
                     join bd in db.tbSLLDT_BangDiems on hstl.hstl_id equals bd.hstl_id
                     join bdct in db.tbSLLDT_BangDiemChiTiets on bd.bangdiem_id equals bdct.bangdiem_id
                     where hstl.lop_id == selected
                     select new
                     {
                         acc.account_id,
                         acc.account_vn,
                         bdct.bangdiemchitiet_unit1_sp,
                         bdct.bangdiemchitiet_unit1_other,
                         bdct.bangdiemchitiet_unit2_sp,
                         bdct.bangdiemchitiet_unit2_other,
                         bdct.bangdiemchitiet_unit3_sp,
                         bdct.bangdiemchitiet_unit3_other,
                         bdct.bangdiemchitiet_unit4_sp,
                         bdct.bangdiemchitiet_unit4_other,
                         bdct.bangdiemchitiet_unit5_sp,
                         bdct.bangdiemchitiet_unit5_other,
                         bdct.bangdiemchitiet_giuaki_sp,
                         bdct.bangdiemchitiet_giuaki_other,
                         bdct.bangdiemchitiet_cuoiki_sp,
                         bdct.bangdiemchitiet_cuoiki_other
                     };
        rpListHocSinh.DataSource = listHS;
        rpListHocSinh.DataBind();
        txtAccountid.Value = String.Join(",", listHS.Select(x => x.account_id));
    }

    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selected = Int32.Parse(ddlCoSo.SelectedValue.ToString());
        var lop = from lp in db.tbLops
                  where lp.coso_id == selected && lp.lop_tinhtrang == null
                  select new
                  {
                      lp.lop_name,
                      lp.lop_id
                  };
        ddlLop.DataSource = lop;
        ddlLop.DataTextField = "lop_name";
        ddlLop.DataValueField = "lop_id";
        ddlLop.DataBind();
        ddlLop.Items.Insert(0, "--Vui lòng chọn lớp--");
        ddlLop.Items[0].Selected = true;
        stt = 1;
        bl_lop.Visible = true;
        //btn_luu.Visible = false;

    }
}