﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_SLLDT_BieuDoSoSanhDiem.aspx.cs" Inherits="admin_page_module_function_SLLDT_module_SLLDT_BieuDoSoSanhDiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../../../admin_js/js_base/jquery-1.9.1.js"></script>
    <style>
        label {
            margin-right: 1%;
        }

        .checkbox-list {
            column-count: 2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block box-admin">
        <div class="form-group-name">
            BIỂU ĐỒ SO SÁNH
        </div>
        <div class="card card-block">
            <asp:UpdatePanel ID="upNhapDiem" runat="server">
                <ContentTemplate>
                    <div class="col-12 form-group">
                        <label class="col-2">Cơ sở:</label>
                        <div class="col-4">
                            <asp:DropDownList AutoPostBack="true" ID="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-12 form-group" id="blLop" runat="server">
                        <label class="col-2">Lớp:</label>
                        <div class="col-4">
                            <div class="checkbox-list">
                                <asp:Repeater ID="rpLop" runat="server">
                                    <ItemTemplate>
                                        <div>
                                            <input type="checkbox" id="cbLop_<%# Eval("lop_name") %>" value="<%# Eval("lop_id") %>" />
                                            <label><%# Eval("lop_name") %></label>
                                        </div>

                                    </ItemTemplate>

                                </asp:Repeater>
                            </div>

                        </div>
                    </div>
                    <asp:Repeater ID="rpTest" runat="server">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div>
                        <textarea id="jsonitem" runat="server"></textarea>
                        <input type="button" onclick="getData()" />
                    </div>
                    <div id="print"></div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
    <script>
        function getData() {
            var jsonData = document.getElementById("<%= jsonitem.ClientID%>").value;
            var result = JSON.parse(jsonData);
            document.getElementById("print").innerHTML = jsonData;

            console.log(result);
        }
    </script>
    <script>
        //$.get("http://api.cncstone.vn/api/Controller_API010_TruongTrinhHoc_/getDataChuongTrinhHoc", function (data) {
        //    // Xử lý dữ liệu trả về
        //    console.log(data);
        //});
        //$.ajax({
        //    url: 'http://mysite.microsoft.sample.xyz.com/api/mycall',
        //    headers: {
        //        'Content-Type': 'application/x-www-form-urlencoded'
        //    },
        //    type: "POST", /* or type:"GET" or type:"PUT" */
        //    dataType: "json",
        //    data: {
        //    },
        //    success: function (result) {
        //        console.log(result);
        //    },
        //    error: function () {
        //        console.log("error");
        //    }
        //});
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

