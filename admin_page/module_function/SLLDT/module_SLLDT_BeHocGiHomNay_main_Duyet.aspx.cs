﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_SLLDT_module_SLLDT_BeHocGiHomNay_main_Duyet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {

            var getUserClass = from nx in db.tbSLLDT_BeHocGiHomNays
                               where nx.behocgihomnay_tinhtrang =="Chưa duyệt"
                               group nx by new { nx.nhanxet_group } into gr
                               orderby gr.Key.nhanxet_group descending
                               select new
                               {
                                   nhanxet_group = Convert.ToInt32(gr.Key.nhanxet_group),
                                   coso_name = (from cs in db.tbCosos
                                                join l in db.tbLops on cs.coso_id equals l.coso_id
                                                join bh in db.tbSLLDT_BeHocGiHomNays on l.lop_id equals bh.lop_id
                                                where bh.nhanxet_group == Convert.ToInt32(gr.Key.nhanxet_group)
                                                select cs).FirstOrDefault().coso_name,
                                   lop_name = (from l in db.tbLops
                                               join bh in db.tbSLLDT_BeHocGiHomNays on l.lop_id equals bh.lop_id
                                               where bh.nhanxet_group == Convert.ToInt32(gr.Key.nhanxet_group)
                                               select l).FirstOrDefault().lop_name,
                                   ngay = gr.First().behocgihomnay_ngaynhanxet,
                                   behocgihomnay_tinhtrang = gr.First().behocgihomnay_tinhtrang,
                                   username_fullname = (from u in db.admin_Users
                                                        where u.username_id == gr.First().username_id
                                                        select u).FirstOrDefault().username_fullname
                               };
            grvList.DataSource = getUserClass;
            grvList.DataBind();
        }
        else
        {
            Response.Redirect("/admin-login");
        }

    }



    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "nhanxet_group" }));
        Session["id"] = id;
        Response.Redirect("/admin-xem-duyet-be-hoc-gi-hom-nay-" + id);

    }

    protected void btnThem_Click(object sender, EventArgs e)
    {

    }

    protected void grvList_SelectionChanged(object sender, EventArgs e)
    {
        // id = Convert.ToInt32(grvList.GetSelectedFieldValues("id_behocgihomnay"));
    }
}