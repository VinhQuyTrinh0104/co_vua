﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_SLLDT_NhapDiem.aspx.cs" Inherits="admin_page_module_function_module_NhapDiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .col-title-center {
            text-align: center;
        }

        td input {
            /*width: 50%;
            height: 50%;*/
            position: absolute;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            text-align: center;
            border: none;
        }

        tr th {
            text-align: center;
        }

        td {
            position: relative;
        }

        .Unit_list td {
            padding-left: 20px;
            padding-right: 20px;
        }

        .name_ddr {
            padding-left: 200px !important;
            padding-right: 20px !important;
        }

        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="card card-block box-admin">
        <div class="form-group-name">
            NHẬP ĐIỂM
        </div>
        <div class="card card-block">
            <asp:UpdatePanel ID="upNhapDiem" runat="server">
                <ContentTemplate>
                    <div class="col-12 form-group">
                        <label class="col-2">Giáo viên:</label>
                        <div class="col-4">
                            <asp:DropDownList AutoPostBack="true" ID="ddlGiaoVien" OnSelectedIndexChanged="ddlGiaoVien_SelectedIndexChanged" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label class="col-2">Cơ sở:</label>
                        <div class="col-4">
                            <asp:DropDownList AutoPostBack="true" ID="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-12 form-group" id="bl_lop" runat="server">
                        <label class="col-2">Lớp:</label>
                        <div class="col-4">
                            <asp:DropDownList ID="ddlLop" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <span>Lưu ý: Vui lòng nhập "<b>.</b>" không nhập dấu "<b>,</b>" cho điểm thập phân</span>
                    <br />
                    <span>Vd: Nhập đúng là: <b>8.5</b> hoặc <b>9.5</b></span>
                    <div class="col-12 form-group" style="overflow-x: auto" id="tb_diem" runat="server">
                        <table class="table-bordered">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Họ và tên học sinh</th>
                                <th scope="col" colspan="2">TX 1</th>
                                <th scope="col" colspan="2">TX 2</th>
                                <th scope="col" colspan="2">TX 3</th>
                                <th scope="col" colspan="2">TX 4</th>
                                <th scope="col" colspan="2">TX 5</th>
                                <%-- <th scope="col" colspan="2">Unit 6</th>
                                <th scope="col" colspan="2">Unit 7</th>
                                <th scope="col" colspan="2">Unit 8</th>--%>
                                <th scope="col" colspan="2">Giữa kỳ</th>
                                <th scope="col" colspan="2">Cuối kỳ</th>
                            </tr>
                            <tr class="Unit_list">
                                <td></td>
                                <td class="name_ddr"></td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                                <td>S</td>
                                <td>L,R,W</td>
                            </tr>
                            <asp:Repeater ID="rpListHocSinh" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center;"><%=stt++ %></td>
                                        <td><span><%# Eval("account_vn") %></span></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" oninput="checkTextInput(this)" id="Unit_1_Sk_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit1_sp")%>" /> 
                                        </td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_1_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit1_other") %>" /></td>
                                      <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_2_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit2_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_2_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit2_other") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_3_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit3_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_3_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit3_other") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_4_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit4_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_4_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit4_other") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_5_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit5_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_5_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_unit5_other") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_GiuaKi_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_giuaki_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_GiuaKi_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_giuaki_other") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_CuoiKi_Sk<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_cuoiki_sp") %>" /></td>
                                        <td>
                                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode ==  46" onchange="check_history(<%#Eval("account_id")%>)" id="Unit_CuoiKi_<%#Eval("account_id")%>" value="<%# Eval("bangdiemchitiet_cuoiki_other") %>" /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                    <div style="display: none;">
                        <input type="text" id="txtAccountid" runat="server" />
                        <input type="text" id="txtDiemHS" runat="server" />
                        <input type="text" id="txtHistory" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="col-12 form-group" runat="server" id="button_Save">
            <a href="#" style="float: right; margin-right: 1.6%;" class="btn btn-primary" runat="server" id="btn_luu" onclick="insertData()" onserverclick="btn_luu_ServerClick">Lưu thay đổi</a>
        </div>
    </div>
    <script>
        let history = new Set();
        function check_history(id) {
            history.add(id);
            var myArray = Array.from(history);
            document.getElementById("<%= txtHistory.ClientID%>").value = myArray;
        }
        console.log(history);

    </script>

    <script>
        function insertData() {
            let accountid = document.getElementById("<%= txtAccountid.ClientID%>").value.split(",");
            var arrdiem = [];
            for (let i = 0; i < accountid.length; i++) {
                var unit_1_sp = document.getElementById("Unit_1_Sk_" + accountid[i]).value;
                var unit_1_other = document.getElementById("Unit_1_" + accountid[i]).value;
                var unit_2_sp = document.getElementById("Unit_2_Sk" + accountid[i]).value;
                var unit_2_other = document.getElementById("Unit_2_" + accountid[i]).value;
                var unit_3_sp = document.getElementById("Unit_3_Sk" + accountid[i]).value;
                var unit_3_other = document.getElementById("Unit_3_" + accountid[i]).value;
                var unit_4_sp = document.getElementById("Unit_4_Sk" + accountid[i]).value;
                var unit_4_other = document.getElementById("Unit_4_" + accountid[i]).value;
                var unit_5_sp = document.getElementById("Unit_5_Sk" + accountid[i]).value;
                var unit_5_other = document.getElementById("Unit_5_" + accountid[i]).value;
                var unit_giua_ki_sp = document.getElementById("Unit_GiuaKi_Sk" + accountid[i]).value;
                var unit_giua_ki_other = document.getElementById("Unit_GiuaKi_" + accountid[i]).value;
                var unit_cuoi_ki_sp = document.getElementById("Unit_CuoiKi_Sk" + accountid[i]).value;
                var unit_cuoi_ki_other = document.getElementById("Unit_CuoiKi_" + accountid[i]).value;

                var diem = unit_1_sp + "," + unit_1_other + "," + unit_2_sp + "," + unit_2_other + "," + unit_3_sp + "," + unit_3_other + "," + unit_4_sp + "," + unit_4_other + "," + unit_5_sp + "," + unit_5_other + "," + unit_giua_ki_sp + "," + unit_giua_ki_other + "," + unit_cuoi_ki_sp + "," + unit_cuoi_ki_other;
                arrdiem.push(diem);
            }
            document.getElementById("<%=txtDiemHS.ClientID%>").value = arrdiem.join("|");


        }

    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

