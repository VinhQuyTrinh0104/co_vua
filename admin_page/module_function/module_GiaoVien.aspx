﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_GiaoVien.aspx.cs" Inherits="admin_page_module_function_module_GiaoVien" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtTaiKhoan.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên học sinh không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false; return true;
        };
    </script>
    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }

        .dxpc-content {
            height: 83vh !important;
        }

        .col-12 {
            display: flex;
            align-items: center !important;
        }
    </style>
   <%-- <div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>--%>
      <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN GIÁO VIÊN
        </div>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
               
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                        <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        <input type="submit" id="btnDlt" runat="server" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                         <asp:Button ID="btnGioiThieu" runat="server" Text="Mô tả trên website" CssClass="btn btn-primary" OnClick="btnGioiThieu_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div>
            Lưu ý: mật khẩu mới: 12345
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="username_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                     <dx:GridViewDataColumn Caption="Loại" FieldName="groupuser_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Tài khoản đăng nhập" FieldName="username_username" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ và tên giáo viên" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số điện thoại" FieldName="username_phone" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Email" FieldName="username_email" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} học sinh)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="800px" Height="600px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Thông tin học sinh" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="row">
                                    <div class="col-6" style="margin: 0">
                                         <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Loại:</label>
                                            <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlLoai" runat="server" ValueType="System.Int32" TextField="groupuser_name" ValueField="groupuser_id" ClientInstanceName="ddlLoai"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Tài khoản:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtTaiKhoan" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Họ và tên:</label>
                                            <div class="col-10">
                                                <input type="text" id="txtHoTen" runat="server" class="form-control boxed" style="width: 95%" />
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">SĐT:</label>
                                            <div class="col-10">
                                                <input type="text" id="txtSDT" runat="server" class="form-control boxed" style="width: 95%" />
                                            </div>
                                        </div>
                                       <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Email:</label>
                                            <div class="col-10">
                                                <input type="text" id="txtEmail" runat="server" class="form-control boxed" style="width: 95%" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



