﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSach_HocSinh_ThongKeChiTiet_backup : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
           
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                 var getThongTinHocSinh = (from hs in db.tbAccounts
                                           join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                           join l in db.tbLops on hstl.lop_id equals l.lop_id
                                           join cs in db.tbCosos on l.coso_id equals cs.coso_id
                                           where hs.account_id == Convert.ToInt32(RouteData.Values["id"])
                                           orderby hstl.hstl_id descending
                                           select new { 
                                           cs.coso_name,
                                           hs.account_vn,
                                           hs.account_ngaysinh,
                                           hs.account_phone,
                                           hs.account_truonghoc,
                                           l.lop_name,
                                           hs.account_en,
                                           hs.account_gioitinh,
                                           hs.account_email,
                                           hs.account_noisinh,
                                           hs.account_noiohientai,
                                           hs.account_tenba,
                                           hs.account_emailba,
                                           hs.account_sdtba,
                                           hs.account_tenme,
                                           hs.account_emailme,
                                           hs.account_sdtme,
                                           hs.account_nghenghiepba,
                                           hs.account_nghenghiepme,
                                           hs.account_mongmuon
                                           }).FirstOrDefault();
                txtCoSo.Text = getThongTinHocSinh.coso_name;
                txtFullName.Text = getThongTinHocSinh.account_vn;
                txtNgaySinh.Text = getThongTinHocSinh.account_ngaysinh.Value.ToString("dd/MM/yyyy");
                txtSDTHocSinh.Text = getThongTinHocSinh.account_phone;
                txtTruongHoc.Text = getThongTinHocSinh.account_truonghoc;
                txtLop.Text = getThongTinHocSinh.lop_name;
                txtBietDanh.Text = getThongTinHocSinh.account_en;
                if (getThongTinHocSinh.account_gioitinh == true)
                    txtGioiTinh.Text = "Nam";
                else
                    txtGioiTinh.Text = "Nữ";
                txtEmailHocSinh.Text = getThongTinHocSinh.account_email;
                txtNoiSinh.Text = getThongTinHocSinh.account_noisinh;
                txtChoOHienTai.Text = getThongTinHocSinh.account_noiohientai;
                txtTenBa.Text = getThongTinHocSinh.account_tenba;
                txtSDTBa.Text = getThongTinHocSinh.account_sdtba;
                txtEmaiBa.Text = getThongTinHocSinh.account_emailba;
                txtTenMe.Text = getThongTinHocSinh.account_tenme;
                txtSDTMe.Text = getThongTinHocSinh.account_sdtme;
                txtEmailMe.Text = getThongTinHocSinh.account_emailme;
                txtNgheNghiep.Text = getThongTinHocSinh.account_nghenghiepba;
                txtNgheNghiepMe.Text = getThongTinHocSinh.account_nghenghiepme;
                txtMongMuon.Text = getThongTinHocSinh.account_mongmuon;
                loadDongHocPhiHocSinh();
                loadDataBaoLuu();
            }

        }
    }
    private void loadDongHocPhiHocSinh()
    {
        // load data đổ vào var danh sách
        var getData = from hp in db.tbHocPhis
                      join u in db.admin_Users on hp.username_id equals u.username_id
                      join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
                      join hs in db.tbAccounts on hpct.hocsinh_id equals hs.account_id
                      join l in db.tbLops on hpct.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      orderby hp.hp_id descending
                      where hs.account_id == Convert.ToInt32(RouteData.Values["id"])
                      select new
                      {
                          hpct.hstl_id,
                          cs.coso_name,
                          l.lop_name,
                          hp.hp_id,
                          hp.hocphi_ngaydongtien,
                          hpct.hocphichitiet_phieuthu,
                          hpct.hocphichitiet_maso,
                          u.username_fullname,
                          hs.account_vn,
                          hp.hocphi_tinhtrangdong,
                          hp.hocphi_chitiet,
                          hp.hocphi_tongtien,
                          hpct.hocphichitiet_sotiendongkhoahoc,
                          hpct.hocphichitiet_sotiendadong,
                          hpct.hocphichitiet_sotienconlai,
                          hpct.hocphichitiet_sotiengiaotrinh,
                          hpct.hocphichitiet_sobuoihoc,
                          hpct.hocphichitiet_ngaybatdau,
                          ngayketthuc = hpct.hocphichitiet_ngayketthuc,
                          hpct.hocphichitiet_chuongtrinhgiam,
                          hpct.hocphichitiet_hinhthucthanhtoan,
                          hpct.hocphichitiet_ghichu
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void loadDataBaoLuu()
    {

        // load data đổ vào var danh sách
        var getData = from bl in db.tbHocPhiBaoLuus
                      join hstl in db.tbhocsinhtronglops on bl.hstl_id equals hstl.hstl_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      //join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      where hs.account_id == Convert.ToInt32(RouteData.Values["id"])
                      orderby bl.baoluu_id descending
                      select new
                      {
                          hstl.hstl_id,
                          bl.baoluu_name,
                          bl.baoluu_id,
                          hs.account_vn,
                          bl.baoluu_ngayketthuc,
                          bl.baoluu_ngayketthuc_hidden,
                          hocphichitiet_ngaybatdau = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_ngaybatdau,
                          hocphichitiet_sobuoihoc = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
                          bl.baoluu_sobuoi,
                          l.lop_name,
                          bl.baoluu_loai,
                          cs.coso_name,
                          bl.baoluu_NgayNhap,
                          bl.baoluu_ghichu,
                      };
        // đẩy dữ liệu vào gridivew
        grvListBaoLuu.DataSource = getData;
        grvListBaoLuu.DataBind();
    }
}