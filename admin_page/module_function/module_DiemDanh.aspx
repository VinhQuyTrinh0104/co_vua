﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DiemDanh.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh" %>

<%--<%@ Register Src="~/web_usercontrol/MenuNhanVien.ascx" TagPrefix="uc1" TagName="MenuNhanVien" %>
<%@ Register Src="~/web_usercontrol/MenuQuanTri.ascx" TagPrefix="uc1" TagName="MenuQuanTri" %>
<%@ Register Src="~/web_usercontrol/MenuGiaoVien.ascx" TagPrefix="uc1" TagName="MenuGiaoVien" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        function myDiemDanh(id, vang, ghichu) {
            document.getElementById("<%=txtHocSinhTrongLop.ClientID%>").value = id;
            document.getElementById("<%=txtDiemDanhVangHidden.ClientID%>").value = vang;
            document.getElementById("<%=txtGhiChuHidden.ClientID%>").value = ghichu;
            document.getElementById("<%=btnSave.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <%-- <div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
     <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block">
        <div style="font-size: 28px; font-weight: bold; text-align: center; color: blue">
            QUẢN LÝ THÔNG TIN ĐIỂM DANH
        </div>
    </div>
    <div class="card card-block">

        <div class="col-12 form-group">
            <label class="col-2">Chọn ngày:</label>
            <div class="col-4">
                <input type="date" id="dteNgay" runat="server" class="form-control" />
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Cơ sở:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlCoSo" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlCoSo" OnSelectedIndexChanged="ddlCoSo_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn cơ sở" Width="95%"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2">Lớp:</label>
            <div class="col-4">
                <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
            </div>
            <label class="col-1 form-control-label">Giờ học:</label>
            <div class="col-4">
                <b>
                    <asp:Label ID="lblGioHoc" runat="server"></asp:Label></b>
            </div>
        </div>
        <div class="col-12 form-group">
            <label class="col-2"></label>
            <div class="col-6">
                <div class="col-12 form-group">
                    <asp:CheckBox ID="ckThu2" runat="server" Text="Thứ 2" />
                    &nbsp; &nbsp;
                <asp:CheckBox ID="ckThu3" runat="server" Text="Thứ 3" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu4" runat="server" Text="Thứ 4" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu5" runat="server" Text="Thứ 5" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu6" runat="server" Text="Thứ 6" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckThu7" runat="server" Text="Thứ 7" />&nbsp; &nbsp;
                <asp:CheckBox ID="ckChuNhat" runat="server" Text="Chủ nhật" />
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">Điểm danh</th>
                    <th scope="col">Lý do</th>
                    <th scope="col">#</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%=STT++ %></th>
                            <td><%#Eval("account_vn") %></td>
                            <td>
                                <select id="ddlDiemDanhVang">
                                    <option value="Đúng giờ">Đúng giờ</option>
                                    <option value="Vắng có phép">Vắng có phép</option>
                                    <option value="Vắng không phép">Vắng không phép</option>
                                    <option value="Bảo lưu">Bảo lưu</option>
                                </select></td>
                            <td>
                                <input id="txtGhiChu" type="text" /></td>
                            <td><a id="btnLuu" onclick="myDiemDanh('<%#Eval("hstl_id") %>', document.getElementById('ddlDiemDanhVang').value, document.getElementById('txtGhiChu').value)" class="btn btn-primary">Xác nhận</a></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <div style="display: none">
            <input type="text" id="txtDiemDanhVangHidden" runat="server" />
            <input type="text" id="txtGhiChuHidden" runat="server" />
            <input type="text" id="txtHocSinhTrongLop" runat="server" />
            <a id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



