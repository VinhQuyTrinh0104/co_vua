﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_SoLienLac : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();

    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        //BindDropdown();

        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (logedMember.username_id == 1 || logedMember.username_id == 2)
        {
            cbblop.DataSource = from l in db.tbLops
                                    //where l.lop_id == logedMember.username_lopId
                                select l;
            cbblop.DataBind();
            cbbMonhoc.DataSource = from mh in db.tbMonHocs
                                   select mh;
            cbbMonhoc.DataBind();
        }
        else
        {
            cbblop.DataSource = from l in db.tbLops
                                join gvtl in db.admin_Users on l.username_idVn equals gvtl.username_id
                                where gvtl.username_id == logedMember.username_id
                                select l;
            cbblop.DataBind();
            cbbMonhoc.DataSource = from mh in db.tbMonHocs
                                   select mh;
            cbbMonhoc.DataBind();
        }
        var new_day = DateTime.Now.ToString("dd/MM/yyyy");
        txtNgay.Value = new_day;
    }
    private void setNULL()
    {
        txtdanhgiaGV.Value = "";
        txtKqcuoiky.Value = "";
        txtKqgiuaky.Value = "";

    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        int id = 0;
        string account_vn;
        List<object> findvalues = grvList.GetSelectedFieldValues(new string[] { "hstl_id", "account_id", "account_vn" });
        foreach (object[] value in findvalues)
        {
            id = Convert.ToInt32(value[1]);
            account_vn = value[2].ToString();
            //Kiểm tra dữ liệu trong Sổ Liên Lạc
            var check = (from c in db.tbSoLienLacs
                         where c.account_id == id && c.sll_date == Convert.ToDateTime(txtNgay.Value) && c.monhoc_id == Convert.ToInt32(cbbMonhoc.SelectedItem.Value)
                         select c).SingleOrDefault();

            if (check == null)
            {
                //Dữ liệu == null thì insert vào
                tbSoLienLac ins = new tbSoLienLac();
                ins.sll_danhgiagv = txtdanhgiaGV.Value;
                //ins.sll_nangluchs = txtdanhgiaHS.Value;
                ins.sll_date = DateTime.Now;
                ins.sll_trangthai = false;
                ins.monhoc_id = Convert.ToInt32(cbbMonhoc.SelectedItem.Value);
                //ins.sll_tinhtrang = slTinhtrang.Value.ToString();
                ins.sll_kqcuoiky = txtKqcuoiky.Value;
                ins.sll_kqgiuaky = txtKqgiuaky.Value;
                ins.account_id = id;
                ins.account_vn = account_vn;
                ins.sll_hidden = false;
                ins.username_id = logedMember.username_id;
                db.tbSoLienLacs.InsertOnSubmit(ins);
                try
                {
                    db.SubmitChanges();
                   // ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                    alert.alert_Success(Page, "Lưu thành công!", "");
                }
                catch { }
                //lưu vào lịch sử
                tbSoLienLacHistory htr = new tbSoLienLacHistory();
                htr.htr_date = DateTime.Now;
                htr.htr_danhgiagv = txtdanhgiaGV.Value;
                htr.username_id = logedMember.username_id;
                //htr.htr_danhgiahs = txtdanhgiaHS.Value;
                htr.account_id = id;
                htr.monhoc_id= Convert.ToInt32(cbbMonhoc.SelectedItem.Value);
                htr.htr_trangthai = true;
                htr.htr_kqgiuaky = txtKqgiuaky.Value;
                //htr.htr_tinhtrang = slTinhtrang.Value.ToString();
                htr.htr_kqcuoiky = txtKqcuoiky.Value;
                htr.account_vn = account_vn;
                htr.htr_hidden = false;
                db.tbSoLienLacHistories.InsertOnSubmit(htr);
                try
                {
                    db.SubmitChanges();
                }
                catch { }
            }
            else
            {
               // ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Học sinh này đã được đánh giá! Vui lòng sang lịch sử để xem lại','','warning').then(function(){grvList.Refresh();})", true);
                alert.alert_Warning(Page, "Học sinh này đã được đánh giá! Vui lòng sang lịch sử để xem lại", "");
                ////Dữ liệu có rồi thì cập nhật lại dữ liệu
                //check.sll_danhgiagv = txtdanhgiaGV.Value;
                ////check.sll_nangluchs = txtdanhgiaHS.Value;
                //check.sll_kqgiuaky = txtKqgiuaky.Value;
                ////check.sll_tinhtrang = slTinhtrang.Value.ToString();
                //check.sll_kqcuoiky = txtKqcuoiky.Value;
                //check.sll_date = DateTime.Now;
                //check.sll_trangthai = false;
                //try
                //{
                //    db.SubmitChanges();
                //    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                //}
                //catch { }

            }
            //Lưu vào Lịch Sử
           
        }
        setNULL();
    }

    protected void cbblop_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getData = from l in db.tbLops
                      join hstl in db.tbhocsinhtronglops on l.lop_id equals hstl.lop_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                     
                      where hstl.lop_id == Convert.ToInt32(cbblop.SelectedItem.Value)
                      select new
                      {
                          hstl.hstl_id,
                          hs.account_vn,
                          hs.account_en,
                          hs.account_phone,
                          hs.account_id,
                          hstl.hstl_ngaybatdauhoc,
                          hstl.hstl_ngayketthuchoc
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void cbbMonhoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getData = from l in db.tbLops
                      join hstl in db.tbhocsinhtronglops on l.lop_id equals hstl.lop_id
                      join mh in db.tbMonHocs on hstl.monhoc_id equals mh.monhoc_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      where hstl.lop_id == Convert.ToInt32(cbblop.SelectedItem.Value)
                      && mh.monhoc_id == Convert.ToInt32(cbbMonhoc.SelectedItem.Value)
                      select new
                      {
                          hstl.hstl_id,
                          hs.account_vn,
                          hs.account_en,
                          hs.account_phone,
                          hs.account_id,
                          hstl.hstl_ngaybatdauhoc,
                          hstl.hstl_ngayketthuchoc
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }
}