﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhGia_ThongKe : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                ddlCoSo.DataSource = from cs in db.tbCosos where cs.coso_hidden==false select cs;
                ddlCoSo.DataBind();
            }
        }
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                            && l.hidden==false && l.lop_tinhtrang==null
                            select l;
        ddlLop.DataBind();
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getData = from hs in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                      join dg in db.tbDanhGias on hstl.hstl_id equals dg.hstl_id
                      join u in db.admin_Users on dg.username_id equals u.username_id
                      where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedItem.Value) && hstl.hstl_hidden == false
                      select new
                      {
                          hs.account_vn,
                          hs.account_en,
                          //dg.attitude_active,
                          //dg.attitude_confident,
                          //dg.attitude_easy,
                          //dg.attitude_focus,
                          //dg.attitude_good,
                          //dg.attitude_less,
                          //dg.attitude_need,
                          dg.danhgia_id,
                          dg.danhgia_month,
                          dg.danhgia_ngay,
                          dg.danhgia_remark,
                          //dg.homework_finished,
                          //dg.homework_missing,
                          //dg.homework_unfinished,
                          //dg.skill_good_gram,
                          //dg.skill_good_listening,
                          //dg.skill_good_pron,
                          //dg.skill_good_speaking,
                          //dg.skill_need,
                          u.username_fullname
                      };
        rpList.DataSource = getData;
        rpList.DataBind();
    }
}