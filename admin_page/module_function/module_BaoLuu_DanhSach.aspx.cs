﻿using ClosedXML.Excel;
using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaoLuu_DanhSach : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (checkButton.groupuser_id == 6)
            {
              
                btnDlt.Visible = false;
                btnDlt2.Visible = false;
                btnDlt3.Visible = false;
                btnDlt4.Visible = false;
                btnThem.Visible = true;
                btnDuyetLop.Visible = false;
                btnDuyetChuyenLop.Visible = false;
                btnDuyethocSinh.Visible = false;
                btnDuyetNghiHan.Visible = false;
               
            }
            else
            {
                if (checkButton.groupuser_id == 2)
                {
                   
                    btnDlt.Visible = true;
                    btnDlt2.Visible = true;
                    btnDlt3.Visible = true;
                    btnDlt4.Visible = true;
                }
                
            }
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadDataHocSinh();
            loadDataLop();
            loadDataChuyenLop();
            loadDataNghiHan();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadDataLop()
    {
        var getDatLop = (from bl in db.tbHocPhiChiTiets
                         where bl.baoluu_loai == "Trung tâm bảo lưu"
                         group bl by bl.hocphichitiet_ghichu into g
                         select new
                         {
                             hocphichitiet_ghichu = g.Key,
                             hocphichitiet_id = (from bl1 in db.tbHocPhiChiTiets where bl1.hocphichitiet_ghichu == g.Key select bl1).FirstOrDefault().hocphichitiet_id,
                             lop_name = (from bl1 in db.tbHocPhiChiTiets
                                         join l1 in db.tbLops on bl1.lop_id equals l1.lop_id
                                         where bl1.hocphichitiet_ghichu == g.Key
                                         select l1).FirstOrDefault().lop_name,
                             coso_name = (from bl1 in db.tbHocPhiChiTiets
                                          join l1 in db.tbLops on bl1.lop_id equals l1.lop_id
                                          join cs1 in db.tbCosos on l1.coso_id equals cs1.coso_id
                                          where bl1.hocphichitiet_ghichu == g.Key
                                          select cs1).FirstOrDefault().coso_name,
                             hocphichitiet_createdate = (from bl1 in db.tbHocPhiChiTiets where bl1.hocphichitiet_ghichu == g.Key select bl1).FirstOrDefault().hocphichitiet_createdate,
                             baoluu_sobuoi = (from bl1 in db.tbHocPhiChiTiets where bl1.hocphichitiet_ghichu == g.Key select bl1).FirstOrDefault().baoluu_sobuoi,
                             baoluu_tinhtrang = (from bl1 in db.tbHocPhiChiTiets where bl1.hocphichitiet_ghichu == g.Key select bl1).FirstOrDefault().baoluu_tinhtrang,
                         }).OrderByDescending(x => x.hocphichitiet_id);
        grvListLop.DataSource = getDatLop;
        grvListLop.DataBind();
    }
    private void loadDataHocSinh()
    {
        // load data đổ vào var danh sách
        var getData = from bl in db.tbHocPhiChiTiets
                      join hstl in db.tbhocsinhtronglops on bl.hstl_id equals hstl.hstl_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      //join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      where bl.baoluu_loai == "Cá nhân bảo lưu"
                      orderby bl.hocphichitiet_id descending
                      select new
                      {
                          hstl.hstl_id,
                          bl.hocphichitiet_id,
                          hs.account_vn,
                          //bl.baoluu_ngayketthuc,
                          bl.hocphichitiet_ngayketthuc,
                          hocphichitiet_ngaybatdau = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_ngaybatdau,
                          hocphichitiet_sobuoihoc = (from hpct in db.tbHocPhiChiTiets where hpct.hstl_id == hstl.hstl_id orderby hpct.hocphi_id descending select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
                          bl.baoluu_sobuoi,
                          l.lop_name,
                          bl.baoluu_loai,
                          cs.coso_name,
                          bl.hocphichitiet_createdate,
                          bl.hocphichitiet_ghichu,
                          bl.baoluu_tinhtrang
                      };
        // đẩy dữ liệu vào gridivew
        grvListHocSinh.DataSource = getData;
        grvListHocSinh.DataBind();
    }
    private void loadDataChuyenLop()
    {

        // load data đổ vào var danh sách
        var getData = from bl in db.tbHocPhiChiTiets
                      join hstl in db.tbhocsinhtronglops on bl.hstl_id equals hstl.hstl_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      //join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      where bl.baoluu_loai == "Chuyển lớp"
                      orderby bl.hocphichitiet_id descending
                      select new
                      {
                          hstl.hstl_id,
                          bl.hocphichitiet_id,
                          hs.account_vn,
                          bl.hocphichitiet_ngayketthuc,
                          bl.hocphichitiet_ngaybatdau,
                          bl.hocphichitiet_sobuoihoc,
                          bl.baoluu_sobuoi,
                          l.lop_name,
                          bl.baoluu_loai,
                          cs.coso_name,
                          bl.hocphichitiet_createdate,
                          bl.baoluu_ghichu,
                          bl.baoluu_tinhtrang
                      };
        // đẩy dữ liệu vào gridivew
        grvChuyenLop.DataSource = getData;
        grvChuyenLop.DataBind();
    }
    private void loadDataNghiHan()
    {
        // load data đổ vào var danh sách
        var getData = from bl in db.tbHocPhiChiTiets
                      join hstl in db.tbhocsinhtronglops on bl.hstl_id equals hstl.hstl_id
                      join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                      //join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      where bl.baoluu_loai == "Nghỉ hẵn"
                      orderby bl.hocphichitiet_id descending
                      select new
                      {
                          hstl.hstl_id,
                          bl.hocphichitiet_id,
                          hs.account_vn,
                          bl.hocphichitiet_ngayketthuc,
                          bl.hocphichitiet_ngaybatdau,
                          bl.hocphichitiet_sobuoihoc,
                          bl.baoluu_sobuoi,
                          l.lop_name,
                          bl.baoluu_loai,
                          cs.coso_name,
                          bl.hocphichitiet_createdate,
                          bl.hocphichitiet_ghichu,
                          bl.baoluu_tinhtrang
                      };
        // đẩy dữ liệu vào gridivew
        grvNghiHan.DataSource = getData;
        grvNghiHan.DataBind();
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin-bao-luu-0");
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvListLop.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhiChiTiet checkdsTrungTam = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                var delete = (from bl in db.tbHocPhiChiTiets where bl.hocphichitiet_ghichu == checkdsTrungTam.hocphichitiet_ghichu select bl);
                db.tbHocPhiChiTiets.DeleteAllOnSubmit(delete);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
                loadDataLop();
            }
        }

    }

    protected void btnXoaBaoLuuHocSinh_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvListHocSinh.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhiChiTiet delete = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                db.tbHocPhiChiTiets.DeleteOnSubmit(delete);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
                loadDataHocSinh();
            }
        }
    }

    protected void btnXoaChuyenLop_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvChuyenLop.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhiChiTiet delete = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                db.tbHocPhiChiTiets.DeleteOnSubmit(delete);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
                loadDataChuyenLop();
            }
        }
    }

    protected void btnXoaNghiHan_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvNghiHan.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbhocsinhtronglop update = (from hstl in db.tbhocsinhtronglops
                                            join bl in db.tbHocPhiChiTiets on hstl.hstl_id equals bl.hstl_id
                                            where bl.hocphichitiet_id == Convert.ToInt32(item)
                                            select hstl).FirstOrDefault();
                update.hstl_hidden = false;
                db.SubmitChanges();
                tbHocPhiChiTiet delete = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                db.tbHocPhiChiTiets.DeleteOnSubmit(delete);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
                loadDataNghiHan();
            }
        }
    }
    protected void btnDuyetLop_ServerClick(object sender, EventArgs e)
    {
       // tối giải quyết
    }

    protected void btnDuyethocSinh_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvListHocSinh.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhiBaoLuu update = (from hpct in db.tbHocPhiBaoLuus where hpct.baoluu_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                update.baoluu_tinhtrang = "Đã duyệt";
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã duyệt", "");
                loadDataNghiHan();
            }
        }
    }

    protected void btnDuyetChuyenLop_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvChuyenLop.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbHocPhiChiTiet update = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                update.baoluu_tinhtrang = "Đã duyệt";
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã duyệt", "");
                loadDataNghiHan();
            }
        }
    }

    protected void btnDuyetNghiHan_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvNghiHan.GetSelectedFieldValues(new string[] { "hocphichitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbhocsinhtronglop update1 = (from hstl in db.tbhocsinhtronglops
                                            join bl in db.tbHocPhiChiTiets on hstl.hstl_id equals bl.hstl_id
                                            where bl.hocphichitiet_id == Convert.ToInt32(item)
                                            select hstl).FirstOrDefault();
                update1.hstl_hidden = true;
                db.SubmitChanges();
                tbHocPhiChiTiet update = (from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_id == Convert.ToInt32(item) select hpct).FirstOrDefault();
                update.baoluu_tinhtrang = "Đã duyệt";
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã duyệt", "");
                loadDataNghiHan();
            }
        }
    }
}