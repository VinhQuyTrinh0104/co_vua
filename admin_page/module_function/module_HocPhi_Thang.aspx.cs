﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocPhi_Thang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    DateTime ngayketthuc;
    bool gioitinh = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            if(dteTuNgay.Value!="")
            loadData();
        }
      
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var getLop = from tt in db.tbLops select tt;

        admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        //admin_User là ROOT or ADMIN thì xem được hết danh sách account
        var getData = from ac in db.tbAccounts
                      join hstl in db.tbhocsinhtronglops on ac.account_id equals hstl.account_id
                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                      join cs in db.tbCosos on l.coso_id equals cs.coso_id
                      join hpct in db.tbHocPhiChiTiets on hstl.hstl_id equals hpct.hstl_id
                      join hp in db.tbHocPhis on hpct.hocphi_id equals hp.hp_id
                      where hp.hocphi_ngaydongtien >= Convert.ToDateTime(dteTuNgay.Value) && hp.hocphi_ngaydongtien <= Convert.ToDateTime(dteDenNgay.Value)
                      select new
                      {
                          cs.coso_name,
                          l.lop_name,
                          ac.account_vn,
                          hpct.hocphichitiet_sotiendadong,
                          hp.hocphi_ngaydongtien,
                          hpct.hocphichitiet_ghichu
                      };

        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        if (getData.Count() > 0)
        {
            lblTongDoanhThu.Text = string.Format("{0:#,0.##}", Convert.ToInt32(getData.Sum(x => x.hocphichitiet_sotiendadong).ToString()));
        }
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        loadData();
    }

   
}