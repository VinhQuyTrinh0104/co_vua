﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_DanhSach : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Request.Cookies["UserName"] != null)
        {

            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                cbbCoSoCaNhan.DataSource = from cs in db.tbCosos select cs;
                cbbCoSoCaNhan.DataBind();
            }
            //loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        rpDanhSach.DataSource = from hstl in db.tbhocsinhtronglops
                                join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                where hstl.hstl_hidden == false && ac.hidden == false
                                && hstl.lop_id == Convert.ToInt16(cbbLopCaNhan.SelectedItem.Value)
                                group hstl by new { hstl.hstl_id, hstl.account_id } into g
                                select new
                                {
                                    hstl_id = g.Key.hstl_id,

                                    account_vn = (from hs1 in db.tbAccounts
                                                  join hsct1 in db.tbhocsinhtronglops on hs1.account_id equals hsct1.account_id
                                                  where hsct1.hstl_id == g.Key.hstl_id
                                                  select hs1).FirstOrDefault().account_vn,
                                    account_en = (from hs2 in db.tbAccounts
                                                  join hsct2 in db.tbhocsinhtronglops on hs2.account_id equals hsct2.account_id
                                                  where hsct2.hstl_id == g.Key.hstl_id
                                                  select hs2).FirstOrDefault().account_en,
                                    so_buoi_dang_ki_moi =
                                                            (from hpct in db.tbHocPhiChiTiets
                                                             join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
                                                             where hpct.hstl_id == g.Key.hstl_id && hstl.hstl_hidden == false
                                                             orderby hpct.hocphichitiet_id descending
                                                             select hpct).FirstOrDefault().hocphichitiet_ngayketthuc <
                                                            (from hpbl in db.tbHocPhiBaoLuus
                                                             join hstl in db.tbhocsinhtronglops on hpbl.hstl_id equals hstl.hstl_id
                                                             where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp" && hstl.hstl_hidden == false
                                                             orderby hpbl.baoluu_id descending
                                                             select hpbl).FirstOrDefault().baoluu_ngayketthuc_hidden ?
                                                            (from hpbl in db.tbHocPhiBaoLuus
                                                             join hstl in db.tbhocsinhtronglops on hpbl.hstl_id equals hstl.hstl_id
                                                             where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp" && hstl.hstl_hidden == false
                                                             orderby hpbl.baoluu_id descending
                                                             select hpbl).FirstOrDefault().baoluu_sobuoi :
                                                            (from hpct in db.tbHocPhiChiTiets
                                                             join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
                                                             where hstl.hstl_id == g.Key.hstl_id && hstl.hstl_hidden == false
                                                             orderby hpct.hocphichitiet_sobuoihoc descending
                                                             select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,

                                    tong_so_buoi_con_lai =
                                                            (from hpct in db.tbHocPhiChiTiets
                                                             join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
                                                             where hpct.hstl_id == g.Key.hstl_id && hstl.hstl_hidden == false
                                                             orderby hpct.hocphichitiet_id descending
                                                             select hpct).FirstOrDefault().hocphichitiet_ngayketthuc <
                                                            (from hpbl in db.tbHocPhiBaoLuus
                                                             join hstl in db.tbhocsinhtronglops on hpbl.hstl_id equals hstl.hstl_id
                                                             where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp" && hstl.hstl_hidden == false
                                                             orderby hpbl.baoluu_id descending
                                                             select hpbl).FirstOrDefault().baoluu_ngayketthuc_hidden ?
                                                            (from hpbl in db.tbHocPhiBaoLuus
                                                             join hstl in db.tbhocsinhtronglops on hpbl.hstl_id equals hstl.hstl_id
                                                             where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp" && hstl.hstl_hidden == false
                                                             orderby hpbl.baoluu_id descending
                                                             select hpbl).FirstOrDefault().baoluu_sobuoi :
                                                            (from hpct in db.tbHocPhiChiTiets
                                                             where hpct.hocsinh_id == g.Key.account_id
                                                             select hpct).Sum(x => x.hocphichitiet_sobuoihoc)
                                                            -
                                                            (from dd in db.tbDiemDanhs
                                                             join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                                                             where hstl.account_id == g.Key.account_id && dd.diemdanh_vang != "Bảo lưu"
                                                             select dd).Count(),
                                    tong_sobuoi_diemdanh =
                                                            ((from hpbl in db.tbHocPhiBaoLuus
                                                              where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp"
                                                              select hpbl).Count() > 0 ?
                                                             (from hpbl in db.tbHocPhiBaoLuus
                                                              where hpbl.hstl_id == g.Key.hstl_id && hpbl.baoluu_loai == "Chuyển lớp"
                                                              select hpbl).FirstOrDefault().baoluu_sobuoi :
                                                            (from hpct in db.tbHocPhiChiTiets
                                                             join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
                                                             where hstl.hstl_id == g.Key.hstl_id && hstl.hstl_hidden == false
                                                             orderby hpct.hocphichitiet_sobuoihoc descending
                                                             select hpct).FirstOrDefault().hocphichitiet_sobuoihoc)
                                                            -
                                                           (
                                                           (from hpct in db.tbHocPhiChiTiets
                                                            join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
                                                            where hpct.hocsinh_id == g.Key.account_id
                                                            select hpct).Sum(x => x.hocphichitiet_sobuoihoc)
                                                            -
                                                            (from dd in db.tbDiemDanhs
                                                             join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                                                             where hstl.account_id == g.Key.account_id && dd.diemdanh_vang != "Bảo lưu"
                                                             select dd).Count()
                                                           )

                                };
        rpDanhSach.DataBind();
        rpDiemDanh.DataSource = from hstl in db.tbhocsinhtronglops
                                join dd in db.tbDiemDanhs on hstl.hstl_id equals dd.hstl_id
                                where hstl.hstl_hidden == false
                                group hstl by hstl.hstl_id into g
                                select new
                                {
                                    hstl_id = g.Key
                                };
        rpDiemDanh.DataBind();
    }
    protected void rpDiemDanh_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpDiemDanhChiTiet = e.Item.FindControl("rpDiemDanhChiTiet") as Repeater;
        int hstl_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "hstl_id").ToString());
        var getDiemDanh = (from dd in db.tbDiemDanhs
                           join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                           join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                           where dd.hstl_id == hstl_id
                           select new
                           {
                               hs.account_vn,
                               dd.diemdanh_vang,
                               dd.diemdanh_ngay
                           });
        rpDiemDanhChiTiet.DataSource = getDiemDanh;
        rpDiemDanhChiTiet.DataBind();
    }
    protected void cbbCoSoCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbbLopCaNhan.DataSource = from l in db.tbLops
                                  where l.coso_id == Convert.ToInt16(cbbCoSoCaNhan.SelectedItem.Value)
                                    && l.hidden == false && l.lop_tinhtrang == null && !l.lop_name.Contains("GS")
                                  select l;
        cbbLopCaNhan.DataBind();
    }
    protected void cbbLopCaNhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbbLopCaNhan.Text != "")
        {
            loadData();
        }
    }
}
// sẽ lấy số buổi còn lại của khóa trước + số buổi đăng ký khóa mới

//tong_so_buoi_dang_ki = (from hpct in db.tbHocPhiChiTiets
//                        join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
//                        where hpct.hocsinh_id == g.Key.account_id && (hstl.hstl_hidden == true)
//                                                        select hpct).Count() > 0 ?
//                                                        //"123"
//                                                        // Trường hợp học sinh cũ: lấy sổ buổi đăng kí cũ  - điểm danh ra cái còn lại rồi
//                                                        // + với sổ buổi đăng kí mới
//                                                        (from hpct in db.tbHocPhiChiTiets
//                                                         join hstl in db.tbhocsinhtronglops on hpct.hstl_id equals hstl.hstl_id
//                                                         where hpct.hocsinh_id == g.Key.account_id && (hstl.hstl_hidden == true)
//                                                         select hpct).Sum(x => x.hocphichitiet_sobuoihoc)
//                                                        -
//                                                        (from dd in db.tbDiemDanhs
//                                                         join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
//                                                         where hstl.account_id == g.Key.account_id && (hstl.hstl_hidden == true) && dd.diemdanh_vang != "Bảo lưu"
//                                                         select dd).Count()
//                                                         +
//                                                          (from hpct in db.tbHocPhiChiTiets
//                                                           where hpct.hstl_id == g.Key.hstl_id
//                                                           orderby hpct.hocphichitiet_id descending
//                                                           select hpct).FirstOrDefault().hocphichitiet_sobuoihoc==null? 0 : (from hpct in db.tbHocPhiChiTiets
//                                                                                                                             where hpct.hstl_id == g.Key.hstl_id
//                                                                                                                             orderby hpct.hocphichitiet_id descending
//                                                                                                                             select hpct).FirstOrDefault().hocphichitiet_sobuoihoc
//                                                           :
//                                                            //"bbb"
//                                                            (from hpct in db.tbHocPhiChiTiets
//                                                             where hpct.hstl_id == g.Key.hstl_id && hpct.hocphichitiet_sobuoihoc != null
//                                                             orderby hpct.hocphichitiet_id descending
//                                                             select hpct).FirstOrDefault().hocphichitiet_sobuoihoc,
//tong_sobuoi_diemdanh = (from dd1 in db.tbDiemDanhs
//                        where dd1.hstl_id == g.Key.hstl_id && dd1.diemdanh_vang != "Bảo lưu"
//                        select dd1).Count(),
