﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_Xuat_Excel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
        var newFile = new FileInfo("D:/2023/toantuduy/admin_page/module_function/newfile.xlsx");
        using (var package = new ExcelPackage(newFile))
        {
            // Thêm một trang tính mới vào file Excel
            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
            // Điền dữ liệu vào ô A1 của trang tính
            worksheet.Cells["A1"].Value = "Hello World 1!";
            worksheet.Cells["A2"].Value = "Hello World 2!";
            worksheet.Cells["A3"].Value = "Hello World 3!";
            worksheet.Cells["B1"].Value = "Hello World 4!";
            worksheet.Cells["B2"].Value = "Hello World 5!";
            worksheet.Cells["B3"].Value = "Hello World 6!";
            // Lưu file Excel
            package.Save();
            string filePath = Server.MapPath("newfile.xlsx");

            // Gửi file Excel về trình duyệt của người dùng
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filePath));
            Response.TransmitFile(filePath);
            Response.End();
            DeleteExcelFile();
        }
    }
    protected void DeleteExcelFile()
    {
        string filePath = Server.MapPath("D:/2023/toantuduy/admin_page/module_function/newfile.xlsx");
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
    }

}