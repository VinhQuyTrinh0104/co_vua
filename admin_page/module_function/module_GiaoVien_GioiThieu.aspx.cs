﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoVien_GioiThieu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            admin_User getData = (from u in db.admin_Users where u.username_id == Convert.ToInt32(RouteData.Values["id"]) select u).FirstOrDefault();
            edtnoidung.Html = getData.username_description;
            imgPreview1.Src = getData.username_image;
        }
      
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        admin_User update = (from u in db.admin_Users where u.username_id == Convert.ToInt32(RouteData.Values["id"]) select u).FirstOrDefault();
        update.username_description = edtnoidung.Html;
        db.SubmitChanges();
        alert.alert_Success(Page, "cập nhật thành công", "");
    }
}