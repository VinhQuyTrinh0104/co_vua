﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_Account.aspx.cs" Inherits="admin_page_module_function_module_Account" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtFullName.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên học sinh không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false; return true;
        };
    </script>
    <style>
        input[type=number] {
            width: 30%;
        }

        input[type=date] {
            width: 60%;
        }

        .radio {
            display: block;
        }

        .input-group-prepend, .input-group-append {
            border-radius: 0;
        }

        .input-group-append {
            margin-left: -1px;
        }

        .input-group-prepend, .input-group-append {
            display: flex;
        }

        .input-group-text {
            padding-top: 0;
            padding-bottom: 0;
            align-items: center;
            border-radius: 0;
        }

        .input-group-text {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            margin-bottom: 0;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #596882;
            text-align: center;
            white-space: nowrap;
            background-color: #e3e7ed;
            border: 1px solid #cdd4e0;
            border-radius: 1px;
        }

        .input-group-append > .input-group-text {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }

        #basic-addon2 {
            font-weight: 600;
            font-size: 18px;
        }

        .dxpc-content {
            height: 83vh !important;
        }

        .col-12 {
            display: flex;
            align-items: center !important;
        }
    </style>
    <%--<div id="menuNhanVien" runat="server">
        <uc1:MenuNhanVien ID="MenuNhanVien1" runat="server" />
    </div>
    <div id="menuQuanTri" runat="server">
        <uc1:MenuQuanTri ID="MenuQuanTri1" runat="server" />
    </div>
     <div id="menuGiaoVien" runat="server">
        <uc1:MenuGiaoVien ID="MenuGiaoVien1" runat="server" />
    </div>--%>
    <div class="card card-block box-admin">
        <div class="form-group-name">
            QUẢN LÝ THÔNG TIN HỌC SINH
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <div class="col-sm-10">
                    <asp:UpdatePanel ID="udButton" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                            <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                            <input type="submit" id="btnDlt" runat="server" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                            <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12 form-group">
                    <label class="col-1">Chọn tháng:</label>
                    <div class="col-2">
                        <asp:DropDownList ID="ddlThang" runat="server" CssClass="form-control">
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <label class="col-1">Chọn năm:</label>
                    <div class="col-2">
                        <asp:DropDownList ID="ddlNam" runat="server" CssClass="form-control">
                            <asp:ListItem Value="2022">2022</asp:ListItem>
                            <asp:ListItem Value="2023">2023</asp:ListItem>
                            <asp:ListItem Value="2024">2024</asp:ListItem>
                            <asp:ListItem Value="2025">2025</asp:ListItem>
                            <asp:ListItem Value="2026">2026</asp:ListItem>
                            <asp:ListItem Value="2027">2027</asp:ListItem>
                            <asp:ListItem Value="2028">2028</asp:ListItem>
                            <asp:ListItem Value="2029">2029</asp:ListItem>
                            <asp:ListItem Value="2030">2030</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-2">
                        &nbsp;&nbsp; &nbsp;&nbsp;<a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                    <div class="col-2">
                        <a href="#" id="btnXuatExcel" runat="server" onserverclick="btnXuatExcel_ServerClick" class="btn btn-primary">Xuất Excel</a>
                    </div>
                </div>
            </div>
            <div class="form-group table-responsive">
                <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="account_id" Width="100%">
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn Caption="Ngày nhập" FieldName="account_ngaydangky" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Cơ sở" FieldName="coso_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Lớp" FieldName="lop_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Họ và tên học sinh" FieldName="account_vn" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Enghlish name" FieldName="account_en" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giới tính" FieldName="account_gioitinh" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Số điện thoại học sinh" FieldName="account_phone" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Tên ba" FieldName="account_tenba" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Số điện thoại ba" FieldName="account_sdtba" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Tên mẹ" FieldName="account_tenme" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Số điện thoại mẹ" FieldName="account_sdtme" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>

                    </Columns>
                    <ClientSideEvents RowDblClick="btnChiTiet" />
                    <SettingsSearchPanel Visible="true" />
                    <SettingsBehavior AllowFocusedRow="true" />
                    <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                    <SettingsLoadingPanel Text="Đang tải..." />
                    <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} học sinh)"></SettingsPager>
                </dx:ASPxGridView>
            </div>
        </div>
        <dx:ASPxPopupControl ID="popupControl" runat="server" Width="1200px" Height="720px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
            HeaderText="Thông tin học sinh" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <asp:UpdatePanel ID="udPopup" runat="server">
                        <ContentTemplate>
                            <div class="popup-main">
                                <div class="div_content col-12">
                                    <div class="row">



                                        <div class="col-6" style="margin: 0">
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Cơ sở:*</label>
                                                <div class="col-10">
                                                    <dx:ASPxComboBox ID="ddlcoso" runat="server" ValueType="System.Int32" TextField="coso_name" ValueField="coso_id" ClientInstanceName="ddlcoso" OnSelectedIndexChanged="ddlcoso_SelectedIndexChanged" AutoPostBack="true"></dx:ASPxComboBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Họ Và Tên:*</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtFullName" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Ngày Sinh:*</label>
                                                <div class="col-10">
                                                    <input type="date" id="dteNgaySinh" runat="server" class="form-control boxed" style="width: 95%" />
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">SĐT:</label>
                                                <div class="col-10">
                                                    <input type="text" id="txtSDTHocSinh" runat="server" class="form-control boxed" style="width: 95%" />
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Trường học:</label>
                                                <div class="col-10">
                                                    <input type="text" id="txtTruongHoc" runat="server" class="form-control boxed" style="width: 95%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Lớp: *</label>
                                                <div class="col-10">
                                                    <dx:ASPxComboBox ID="ddlLop" runat="server" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddlLop" CssClass="" NullText="Chọn lớp" Width="95%"></dx:ASPxComboBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">English name:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtenEn" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Giới tính:</label>
                                                <div class="col-10">
                                                    <input type="radio" id="rdNam" runat="server" name="fav_language" value="true" />
                                                    <label for="html">Nam</label>
                                                    &nbsp; &nbsp;
                                                <input type="radio" id="rdNu" runat="server" name="fav_language" value="false" />
                                                    <label for="css">Nữ</label>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Email:</label>
                                                <div class="col-10">
                                                    <input type="text" id="txtEmailHocSinh" runat="server" class="form-control boxed" style="width: 100%" />
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Nơi sinh:</label>
                                                <div class="col-10">
                                                    <input type="text" id="txtNoiSinh" runat="server" class="form-control boxed" style="width: 100%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Chổ ở hiện tại:</label>
                                            <div class="col-10">
                                                <input type="text" id="txtChoOHienTai" runat="server" class="form-control boxed" style="width: 96.5%" />
                                            </div>
                                        </div>
                                        <div class="col-6" style="margin: 0">
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Tên ba:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtTenBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">SĐT ba:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtsdtBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Nghề nghiệp:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtNgheNghiepBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Email:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtEmailBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Tên mẹ:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtTenMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">SĐT mẹ:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtSDTMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Nghề nghiệp mẹ:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtNgheNghiepMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-12 form-group">
                                                <label class="col-2 form-control-label">Email mẹ:</label>
                                                <div class="col-10">
                                                    <asp:TextBox ID="txtEmailMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Mong muốn nhu cầu của ba mẹ khi cho con học tại VJLC:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtMongMuon" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="96.5%"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </dx:PopupControlContentControl>
            </ContentCollection>
            <FooterContentTemplate>
                <div class="mar_but button">
                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                </div>
            </FooterContentTemplate>
            <ContentStyle>
                <Paddings PaddingBottom="0px" />
            </ContentStyle>
        </dx:ASPxPopupControl>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



