﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GamHocTap_module_LoaiGameLatHinh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            loadData();
            ddlSach.DataSource = from s in db.tbSLLDT_Saches
                                 select s;
            ddlSach.DataBind();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void loadData()
    {
        var getData = (from tx in db.tbgame_LatHinhs
                       orderby tx.unit_id descending
                       group tx by tx.lathinh_code into k
                       select new
                       {
                           lathinh_code = k.Key,
                           sach_title = (from c in db.tbSLLDT_Saches
                                         where c.sach_id == k.FirstOrDefault().sach_id
                                         select c.sach_title).FirstOrDefault(),
                           unit_title = (from c in db.tbSLLDT_Units
                                         where c.unit_id == k.FirstOrDefault().unit_id
                                         select c.unit_title).FirstOrDefault(),
                           lathinh_image = k.First().lathinh_image,
                           lathinh_mp3 = k.First().lathinh_mp3,
                       });
        grvList.DataSource = getData;
        grvList.DataBind();

        //var listChuDe = from cd in db.tbGameMamNon_ChuDes
        //                where cd.Group_id == null
        //                select cd;
        //ddlChuDe.DataSource = listChuDe;
        //ddlChuDe.DataBind();
    }
    protected bool themImg(string img, string mp3, string code)
    {
        tbgame_LatHinh insert = new tbgame_LatHinh();
        insert.lathinh_image = img;
        insert.lathinh_mp3 = mp3;
        insert.sach_id = Convert.ToInt32(ddlSach.SelectedItem.Value);
        insert.unit_id = Convert.ToInt32(ddlUnit.SelectedItem.Value);
        insert.lesson_id = Convert.ToInt32(ddlLesson.SelectedItem.Value);
        insert.lathinh_code = code;
        db.tbgame_LatHinhs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        //var getChude = (from cd in db.tbGameMamNon_ChuDes
        //                where cd.mamnonchude_id == Convert.ToInt32(ddlChuDe.SelectedItem.Value)
        //                select cd.mamnonchude_name);
        var getLastNameBlock = (from img in db.tbgame_LatHinhs
                                    //where img.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value) && img.unit_id == Convert.ToInt32(ddlUnit.SelectedItem.Value)
                                orderby img.lathinh_id descending
                                select img);
        string code = "lathinh_";
        int lastBlock = 0;
        int number = 0;
        if (getLastNameBlock.Count() > 0)
            number = Convert.ToInt32(getLastNameBlock.FirstOrDefault().lathinh_code.Remove(0, code.Length)) + 1;
        string[] arrChecked = txtDanhSachChecked.Value.Split(',');
        if (Session["_id"].ToString() == "0")
        {
            foreach (var item in arrChecked)
            {
                for (int j = 0; j < 2; j++)
                {
                    var checkImage = (from img in db.tbgame_HinhAnhGameTracNghiems
                                      where img.hinhanhgame_id == Convert.ToInt32(item)
                                      select img).FirstOrDefault();
                    themImg(checkImage.hinhanhgame_image, checkImage.hinhanhgame_mp3, code + number);
                }
                number++;
            }
            alert.alert_Success(Page, "Thêm thành công", "");
        }
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        txtCountChecked.Value = "";
        txtDanhSachChecked.Value = "";
        ddlSach.SelectedIndex = -1;
        ddlUnit.SelectedIndex = -1;
        ddlLesson.SelectedIndex = -1;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        //string _id = grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "trucxanh_code" }).ToString();
        //Session["_id"] = _id;

        //var getData = (from n in db.tbGame_TrucXanhs
        //               join cd in db.tbChuDes on n.chude_id equals cd.chude_id
        //               group n by new { n.blockgame, n.trucxanh_image } into k
        //               where k.Key.blockgame == _id
        //               select new
        //               {
        //                   blockgame = k.Key.blockgame,
        //                   trucxanh_image = k.Key.trucxanh_image,
        //                   chude_name = (from cdd in db.tbChuDes
        //                                 join tx in db.tbGame_TrucXanhs on cdd.chude_id equals tx.chude_id
        //                                 where tx.blockgame == k.Key.blockgame
        //                                 select cdd.chude_name).FirstOrDefault(),
        //               });
        //ddlChuDe.Text = getData.FirstOrDefault().chude_name;
        //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected bool Linq_Xoa(string news_id)
    {
        var del = (from tx in db.tbgame_LatHinhs where tx.lathinh_code == news_id select tx);
        db.tbgame_LatHinhs.DeleteAllOnSubmit(del);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lathinh_code" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                if (Linq_Xoa(item.ToString()))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "grvList.Refresh(); ", true);
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void ddlSach_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlUnit.DataSource = from s in db.tbSLLDT_Units
                             where s.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value)
                             select s;
        ddlUnit.DataBind();
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUnit.SelectedItem.Value != null)
        {
            var getHinhAnh = from img in db.tbgame_HinhAnhGameTracNghiems
                             where img.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value)
                             && img.unit_id == Convert.ToInt32(ddlUnit.SelectedItem.Value)
                             select img;
            rpHinhAnh.DataSource = getHinhAnh;
            rpHinhAnh.DataBind();
            //txtCountDapAnSai.Value = "";
            //txtDapAnDung.Value = "";
            //txtDapAnSai.Value = "";
            ddlLesson.DataSource = from l in db.tbSLLDT_lessons
                                   where l.unit_id == Convert.ToInt32(ddlUnit.SelectedItem.Value)
                                   select l;
            ddlLesson.DataBind();
        }
    }
}