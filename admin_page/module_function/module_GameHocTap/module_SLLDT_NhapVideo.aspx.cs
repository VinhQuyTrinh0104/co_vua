﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_SLLDT_module_SLLDT_NhapVideo : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    private int sach_id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        LoadData();
        loadSach();
    }

    private void LoadData()
    {
        var list = from s in db.tbSLLDT_Saches
                   join u in db.tbSLLDT_Units on s.sach_id equals u.sach_id
                   join l in db.tbSLLDT_lessons on u.unit_id equals l.unit_id
                   join v in db.tbSLLDT_Videos on l.lesson_id equals v.lesson_id
                   orderby s.sach_id ascending
                   select new
                   {
                       s.sach_id,
                       s.sach_title,
                       u.unit_id,
                       u.unit_title,
                       l.lesson_id,
                       l.lesson_title,
                       v.video_id,
                       v.video_link,
                       v.video_content
                   };
        grvList.DataSource = list;
        grvList.DataBind();
    }
    private void loadSach()
    {
        var listSach = from s in db.tbSLLDT_Saches
                       select s;
        ddlSach.DataSource = listSach;
        ddlSach.DataBind();
    }
    private void loadUnit(int id)
    {
        var listUnit = from u in db.tbSLLDT_Units
                       join s in db.tbSLLDT_Saches on u.sach_id equals s.sach_id
                       where u.sach_id == id
                       select u;
        ddlUnit.DataSource = listUnit;
        ddlUnit.DataBind();
    }
    private void loadLesson(int id)
    {
        var listLesson = from l in db.tbSLLDT_lessons
                         //join u in db.tbSLLDT_Units on l.unit_id equals u.unit_id
                         where l.unit_id == id
                         select l;
        ddlLesson.DataSource = listLesson;
        ddlLesson.DataBind();

    }
    private void setNull()
    {
        ddlSach.SelectedIndex = -1;
        ddlUnit.SelectedIndex = -1;
        ddlLesson.SelectedIndex = -1;
        txtVideoContent.Value = "";
        txtVideoLink.Value = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        setNull();
        Session["_id"] = 0;
        //loadSach();
        //ddlLesson.Enabled = false;
        //ddlUnit.Enabled = false;
        //txtVideoLink.Disabled = true;
        //txtVideoContent.Disabled = true;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "video_id" }));
        Session["_id"] = _id;
        var getItem = (from vd in db.tbSLLDT_Videos
                       join l in db.tbSLLDT_lessons on vd.lesson_id equals l.lesson_id
                       join u in db.tbSLLDT_Units on l.unit_id equals u.unit_id
                       join s in db.tbSLLDT_Saches on u.sach_id equals s.sach_id
                       where vd.video_id == _id
                       select new
                       {
                           vd.video_id,
                           vd.video_link,
                           vd.video_content,
                           l.lesson_id,
                           u.unit_id,
                           s.sach_id
                       }).SingleOrDefault();
        loadSach();
        loadUnit(getItem.sach_id);
        loadLesson(getItem.unit_id);
        ddlSach.Items.FindByValue(getItem.sach_id).Selected = true;
        ddlUnit.Items.FindByValue(getItem.unit_id).Selected = true;
        ddlLesson.Items.FindByValue(getItem.lesson_id).Selected = true;
        txtVideoLink.Value = getItem.video_link;
        txtVideoContent.Value = getItem.video_content;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "video_id" }));
        var delete = (from vd in db.tbSLLDT_Videos
                      where vd.video_id == _id
                      select vd).SingleOrDefault();
        db.tbSLLDT_Videos.DeleteOnSubmit(delete);
        db.SubmitChanges();
        LoadData();
        alert.alert_Success(Page, "", "Xóa thành công");
    }

    protected void ddlSach_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlUnit.Enabled = true;
        //ddlLesson.Enabled = false;
        //txtVideoLink.Disabled = true;
        //txtVideoContent.Disabled = true;
        //sach_id = Convert.ToInt32(ddlSach.Value);
        loadUnit(Convert.ToInt32(ddlSach.Value));
        //ddlUnit.Items.Add("--vui lòng chọn--");
        //ddlUnit.Items.FindByText("--vui lòng chọn--").Selected = true;
        //loadSach();
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlLesson.Enabled = true;
        int unit_id = Convert.ToInt32(ddlUnit.Value);
        loadLesson(unit_id);
        //ddlLesson.Items.FindByText("--vui lòng chọn--").Selected = true;
        //txtVideoLink.Disabled = true;
        //txtVideoContent.Disabled = true;
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (Session["_id"].ToString() == "0")
        {
            tbSLLDT_Video insert = new tbSLLDT_Video();
            insert.lesson_id = Convert.ToInt32(ddlLesson.Value);
            insert.book_id = Convert.ToInt32(ddlSach.Value);
            insert.unit_id = Convert.ToInt32(ddlUnit.Value);
            insert.video_link = txtVideoLink.Value;
            insert.video_content = txtVideoContent.Value;
            db.tbSLLDT_Videos.InsertOnSubmit(insert);
            db.SubmitChanges();
            alert.alert_Success(Page, "", "Thêm thành công");
            popupControl.ShowOnPageLoad = false;
            LoadData();
        }
        else
        {
            var update = (from vd in db.tbSLLDT_Videos
                          where vd.video_id == Convert.ToInt32(Session["_id"].ToString())
                          select vd).SingleOrDefault();
            update.video_content = txtVideoContent.Value;
            update.video_link = txtVideoLink.Value;
            update.lesson_id = Convert.ToInt32(ddlLesson.Value);
            update.book_id = Convert.ToInt32(ddlSach.Value);
            update.unit_id = Convert.ToInt32(ddlUnit.Value);
            db.SubmitChanges();
            alert.alert_Success(Page, "", "Cập nhật thành công");
            popupControl.ShowOnPageLoad = false;
            LoadData();
        }
    }

}