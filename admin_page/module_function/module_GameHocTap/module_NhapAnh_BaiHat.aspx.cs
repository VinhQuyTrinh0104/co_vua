﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GameHocTap_module_NhapAnh_BaiHat : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {

        ddlBook.DataSource = from tb in db.tbSLLDT_Saches select tb;
        ddlBook.DataBind();
        ddlUnit.DataSource = from tb in db.tbSLLDT_Units select tb;
        ddlUnit.DataBind();
        var getData = from a in db.tbGame_SingASongs
                      join s in db.tbSLLDT_Saches on a.book_id equals s.sach_id
                      join u in db.tbSLLDT_Units on a.unit_id equals u.unit_id
                      join l in db.tbSLLDT_lessons on a.lesson_id equals l.lesson_id
                      orderby a.song_id descending
                      select new
                      {
                          a.song_id,
                          s.sach_title,
                          u.unit_title,
                          a.song_image,
                          a.song_mp3,
                          s.sach_id,
                          u.unit_id,
                          l.lesson_id,
                          l.lesson_title,
                      };

        grvList.DataSource = getData;
        grvList.DataBind();

    }
    private void setNULL()
    {
        txtHinhAnh.Text = "";
        txtAmThanh.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show()", true);
        //loadData();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "song_id" }));
        Session["_id"] = _id;
        var getData = (from a in db.tbGame_SingASongs
                       join s in db.tbSLLDT_Saches on a.book_id equals s.sach_id
                       join u in db.tbSLLDT_Units on a.unit_id equals u.unit_id
                       join l in db.tbSLLDT_lessons on a.lesson_id equals l.lesson_id
                       where a.song_id == _id
                       select new
                       {
                           a.song_id,
                           s.sach_title,
                           u.unit_title,
                           a.song_image,
                           a.song_mp3,
                           l.lesson_title,
                           s.sach_id,
                           u.unit_id,
                           l.lesson_id,
                       }).Single();
        ddlBook.Value = Convert.ToInt32(getData.sach_id);
        ddlUnit.Value = Convert.ToInt32(getData.unit_id);
        ddlLesson.Value = Convert.ToInt32(getData.lesson_id);
        txtHinhAnh.Text = getData.song_image;
        txtAmThanh.Text = getData.song_mp3;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "song_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbGame_SingASong checkImage = (from a in db.tbGame_SingASongs where a.song_id == Convert.ToInt32(item) select a).SingleOrDefault();
                db.tbGame_SingASongs.DeleteOnSubmit(checkImage);
                db.SubmitChanges();
                alert.alert_Success(Page, "Xóa thành công", "");
            }
            loadData();
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txtHinhAnh.Text != "" || txtAmThanh.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                tbGame_SingASong insert = new tbGame_SingASong();
                insert.book_id = Convert.ToInt32(ddlBook.SelectedItem.Value);
                insert.unit_id = Convert.ToInt32(ddlUnit.SelectedItem.Value);
                insert.lesson_id = Convert.ToInt32(ddlLesson.SelectedItem.Value);
                insert.song_image = txtHinhAnh.Text;
                insert.song_mp3 = txtAmThanh.Text;
                db.tbGame_SingASongs.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Thêm thành công", "");
                loadData();
            }
            else
            {
                tbGame_SingASong update = db.tbGame_SingASongs.Where(x => x.song_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
                update.book_id = Convert.ToInt32(ddlBook.SelectedItem.Value);
                update.unit_id = Convert.ToInt32(ddlUnit.SelectedItem.Value);
                update.lesson_id = Convert.ToInt32(ddlLesson.SelectedItem.Value);
                update.song_image = txtHinhAnh.Text;
                update.song_mp3 = txtAmThanh.Text;
                alert.alert_Success(Page, "Cập nhật thành công", "");
                db.SubmitChanges();
                loadData();
            }
            popupControl.ShowOnPageLoad = false;
        }
    }

    protected void ddlBook_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlBook.SelectedIndex > -1)
        {
            var getDataUnit = from s in db.tbSLLDT_Units
                              where s.sach_id == Convert.ToInt16(ddlBook.SelectedItem.Value)
                              select s;
            ddlUnit.DataSource = getDataUnit;
            ddlUnit.DataBind();
        }
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUnit.SelectedIndex > -1)
        {
            var getDataLesson = from s in db.tbSLLDT_lessons
                                where s.unit_id == Convert.ToInt16(ddlUnit.SelectedItem.Value)
                                select s;
            ddlLesson.DataSource = getDataLesson;
            ddlLesson.DataBind();
        }
    }
}