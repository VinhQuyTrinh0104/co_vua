﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_CapNhatName.aspx.cs" Inherits="admin_page_module_function_module_GameHocTap_module_CapNhatName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="card card-block box-admin">
                <div class="form-group-name">
                    CẬP NHẬT TÊN HÌNH ẢNH TRẮC NGHIỆM
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th>Image</th>
                        <th>Mp3</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                    <asp:Repeater runat="server" ID="rpList">
                        <ItemTemplate>
                            <tr>
                                <th>
                                    <img src="<%#Eval("tracnghiem_traloi_image")%>" width="140" height="100" class="jus" />

                                </th>
                                <th>
                                    <audio src="<%#Eval("tracnghiem_traloi_mp3")%>" controls="controls"></audio>
                                </th>
                                <th>
                                    <input type="text" name="name" id="txt_<%#Eval("tracnghiem_traloi_id")%>" value="<%#Eval("tracnghiem_traloi_name")%>" />
                                </th>
                                <td>
                                    <a href="#" class="btn btn-primary" onclick="capNhat(<%#Eval("tracnghiem_traloi_id")%>)">Lưu</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
             <a href="#" class="btn btn-success" onclick="return checkName()">Lưu</a>
            <input type="text" id="txtID" runat="server" />
            <input type="text" id="txtDanhSach" runat="server" />
            <input type="text" id="txtNoiDung" runat="server" />
            <input type="text" id="txtName" runat="server" />
            <a href="#" id="btnLuu" runat="server" onserverclick="btnLuu_ServerClick"></a>
            <a href="#" id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function capNhat(id) {
            document.getElementById('<%=txtID.ClientID%>').value = id;
            document.getElementById('<%=txtName.ClientID%>').value = document.getElementById("txt_" + id).value;
            document.getElementById('<%=btnLuu.ClientID%>').click();
        }
        function checkName() {
            //debugger;
          <%--  if (document.getElementById("<%=dteNgay.ClientID%>").value == "") {
                swal("Vui lòng chọn ngày nhập!");
                return false
            }--%>
            let str_danhsachID = document.getElementById("<%=txtDanhSach.ClientID%>").value.split(",");
            var ghichu = [];
            for (let i = 0; i < str_danhsachID.length; i++) {
                var _ghichu = document.getElementById("txt_" + str_danhsachID[i]).value;
                ghichu.push(_ghichu);
            }
            document.getElementById("<%=txtNoiDung.ClientID%>").value = ghichu.join("|");
            document.getElementById("<%=btnSave.ClientID%>").click();
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

