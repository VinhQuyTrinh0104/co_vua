﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_SLLDT_module_SLLDT_NhapLesson : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    int vitri;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {

        ddlBook.DataSource = from tb in db.tbSLLDT_Saches select tb;
        ddlBook.DataBind();
        ddlUnit.DataSource = from tb in db.tbSLLDT_Units select tb;
        ddlUnit.DataBind();
        var getData = from l in db.tbSLLDT_lessons
                      join u in db.tbSLLDT_Units on l.unit_id equals u.unit_id
                      join s in db.tbSLLDT_Saches on u.sach_id equals s.sach_id
                      orderby l.lesson_id descending
                      select new
                      {
                          l.lesson_id,
                          s.sach_title,
                          u.unit_title,
                          l.lesson_title,
                          l.lesson_link,
                          l.lesson_back,
                          l.lesson_next,
                          s.sach_id,
                          u.unit_id,
                          l.lesson_avatar,
                          l.lesson_tuvung,
                      };

        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txtLinkLesson.Text = "";
        txtLinkBack.Text = "";
        txtLinkNext.Text = "";
        txtLessonTitle.Text = "";
        
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lesson_id" }));
        Session["_id"] = _id;
        var getData = (from l in db.tbSLLDT_lessons
                       join u in db.tbSLLDT_Units on l.unit_id equals u.unit_id
                       join s in db.tbSLLDT_Saches on u.sach_id equals s.sach_id
                       where l.lesson_id == _id
                       orderby l.lesson_id descending
                       select new
                       {
                           l.lesson_id,
                           s.sach_title,
                           u.unit_title,
                           l.lesson_link,
                           l.lesson_title,
                           l.lesson_back,
                           l.lesson_next,
                           s.sach_id,
                           u.unit_id,
                           l.lesson_avatar,
                           l.lesson_tuvung,
                       }).SingleOrDefault();
        //ddlBook.Text = getData.sach_title;
        //ddlUnit.Text = getData.unit_title;
        ddlBook.Value = Convert.ToInt32(getData.sach_id);
        ddlUnit.Value = Convert.ToInt32(getData.unit_id);
        txtLessonTitle.Text = getData.lesson_title;
        txtLinkLesson.Text = getData.lesson_link;
        txtLinkNext.Text = getData.lesson_next;
        txtLinkBack.Text = getData.lesson_back;
        txtLessonAvatar.Text = getData.lesson_avatar;
        txtTuVung.Text = getData.lesson_tuvung;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_NhapLesson cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lesson_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_NhapLesson();
                tbSLLDT_lesson checkImage = (from l in db.tbSLLDT_lessons where l.lesson_id == Convert.ToInt32(item) select l).SingleOrDefault();
                //string _image = checkImage.hinhanhgame_image.Split('/')[4];
                //string pathToFiles = Server.MapPath(_image);
                //delete(pathToFiles);
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txtLessonTitle.Text != "" || txtLinkLesson.Text != "" || txtLinkBack.Text != "" || txtLinkNext.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_NhapLesson cls = new cls_NhapLesson();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {

                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                else
                {
                }
                if (cls.Linq_Them(Convert.ToInt32(ddlBook.SelectedItem.Value), Convert.ToInt32(ddlUnit.SelectedItem.Value), txtLessonTitle.Text, txtLinkLesson.Text, txtLinkBack.Text, txtLinkNext.Text, txtLessonAvatar.Text, txtTuVung.Text))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();

                }
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), Convert.ToInt32(ddlBook.SelectedItem.Value), Convert.ToInt32(ddlUnit.SelectedItem.Value), txtLessonTitle.Text, txtLinkLesson.Text, txtLinkBack.Text, txtLinkNext.Text, txtLessonAvatar.Text, txtTuVung.Text))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
        }
    }
    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }


    protected void ddlBook_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getDataUnit = from s in db.tbSLLDT_Units
                          where s.sach_id == Convert.ToInt16(ddlBook.SelectedItem.Value)
                          select s;
        ddlUnit.DataSource = getDataUnit;
        ddlUnit.DataBind();
    }
}