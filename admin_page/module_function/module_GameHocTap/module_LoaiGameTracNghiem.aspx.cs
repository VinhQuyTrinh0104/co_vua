﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GameHocTap_module_LoaiGameTracNghiem : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected string style;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            ddlSach.DataSource = from s in db.tbSLLDT_Saches
                                 select s;
            ddlSach.DataBind();
            //if (Convert.ToInt32(ddlSach.SelectedItem.Value) > 0)
            //{
            //    ddlUnit.DataSource = from u in db.tbSLLDT_Units
            //                         where u.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value)
            //                         select u;
            //    ddlUnit.DataBind();
            //}
            //if (Convert.ToInt32(ddlUnit.SelectedItem.Value) > 0)
            //{

            //}
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void loadData()
    {
        //var getData = (from cd in db.tbChuDes
        //               join nb in db.tbgame_NhanBiets on cd.chude_id equals nb.chude_id
        //               orderby nb.nhanbiet_id descending
        //               select new
        //               {
        //                   cd.chude_id,
        //                   cd.chude_name,
        //                   nb.nhanbiet_name,
        //                   nb.nhanbiet_id,
        //                   //nb.nhanbiet_image,
        //               }); 
        var getData = (from ch in db.tbgame_TracNghiem_CauHois
                       join s in db.tbSLLDT_Saches on ch.sach_id equals s.sach_id
                       join u in db.tbSLLDT_Units on ch.unit_id equals u.unit_id
                       //join l in db.tbSLLDT_lessons on ch.lesson_id equals l.lesson_id
                       orderby ch.tracnghiem_cauhoi_id descending
                       select new
                       {
                           ch.tracnghiem_cauhoi_id,
                           ch.tracnghiem_cauhoi_mp3,
                           dapandung = (from d in db.tbgame_TracNghiem_TraLois
                                        where d.tracnghiem_cauhoi_id == ch.tracnghiem_cauhoi_id && d.tracnghiem_traloi_dung == "đúng"
                                        select d.tracnghiem_traloi_image).First(),
                           amthanhdung = (from d in db.tbgame_TracNghiem_TraLois
                                          where d.tracnghiem_cauhoi_id == ch.tracnghiem_cauhoi_id && d.tracnghiem_traloi_dung == "đúng"
                                          select d.tracnghiem_traloi_mp3).First(),
                           s.sach_title,
                           u.unit_title,
                           //l.lesson_title
                       });
        grvList.DataSource = getData;
        grvList.DataBind();

        //var listChuDe = from cd in db.tbGameMamNon_ChuDes
        //                where cd.Group_id == null
        //                select cd;
        //ddlChuDe.DataSource = listChuDe;
        //ddlChuDe.DataBind();
    }
    public string getIdFromUrl(string url)
    {
        Regex r = new Regex(@"\/d\/(.+)\/", RegexOptions.IgnoreCase);
        Match m = r.Match(url);
        string file_ID = m.ToString().TrimStart('/', 'd').Trim('/');
        return file_ID;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        string strFolderAudio;
        if (fileAudio.HasFile)
        {
            strFolderAudio = Server.MapPath("~/mp3Game/amthanh_tracnghiem");
            string audioPath = "";
            if (!Directory.Exists(strFolderAudio))
            {
                Directory.CreateDirectory(strFolderAudio);
            }
            string fileAudio_save = Path.Combine(Server.MapPath("~/mp3Game/amthanh_tracnghiem/"), Path.GetFileName(fileAudio.FileName));
            fileAudio.SaveAs(fileAudio_save);
            audioPath = "/mp3Game/amthanh_tracnghiem/" + Path.GetFileName(fileAudio.FileName);
            if (Session["_id"].ToString() == "0")
            {

                tbgame_TracNghiem_CauHoi ch = new tbgame_TracNghiem_CauHoi();
                ch.tracnghiem_cauhoi_mp3 = audioPath;
                ch.sach_id = Convert.ToInt32(ddlSach.SelectedItem.Value);
                ch.unit_id = Convert.ToInt32(ddlUnit.SelectedItem.Value);
                ch.lesson_id = Convert.ToInt32(ddlLesson.SelectedItem.Value);
                ch.tracnghiem_cauhoi_image = txtHinhAnh.Text;
                ch.tracnghiem_vocabulary_mp3 = txtAmThanh.Text;
                db.tbgame_TracNghiem_CauHois.InsertOnSubmit(ch);
                db.SubmitChanges();
                //lưu đáp án đúng
                var checkDapAnDung = (from d in db.tbgame_HinhAnhGameTracNghiems
                                      where d.hinhanhgame_id == Convert.ToInt32(txtDapAnDung.Value)
                                      select d).FirstOrDefault();
                tbgame_TracNghiem_TraLoi dung = new tbgame_TracNghiem_TraLoi();
                dung.tracnghiem_traloi_image = checkDapAnDung.hinhanhgame_image;
                dung.tracnghiem_traloi_mp3 = checkDapAnDung.hinhanhgame_mp3;
                dung.tracnghiem_traloi_dung = "đúng";
                dung.tracnghiem_cauhoi_id = ch.tracnghiem_cauhoi_id;
                dung.tracnghiem_traloi_name = checkDapAnDung.hinhanhgame_name;
                db.tbgame_TracNghiem_TraLois.InsertOnSubmit(dung);
                db.SubmitChanges();
                string[] arrChecked = txtDapAnSai.Value.Split(',');
                foreach (var item in arrChecked)
                {
                    var checkDapAnSai = (from d in db.tbgame_HinhAnhGameTracNghiems
                                         where d.hinhanhgame_id == Convert.ToInt32(item)
                                         select d).FirstOrDefault();
                    tbgame_TracNghiem_TraLoi sai1 = new tbgame_TracNghiem_TraLoi();
                    sai1.tracnghiem_traloi_image = checkDapAnSai.hinhanhgame_image;
                    sai1.tracnghiem_traloi_mp3 = checkDapAnSai.hinhanhgame_mp3;
                    sai1.tracnghiem_traloi_dung = "s";
                    sai1.tracnghiem_cauhoi_id = ch.tracnghiem_cauhoi_id;
                    sai1.tracnghiem_traloi_name = checkDapAnSai.hinhanhgame_name;
                    db.tbgame_TracNghiem_TraLois.InsertOnSubmit(sai1);
                    db.SubmitChanges();
                }
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công!','','success').then(function(){grvList.Refresh();})", true);
                popupControl.ShowOnPageLoad = false;
            }
            else
            {
            }
        }
        else
        {
            alert.alert_Warning(Page, "Vui lòng chọn file", "");
        }
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        txtCountDapAnSai.Value = "";
        txtDapAnDung.Value = "";
        txtDapAnSai.Value = "";
        ddlSach.SelectedIndex = -1;
        ddlUnit.SelectedIndex = -1;
        ddlLesson.SelectedIndex = -1;
        rpDapAnDung.DataSource = null;
        rpDapAnDung.DataBind();
        rpDapAnSai.DataSource = null;
        rpDapAnSai.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected bool Linq_Xoa(int cauhoi_id)
    {
        tbgame_TracNghiem_CauHoi delete = db.tbgame_TracNghiem_CauHois.Where(x => x.tracnghiem_cauhoi_id == cauhoi_id).FirstOrDefault();
        db.tbgame_TracNghiem_CauHois.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "tracnghiem_cauhoi_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                if (Linq_Xoa(Convert.ToInt32(item)))
                {
                    var traloi = from tl in db.tbgame_TracNghiem_TraLois
                                 where tl.tracnghiem_cauhoi_id == Convert.ToInt32(item)
                                 select tl;
                    db.tbgame_TracNghiem_TraLois.DeleteAllOnSubmit(traloi);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Xóa thành công", "");
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "grvList.Refresh(); ", true);
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }



    protected void ddlSach_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlUnit.DataSource = from s in db.tbSLLDT_Units
                             where s.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value)
                             select s;
        ddlUnit.DataBind();
    }

    protected void ddlUnit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUnit.SelectedItem.Value !=null)
        {
            var getHinhAnh = from img in db.tbgame_HinhAnhGameTracNghiems
                             where img.sach_id == Convert.ToInt32(ddlSach.SelectedItem.Value)
                             && img.unit_id == Convert.ToInt32(ddlUnit.SelectedItem.Value)
                             select img;
            rpDapAnDung.DataSource = getHinhAnh;
            rpDapAnDung.DataBind();
            rpDapAnSai.DataSource = getHinhAnh;
            rpDapAnSai.DataBind();
            //txtCountDapAnSai.Value = "";
            //txtDapAnDung.Value = "";
            //txtDapAnSai.Value = "";
            ddlLesson.DataSource = from l in db.tbSLLDT_lessons
                                   where l.unit_id == Convert.ToInt32(ddlUnit.SelectedItem.Value)
                                   select l;
            ddlLesson.DataBind();
        }
    }
}

