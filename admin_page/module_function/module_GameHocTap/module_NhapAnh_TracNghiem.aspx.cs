﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_NhapAnh_TracNghiem : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    int vitri;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
                Session["_id"] = 0;

            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {

        ddlBook.DataSource = from tb in db.tbSLLDT_Saches select tb;
        ddlBook.DataBind();
        ddlUnit.DataSource = from tb in db.tbSLLDT_Units select tb;
        ddlUnit.DataBind();
        var getData = from a in db.tbgame_HinhAnhGameTracNghiems
                      join s in db.tbSLLDT_Saches on a.sach_id equals s.sach_id
                      join u in db.tbSLLDT_Units on a.unit_id equals u.unit_id
                      orderby a.hinhanhgame_id descending
                      select new
                      {
                          a.hinhanhgame_id,
                          s.sach_title,
                          u.unit_title,
                          a.hinhanhgame_image,
                          a.hinhanhgame_mp3,
                          s.sach_id,
                          u.unit_id,
                          a.hinhanhgame_name
                      };

        grvList.DataSource = getData;
        grvList.DataBind();


    }
    private void setNULL()
    {
        txtHinhAnh.Text = "";
        txtAmThanh.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();showImg('');", true);
        loadData();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hinhanhgame_id" }));
        Session["_id"] = _id;
        var getData =( from a in db.tbgame_HinhAnhGameTracNghiems
                      join s in db.tbSLLDT_Saches on a.sach_id equals s.sach_id
                      join u in db.tbSLLDT_Units on a.unit_id equals u.unit_id
                       where a.hinhanhgame_id == _id
                       select new
                      {
                          a.hinhanhgame_id,
                          s.sach_title,
                          u.unit_title,
                          a.hinhanhgame_image,
                          a.hinhanhgame_mp3,
                          a.hinhanhgame_name,
                      }).Single();
        ddlBook.Text = getData.sach_title;
        ddlUnit.Text = getData.unit_title;
        txtHinhAnh.Text = getData.hinhanhgame_image;
        txtAmThanh.Text = getData.hinhanhgame_mp3;
        txtName.Text = getData.hinhanhgame_name;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_HinhAnh_TracNghiem cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hinhanhgame_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_HinhAnh_TracNghiem();
                tbgame_HinhAnhGameTracNghiem checkImage = (from a in db.tbgame_HinhAnhGameTracNghiems where a.hinhanhgame_id == Convert.ToInt32(item) select a).SingleOrDefault();
                //string _image = checkImage.hinhanhgame_image.Split('/')[4];
                //string pathToFiles = Server.MapPath(_image);
                //delete(pathToFiles);
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    loadData();
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txtHinhAnh.Text != "" || txtAmThanh.Text!="")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_HinhAnh_TracNghiem cls = new cls_HinhAnh_TracNghiem();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                else
                {
                }
                if (cls.Linq_Them(Convert.ToInt32( ddlBook.SelectedItem.Value ), Convert.ToInt32( ddlUnit.SelectedItem.Value), txtHinhAnh.Text,txtAmThanh.Text, txtName.Text))
                {
                    alert.alert_Success(Page, "Thêm thành công", "");
                    loadData();

                }
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (image == null)
                {
                    image = "/images/anh-dai-dien.png";
                }
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), Convert.ToInt32(ddlBook.SelectedItem.Value), Convert.ToInt32(ddlUnit.SelectedItem.Value), txtHinhAnh.Text, txtAmThanh.Text, txtName.Text))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
        }
    }
    public void delete(string sFileName)
    {
        if (sFileName != String.Empty)
        {
            if (File.Exists(sFileName))

                File.Delete(sFileName);
        }
    }


    protected void ddlBook_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getDataUnit = from s in db.tbSLLDT_Units
                            where s.sach_id == Convert.ToInt16(ddlBook.SelectedItem.Value)
                            select s;
        ddlUnit.DataSource = getDataUnit;
        ddlUnit.DataBind();
    }
}