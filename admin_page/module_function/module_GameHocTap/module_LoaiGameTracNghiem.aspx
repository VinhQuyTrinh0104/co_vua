﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_LoaiGameTracNghiem.aspx.cs" Inherits="admin_page_module_function_module_GameHocTap_module_LoaiGameTracNghiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .main-omt {
            border: 1px solid #226e54;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #226e54;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }
    </style>
       <script>
           function func() {
               grvList.Refresh();
               popupControl.Hide();
           }

           function btnChiTiet() {
               document.getElementById('<%=btnChiTiet.ClientID%>').click();
           }

           function popupHide() {
               document.getElementById('btnClosePopup').click();
           }

           function confirmDel() {
               swal("Bạn có thực sự muốn xóa?",
                   "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                   "warning",
                   {
                       buttons: true,
                       dangerMode: true
                   }).then(function (value) {
                       if (value == true) {
                           var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
           }

           function showAudio(img) {
               $('#audio').attr('src', img);
               $('#audio').css('display', 'block');
           }
           function checkNULL() {
               var dapandung = document.getElementById('<%=txtDapAnDung.ClientID%>');
            var dapansai = document.getElementById('<%=txtCountDapAnSai.ClientID%>').value;
               console.log(dapansai);
               if (Number(dapansai) < 2) {
                   swal('Vui lòng chọn tối thiểu 2 đáp án sai!', '', 'warning');
                   return false;
               }
               if (dapandung.value.trim() == "") {
                   swal('Vui lòng chọn đáp án đúng!', '', 'warning');
                   return false;
               }
               return true;
           }
           function OnCountryChanged(cmbCountry) {
               document.getElementById('<%=txtDapAnDung.ClientID%>').value = "";
            document.getElementById('<%=txtCountDapAnSai.ClientID%>').value = "";
            document.getElementById('<%=txtDapAnSai.ClientID%>').value = "";
        }
        //func chọn đáp án đúng
        function myDapAnDung(id) {
            document.getElementById('<%=txtDapAnDung.ClientID%>').value = id;
        }
        //func chọn đáp án sai
        function myDapAnSai(id) {
            var arrayvalue = document.getElementById("<%= txtDapAnSai.ClientID %>").value;
            var array = JSON.parse("[" + arrayvalue + "]");
            var index = array.indexOf(id);
            if (index > -1) {
                array.splice(index, 1);
                document.getElementById("<%= txtDapAnSai.ClientID %>").value = array;
                document.getElementById("<%= txtCountDapAnSai.ClientID %>").value = document.getElementById("<%= txtCountDapAnSai.ClientID %>").value - 1;
            }
            else {
                document.getElementById("<%= txtCountDapAnSai.ClientID %>").value = array.push(id);
                document.getElementById("<%= txtDapAnSai.ClientID %>").value = array;
               }
           }
           function showPreviewAudio(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#audio_cauhoi').attr('src', e.target.result);
                       //$('#audio').css('display', 'block');
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showPreview1(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview1').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg1(img) {
               $('#imgPreview1').attr('src', img);
           }
           function showPreview2(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview2').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg2(img) {
               $('#imgPreview2').attr('src', img);
           }
           function showPreview3(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview3').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg3(img) {
               $('#imgPreview3').attr('src', img);
           }
           function showPreview4(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview4').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg4(img) {
               $('#imgPreview4').attr('src', img);
           }
           function showPreview5(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview5').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg5(img) {
               $('#imgPreview5').attr('src', img);
           }
           function showPreview6(input) {
               if (input.files && input.files[0]) {
                   var filerdr = new FileReader();
                   filerdr.onload = function (e) {
                       $('#imgPreview6').attr('src', e.target.result);
                   }
                   filerdr.readAsDataURL(input.files[0]);
               }
           }
           function showImg6(img) {
               $('#imgPreview6').attr('src', img);
           }
           function clickavatar1() {
               $("#up1 input[type=file]").click();
           }
           function clickavatar2() {
               $("#up2 input[type=file]").click();
           }
           function clickavatar3() {
               $("#up3 input[type=file]").click();
           }
           function clickavatar4() {
               $("#up4 input[type=file]").click();
           }
           function clickavatar5() {
               $("#up5 input[type=file]").click();
           }
           function clickavatar6() {
               $("#up6 input[type=file]").click();
           }
       </script>
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản lý game trắc nghiệm</h4>
        </div>
    </div>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-12">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-12">
                            <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                            <asp:Button ID="btnChiTiet" runat="server" Text="Cập nhật" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" Visible="false" />
                            <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                            <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div class="form-group table-responsive">
        <dx:ASPxGridView ID="grvList" runat="server" CssClass="table-hover" ClientInstanceName="grvList" KeyFieldName="tracnghiem_cauhoi_id" Width="100%">
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn Caption="Câu hỏi" HeaderStyle-HorizontalAlign="Center" Width="20%">
                    <DataItemTemplate>
                        <audio controls="controls">
                            <source src="<%#Eval("tracnghiem_cauhoi_mp3") %>" type="audio/mp3" />
                        </audio>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Đáp án đúng" HeaderStyle-HorizontalAlign="Center" Width="20%">
                    <DataItemTemplate>
                        <img src="<%#Eval("dapandung") %>" alt="Alternate Text" width="100px" height="100px" />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Âm thanh đúng" HeaderStyle-HorizontalAlign="Center" Width="20%">
                    <DataItemTemplate>
                        <audio controls="controls">
                            <source src="<%#Eval("amthanhdung") %>" type="audio/mp3" />
                        </audio>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Sách" FieldName="sach_title" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Unit" FieldName="unit_title" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                <%--<dx:GridViewDataColumn Caption="Lesson" FieldName="lesson_title" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>--%>
            </Columns>
            <SettingsSearchPanel Visible="true" />
            <SettingsBehavior AllowFocusedRow="true" />
            <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
            <SettingsLoadingPanel Text="Đang tải..." />
            <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
        </dx:ASPxGridView>
    </div>

    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="1000px" Height="550px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Game trắc nghiệm" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Sách:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlSach" runat="server" TextField="sach_title"
                                                ValueField="sach_id" ValueType="System.Int32" ClientInstanceName="ddlSach" Width="95%" OnSelectedIndexChanged="ddlSach_SelectedIndexChanged" AutoPostBack="true">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Unit:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlUnit" runat="server" TextField="unit_title"
                                                ValueField="unit_id" ValueType="System.Int32" ClientInstanceName="ddlUnit" Width="95%" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged" AutoPostBack="true">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Lesson:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlLesson" runat="server" TextField="lesson_title"
                                                ValueField="lesson_id" ValueType="System.Int32" ClientInstanceName="ddlLesson" Width="95%">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                     <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Link hình ảnh:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtHinhAnh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-12 form-group">
                                            <label class="col-2 form-control-label">Link âm thanh:</label>
                                            <div class="col-10">
                                                <asp:TextBox ID="txtAmThanh" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                            </div>
                                        </div>
                                    <div class="col-12  form-group">
                                        <label class="col-2 form-control-label">Âm thanh câu hỏi:</label>
                                        <asp:FileUpload ID="fileAudio" runat="server" onchange="showPreviewAudio(this)" />
                                        <audio controls="controls" id="audio_cauhoi">
                                            <source src="" type="audio/mp3" />
                                        </audio>
                                    </div>
                                    <div class="col-12  form-group">
                                        <div class="col-6">
                                            Đáp án đúng
                                            <br />
                                            <asp:Repeater ID="rpDapAnDung" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-2">
                                                        <input type="radio" id="<%#Eval("hinhanhgame_id") %>" name="dapan" onclick="myDapAnDung('<%#Eval("hinhanhgame_id") %>')" />
                                                        <label class="tracnghiem-title__answer" for="<%#Eval("hinhanhgame_id") %>">
                                                            <img src="<%#Eval("hinhanhgame_image") %>" alt="Alternate Text" style="width: 100%" />
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="col-6">
                                            Đáp án sai
                                            <br />
                                            <asp:Repeater ID="rpDapAnSai" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-2">
                                                        <input type="checkbox" id="ck_<%#Eval("hinhanhgame_id") %>" onclick="myDapAnSai(<%#Eval("hinhanhgame_id") %>)" name="ck_<%#Eval("hinhanhgame_id") %>" />
                                                        <label class="tracnghiem-title__answer" for="ck_<%#Eval("hinhanhgame_id") %>">
                                                            <img src="<%#Eval("hinhanhgame_image") %>" alt="Alternate Text" style="width: 100%" />
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <input type="text" id="txtDapAnDung" runat="server" />
                                    <input type="text" id="txtDapAnSai" runat="server" />
                                    <input type="text" id="txtCountDapAnSai" runat="server" style="display: none" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

 
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

