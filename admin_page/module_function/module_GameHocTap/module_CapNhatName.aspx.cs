﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GameHocTap_module_CapNhatName : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loadData();
        }
    }
    private void loadData()
    {
        var getData = from a in db.tbgame_TracNghiem_TraLois
                      select a;

        rpList.DataSource = getData;
        rpList.DataBind();
        txtDanhSach.Value = string.Join(",", getData.Select(x => x.tracnghiem_traloi_id));
    }

    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        tbgame_TracNghiem_TraLoi update = db.tbgame_TracNghiem_TraLois.Where(x => x.tracnghiem_traloi_id == Convert.ToInt32(txtID.Value)).First();
        update.tracnghiem_traloi_name = txtName.Value;
        db.SubmitChanges();
        alert.alert_Success(Page, "Thành công", "");
        //loadData();
    }

    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        string[] arrID = txtDanhSach.Value.Split(',');
        string[] arrName = txtNoiDung.Value.Split('|');
        for (int i = 0; i < arrID.Length; i++)
        {
            tbgame_TracNghiem_TraLoi update = db.tbgame_TracNghiem_TraLois.Where(x => x.tracnghiem_traloi_id == Convert.ToInt32(arrID[i])).First();
            update.tracnghiem_traloi_name = arrName[i]+"";
            db.SubmitChanges();
            alert.alert_Success(Page, "Thành công", "");
        }
    }
}