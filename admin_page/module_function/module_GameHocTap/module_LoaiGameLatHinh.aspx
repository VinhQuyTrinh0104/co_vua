﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_LoaiGameLatHinh.aspx.cs" Inherits="admin_page_module_function_module_GamHocTap_module_LoaiGameLatHinh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .main-omt {
            border: 1px solid #226e54;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #226e54;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Nhập liệu game lật hình</h4>
        </div>
    </div>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-12">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-12">
                            <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                            <asp:Button ID="btnChiTiet" runat="server" Text="Cập nhật" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" Visible="false" />
                            <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                            <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div class="form-group table-responsive">
        <dx:ASPxGridView ID="grvList" runat="server" CssClass="table-hover" ClientInstanceName="grvList" KeyFieldName="lathinh_code" Width="100%">
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn Caption="Code" FieldName="lathinh_code" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Sách" FieldName="sach_title" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Unit" FieldName="unit_title" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Hình ảnh" HeaderStyle-HorizontalAlign="Center" Width="20%">
                    <DataItemTemplate>
                        <img src='<%#Eval("lathinh_image") %>' width="100px" height="100px" />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Mp3" HeaderStyle-HorizontalAlign="Center" Width="10%">
                    <DataItemTemplate>
                        <audio controls="controls" src="<%#Eval("lathinh_mp3") %>" style="width: 150px"></audio>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
            <SettingsSearchPanel Visible="true" />
            <SettingsBehavior AllowFocusedRow="true" />
            <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
            <SettingsLoadingPanel Text="Đang tải..." />
            <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
        </dx:ASPxGridView>
    </div>

    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="1000px" Height="550px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Game lật hình" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Sách:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlSach" runat="server" TextField="sach_title"
                                                ValueField="sach_id" ValueType="System.Int32" ClientInstanceName="ddlSach" Width="95%" OnSelectedIndexChanged="ddlSach_SelectedIndexChanged" AutoPostBack="true">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Unit:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlUnit" runat="server" TextField="unit_title"
                                                ValueField="unit_id" ValueType="System.Int32" ClientInstanceName="ddlUnit" Width="95%" OnSelectedIndexChanged="ddlUnit_SelectedIndexChanged" AutoPostBack="true">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Lesson:</label>
                                        <div class="col-10">
                                            <dx:ASPxComboBox ID="ddlLesson" runat="server" TextField="lesson_title"
                                                ValueField="lesson_id" ValueType="System.Int32" ClientInstanceName="ddlLesson" Width="95%">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                            </dx:ASPxComboBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="col-10 mt-1">
                                            <asp:Repeater ID="rpHinhAnh" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-2">
                                                        <input type="checkbox" id="ck_<%#Eval("hinhanhgame_id") %>" onclick="myHinhAnh(<%#Eval("hinhanhgame_id") %>)" name="ck_<%#Eval("hinhanhgame_id") %>" />
                                                        <label class="tracnghiem-title__answer" for="ck_<%#Eval("hinhanhgame_id") %>">
                                                            <img src="<%#Eval("hinhanhgame_image") %>" alt="Alternate Text" style="width: 100%" />
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <input type="text" id="txtDanhSachChecked" runat="server" style="display: none" />
                                        <input type="text" id="txtCountChecked" runat="server" style="display: none" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <script>

        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }

        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }

        function popupHide() {
            document.getElementById('btnClosePopup').click();
        }
        function OnCountryChanged(cmbCountry) {
            document.getElementById('<%=txtCountChecked.ClientID%>').value = "";
            document.getElementById('<%=txtDanhSachChecked.ClientID%>').value = "";
        }
        //func chọn hình ảnh
        function myHinhAnh(id) {
            var arrayvalue = document.getElementById("<%= txtDanhSachChecked.ClientID %>").value;
            var array = JSON.parse("[" + arrayvalue + "]");
            var index = array.indexOf(id);
            if (index > -1) {
                array.splice(index, 1);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
                document.getElementById("<%= txtCountChecked.ClientID %>").value = document.getElementById("<%= txtCountChecked.ClientID %>").value - 1;
            }
            else {
                document.getElementById("<%= txtCountChecked.ClientID %>").value = array.push(id);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
            }
        }
        function checkNULL() {
            var dapansai = document.getElementById('<%=txtCountChecked.ClientID%>').value;
            console.log(dapansai);
            if (Number(dapansai) < 2) {
                swal('Vui lòng chọn tối thiểu 2 hình ảnh!', '', 'warning');
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
    </script>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

