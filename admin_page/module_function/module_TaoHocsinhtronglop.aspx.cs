﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_TaoHocsinhtronglop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public DateTime ngaycuoi;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            if (!IsPostBack)
            {
               
                loadData();
                if (RouteData.Values["id"].ToString() == "0")
                {

                }
                else
                {
                    var getChiTietHocPhi = (from hp in db.tbHocPhis
                                            join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
                                            join hstl in db.tbhocsinhtronglops on hp.hstl_id equals hstl.hstl_id
                                            join hs in db.tbAccounts on hstl.account_id equals hs.account_id
                                            join l in db.tbLops on hstl.lop_id equals l.lop_id
                                            join cs in db.tbCosos on l.coso_id equals cs.coso_id
                                            join u in db.admin_Users on hp.username_id equals u.username_id
                                            where hp.hp_id == Convert.ToInt32(RouteData.Values["id"].ToString())
                                            orderby hpct.hocphichitiet_id descending
                                            select new
                                            {
                                                hp.hocphi_tinhtrangdong,
                                                hp.hocphi_chitiet,
                                                cs.coso_name,
                                                l.lop_name,
                                                hs.account_id,
                                                hs.account_vn,
                                                giohoc = l.giohoc_batdau + "-" + l.giohoc_ketthuc,
                                                l.lop_thu2,
                                                l.lop_thu3,
                                                l.lop_thu4,
                                                l.lop_thu5,
                                                l.lop_thu6,
                                                l.lop_thu7,
                                                l.lop_chunhat,
                                                hpct.hocphichitiet_ngaybatdau,
                                                hpct.hocphichitiet_ngayketthuc,
                                                hpct.hocphichitiet_sobuoihoc,
                                                hocphichitiet_sotiendongkhoahoc_hidden = hpct.hocphichitiet_sotiendongkhoahoc,
                                                hocphichitiet_sotiendongkhoahoc = hpct.hocphichitiet_sotiendongkhoahoc <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hpct.hocphichitiet_sotiendongkhoahoc.ToString()), ""),
                                                hocphichitiet_chuongtrinhgiam_hidden = hpct.hocphichitiet_chuongtrinhgiam,
                                                hocphichitiet_chuongtrinhgiam = hpct.hocphichitiet_chuongtrinhgiam <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hpct.hocphichitiet_chuongtrinhgiam.ToString()), ""),
                                                hocphichitiet_sotiendadong_hidden = hpct.hocphichitiet_sotiendadong,
                                                hocphichitiet_sotiendadong = hpct.hocphichitiet_sotiendadong <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hpct.hocphichitiet_sotiendadong.ToString()), ""),
                                                hocphichitiet_sotienconlai_hidden = hpct.hocphichitiet_sotienconlai,
                                                hocphichitiet_sotienconlai = hpct.hocphichitiet_sotienconlai <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hpct.hocphichitiet_sotienconlai.ToString()), ""),
                                                hocphichitiet_sotiengiaotrinh_hidden = hpct.hocphichitiet_sotiengiaotrinh,
                                                hocphichitiet_sotiengiaotrinh = hpct.hocphichitiet_sotiengiaotrinh <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hpct.hocphichitiet_sotiengiaotrinh.ToString()), ""),
                                                hocphi_tongtien_hidden = hp.hocphi_tongtien,
                                                hocphi_tongtien = hp.hocphi_tongtien <= 0 ? "0" : String.Format("{0:#,0.##} {1}", Convert.ToDouble(hp.hocphi_tongtien.ToString()), ""),
                                                hpct.hocphichitiet_phieuthu,
                                                hpct.hocphichitiet_maso,
                                                u.username_fullname,
                                                hpct.hocphichitiet_hinhthucthanhtoan,
                                                hpct.hocphichitiet_ghichu
                                            }).FirstOrDefault();
                    ddlCoSo.Text = getChiTietHocPhi.coso_name;
                    ddllop.Text = getChiTietHocPhi.lop_name;
                    ddlHocSinh.Text = getChiTietHocPhi.account_vn;
                    ddlTinhTrangDongHocPhi.Text = getChiTietHocPhi.hocphi_tinhtrangdong;
                    ddlChiTiet.Text = getChiTietHocPhi.hocphi_chitiet;
                    lblGioHoc.Text = getChiTietHocPhi.giohoc;
                    if (getChiTietHocPhi.lop_thu2 == true)
                        ckThu2.Checked = true;
                    else
                        ckThu2.Checked = false;
                    if (getChiTietHocPhi.lop_thu3 == true)
                        ckThu3.Checked = true;
                    else
                        ckThu3.Checked = false;
                    if (getChiTietHocPhi.lop_thu4 == true)
                        ckThu4.Checked = true;
                    else
                        ckThu4.Checked = false;
                    if (getChiTietHocPhi.lop_thu5 == true)
                        ckThu5.Checked = true;
                    else
                        ckThu5.Checked = false;
                    if (getChiTietHocPhi.lop_thu6 == true)
                        ckThu6.Checked = true;
                    else
                        ckThu6.Checked = false;
                    if (getChiTietHocPhi.lop_thu7 == true)
                        ckThu7.Checked = true;
                    else
                        ckThu7.Checked = false;
                    if (getChiTietHocPhi.lop_chunhat == true)
                        ckChuNhat.Checked = true;
                    else
                        ckChuNhat.Checked = false;
                    dteNgayBatDauHoc.Value = getChiTietHocPhi.hocphichitiet_ngaybatdau.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                    txtTongSoBuoiHoc.Value = getChiTietHocPhi.hocphichitiet_sobuoihoc + "";
                    dteNgayKetThuc.Value = getChiTietHocPhi.hocphichitiet_ngayketthuc.Value.ToString("dd/MM/yyyy");
                    txtSoTienDongKhoaHoc.Value = getChiTietHocPhi.hocphichitiet_sotiendongkhoahoc + "";
                    txtSoTienDongKhoaHoc_Hidden.Value = getChiTietHocPhi.hocphichitiet_sotiendongkhoahoc_hidden + "";
                    txtChuongTrinhGiamHocPhi.Value = getChiTietHocPhi.hocphichitiet_chuongtrinhgiam + "";
                    txtChuongTrinhGiamHocPhi_Hidden.Value = getChiTietHocPhi.hocphichitiet_chuongtrinhgiam_hidden + "";
                    txtSoTienDaDong.Value = getChiTietHocPhi.hocphichitiet_sotiendadong + "";
                    txtSoTienDaDong_Hidden.Value = getChiTietHocPhi.hocphichitiet_sotiendadong_hidden + "";
                    txtSoTienConLai.Value = getChiTietHocPhi.hocphichitiet_sotienconlai + "";
                    txtSoTienConLai_Hidden.Value = getChiTietHocPhi.hocphichitiet_sotienconlai_hidden + "";
                    txtGiaoTrinh.Value = getChiTietHocPhi.hocphichitiet_sotiengiaotrinh + "";
                    txtGiaoTrinh_Hidden.Value = getChiTietHocPhi.hocphichitiet_sotiengiaotrinh_hidden + "";
                    txtTongThu.Value = getChiTietHocPhi.hocphi_tongtien + "";
                    txtTongThu_Hidden.Value = getChiTietHocPhi.hocphi_tongtien_hidden + "";
                    txtPhieuThu.Value = getChiTietHocPhi.hocphichitiet_phieuthu;
                    txtMaSo.Value = getChiTietHocPhi.hocphichitiet_maso;
                    ddlNhanVien.Text = getChiTietHocPhi.username_fullname;
                    ddlThanhToan.Text = getChiTietHocPhi.hocphichitiet_hinhthucthanhtoan;
                    txtGhiChu.Value = getChiTietHocPhi.hocphichitiet_ghichu;

                }


            }

        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        ddlCoSo.DataSource = from cs in db.tbCosos where cs.coso_hidden == false select cs;
        ddlCoSo.DataBind();
        ddlTinhTrangDongHocPhi.DataSource = from tt in db.tbHocPhiTinhTrangDongs select tt;
        ddlTinhTrangDongHocPhi.DataBind();
        ddlChiTiet.DataSource = from ct in db.tbHocPhiChiTietTinhTrangs select ct;
        ddlChiTiet.DataBind();
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (ddlCoSo.Text != "")
        {
            ddllop.DataSource = from l in db.tbLops
                                where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value)
                                where l.hidden == false
                                select l;
            ddllop.DataBind();
        }
        ddlHocSinh.DataSource = from a in db.tbAccounts where a.hidden == true select a;
        ddlHocSinh.DataBind();
        ddlNhanVien.DataSource = from nv in db.admin_Users where nv.groupuser_id == 6 select nv;
        ddlNhanVien.DataBind();

    }
    private bool setNULL()
    {
        if (ddllop.SelectedItem == null)
        {
            return true;
        }
        else return false;
    }
    public void checkNgayKetThuc()
    {
        if (dteNgayBatDauHoc.Value == "")
        {
            alert.alert_Warning(Page, "Vui lòng chọn ngày bắt đầu học!", "");
        }
        else
        {
            // Lấy số buổi tổng thời gian học txtTongSoBuoiHoc
            int tongsobuoi = Convert.ToInt16(txtTongSoBuoiHoc.Value);
            int i = 1;
            // cho chạy từ ngày bắt đầu cho tới khi tổng số buổi = tongssobuoi thì out

            string kiemtrathu = Convert.ToDateTime(dteNgayBatDauHoc.Value).DayOfWeek + "";
            for (int j = 1; j <= 1000; j++)
            {
                // ví dụ học sinh học 3 buổi thứ 2,4,6
                // ngày bắt đầu là ngày thứ 4 
                DateTime ngaytieptheo = Convert.ToDateTime(dteNgayBatDauHoc.Value).AddDays(j);
                kiemtrathu = ngaytieptheo.DayOfWeek + "";

                if (kiemtrathu == "Monday" && ckThu2.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Tuesday" && ckThu3.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Wednesday" && ckThu4.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Thursday" && ckThu5.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Friday" && ckThu6.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Saturday" && ckThu7.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (kiemtrathu == "Sunday" && ckChuNhat.Checked == true)
                {
                    i = i + 1;
                    ngaycuoi = ngaytieptheo;
                }
                if (i == tongsobuoi)
                {
                    dteNgayKetThuc.Value = ngaycuoi.ToString("dd/MM/yyyy");
                    break;
                }
                else
                {

                }
            }
        }

    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        // Lưu vào bảng học phí, học phí chi tiết
        //Kiểm tra lại vụ cbb Lớp với cbb học sinh, cần phải nhận dc id chứ ko là tiêu.

        cls_TaoHocsinhtronglop cls = new cls_TaoHocsinhtronglop();
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();

        if (RouteData.Values["id"].ToString() != "0")
        {
            var geths = (from hs in db.tbAccounts
                         join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                         orderby hs.account_id descending
                         where hs.account_vn.Trim() == ddlHocSinh.Text.Trim()
                         select new
                         {
                             hs.account_id,
                             hstl.hstl_id,
                             hs.account_vn,
                             hs.hidden,
                             hstl.lop_id
                         }).First();

            tbHocPhi updateHocPhi = (from hp in db.tbHocPhis where hp.hp_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select hp).FirstOrDefault();
            updateHocPhi.hocphi_tinhtrangdong = ddlTinhTrangDongHocPhi.Text;
            updateHocPhi.hocphi_chitiet = ddlChiTiet.Text;
            updateHocPhi.account_id = geths.account_id;
            updateHocPhi.username_id = Convert.ToInt32(ddlNhanVien.SelectedItem.Value);
            updateHocPhi.hocphi_tongtien = Convert.ToInt32(txtTongThu_Hidden.Value);
            updateHocPhi.hp_ghichu = txtGhiChu.Value;
            updateHocPhi.hstl_id = geths.hstl_id;
            updateHocPhi.hocphi_ngaydongtien = DateTime.Now;
            db.SubmitChanges();
            tbHocPhiChiTiet updateHPCT = (from hpct in db.tbHocPhiChiTiets where hpct.hocphi_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select hpct).FirstOrDefault();
            updateHPCT.hocphi_id = Convert.ToInt32(RouteData.Values["id"].ToString());
            updateHPCT.hocsinh_id = geths.account_id;
            updateHPCT.hstl_id = geths.hstl_id;
            updateHPCT.hocsinh_fullname = ddlHocSinh.Text;
            updateHPCT.lop_id = geths.lop_id;
            if (txtTongSoBuoiHoc.Value == "")
                updateHPCT.hocphichitiet_sobuoihoc = 0;
            else
                updateHPCT.hocphichitiet_sobuoihoc = Convert.ToInt32(txtTongSoBuoiHoc.Value);
            if (dteNgayBatDauHoc.Value != "")
            {
                updateHPCT.hocphichitiet_ngaybatdau = Convert.ToDateTime(dteNgayBatDauHoc.Value);
                updateHPCT.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayKetThuc.Value);
            }
            if (txtSoTienDongKhoaHoc_Hidden.Value != "")
                updateHPCT.hocphichitiet_sotiendongkhoahoc = Convert.ToInt32(txtSoTienDongKhoaHoc_Hidden.Value);
            else
                updateHPCT.hocphichitiet_sotiendongkhoahoc = 0;
            if (txtSoTienDaDong_Hidden.Value != "")
                updateHPCT.hocphichitiet_sotiendadong = Convert.ToInt32(txtSoTienDaDong_Hidden.Value);
            else
                updateHPCT.hocphichitiet_sotiendadong = 0;
            if (txtSoTienConLai_Hidden.Value != "")
                updateHPCT.hocphichitiet_sotienconlai = Convert.ToInt32(txtSoTienConLai_Hidden.Value);
            else
                updateHPCT.hocphichitiet_sotienconlai = 0;
            if (txtGiaoTrinh_Hidden.Value != "")
                updateHPCT.hocphichitiet_sotiengiaotrinh = Convert.ToInt32(txtGiaoTrinh_Hidden.Value);
            else
                updateHPCT.hocphichitiet_sotiengiaotrinh = 0;
            updateHPCT.hocphichitiet_phieuthu = txtPhieuThu.Value;
            if (txtChuongTrinhGiamHocPhi_Hidden.Value == "")
            {
                txtChuongTrinhGiamHocPhi_Hidden.Value = "0";
            }
            updateHPCT.hocphichitiet_hocphitungngay = (Convert.ToDouble(Convert.ToInt32(txtSoTienDongKhoaHoc_Hidden.Value) - Convert.ToInt32(txtChuongTrinhGiamHocPhi_Hidden.Value)) / Convert.ToDouble(txtTongSoBuoiHoc.Value)) + "";
            updateHPCT.hocphichitiet_maso = txtMaSo.Value;
            updateHPCT.hocphichitiet_nhanvien = ddlNhanVien.Text;
            updateHPCT.hocphichitiet_hinhthucthanhtoan = ddlThanhToan.Text;
            updateHPCT.hocphichitiet_ghichu = txtGhiChu.Value;
            if (txtChuongTrinhGiamHocPhi_Hidden.Value != "")
                updateHPCT.hocphichitiet_chuongtrinhgiam = Convert.ToInt32(txtChuongTrinhGiamHocPhi_Hidden.Value);
            else
                updateHPCT.hocphichitiet_chuongtrinhgiam = 0;
            db.SubmitChanges();
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.UnselectRows();})", true);
            loadData();
        }
        else
        {
            if (setNULL() == true)
            {
                alert.alert_Warning(Page, "Chưa chọn lớp hoặc môn học!", "");
            }
            else
            {
                var checkMaSo = from hpct in db.tbHocPhiChiTiets where hpct.hocphichitiet_maso == txtMaSo.Value select hpct;
                if (checkMaSo.Count() > 0)
                {
                    alert.alert_Warning(Page, "Mã số đã tồn tại vui lòng kiểm tra lại!", "");
                }
                else
                {
                    var geths = (from hs in db.tbAccounts
                                 join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                                 where hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value)
                                 orderby hstl.hstl_id descending
                                 select new
                                 {
                                     hs.account_id,
                                     hstl.hstl_id,
                                     hs.account_vn,
                                     hs.hidden
                                 }).First();
                    //geths.hidden = false;

                    tbHocPhi insertHP = new tbHocPhi();
                    insertHP.hocphi_tinhtrangdong = ddlTinhTrangDongHocPhi.Text;
                    insertHP.hocphi_chitiet = ddlChiTiet.Text;
                    insertHP.account_id = Convert.ToInt32(ddlHocSinh.SelectedItem.Value);
                    insertHP.username_id = Convert.ToInt32(ddlNhanVien.SelectedItem.Value);
                    insertHP.hocphi_tongtien = Convert.ToInt32(txtTongThu_Hidden.Value);
                    insertHP.hp_ghichu = txtGhiChu.Value;
                    insertHP.hstl_id = geths.hstl_id;
                    insertHP.hocphi_ngaydongtien = DateTime.Now;
                    db.tbHocPhis.InsertOnSubmit(insertHP);
                    db.SubmitChanges();
                    tbHocPhiChiTiet insertChiTiet = new tbHocPhiChiTiet();
                    insertChiTiet.hocphi_id = insertHP.hp_id;
                    insertChiTiet.hocsinh_id = geths.account_id;
                    insertChiTiet.hstl_id = geths.hstl_id;
                    insertChiTiet.hocsinh_fullname = ddlHocSinh.Text;
                    insertChiTiet.lop_id = Convert.ToInt32(ddllop.SelectedItem.Value);
                    if (txtTongSoBuoiHoc.Value == "")
                        insertChiTiet.hocphichitiet_sobuoihoc = 0;
                    else
                        insertChiTiet.hocphichitiet_sobuoihoc = Convert.ToInt32(txtTongSoBuoiHoc.Value);
                    if (dteNgayBatDauHoc.Value != "")
                    {
                        insertChiTiet.hocphichitiet_ngaybatdau = Convert.ToDateTime(dteNgayBatDauHoc.Value);
                        insertChiTiet.hocphichitiet_ngayketthuc = Convert.ToDateTime(dteNgayKetThuc.Value);
                    }
                    if (txtSoTienDongKhoaHoc_Hidden.Value != "")
                        insertChiTiet.hocphichitiet_sotiendongkhoahoc = Convert.ToInt32(txtSoTienDongKhoaHoc_Hidden.Value);
                    else
                        insertChiTiet.hocphichitiet_sotiendongkhoahoc = 0;
                    if (txtSoTienDaDong_Hidden.Value != "")
                        insertChiTiet.hocphichitiet_sotiendadong = Convert.ToInt32(txtSoTienDaDong_Hidden.Value);
                    else
                        insertChiTiet.hocphichitiet_sotiendadong = 0;
                    if (txtSoTienConLai_Hidden.Value != "")
                        insertChiTiet.hocphichitiet_sotienconlai = Convert.ToInt32(txtSoTienConLai_Hidden.Value);
                    else
                        insertChiTiet.hocphichitiet_sotienconlai = 0;
                    if (txtGiaoTrinh_Hidden.Value != "")
                        insertChiTiet.hocphichitiet_sotiengiaotrinh = Convert.ToInt32(txtGiaoTrinh_Hidden.Value);
                    else
                        insertChiTiet.hocphichitiet_sotiengiaotrinh = 0;
                    insertChiTiet.hocphichitiet_phieuthu = txtPhieuThu.Value;
                    if (txtChuongTrinhGiamHocPhi_Hidden.Value == "")
                    {
                        txtChuongTrinhGiamHocPhi_Hidden.Value = "0";
                    }
                    //if (txtTongSoBuoiHoc.Value != "")
                    //{
                    //    insertChiTiet.hocphichitiet_hocphitungngay = (Convert.ToDouble(Convert.ToInt32(txtSoTienDongKhoaHoc_Hidden.Value) - Convert.ToInt32(txtChuongTrinhGiamHocPhi_Hidden.Value)) / Convert.ToDouble(txtTongSoBuoiHoc.Value)) + "";
                    //}
                    insertChiTiet.hocphichitiet_maso = txtMaSo.Value;
                    insertChiTiet.hocphichitiet_nhanvien = ddlNhanVien.Text;
                    insertChiTiet.hocphichitiet_hinhthucthanhtoan = ddlThanhToan.Text;
                    insertChiTiet.hocphichitiet_ghichu = txtGhiChu.Value;
                    if (txtChuongTrinhGiamHocPhi_Hidden.Value != "")
                        insertChiTiet.hocphichitiet_chuongtrinhgiam = Convert.ToInt32(txtChuongTrinhGiamHocPhi_Hidden.Value);
                    else
                        insertChiTiet.hocphichitiet_chuongtrinhgiam = 0;
                    db.tbHocPhiChiTiets.InsertOnSubmit(insertChiTiet);
                    //try
                    //{
                    db.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.UnselectRows();})", true);
                    loadData();
                    //}
                    //catch
                    //{
                    //}
                    //Lưu lịch sử thao tác
                    tbOperationHistory ins = new tbOperationHistory();
                    ins.history_day = DateTime.Now;
                    ins.username_id = logedMember.username_id;
                    ins.history_content = "Thêm học sinh " + geths.account_vn + " vào lớp " + ddllop.Text;
                    db.tbOperationHistories.InsertOnSubmit(ins);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {
                    }
                }
            }
        }
    }

    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        var getData = (from l in db.tbLops
                       where l.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value)
                       select l).SingleOrDefault();
        var getHocSinh = from hs in db.tbAccounts
                         join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
                         where hstl.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden == false
                         select hs;
        ddlHocSinh.DataSource = getHocSinh;
        ddlHocSinh.DataBind();
        lblGioHoc.Text = getData.giohoc_batdau + "-" + getData.giohoc_ketthuc;
        if (getData.lop_thu2 == true)
            ckThu2.Checked = true;
        else
            ckThu2.Checked = false;
        if (getData.lop_thu3 == true)
            ckThu3.Checked = true;
        else
            ckThu3.Checked = false;
        if (getData.lop_thu4 == true)
            ckThu4.Checked = true;
        else
            ckThu4.Checked = false;
        if (getData.lop_thu5 == true)
            ckThu5.Checked = true;
        else
            ckThu5.Checked = false;
        if (getData.lop_thu6 == true)
            ckThu6.Checked = true;
        else
            ckThu6.Checked = false;
        if (getData.lop_thu7 == true)
            ckThu7.Checked = true;
        else
            ckThu7.Checked = false;
        if (getData.lop_chunhat == true)
            ckChuNhat.Checked = true;
        else
            ckChuNhat.Checked = false;

    }
    protected void btnKiemtra_ServerClick(object sender, EventArgs e)
    {
        checkNgayKetThuc();
    }
    protected void ddlCoSo_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddllop.DataSource = from l in db.tbLops
                            where l.coso_id == Convert.ToInt16(ddlCoSo.SelectedItem.Value) && l.hidden == false && l.lop_tinhtrang == null
                            select l;
        ddllop.DataBind();
    }

    protected void ddlHocSinh_SelectedIndexChanged(object sender, EventArgs e)
    {
        //var checkDongHocPhi = (from hs in db.tbAccounts
        //                       join hstl in db.tbhocsinhtronglops on hs.account_id equals hstl.account_id
        //                       join hp in db.tbHocPhis on hstl.hstl_id equals hp.hstl_id
        //                       join hpct in db.tbHocPhiChiTiets on hp.hp_id equals hpct.hocphi_id
        //                       where hs.account_id == Convert.ToInt32(ddlHocSinh.SelectedItem.Value) && hstl.hstl_hidden == false && hs.hidden == false
        //                       orderby hp.hp_id descending
        //                       select new
        //                       {
        //                           hp.hocphi_tongtien,
        //                           hpct.hocphichitiet_sotiendongkhoahoc,
        //                           hpct.hocphichitiet_sotiendadong,
        //                           hpct.hocphichitiet_sobuoihoc,
        //                           hpct.hocphichitiet_ngaybatdau,
        //                           hpct.hocphichitiet_ngayketthuc
        //                       }).FirstOrDefault();
        //if (checkDongHocPhi != null)
        //{
        //    txtSoTienDongKhoaHoc.Value = checkDongHocPhi.hocphichitiet_sotiendongkhoahoc + "";
        //    txtSoTienDaDong.Value = checkDongHocPhi.hocphichitiet_sotiendadong + "";
        //    txtTongThu.Value = checkDongHocPhi.hocphi_tongtien + "";
        //    txtTongSoBuoiHoc.Value = checkDongHocPhi.hocphichitiet_sobuoihoc + "";
        //    if (checkDongHocPhi.hocphichitiet_ngaybatdau != null)
        //    {
        //        dteNgayBatDauHoc.Value = checkDongHocPhi.hocphichitiet_ngaybatdau.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        //        dteNgayKetThuc.Value = checkDongHocPhi.hocphichitiet_ngayketthuc.Value.ToString("dd/MM/yyyy") + "";
        //    }
        //}
    }

    protected void ddlChiTiet_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChiTiet.SelectedItem.Value.ToString() == "2" || ddlChiTiet.SelectedItem.Value.ToString() == "5" || ddlChiTiet.SelectedItem.Value.ToString() == "6")
        {
            div_NgayBatDau.Visible = false;
        }
        else
        {
            div_NgayBatDau.Visible = true;
        }
    }

   
}