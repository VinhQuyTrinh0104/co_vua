﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_PhanHoiPhuHuynh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
                admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
            //đối với tk admin
            if (logedMember.username_id == 2)
            {
                var getData = from ph in db.tbPhanHoiPhuHuynhs
                               join ac in db.tbAccounts on ph.account_id equals ac.account_id
                               orderby ph.phph_tinhtrang ascending
                               select new
                               {
                                   ac.account_id,
                                   ac.account_vn,
                                   ph.phph_id,
                                   ph.phph_noidung,
                                   ph.phph_datePh,
                                   ac.account_email,
                                   ac.account_phuhuynh,
                                   ac.account_phone,
                                   phph_tinhtrang = ph.phph_tinhtrang == true ? "Đã Xem" : "Chưa Xem"
                               };
                grvList.DataSource = getData;
                grvList.DataBind();
                //btnChiTiet.Visible = false;
                
            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        var loadData = from ph in db.tbPhanHoiPhuHuynhs
                       join ac in db.tbAccounts on ph.account_id equals ac.account_id
                       join l in db.tbLops on logedMember.username_id equals l.username_idVn
                       orderby ph.phph_tinhtrang ascending
                       select new
                       {
                           ac.account_id,
                           ac.account_vn,
                           ph.phph_id,
                           ph.phph_noidung,
                           ph.phph_datePh,
                           ac.account_email,
                           ac.account_phuhuynh,
                           ac.account_phone,
                           phph_tinhtrang = ph.phph_tinhtrang == true ? "Đã Xem" : "Chưa Xem"
                       };
        grvList.DataSource = loadData;
        grvList.DataBind();
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "phph_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbPhanHoiPhuHuynhs
                       join ac in db.tbAccounts on nc.account_id equals ac.account_id
                       where nc.phph_id == _id
                       select new
                       {
                           ac.account_id,
                           ac.account_vn,
                           nc.phph_id,
                           nc.phph_noidung,
                           nc.phph_datePh,
                           ac.account_email,
                           ac.account_phuhuynh,
                           ac.account_phone,
                           nc.phph_tinhtrang,
                       }).Single();
        txttenHS.Text = getData.account_vn;
        txttenPH.Text = getData.account_phuhuynh;
        txtEmail.Text = getData.account_email;
        txtPhone.Text = getData.account_phone;
        txtnoidung.Value = getData.phph_noidung;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        //getData.phph_tinhtrang = true;
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (logedMember.groupuser_id!=2)
        {
            var getTF = (from ph in db.tbPhanHoiPhuHuynhs
                         where ph.phph_id == _id
                         select ph).Single();
            getTF.phph_tinhtrang = true;
            db.SubmitChanges();
        }
        else { }
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "function(){grvList.Refresh();}", true);
        loadData();
    }

    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        var list = from ph in db.tbPhanHoiPhuHuynhs
                   join ac in db.tbAccounts on ph.account_id equals ac.account_id
                   orderby ph.phph_tinhtrang descending
                   where ph.phph_datePh.Value.Date == Convert.ToDateTime(txtTime.Value).Date && ph.phph_datePh.Value.Month == Convert.ToDateTime(txtTime.Value).Month
                   //where acc.account_id == gr.account_id && gr.htr_date.Value.Date == Convert.ToDateTime(Date1.Value).Date && gr.htr_date.Value.Month == Convert.ToDateTime(Date1.Value).Month
                   select new
                   {
                       ac.account_id,
                       ac.account_vn,
                       ph.phph_id,
                       ph.phph_noidung,
                       ph.phph_datePh,
                       ac.account_email,
                       ac.account_phuhuynh,
                       ac.account_phone,
                       phph_tinhtrang = ph.phph_tinhtrang == true ? "Đã Xem" : "Chưa Xem"
                   }; ;
        grvList.DataSource = list;
        grvList.DataBind();
    }

    //protected void btnLuu_Click(object sender, EventArgs e)
    //{

    //}
}