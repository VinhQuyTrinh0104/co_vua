﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Infor : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id, tong, sumcolor;
    public string color;
    int visibleIndex;
    public string tranghai;
    //  tbAccount acc = (tbAccount)Session["TaiKhoan"];
    protected void Page_Load(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        if (Session["TaiKhoan"] == null)
        {
            Response.Redirect("/");
        }
        else
        {
            if (!IsPostBack)
            {
                txtMatKhau.Value = "";
                txtMatKhauMoi.Value = "";
                txtNhapLaiMatKhau.Value = "";
                txtHovaTen.Text = acc.account_vn;
                txtDangNhap.Text = acc.account_code;
                txtTenPhuHuynh.Value = acc.account_phuhuynh;
                txtDienThoai.Text = acc.account_phone;
                txtEmail.Text = acc.account_email;

            }
            loadBaiKiemTra();
            loadsll();
            loaSllmonToan();
            txtNgay.Value = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
    private void loadBaiKiemTra()
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        var list = from bkt in db.tb_BaiTapKiemTraHocSinhs
                   join hstl in db.tbhocsinhtronglops on bkt.hstl_id equals hstl.hstl_id
                   where acc.account_id == bkt.account_id
                   select new
                   {
                       bkt.account_id,
                       bkt.bkt_id,
                       bkt.bkt_name,
                       bkt.bkt_loaibai,
                       bkt.bkt_thoigianlambai,
                       bkt.bkt_tongdiem,
                       bkt.bkt_startdate,
                       bkt.Star
                       // xem = "xem"
                   };
        grvList.DataSource = list;
        grvList.DataBind();
    }
    //Load Sổ liên lạc môn Tiếng ANh
    private void loadsll()
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        var listsll = from sll in db.tbSoLienLacs
                      where sll.sll_hidden==false
                      orderby sll.sll_trangthai ascending
                      where sll.account_id == acc.account_id && sll.monhoc_id == 2
                      select new
                      {
                          sll.sll_id,
                          sll.sll_date,
                          sll.sll_danhgiagv,
                          sll_trangthai= sll.sll_trangthai==true?"Đã Xem":"Xem"
                      };
        grvListdanhgia.DataSource = listsll;
        grvListdanhgia.DataBind();
       
    }
    //Load Sổ Liên lạc môn Toán
    private void loaSllmonToan()
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        var listsllmt = from sll in db.tbSoLienLacs
                      where sll.sll_hidden == false
                      orderby sll.sll_trangthai ascending
                      where sll.account_id == acc.account_id && sll.monhoc_id == 1
                      select new
                      {
                          sll.sll_id,
                          sll.sll_date,
                          sll.sll_danhgiagv,
                          sll_trangthai = sll.sll_trangthai == true ? "Đã Xem" : "Xem"
                      };
        grvBangDanhGia.DataSource = listsllmt;
        grvBangDanhGia.DataBind();
    }
    protected void btnDangXuat_Click(object sender, EventArgs e)
    {
        Session["name"] = null;
        Response.Redirect("/");
    }

    protected void btnThayDoi_Click(object sender, EventArgs e)
    {
        tbAccount update_pass = (tbAccount)Session["TaiKhoan"];
        cls_security md5 = new cls_security();
        string passmd5 = md5.HashCode(txtMatKhau.Value);
        var checkTaiKhoan = (from tk in db.tbAccounts where tk.account_code == Session["TaiKhoan"].ToString() && tk.pass == passmd5 select tk).SingleOrDefault();
        if (update_pass.pass != passmd5)
        {
            alert.alert_Error(Page, "Mật khẩu cũ nhập không đúng!", "");

        }
        else if (update_pass.pass == passmd5)
        {
            if (txtMatKhauMoi.Value == "" || txtNhapLaiMatKhau.Value == "")
            {
                alert.alert_Error(Page, "Bạn chưa nhập mật khẩu mới!", "");
            }
            else if (txtMatKhauMoi.Value != txtNhapLaiMatKhau.Value)
            {
                alert.alert_Error(Page, "Mật khẩu nhập không khớp!", "");
            }
            else
            {
                tbAccount checkTaiKhoan1 = (from tk in db.tbAccounts where tk.account_id == update_pass.account_id select tk).SingleOrDefault();
                checkTaiKhoan1.pass = md5.HashCode(txtNhapLaiMatKhau.Value);
                db.SubmitChanges();
                alert.alert_Success(Page, "Mật khẩu mới đã thay đổi", "");
            }
        }

    }


    protected void btnChiTiet_ServerClick(object sender, EventArgs e)
    {

        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "bkt_id" }));
        Session["_id"] = _id;
        var list1 = from gr1 in db.tb_BaiKiemTraChiTiets
                    join ct2 in db.tb_BaiTapKiemTraHocSinhs on gr1.bkt_id equals ct2.bkt_id
                    where gr1.bkt_id == _id
                    select gr1;
        grvChiTiet.DataSource = list1;
        grvChiTiet.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);

    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        tbAccount updatethongtin = (tbAccount)Session["TaiKhoan"];
        var getthongtin = (from tt in db.tbAccounts
                           where tt.account_id == updatethongtin.account_id
                           select tt).SingleOrDefault();
        getthongtin.account_vn = txtHovaTen.Text;
        getthongtin.account_phuhuynh = txtTenPhuHuynh.Value;
        db.SubmitChanges();
        alert.alert_Success(Page, "Cập nhật thông tin thành công", "");
        txtMatKhau.Value = "";
        txtMatKhauMoi.Value = "";
        txtNhapLaiMatKhau.Value = "";
        var getthongtin2 = (from tt in db.tbAccounts
                            where tt.account_id == updatethongtin.account_id
                            select tt).SingleOrDefault();
        Session["TaiKhoan"] = getthongtin2;
        var ac = (tbAccount)Session["TaiKhoan"];
        txtHovaTen.Text = ac.account_vn;
        txtDangNhap.Text = ac.account_code;
        txtTenPhuHuynh.Value = ac.account_phuhuynh;
        txtDienThoai.Text = ac.account_phone;
        txtEmail.Text = ac.account_email;
    }

    protected void grvChiTiet_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        int value = (int)e.GetValue("bktct_id");
        if (value % 2 != 0)
        {
            e.Row.BackColor = System.Drawing.Color.FromArgb(0xe6e6e6);
        }
        if (value % 2 == 0)
        {
            e.Row.BackColor = System.Drawing.Color.GhostWhite;
        }
        var getdata = from sh in db.tb_BaiKiemTraChiTiets
                      join bkt in db.tb_BaiTapKiemTraHocSinhs on sh.bkt_id equals bkt.bkt_id
                      select new
                      {
                          sh.bkt_sohang1,
                          sh.bkt_sohang2,
                          sh.bkt_sohang3,
                          sh.bkt_ketqua
                      };

    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
    //    tbAccount acc = (tbAccount)Session["TaiKhoan"];
    //    _id = Convert.ToInt32(grvListdanhgia.GetRowValues(grvListdanhgia.FocusedRowIndex, new string[] { "htr_id" }));
    //    Session["_id"] = _id;
    //    var show = from s in db.tbSoLienLacHistories
    //               where s.htr_id == _id
    //               select s;
    //    //show.sll_trangthai = true;
    //    //db.SubmitChanges();
    //    grvShow.DataSource = show;
    //    grvShow.DataBind();
    //    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "Showpopup.Show();", true);
    //    //Trạng Thái False => True
    //    var getId = (from d in db.tbSoLienLacHistories
    //                 where d.htr_id == _id
    //                 select d).FirstOrDefault();
    //    getId.htr_trangthai = true;
    //    try
    //    {
    //        db.SubmitChanges();
    //    }
    //    catch { }
    //    //Nếu False => hiện "Xem" trên web, True => hiện "Đã Xem"
    //    ASPxButton button = grvListdanhgia.FindRowCellTemplateControl(visibleIndex, null, "btnXem") as ASPxButton;
    //    var check = from ch in db.tbSoLienLacHistories
    //                select ch;
    //    foreach (var item in check)
    //    {
    //        if (item.htr_trangthai == true)
    //        {
    //            button.Text = "Đã Xem";
    //        }
    //        else
    //        {
    //            button.Text = "Xem";
    //        }
    //    }

   }
    protected void btnTimKiemDanhGia_Click(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];

        var listdg = from gr in db.tbSoLienLacs
                         //join hstl2 in db.tbhocsinhtronglops on gr.hstl_id equals hstl2.hstl_id
                     where acc.account_id == gr.account_id && gr.sll_date.Value.Date == Convert.ToDateTime(Date1.Value).Date && gr.sll_date.Value.Month == Convert.ToDateTime(Date1.Value).Month
                     select new
                     {
                         gr.sll_id,
                         gr.sll_date,
                         gr.sll_danhgiagv,
                         sll_trangthai = gr.sll_trangthai == true ? "Đã Xem" : "Xem"
                     };
        grvListdanhgia.DataSource = listdg;
        grvListdanhgia.DataBind();
    }
    protected void btnTimKiem_Click(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];

        var list = from gr in db.tb_BaiTapKiemTraHocSinhs
                   join hstl2 in db.tbhocsinhtronglops on gr.hstl_id equals hstl2.hstl_id
                   where acc.account_id == hstl2.account_id && gr.bkt_startdate.Value.Date == Convert.ToDateTime(dteDate.Value).Date && gr.bkt_startdate.Value.Month == Convert.ToDateTime(dteDate.Value).Month
                   select gr;
        grvList.DataSource = list;
        grvList.DataBind();
    }

    //protected void grvListdanhgia_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    //{
    //    tbAccount acc = (tbAccount)Session["TaiKhoan"];
    //    _id = Convert.ToInt32(grvListdanhgia.GetRowValues(grvListdanhgia.FocusedRowIndex, new string[] { "htr_id" }));
    //    if (e.RowType != GridViewRowType.Data) return;

    //    ASPxButton btn = grvListdanhgia.FindRowCellTemplateControl(e.VisibleIndex, null,
    //    "btnXem") as ASPxButton;
    //    var check = (from ch in db.tbSoLienLacHistories
    //                where ch.htr_id == _id
    //                 select ch);
    //    foreach (var item in check)
    //    {
    //        if (item.htr_trangthai == false)
    //        {
    //            btn.Text = "Xem";
    //        }
    //        else
    //        {
    //            btn.Text = "Đã Xem";
    //        }
    //    }
        
    //}

    protected void btnPhanHoi_Click(object sender, EventArgs e)
    {
        txtNoidungPh.Value = "";
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "ShowpopupPH.Show();", true);
    }

    protected void btnGuiPH_Click(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];

        tbPhanHoiPhuHuynh ins = new tbPhanHoiPhuHuynh();
        ins.phph_noidung = txtNoidungPh.Value;
        ins.phph_datePh = DateTime.Now;
        ins.account_id = acc.account_id;
        ins.phph_tinhtrang = false;
        db.tbPhanHoiPhuHuynhs.InsertOnSubmit(ins);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã gửi phản hồi!", "");
        SendMail(acc.account_email);
    }
    private bool SendMail(string email)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        //string link = "https://ansipan.com/verify-email-" + gettoken;
        if (email != "")
        {
            try
            {
                //-----gửi mail------
                //get email phụ huynh
                string from = "mailteamgenera@gmail.com";
                const string fromPassword = "xkspgfsgcghagqbk";
                //get mail giáo viên
                var checkmail = (from user in db.admin_Users
                                 join l in db.tbLops on user.username_id equals l.username_idVn
                                 join hstl in db.tbhocsinhtronglops on l.lop_id equals hstl.lop_id
                                 select user).First();
                string to = checkmail.username_email;
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(from, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(from, "SuperFriends.com");
                mm.Subject = "Thông báo mới từ Trung tâm";
                mm.To.Add(to);
                mm.IsBodyHtml = true;
                mm.Body = "<!DOCTYPE html><html><head><title></title></head><body style=\" width:600px; margin:0px;\"><div>" +
                "<p style=\"margin-top:0px; text-align:left; font-size:18px;color:#000000\">Bạn có một phản hồi mới từ phụ huynh!<br/>Hãy đăng nhập vào hệ thống để kiểm tra.<br/>Xin cảm ơn!</p>" +
                "</div></body></html>";
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        _id = Convert.ToInt32(grvListdanhgia.GetRowValues(grvListdanhgia.FocusedRowIndex, new string[] { "sll_id" }));
        Session["_id"] = _id;
        var show = from s in db.tbSoLienLacs
                   where s.sll_id == _id
                   select s;
        //show.sll_trangthai = true;
        //db.SubmitChanges();
        grvShow.DataSource = show;
        grvShow.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "Showpopup.Show();", true);
        //Trạng Thái False => True
        var getId = (from d in db.tbSoLienLacs
                     where d.sll_id == _id
                     select d).FirstOrDefault();
        getId.sll_trangthai = true;
        try
        {
            db.SubmitChanges();
            loadsll();
        }
        catch { }
    }


    protected void btnXem_Click1(object sender, EventArgs e)
    {
        tbAccount acc = (tbAccount)Session["TaiKhoan"];
        _id = Convert.ToInt32(grvBangDanhGia.GetRowValues(grvBangDanhGia.FocusedRowIndex, new string[] { "sll_id" }));
        Session["_id"] = _id;
        var show = from s in db.tbSoLienLacs
                   where s.sll_id == _id
                   select s;
        //show.sll_trangthai = true;
        //db.SubmitChanges();
        grvShow.DataSource = show;
        grvShow.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "Showpopup.Show();", true);
        //Trạng Thái False => True
        var getId = (from d in db.tbSoLienLacs
                     where d.sll_id == _id
                     select d).FirstOrDefault();
        getId.sll_trangthai = true;
        try
        {
            db.SubmitChanges();
            loaSllmonToan();
        }
        catch { }
    }
}