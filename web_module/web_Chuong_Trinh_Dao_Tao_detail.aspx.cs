﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_Chuong_Trinh_Dao_Tao_detail : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    SqlCommand cmd;
    cls_Alert alert = new cls_Alert();
    public string title, b;
    public int stt = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        var newsss = from ns in db.tbNews
                     join nc in db.tbNewCates on ns.newcate_id equals nc.newcate_id
                     where ns.news_id == Convert.ToInt32(RouteData.Values["id"]) && ns.active == true
                     select new
                     {
                         nc.newcate_id,
                         ns.news_id,
                         ns.news_title,
                         ns.news_image,
                         ns.news_summary,
                         ns.news_content,
                         nc.link_seo,
                         news_datetime = String.Format("{0:dd/MM/yyyy}", ns.news_datetime)
                     };
        title = newsss.First().news_title;
        rpNews.DataSource = newsss;
        rpNews.DataBind();
        //repeater tin liên quan
        rpTinTucLienQuan.DataSource = (from ttlq in db.tbNews
                                       join nc in db.tbNewCates on ttlq.newcate_id equals nc.newcate_id
                                       where ttlq.news_id != Convert.ToInt32(RouteData.Values["id"]) && ttlq.newcate_id == newsss.First().newcate_id && ttlq.active == true
                                       orderby ttlq.news_id descending
                                       select new
                                       {
                                           nc.newcate_id,
                                           ttlq.news_id,
                                           ttlq.news_title,
                                           ttlq.news_image,
                                           ttlq.news_summary,
                                           ttlq.news_content,
                                           nc.link_seo,
                                           news_datetime = String.Format("{0:dd/MM/yyyy}", ttlq.news_datetime)
                                       }).Take(5);
        rpTinTucLienQuan.DataBind();

    }
}