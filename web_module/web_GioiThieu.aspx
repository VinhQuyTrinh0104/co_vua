﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_GioiThieu.aspx.cs" Inherits="web_module_gioithieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section class="blog b-archives single section">
        <div class="container">
            <div class="row">
               <%-- <div class="col-lg-8 col-12">--%>
                    <div class="single-main">
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <div class="blog-detail">

                                    <div class="detail-content">
                                        <div class="img-gioithieu">
                                            <img src="../images/07082019_062516_CH_chitiet.jpg" alt="Alternate Text" />
                                        </div>
                                        <asp:Repeater ID="rpIntroduce" runat="server">
                                            <ItemTemplate>
                                                <h2 class="blog-title"><a href="#"><%#Eval("introduce_title") %> </a></h2>
                                                <p><%#Eval("introduce_content") %> </p>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                                    <div class="comment-main">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               <%-- </div>--%>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

