﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_TinTucChiTiet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int stt = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["active_tc"] = null;
        Session["active_gt"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = "active";
        Session["active_lh"] = null;
        var listNew = (from nc in db.tbNews
                       where nc.news_id == Convert.ToInt32(RouteData.Values["id"])
                       select nc);
        rpNews.DataSource = listNew;
        rpNews.DataBind();

        //rpMenuTinTuc.DataSource = from mtt in db.tbNewCates
        //                          select new
        //                          {
        //                              mtt.newcate_id,
        //                              mtt.newcate_title,
        //                              mtt.link_seo,
        //                              sum_new = (from n in db.tbNews where n.newcate_id== mtt.newcate_id select n).Count()
        //                          };
        //rpMenuTinTuc.DataBind();
        rpTinTucLienQuan.DataSource = (from ttlq in db.tbNews
                                       where ttlq.newcate_id == listNew.First().newcate_id
                                       where ttlq.news_id != Convert.ToInt32(RouteData.Values["id"])
                                       orderby ttlq.news_id descending
                                       select ttlq).Take(5);
        rpTinTucLienQuan.DataBind();

    }
    //code tự tăng

}