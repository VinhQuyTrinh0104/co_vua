﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="Account_Infor.aspx.cs" Inherits="Account_Infor" %>

<%--Header--%>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../admin_js/sweetalert.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        #account .container {
            display: block;
            margin: 5px auto;
        }

            #account .container h2 {
                text-align: center;
                padding: 20px 0;
                color: #ffd800;
            }

            #account .container .nav .nav-item .dangxuat {
                background-color: #ffffff;
                border: none;
                font-weight: 500;
                font-size: 15px;
                margin: 7px 15px;
            }

            #account .container .tab-content .contact .inf, .fade .inf {
                background-color: #fff;
                width: 50%;
                border: none;
                border-bottom: 1px solid #eae4e4;
                color: #000000;
                padding-left: 15px;
            }

            #account .container .tab-content .fade .err {
                color: red;
                font-weight: 700;
            }
    </style>
    <section id="account">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <ul class="nav flex-column nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="true">Thông tin tài khoản</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Đổi mật khẩu </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sổ liên lạc điện tử Toán</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profilee-tab" data-toggle="tab" href="#profilee" role="tab" aria-controls="profile" aria-selected="false">Sổ liên lạc điện tử Tiếng Anh</a>
                        </li>
                        <li class="nav-item" style="display: none;">
                            <a class="nav-link" href="web_Sticker.aspx">Sticker</a>
                        </li>
                        <li class="nav-item">
                            <asp:Button ID="btnDangXuat" CssClass="text-primary dangxuat" runat="server" Text="Thoát tài khoản" OnClick="btnDangXuat_Click" />
                        </li>
                    </ul>
                </div>
                <div class="col-8">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <asp:GridView ID="grvThongtin" runat="server">
                            </asp:GridView>
                            <h3 class="text-uppercase" style="margin: 0 auto;">thông tin cá nhân</h3>
                            <br />
                            <div class="contact">
                                <div class="form-group">
                                    <label>Họ và tên:</label>
                                    <asp:TextBox ID="txtHovaTen" runat="server" CssClass="form-control inf" autocomplete="off"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Tên đăng nhập:</label>
                                    <asp:TextBox ID="txtDangNhap" runat="server" CssClass="form-control inf" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Họ và tên phụ huynh:</label>
                                    <input id="txtTenPhuHuynh" type="text" runat="server" class="form-control inf" autocomplete="off" />
                                </div>
                                <div class="form-group">
                                    <label>Số điện thoại:</label>
                                    <asp:TextBox ID="txtDienThoai" runat="server" CssClass="form-control inf" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Email:</label>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control inf" Enabled="false"></asp:TextBox>
                                </div>
                                <asp:UpdatePanel ID="Updatethongtin" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <asp:Button ID="btnCapNhat" runat="server" Text="Cập nhật" OnClick="btnCapNhat_Click" CssClass="btn btn-primary text-uppercase" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="tab-pane fade show " id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3 class="text-uppercase" style="margin: 0 auto;">Đổi mật khẩu</h3>
                            <div class="form-group">
                                <label>Nhập mật khẩu cũ:</label>
                                <input id="txtMatKhau" runat="server" type="password" class=" form-control inf" />
                            </div>
                            <div class="form-group">
                                <label>Nhập mật khẩu mới:</label>
                                <input id="txtMatKhauMoi" runat="server" type="password" class=" form-control inf" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu mới:</label>
                                <input id="txtNhapLaiMatKhau" runat="server" type="password" class=" form-control inf" />
                            </div>
                            <asp:UpdatePanel ID="updatepass" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <asp:Button ID="btnThayDoi" runat="server" Text="Cập nhật" OnClick="btnThayDoi_Click" CssClass="btn btn-primary text-uppercase" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                        <%--sổ liên lạc toán--%>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <h3>Sổ liên lạc điện tử Toán</h3>
                            <asp:UpdatePanel ID="yhjnj" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <%--<label>Lớp:</label>
                                        <strong><asp:Label ID="lblLop" runat="server" font-size="Medium"></asp:Label></strong>--%>
                                        <label class=" form-control-label">Chọn ngày làm:</label>
                                        <input type="date" id="dteDate" runat="server" />
                                        <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" CssClass="btn btn-primary" OnClick="btnTimKiem_Click" />
                                    </div>
                                    <h5 class="pb-2">Bảng Bài Kiểm Tra Học Sinh</h5>
                                    <div class="form-group table-responsive">
                                        <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="bkt_id" Width="100%">
                                            <Columns>
                                                <%--<dx:GridViewDataColumn Caption="ID hs" FieldName="account_id" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Mã bài" FieldName="bkt_id" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>--%>
                                                <dx:GridViewDataColumn Caption="Tên bài" FieldName="bkt_name" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Loại bài" FieldName="bkt_loaibai" HeaderStyle-HorizontalAlign="Center" Width="15%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Thời gian làm bài" FieldName="bkt_thoigianlambai" HeaderStyle-HorizontalAlign="Center" Width="15%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Tổng điểm" FieldName="bkt_tongdiem" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Ngày làm" FieldName="bkt_startdate" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Sao" FieldName="Star" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Chi Tiết" FieldName="xem" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true">
                                                    <DataItemTemplate>
                                                        <a href="#" id="btnChiTiet" runat="server" onserverclick="btnChiTiet_ServerClick">Xem</a>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsBehavior AllowFocusedRow="true" />
                                            <SettingsText EmptyDataRow="Empty" />
                                            <SettingsLoadingPanel Text="Loading..." />
                                            <SettingsPager PageSize="8" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                        </dx:ASPxGridView>
                                    </div>
                                    <%--Bảng đánh giá--%>
                                    <div class="form-group">

                                        <label class=" form-control-label">Chọn ngày đánh giá:</label>
                                        <input type="date" id="Date2" runat="server" />
                                        <asp:Button ID="Button1" runat="server" Text="Tìm kiếm" CssClass="btn btn-primary" OnClick="btnTimKiemDanhGia_Click" />
                                    </div>
                                    <h5 class="pb-2">Bảng Đánh Giá Học Sinh</h5>
                                    <div class="form-group table-responsive">
                                        <dx:ASPxGridView ID="grvBangDanhGia" runat="server" ClientInstanceName="grvBangDanhGia" KeyFieldName="sll_id" Width="100%">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Ngày đánh giá" FieldName="sll_date" HeaderStyle-HorizontalAlign="Center" Width="12%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Nội dung thông báo" FieldName="sll_danhgiagv" HeaderStyle-HorizontalAlign="Center" Width="15%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <%--<dx:GridViewDataColumn Caption="Đánh giá năng lực học sinh" FieldName="htr_danhgiahs" HeaderStyle-HorizontalAlign="Center" Width="22%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                                                <dx:GridViewDataColumn Caption="Chi Tiết" FieldName="xem" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true">
                                                    <DataItemTemplate>
                                                        <asp:Button runat="server" ID="btnXem" Text='<%# Eval("sll_trangthai") %>' OnClick="btnXem_Click1" CausesValidation="false" CssClass="btn btn-primary" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Gửi Phản Hồi" FieldName="phanhoi" HeaderStyle-HorizontalAlign="Center" Width="13%" HeaderStyle-Font-Bold="true">
                                                    <DataItemTemplate>
                                                        <asp:Button runat="server" ID="btnPhanHoi" Text="Gửi phản hồi" OnClick="btnPhanHoi_Click" CssClass="btn btn-primary" />
                                                        <%--<dx:ASPxButton ID="btnPhanHoi" runat="server" OnClick="btnPhanHoi_Click" Text='Gửi Phản Hồi' Width="80%">
                                                        </dx:ASPxButton>--%>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsBehavior AllowFocusedRow="true" />
                                            <SettingsText EmptyDataRow="Empty" />
                                            <SettingsLoadingPanel Text="Loading..." />
                                            <SettingsPager PageSize="5" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                        </dx:ASPxGridView>
                                    </div>
                                    <%--END bảng đánh giá--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <dx:ASPxPopupControl ID="popupControl" runat="server" Width="600px" Height="530px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl"
                            HeaderText="BÀI LÀM CHI TIẾT" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
                            <ContentCollection>
                                <dx:PopupControlContentControl runat="server">
                                    <asp:UpdatePanel ID="udPopup" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-main">
                                                <div class="div_content">
                                                    <div class="">
                                                        <div class="form-group">
                                                            <dx:ASPxGridView ID="grvChiTiet" runat="server" ClientInstanceName="grvList" KeyFieldName="bktct_id" Width="100%" OnHtmlRowPrepared="grvChiTiet_HtmlRowPrepared">
                                                                <Columns>
                                                                    <dx:GridViewDataColumn Caption="Số hạng 1" FieldName="bkt_sohang1" HeaderStyle-HorizontalAlign="Center" Width="18%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="Số hạng 2" FieldName="bkt_sohang2" HeaderStyle-HorizontalAlign="Center" Width="18%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="Số hạng 3" FieldName="bkt_sohang3" HeaderStyle-HorizontalAlign="Center" Width="18%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="Kết quả" FieldName="bkt_ketqua" HeaderStyle-HorizontalAlign="Center" Width="18%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsBehavior AllowFocusedRow="true" />
                                                                <SettingsText EmptyDataRow="Empty" />
                                                                <SettingsLoadingPanel Text="Loading..." />
                                                                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                                            </dx:ASPxGridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                        <%--sổ liên lạc anh--%>
                        <div class="tab-pane fade" id="profilee" role="tabpanel" aria-labelledby="profile-tab">
                            <h3>Sổ liên lạc điện tử Tiếng Anh</h3>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <%--<label>Lớp:</label>
                                        <strong><asp:Label ID="lblLop" runat="server" font-size="Medium"></asp:Label></strong>--%>
                                        <label class=" form-control-label">Chọn ngày đánh giá:</label>
                                        <input type="date" id="Date1" runat="server" />
                                        <asp:Button ID="btnTimKiemDanhGia" runat="server" Text="Tìm kiếm" CssClass="btn btn-primary" OnClick="btnTimKiemDanhGia_Click" />
                                    </div>
                                    <div class="form-group table-responsive">
                                        <dx:ASPxGridView ID="grvListdanhgia" runat="server" ClientInstanceName="grvListdanhgia" KeyFieldName="sll_id" Width="100%">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Ngày đánh giá" FieldName="sll_date" HeaderStyle-HorizontalAlign="Center" Width="12%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Nội dung thông báo" FieldName="sll_danhgiagv" HeaderStyle-HorizontalAlign="Center" Width="15%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                <%--<dx:GridViewDataColumn Caption="Đánh giá năng lực học sinh" FieldName="htr_danhgiahs" HeaderStyle-HorizontalAlign="Center" Width="22%" Settings-AllowEllipsisInText="true" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                                                <dx:GridViewDataColumn Caption="Chi Tiết" FieldName="xem" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true">
                                                    <DataItemTemplate>
                                                        <asp:Button runat="server" ID="btnXem" Text='<%# Eval("sll_trangthai") %>' OnClick="btnXem_Click" CausesValidation="false" CssClass="btn btn-primary" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Gửi Phản Hồi" FieldName="phanhoi" HeaderStyle-HorizontalAlign="Center" Width="13%" HeaderStyle-Font-Bold="true">
                                                    <DataItemTemplate>
                                                        <asp:Button runat="server" ID="btnPhanHoi" Text="Gửi phản hồi" OnClick="btnPhanHoi_Click" CssClass="btn btn-primary" />
                                                        <%--<dx:ASPxButton ID="btnPhanHoi" runat="server" OnClick="btnPhanHoi_Click" Text='Gửi Phản Hồi' Width="80%">
                                                        </dx:ASPxButton>--%>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsBehavior AllowFocusedRow="true" />
                                            <SettingsText EmptyDataRow="Empty" />
                                            <SettingsLoadingPanel Text="Loading..." />
                                            <SettingsPager PageSize="5" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                        </dx:ASPxGridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <%--Popup Xem chi tiết--%>
                        <dx:ASPxPopupControl ID="Showpopup" runat="server" Width="800px" Height="330px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="Showpopup"
                            HeaderText="BẢNG ĐÁNH GIÁ CHI TIẾT" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
                            <ContentCollection>
                                <dx:PopupControlContentControl runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-main">
                                                <div class="div_content">
                                                    <div class="">
                                                        <div class="form-group">
                                                            <dx:ASPxGridView ID="grvShow" runat="server" ClientInstanceName="grvListdanhgia" KeyFieldName="sll_id" Width="100%">
                                                                <Columns>
                                                                    <dx:GridViewDataColumn Caption="Ngày" FieldName="sll_date" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <%--<dx:GridViewDataColumn Caption="Đánh giá học sinh" FieldName="htr_danhgiahs" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                                                                    <dx:GridViewDataColumn Caption="Nội dung thông báo" FieldName="sll_danhgiagv" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="KQ giữa kỳ" FieldName="sll_kqgiuaky" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="KQ cuối kỳ" FieldName="sll_kqcuoiky" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                                                    <%--<dx:GridViewDataColumn Caption="Tình Trạng" FieldName="htr_tinhtrang" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-BackColor="GhostWhite" CellStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                                                                </Columns>
                                                                <SettingsBehavior AllowFocusedRow="true" />
                                                                <SettingsText EmptyDataRow="Empty" />
                                                                <SettingsLoadingPanel Text="Loading..." />
                                                                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                                            </dx:ASPxGridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                        <%--Popup Phụ huynh gửi phản hồi--%>
                        <dx:ASPxPopupControl ID="ShowpopupPH" runat="server" Width="650px" Height="450px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
                            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="ShowpopupPH"
                            HeaderText="GỬI NỘI DUNG PHẢN HỒI" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
                            <ContentCollection>
                                <dx:PopupControlContentControl runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-main">
                                                <div class="div_content">
                                                    <div class="form-group">
                                                        <div class="col-12 form-group">
                                                            <label class="col-1 col-sm-1 form-control-label mb-2">Ngày:</label>
                                                            <input type="text" id="txtNgay" class="" runat="server" disabled="disabled" />
                                                        </div>
                                                        <div class="col-12 form-group">
                                                            <label><strong>Nội dung phản hồi:</strong></label>
                                                            <textarea id="txtNoidungPh" runat="server" rows="8" class="form-control" style="width: 90%"></textarea>
                                                        </div>
                                                        <asp:Button runat="server" ID="btnGuiPH" Text="Gửi" OnClick="btnGuiPH_Click" CssClass="btn btn-primary" />
                                                        <%--<dx:ASPxButton ID="btnGuiPH" runat="server" OnClick="btnGuiPH_Click" Text="Gửi"></dx:ASPxButton>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </dx:PopupControlContentControl>
                            </ContentCollection>
                        </dx:ASPxPopupControl>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<%--Footer--%>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>



