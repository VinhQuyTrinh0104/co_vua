﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_vjlc_Chuong_Trinh_Khoa_hoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    SqlCommand cmd;
    cls_Alert alert = new cls_Alert();
    public string title, b;
    public int stt = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        var getData = from kh in db.tbIntroduce_KhoaHocs where kh.introkh_link == RouteData.Values["name"].ToString() select kh;
        title = getData.First().introkh_title;
        rpNews.DataSource = getData;
        rpNews.DataBind();
        //repeater khóa học khác
        var getKhoaHocKhac = from kh in db.tbIntroduce_KhoaHocs where kh.introkh_link != RouteData.Values["name"].ToString() select kh;
        rpTinTucLienQuan.DataSource = getKhoaHocKhac;
        rpTinTucLienQuan.DataBind();

    }
}