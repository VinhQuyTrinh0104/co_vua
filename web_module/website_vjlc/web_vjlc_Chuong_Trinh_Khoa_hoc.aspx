﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_vjlc_Chuong_Trinh_Khoa_hoc.aspx.cs" Inherits="web_module_web_vjlc_Chuong_Trinh_Khoa_hoc" %>

<%@ Register Src="~/web_usercontrol/global_pagging.ascx" TagPrefix="uc1" TagName="linqPaging" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        $('#Newtt').easyPaginate({
            paginateElement: 'img',
            elementsPerPage: 3,
            effect: 'climb'
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section class="events section-bg section">
        <div class="container">
            <div class="row">
                <div class="col-12 wow zoomIn">
                    <div class="section-title">
                        <h2><%=title %></h2>
                    </div>
                </div>
            </div>
            <%-- <div class="col-lg-8 col-12">--%>
            <div class="single-main">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="blog-detail">
                            <div class="detail-content">
                                <asp:Repeater ID="rpNews" runat="server">
                                    <ItemTemplate>
                                        <div>
                                            <!-- Single Event -->
                                            <div class="wrap">
                                                <%--<h1 class="p-4 text-center"><%#Eval("news_title")%></h1>--%>
                                                <div class="wrap-content">
                                                    <%--<h1 class="p-4 text-center"><%#Eval("news_summary")%></h1>--%>
                                                    <%--<p><%#Eval("news_title") %></p>--%>
                                                    <p><%#Eval("introkh_content")%></p>
                                                </div>
                                            </div>
                                            <!--/ End Single Event -->
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- </div>--%>
            <div class="courses single">
                <div class="course-required">
                    <h4>Khóa học khác :</h4>
                    <ul>
                        <asp:Repeater ID="rpTinTucLienQuan" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href="../../vjlc-chuong-trinh-<%#Eval("introkh_link") %>/<%#cls_ToAscii.ToAscii(Eval("introkh_title").ToString()) %>-<%#Eval("introkh_id") %>"><span><%=stt++ %></span><%#Eval("introkh_title") %></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
