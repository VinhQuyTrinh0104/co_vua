﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_TuyenDung : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string tintuc;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["active_tc"] = null;
        Session["active_gt"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = null;
        Session["active_td"] = "active";
        Session["active_gph"] = null;
        Session["active_lh"] = null;
        Session["active_ctdt"] = null;
        //var getCate = (from nc in db.tbNewCates where nc.newcate_id == Convert.ToInt32(RouteData.Values["id"]) select nc).SingleOrDefault();
        var getdata = (from ns in db.tbNews where ns.newcate_id == 7 orderby ns.news_id descending select ns);
        int item = 10;
        Decimal totalpage = Math.Ceiling(decimal.Divide((Decimal)getdata.Count(), (Decimal)item));
        if (Request.Params["Page"] != null)
        {
            rpNews.DataSource = getdata.Skip(item * (Convert.ToInt32(Request.Params["page"]) - 1)).Take(item).ToList();
            linqPaging.CurrentPage = Convert.ToInt32(Request.Params["page"]);
        }
        else
        {
            rpNews.DataSource = getdata.Take(item).ToList();
            linqPaging.CurrentPage = 1;
        }
        rpNews.DataBind();
        linqPaging.MaxPage = Convert.ToInt32(totalpage);
        if (getdata.Count() > 0)
        {
            linqPaging.pagingUrl = "tuyen-dung-7";
        }
    }
}