﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_News : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string tintuc;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["active_tc"] = null;
        Session["active_gt"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = "active";
        Session["active_td"] = null;
        Session["active_gph"] = null;
        Session["active_lh"] = null;
        int id_cate = 10;
        var getCate = (from nc in db.tbNewCates where nc.newcate_id == id_cate select nc).SingleOrDefault();
        var getdata = (from ns in db.tbNews where ns.newcate_id == id_cate orderby ns.news_datetime descending select ns);
        int item = 2;
        Decimal totalpage = Math.Ceiling(decimal.Divide((Decimal)getdata.Count(), (Decimal)item));
        if (Request.Params["Page"] != null)
        {
            rpNews.DataSource = getdata.Skip(item * (Convert.ToInt32(Request.Params["page"]) - 1)).Take(item).ToList();
            linqPaging.CurrentPage = Convert.ToInt32(Request.Params["page"]);
        }
        else
        {
            rpNews.DataSource = getdata.Take(item).ToList();
            linqPaging.CurrentPage = 1;
        }
        rpNews.DataBind();
        linqPaging.MaxPage = Convert.ToInt32(totalpage);
        if (getdata.Count() > 0)
        {
            linqPaging.pagingUrl = cls_ToAscii.ToAscii(getCate.newcate_title) + "-" + getCate.newcate_id;
        }
    }
}