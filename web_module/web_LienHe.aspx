﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_LienHe.aspx.cs" Inherits="web_module_lienhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../admin_js/sweetalert.min.js"></script>
    <%--<script>
        function validateEmail(txtEmail) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(txtEmail).toLowerCase());}
    </script>
    <script>

    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section id="contact" class="contact section">
        <div class="container">
            <asp:UpdatePanel ID="up_Button" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-title">
                                <h2>Liên hệ <span>SUPER FRIENDS</span> </h2>

                            </div>
                        </div>
                    </div>
                    <div class="contact-head">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-12">
                                <h2>Hệ thống Trung tâm Anh Ngữ và Toán Tư Duy Super Friends</h2>
                                <asp:Repeater ID="rpDiaChi" runat="server">
                                    <ItemTemplate>
                                        <div class="contact-info">
                                            <div class="icon"><i class="fa fa-map"></i></div>
                                            <h3><%#Eval("diachi_title") %></h3>
                                            <p><%#Eval("diachi_summary") %></p>
                                            <p><%#Eval("diachi_phone") %></p>
                                            <p>Hotline: <%#Eval("diachi_hotline") %></p>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="col-lg-6 col-md-6 col-12">
                                <div class="form-head">
                                    <div class="form-group">
                                        <label>Họ và tên</label>
                                        <input type="text" id="txtName" placeholder="" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" id="txtEmail" placeholder="" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        <label>Số điện thoại</label>
                                        <input type="text" id="txtPhone" placeholder="" runat="server" />
                                    </div>
                                    <div class="form-group">
                                        <label>Nội dung</label>
                                        <textarea id="txtContent" placeholder="Comment" runat="server"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div class="button">
                                            <button id="btn_sendnew" runat="server" onserverclick="btn_sendnew_ServerClick" type="submit" class="btn primary">Gửi tin</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="contact-bottom">
            <nav class="map-lh">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#mapcs1" role="tab" aria-controls="nav-home" aria-selected="true">Cơ sở 1</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#mapcs2" role="tab" aria-controls="nav-profile" aria-selected="false">Cơ sở 2</a>
                </div>
            </nav>
            <br />
            <br />
            <div class="tab-content" id="nav-tabContent">

                <div class="tab-pane fade show active" id="mapcs1" role="tabpanel" aria-labelledby="nav-home">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.332540063623!2d108.23373686648695!3d16.04822445106304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314219d8fc688991%3A0x9abde119e2d0464d!2zMTggRMawxqFuZyBLaHXDqiwgQuG6r2MgTeG7uSBQaMO6LCBOZ8WpIEjDoG5oIFPGoW4sIMSQw6AgTuG6tW5nIDU1MDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1565001631497!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                </div>

                <div class="tab-pane fade show " id="mapcs2" role="tabpanel" aria-labelledby="nav-profile">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.5297595852494!2d108.21593391433636!3d16.037976344535913!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314219ebbbc697bb%3A0x4059a71f0c585970!2zMTk3IEzDqiBUaGFuaCBOZ2jhu4ssIEhvw6AgQ8aw4budbmcgQuG6r2MsIEjhuqNpIENow6J1LCDEkMOgIE7hurVuZywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1565001706926!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

