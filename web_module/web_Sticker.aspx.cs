﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_Sticker : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
            var taikhoan = (tbAccount)Session["TaiKhoan"];
            if (Session["TaiKhoan"] != null)
            {
                var getHSTL = (from hstl in db.tbhocsinhtronglops
                               where hstl.account_id == taikhoan.account_id
                               select hstl).SingleOrDefault();

                var getGift = from gift in db.tbGifts select gift;
                var getQuaHocSinh = from qhs in db.tbQuaCuaHocSinhs
                                    join hstl in db.tbhocsinhtronglops on qhs.hstl_id equals hstl.hstl_id
                                    join gift in db.tbGifts on qhs.gift_id equals gift.gift_id
                                    where taikhoan.account_id == hstl.account_id
                                    select qhs;
                var getGift2 = from gift in db.tbGifts select new { gift.gift_image, gift.gift_id, gift.gift_price };
                var getQuaHocSinh2 = from qhs in db.tbQuaCuaHocSinhs
                                     join hstl in db.tbhocsinhtronglops on qhs.hstl_id equals hstl.hstl_id
                                     join gift in db.tbGifts on qhs.gift_id equals gift.gift_id
                                     where taikhoan.account_id == hstl.account_id
                                     select new { qhs.gift_image, qhs.gift_id, qhs.gift_price };
                var getSticker = (from stk in db.tbStickers
                                  join hstl in db.tbhocsinhtronglops on stk.hstl_id equals hstl.hstl_id
                                  where taikhoan.account_id == hstl.account_id
                                  select stk).FirstOrDefault();
                if (getHSTL == null)
                {
                    tbhocsinhtronglop hstl = new tbhocsinhtronglop();
                    hstl.account_id = taikhoan.account_id;
                    hstl.lop_id = 1004;
                    db.tbhocsinhtronglops.InsertOnSubmit(hstl);
                    db.SubmitChanges();
                }
                else
                {
                    var getLop = (from l in db.tbLops
                                  join hstl in db.tbhocsinhtronglops on l.lop_id equals hstl.lop_id
                                  where taikhoan.account_id == hstl.account_id
                                  select l).SingleOrDefault();
                    lblLop.InnerText = getLop.lop_name;
                }

                lblTen.InnerText = taikhoan.account_vn;
                var getHSTL2 = (from hstl in db.tbhocsinhtronglops
                                where hstl.account_id == taikhoan.account_id
                                select hstl).FirstOrDefault();
                if (getSticker == null)
                {
                    var getsao = (from s in db.tbSumStars
                                  where s.account_id == taikhoan.account_id
                                  select s).SingleOrDefault();

                    tbSticker stk = new tbSticker();
                    stk.hstl_id = getHSTL2.hstl_id;
                    stk.sticker_image = "/images/sticker_1.jpg";
                    stk.sticker_name = "OK";
                if(getsao!=null)
                {
                    int stickers = Convert.ToInt32(getsao.sumstar / 3);
                    stk.sticker_amount = stickers;
                    getsao.sumstar = getsao.sumstar - stickers * 3;
                    db.SubmitChanges();
                }
                else
                stk.sticker_amount = 0;
                    db.tbStickers.InsertOnSubmit(stk);
                    db.SubmitChanges();
                    db.SubmitChanges();
                }
                else
                {
                    var getsao = (from s in db.tbSumStars
                                  where s.account_id == taikhoan.account_id
                                  select s).SingleOrDefault();
                    int stickerss = Convert.ToInt32(getsao.sumstar / 3);
                    getSticker.sticker_amount += stickerss;
                    getsao.sumstar = getsao.sumstar - stickerss * 3;
                    db.SubmitChanges();
                }
                var getSticker2 = (from stk in db.tbStickers
                                   join hstl in db.tbhocsinhtronglops on stk.hstl_id equals hstl.hstl_id
                                   where taikhoan.account_id == hstl.account_id
                                   select stk).FirstOrDefault();
                lblStickerAmount.InnerText = getSticker2.sticker_amount.ToString();
                //Dung DataTable de lay so ban ghi sticker(vd:sticker 20 cai thi DataTable se luu 20 ban ghi sticker) 
                DataTable sticker = new DataTable("Sticker");
                sticker.Columns.Add(new DataColumn(("sticker_image"), typeof(string)));
                for (int i = 0; i < getSticker2.sticker_amount; i++)
                {
                    sticker.Rows.Add(getSticker2.sticker_image);
                }
                //Lay ra danh sach Gift ma hoc sinh chua so huu
                var getQua = getGift2.Except(getQuaHocSinh2);//var layA=A.Except(B); lay cac phan tu thuoc A ma k thuoc B
                rpSticker.DataSource = sticker;
                rpSticker.DataBind();
                rpGift.DataSource = getQua;
                rpGift.DataBind();
                rpQuaHocSinh.DataSource = getQuaHocSinh;
                rpQuaHocSinh.DataBind();
            }
            else
            {
                Response.Redirect("/Default.aspx");
            }
    }
    protected void btnGift_Click(object sender, EventArgs e)
    {


        //lay ra mon qua ma hoc sinh dang muon doi(id=txtGifl.Value)
        var getGift = (from alert1 in db.tbGifts
                       where alert1.gift_id == Convert.ToInt16(txtGift.Value)
                       select alert1).SingleOrDefault();
        var taikhoan = (tbAccount)Session["TaiKhoan"];
        var getSticker = (from stk in db.tbStickers
                          join hstl in db.tbhocsinhtronglops on stk.hstl_id equals hstl.hstl_id
                          where taikhoan.account_id == hstl.account_id
                          select stk).FirstOrDefault();
        if (getSticker.sticker_amount < getGift.gift_price)
        {
            alert.alert_Error(Page, "Số lượng Sticker của bạn không đủ!!!", "");
        }
        else
        {
            getSticker.sticker_amount -= getGift.gift_price;
            var getQuaHocSinh = from qhs in db.tbQuaCuaHocSinhs
                                join hstl in db.tbhocsinhtronglops on qhs.hstl_id equals hstl.hstl_id
                                where taikhoan.account_id == hstl.account_id
                                select qhs;
            var getGift2 = from gift in db.tbGifts select new { gift.gift_image, gift.gift_id, gift.gift_price };
            var getQuaHocSinh2 = from qhs in db.tbQuaCuaHocSinhs
                                 join hstl in db.tbhocsinhtronglops on qhs.hstl_id equals hstl.hstl_id
                                 join gift in db.tbGifts on qhs.gift_id equals gift.gift_id
                                 where taikhoan.account_id == hstl.account_id
                                 select new { qhs.gift_image, qhs.gift_id, qhs.gift_price };
            tbQuaCuaHocSinh qhss = new tbQuaCuaHocSinh();
            qhss.gift_id = getGift.gift_id;
            qhss.hstl_id = getSticker.hstl_id;
            qhss.gift_image = getGift.gift_image;
            qhss.gift_price = getGift.gift_price;
            db.tbQuaCuaHocSinhs.InsertOnSubmit(qhss);
            db.SubmitChanges();
            alert.alert_Success(Page, "Đổi quà thành công!!!", "");
            DataTable sticker = new DataTable("Sticker");
            sticker.Columns.Add(new DataColumn(("sticker_image"), typeof(string)));
            for (int i = 0; i < getSticker.sticker_amount; i++)
            {
                sticker.Rows.Add(getSticker.sticker_image);
            }
            var getQua = getGift2.Except(getQuaHocSinh2);
            rpSticker.DataSource = sticker;
            rpSticker.DataBind();
            rpQuaHocSinh.DataSource = getQuaHocSinh;
            rpQuaHocSinh.DataBind();
            rpGift.DataSource = getQua;
            rpGift.DataBind();
            lblStickerAmount.InnerText = getSticker.sticker_amount.ToString();


            //Response.Redirect("/sticker");

        }
    }
}