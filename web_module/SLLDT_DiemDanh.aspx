﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SLLDT_DiemDanh.aspx.cs" Inherits="web_module_SLLDT_DiemDanh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Page Điểm Danh</title>
    <meta name="viewport" ocntent="width=device-width, initial-scale=1.0" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <script src="../js/bootstrap.min.js"></script>
    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            display: flex;
            overflow: hidden;
            height: 100vh;
            font-family: Arial, sans-serif;
            background-size: cover;
            background-position: center;
            background-image: url(/images/curve-light-blue-background-abstract-free-vector.png);
            align-items: center;
            justify-content: center;
            background-repeat: no-repeat;
        }

        .container {
            border-radius: 13px;
            background: white;
            margin: 0 auto;
            box-shadow: 11px 5px 17px 1px #80808085;
            padding: 20px;
        }

        .header {
            font-size: 28px;
            font-weight: bold;
            margin-bottom: 10px;
            text-align: center;
        }

        .block-main {
            justify-content: space-evenly;
        }

        .block-main__top {
            margin-bottom: 20px;
        }

        .block-main__top--text {
            font-size: 18px;
            margin-bottom: 10px;
        }

        .block-main__top--input {
            width: 100%;
            padding: 10px;
            font-size: 16px;
            background-color: #fff;
            border: 3px solid #c8c6d8;
            border-radius: 4px;
            margin: 6px 0;
        }

            .block-main__top--input:hover {
                border-color: #aaa;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }

            .block-main__top--input:focus {
                outline: none;
                border-color: #2196F3;
                box-shadow: 0 0 5px rgba(33, 150, 243, 0.5);
            }

            .block-main__top--input[type="button"]:active {
                transform: translateY(1px);
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
            }

        .block-main__top--button {
            display: block;
            width: 100%;
            padding: 10px;
            font-size: 16px;
            background-color: #067bba;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-top: 1%;
        }

        .block-main__keyboard {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            gap: 11px;
            margin-top: 20px;
        }

            .block-main__keyboard button {
                font-family: cursive;
                max-height: 67px;
                font-weight: 700;
                padding: 24px;
                font-size: 22px;
                background-color: #bbe6f4;
                color: #333;
                box-shadow: 1px 4px 8px -7px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                display: flex;
                align-items: center;
                justify-content: center;
                transition: background-color 0.3s, color 0.3s, transform 0.3s;
            }

                .block-main__keyboard button:hover {
                    background-color: #067bba;
                    color: #fff;
                    transform: scale(1.1);
                }

        .popup {
            display: none;
            position: fixed;
            z-index: 9999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            opacity: 0;
            transition: opacity 0.3s ease;
        }

        .popup-content {
            position: relative;
            max-width: 400px;
            margin: 100px auto;
            padding: 30px;
            background-color: #fff;
            border-radius: 4px;
            text-align: center;
            opacity: 0;
            transform: scale(0.8);
            transition: opacity 0.3s ease, transform 0.3s ease;
        }

        .popup-content__close {
            position: absolute;
            top: 10px;
            right: 10px;
            font-size: 20px;
            font-weight: bold;
            cursor: pointer;
        }

        .popup-content__buttom {
            width: 100%;
            display: flex;
            justify-content: flex-end;
            margin-top: 9%;
        }

        .close-button, .next-button {
            padding: 10px 20px;
            background-color: #ddd;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin: 0 1%;
        }

        .popup.open {
            display: flex;
            opacity: 1;
            align-items: center;
            justify-content: center;
        }

            .popup.open .popup-content {
                opacity: 1;
                transform: scale(1);
            }

        .green {
            background: #29ff295e;
        }

        .logo {
            text-align: center;
        }

            .logo img {
                width: 15%;
            }

        .popup-content__close--text {
            padding: 1%;
        }

        @media (max-width: 768px) {
        }

        /*@media (min-width: 768px) and (max-width: 992px) {
            .block-main{
                display: flex;
            }
        }*/
      
        @media (min-width: 500px) {
            .block-main{
                display: flex;
            }
        }
    </style>
</head>
<body>
    <div class="container col-xl-6 col-md-8 col-sm-10">
        <div class="logo">
            <img src="../images/logo.jpg" />
            <div class="header">ĐIỂM DANH</div>
        </div>
        <div class="block-main">
            <div class="block-main__top">
                <label class="block-main__top--text">Mã Điểm Danh:</label>
                <input type="text" class="block-main__top--input" id="codeInput" />
                <button id="openPopup" class="block-main__top--button">Điểm Danh</button>
            </div>
            <div class="block-main__keyboard">
                <button>a</button>
                <button>b</button>
                <button>c</button>
                <button>1</button>
                <button>2</button>
                <button>3</button>
                <button>4</button>
                <button>5</button>
                <button>&larr;</button>
            </div>
        </div>
    </div>
    <div id="popup" class="popup">
        <div class="popup-content">
            <span class="popup-content__close">&times;</span>
            <span class="mt-1">Điểm danh với vai trò Là học sinh </span>
            <div class="popup-content__buttom">
                <button class="close-button">Huỷ</button>
                <button class="next-button green">Tiếp tục</button>
            </div>
        </div>
    </div>
</body>
<script>
    const openPopupButton = document.getElementById('openPopup');
    const popup = document.getElementById('popup');
    const closeButtons = document.querySelectorAll('.popup-content__close, .close-button');

    openPopupButton.addEventListener('click', function () {
        popup.classList.add('open');
    });

    closeButtons.forEach(function (closeButton) {
        closeButton.addEventListener('click', function () {
            popup.classList.remove('open');
        });
    });

</script>
</html>



