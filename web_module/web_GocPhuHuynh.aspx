﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_GocPhuHuynh.aspx.cs" Inherits="web_module_web_GocPhuHuynh" %>

<%@ Register Src="~/web_usercontrol/global_pagging.ascx" TagPrefix="uc1" TagName="linqPaging" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        $('#Newtt').easyPaginate({
            paginateElement: 'img',
            elementsPerPage: 3,
            effect: 'climb'
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section id="Newtt" class="blog b-archives section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 d-none d-sm-none d-lg-block col-12">
                    <div class="learnedu-sidebar left">

                        <div class="single-widget categories">
                            <h3 class="title"><span>Danh mục</span></h3>
                            <ul>
                                <li><a href="../../tin-tuc-10">Tin tức</a></li>
                                <li><a href="../../tuyen-dung-7">Tuyển dụng</a></li>
                                <li><a href="../../goc-phu-huynh-11">Góc phụ huynh</a></li>
                                <%--   <asp:Repeater ID="rpMenuTinTuc" runat="server">
                                    <ItemTemplate>
                                        <li><a href="../../<%#Eval("link_seo") %>"><i class="fa fa-angle-right"></i><%#Eval("newcate_title") %><span><%#Eval("sum_new") %></span></a></li>
                                    </ItemTemplate>
                                </asp:Repeater>--%>
                            </ul>
                        </div>
                        <!--/ End Categories -->
                        <!-- Posts -->

                        <!--/ End Posts -->
                        <div class="single-widget fanpage">
                            <h3><span>Facebook</span></h3>

                            <div class="fb-page" data-href="https://www.facebook.com/tienganhthieunhisuperfriends/" data-tabs="timeline" data-height="210" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/tienganhthieunhisuperfriends/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/tienganhthieunhisuperfriends/">Trung Tâm Anh Ngữ & Toán Tư Duy SUPER FRIENDS</a></blockquote>
                            </div>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <!-- Events -->
                    <section class="events archives">
                        <div class="row">
                            <asp:Repeater ID="rpNews" runat="server">
                                <ItemTemplate>
                                    <div class=" col-md-6 col-sm-4 col-12">
                                        <!-- Single Event -->
                                        <div class="single-event">
                                            <div class="head overlay">
                                                <img src="<%#Eval("news_image") %>" alt="#">
                                                <a href="../../goc-phu-huynh/<%#cls_ToAscii.ToAscii(Eval("news_title").ToString()) %>-<%#Eval("news_id") %>" class="btn"><i class="fa fa-search"></i></a>
                                            </div>
                                            <div class="event-content">
                                                <h4><a href="../../goc-phu-huynh/<%#cls_ToAscii.ToAscii(Eval("news_title").ToString()) %>-<%#Eval("news_id") %>"><%#Eval("news_title") %></a></h4>
                                                <p><%#Eval("news_summary") %></p>
                                                <a href="../../goc-phu-huynh/<%#cls_ToAscii.ToAscii(Eval("news_title").ToString()) %>-<%#Eval("news_id") %>">Chi Tiết</a>
                                            </div>
                                        </div>
                                        <!--/ End Single Event -->
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </section>
                    <%--// chuyển trang--%>
                    <div class="paging-container d-flex justify-content-center ">
                        <uc1:linqPaging runat="server" ID="linqPaging" />
                    </div>
                </div>
            </div>

        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

