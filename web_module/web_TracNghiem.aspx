﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="web_TracNghiem.aspx.cs" Inherits="web_module_web_TracNghiem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="../css/Preschool/global.css" rel="stylesheet" />
    <link href="../css/game-preschool/globalPreschool.css" rel="stylesheet" />
    <link href="../css/game-preschool/MutipleChoise.css" rel="stylesheet" />
    <%--<link href="../css/Preschool/bootstrap.css" rel="stylesheet" />--%>
    <link href="../css/SLLDT/GameTrachNghiem.css" rel="stylesheet" />
</head>
<body id="div_game">
    <div class="notification" hidden="hidden">
        <div class="phone">
        </div>
        <div class="message">
            <p>Để trải nghiệm các trò chơi học tập tốt hơn, quý phụ huynh vui lòng chuyển qua chế độ xoay màn hình!</p>
        </div>
    </div>
    <div class="loading" id="img-loading-icon" style="display: none">
        <div class="loading">Loading&#8230;</div>
    </div>
    <div class="btn-position btn_home grow">
        <a>
            <img class="" src="/images/images_button/btn-21.png" />
        </a>
    </div>
    <div class="btn-position btn_BackCate grow" id="divChucNang">
        <a>
            <img class="" src="/images/images_button/btn-35.png" />
        </a>
    </div>
    <img src="/images/Image_TracNghiem/airbubble2.gif" class="image-anime gif-1" />
    <img src="/images/Image_TracNghiem/airbubble1.gif" class="image-anime gif-2" />
    <img class="image-anime gif-3" src="/images/Image_TracNghiem/so.gif" />
    <div class="image-anime gif-4">
        <img src="/images/Image_TracNghiem/2-unscreen.gif" />
    </div>
    <div class="form-game">
        <div class="frame-game">
            <div class="button" style="display: block;">
                <div class="ti-so">
                    <div class="txt-tiso">1/12</div>
                </div>
                <a class="btn btnVolume grow">
                    <img src="/images/images_button/loa-xanhdt-m.png" />
                </a>
                <a class="btn btnSubmit grow">
                    <img src="/images/images_button/btn-2.png" />
                </a>
            </div>
            <div class="KhungGame">
                <div class="answer-list">
                    <div class="answer-list-main">
                        <div class="answer-item">
                            <a class="answer-item__image grow">
                                <img src="/images/Image_TracNghiem/con_bo.png" class="img_TraLoi_C1" />
                            </a>
                            <a class="answer-item__audio-play grow">
                                <img src="/images/images_button/loa-xanhdt-m.png"/>
                            </a>
                        </div>
                        <div class="answer-item">
                            <a class="answer-item__image grow">
                                <img src="/images/Image_TracNghiem/con_bo.png" class="img_TraLoi_C1" />
                            </a>
                             <a class="answer-item__audio-play grow">
                                <img src="/images/images_button/loa-xanhdt-m.png"/>
                            </a>
                        </div>
                        <div class="answer-item">
                            <a class="answer-item__image grow">
                                <img src="/images/Image_TracNghiem/con_bo.png" class="img_TraLoi_C1" />
                            </a>
                             <a class="answer-item__audio-play grow">
                                <img src="/images/images_button/loa-xanhdt-m.png"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
