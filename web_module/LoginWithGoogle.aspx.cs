﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASPSnippets.GoogleAPI;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;

public partial class web_module_LoginWithGoogle : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();

    protected void Page_Load(object sender, EventArgs e)
    {
        GoogleConnect.ClientId = "915982397815-ibkvjhjcooftahpoq3tfrd19n7kpkebb.apps.googleusercontent.com";
        GoogleConnect.ClientSecret = "M7JU_wsYE5O6ni1l4nrwCJbW";

        GoogleConnect.RedirectUri = Request.Url.AbsoluteUri.Split('?')[0];

        if (!string.IsNullOrEmpty(Request.QueryString["code"]))
        {
            string code = Request.QueryString["code"];
            string json = GoogleConnect.Fetch("me", code);
            GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
            lblId.Text = profile.Id;
            lblName.Text = profile.DisplayName;
            lblEmail.Text = profile.Emails.Find(email => email.Type == "account").Value;
            lblGender.Text = profile.Gender;
            lblType.Text = profile.ObjectType;
            ProfileImage.ImageUrl = profile.Image.Url;
            pnlProfile.Visible = true;
            btnLogin.Enabled = false;
        }
        if (Request.QueryString["error"] == "access_denied")
        {
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('Access denied.')", true);
        }
    }
  

    protected void Clear(object sender, EventArgs e)
    {
        GoogleConnect.Clear(Request.QueryString["code"]);
    }
    protected void btngoogle_ServerClick(object sender, EventArgs e)
    {
    }
    public class GoogleProfile
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public Image Image { get; set; }
        public List<Email> Emails { get; set; }
        public string Gender { get; set; }
        public string ObjectType { get; set; }
    }

    public class Email
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }

    public class Image
    {
        public string Url { get; set; }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        GoogleConnect.Authorize("profile", "email");
    }
}