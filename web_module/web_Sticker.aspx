﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_Sticker.aspx.cs" Inherits="web_module_Sticker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
       <%-- function myClick(id) {
            document.getElementById("<%=txtGift.ClientID%>").value = id;
            document.getElementById("<%=btnGift.ClientID%>").click();
        }--%>
        function BuyGift(id) {
            swal("Bạn có chắc chắn đổi món quà này?",
                "",
                "info",
                {
                    buttons: true,
                    buttons: true
                }).then(function (value) {
                    if (value == true) {
                        document.getElementById("<%=txtGift.ClientID%>").value = id;
                        document.getElementById("<%=btnGift.ClientID%>").click();
                    }
                });
        }
</script>
    <script src="../admin_js/sweetalert.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .qua {
            background-image: url();
            transition: 1s all ease;
        }

            .qua:hover {
                transform: scale(1.2);
                text-shadow: 0px 0px 2px #ff0000;
            }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label>Tên: </label>
                <strong>
                    <label id="lblTen" runat="server"></label>
                </strong>
                <label style="margin-left: 20px">Lớp: </label>
                <strong>
                    <label id="lblLop" runat="server"></label>
                </strong>
                <label style="margin-left: 20px">Số lượng Sticker của bạn:</label>
                <strong>
                    <label id="lblStickerAmount" runat="server"></label>
                </strong>
                <div>
                    <asp:Repeater ID="rpSticker" runat="server">
                        <ItemTemplate>
                            <img src="<%#Eval("sticker_image") %>" style="width: 60px" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div>
                    Quà của bạn:
                </div>
                <div class="row">
                    <asp:Repeater ID="rpQuaHocSinh" runat="server">
                        <ItemTemplate>
                            <div class="col-md-3">
                            <div class="card qua"><img src="<%#Eval("gift_image") %>" /></div>
                                </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="col-md-6">
                <strong>Shop đổi quà</strong>
                <div class="row">
                    <asp:Repeater ID="rpGift" runat="server">
                        <ItemTemplate>
                            <div class="col-md-3">
                                <a href="javascript:void(0)" onclick="BuyGift(<%#Eval("gift_id") %>);">
                                    <div class="card qua">
                                        <img src="<%#Eval("gift_image") %>" />
                                        <div class="card-footer">
                                            <div class="button">
                                                <a onclick="BuyGift(<%#Eval("gift_id") %>);" class="btn"><%#Eval("gift_price") %></a>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div style="display: none;">
            <input type="text" id="txtGift" runat="server" />
            <asp:Button ID="btnGift" runat="server" OnClick="btnGift_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

