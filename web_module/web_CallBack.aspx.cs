﻿using ASPSnippets.FaceBookAPI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_CallBack : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        FaceBookConnect.API_Key = "2704697429787289";
        FaceBookConnect.API_Secret = "898cd5b076b5c2a25e886dde77026df9";
        string code = Request.QueryString["code"];
        if (!string.IsNullOrEmpty(code))
        {
            string data = FaceBookConnect.Fetch(code, "me?fields=id,name,email");
            FaceBookUser faceBookUser = new JavaScriptSerializer().Deserialize<FaceBookUser>(data);
            faceBookUser.PictureUrl = string.Format("https://graph.facebook.com/{0}/picture", faceBookUser.Id);
            pnlFaceBookUser.Visible = true;
            lblId.Text = faceBookUser.Id;
            lblUserName.Text = faceBookUser.UserName;
            lblName.Text = faceBookUser.Name;
            lblEmail.Text = faceBookUser.Email;
            ProfileImage.ImageUrl = faceBookUser.PictureUrl;
            btnLogin.Enabled = false;
            //tbAccount acc = new JavaScriptSerializer().Deserialize<cls_Account>(data);
            //acc.account_vn = 
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        FaceBookConnect.Authorize("user_photos,email", Request.Url.AbsoluteUri.Split('?')[0]);
    }
    public class FaceBookUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string PictureUrl { get; set; }
        public string Email { get; set; }
    }
}

