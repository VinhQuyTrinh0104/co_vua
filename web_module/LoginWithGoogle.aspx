﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="LoginWithGoogle.aspx.cs" Inherits="web_module_LoginWithGoogle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../admin_js/sweetalert.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="44487811844-u082rcgv2uffulsdl97rtc0enb7ka184.apps.googleusercontent.com.apps.googleusercontent.com">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <asp:Button ID="btnLogin" Text="Login with Google" runat="server" OnClick="btnLogin_Click" />
    <%--<a href="https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:60276/web_module/LoginWithGoogle.aspx&response_type=code
    &client_id=991905150947-ui6he9o8d8aqapj5s3ciala1npqkb5i3.apps.googleusercontent.com&approval_prompt=force">Login With Google</a>--%>
    <asp:Panel ID="pnlProfile" runat="server" Visible="false">
        <hr />
        <table>
            <tr>
                <td rowspan="6" valign="top">
                    <asp:Image ID="ProfileImage" runat="server" Width="50" Height="50" />
                </td>
            </tr>
            <tr>
                <td>ID:
            <asp:Label ID="lblId" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Name:
            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email:
            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gender:
            <asp:Label ID="lblGender" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Type:
            <asp:Label ID="lblType" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Text="Clear" runat="server" OnClick="Clear" />
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
<%--Footer--%>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
