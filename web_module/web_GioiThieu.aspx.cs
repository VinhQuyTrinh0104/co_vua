﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_gioithieu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        rpIntroduce.DataSource = from introl in db.tbIntroduces select introl;
        rpIntroduce.DataBind();
        //section menu active
        Session["active_gt"] = "active";
        Session["active_tc"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = null;
        Session["active_lh"] = null;
        Session["active_ctdt"] = null;
        Session["active_gph"] = null;
        Session["active_td"] = null;
        //rpGioiThieuKhoaHoc.DataSource = from inkh in db.tbIntroduce_KhoaHocs select inkh;
        //rpGioiThieuKhoaHoc.DataBind();
    }
}