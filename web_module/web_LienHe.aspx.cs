﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_lienhe : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["active_tc"] = null;
        Session["active_gt"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = null;
        Session["active_lh"] = "active";
        rpDiaChi.DataSource = from dc in db.tbDiaChis select dc;
        rpDiaChi.DataBind();
    }

    protected void btn_sendnew_ServerClick(object sender, EventArgs e)
    {
        if (isEmail(txtEmail.Value) == true)
        {
            SendMail();
        }
        else
        {
            alert.alert_Error(Page, "Vui Lòng Kiểm tra lại mail của bạn", "");
        }
        
    }
    private bool isEmail(string txtEmail)
    {
        Regex re = new Regex(@"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
                      RegexOptions.IgnoreCase);
        return re.IsMatch(txtEmail);
    }
    private void SendMail()
    {
        if (txtName.Value != "" && txtEmail.Value != "" && txtPhone.Value != "" && txtContent.Value != "")
        {
            try
            {
                var fromAddress = "landlord.ansipan@gmail.com";//  Email Address from where you send the mail 
                //var toAddress = "phungduc1989@gmail.com";
                var toAddress = "mailteamgenera@gmail.com";
                //cskh.ansipan@gmail.com
                const string fromPassword = "crrzvfnwyddequgr";
                string subject, title;
                title = "Hổ trợ tư vấn ";
                subject = "<!DOCTYPE html><html><head><title></title></head><body style=\" width:600px; margin:0px;\"><div><h1 style=\"margin-top:0px;text-align:center; color:#029ada; fontsize:22px;\">LandLord SuperFriends</h1><h3 style=\"margin-top:0px; text-align:center; color:#029ada;fontsize:20px;\">Thông Tin Khách Hàng: </h3><br/><h4>Email: </h4>" + txtEmail.Value + "<br/><h4>Tên Đăng Nhập: </h4>" + txtName.Value + "<br /><h4>Số Điện Thoại: </h4>" + txtPhone.Value + "<br/><h4 >Nội Dung: </h4>" + txtContent.Value + "<br/></div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "LandLord SuperFriends");
                mm.Subject = title;
                mm.To.Add(toAddress.ToString());
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                alert.alert_Success(Page, "Chúng tối sẽ liên hệ sớm cho bạn trong 24h tới!", "");
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Lỗi!", "");
            }
        }
        else
            alert.alert_Warning(Page, "Vui lòng nhập đầy đủ thông tin", "");
    }
}