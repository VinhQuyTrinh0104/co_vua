﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_CallBack.aspx.cs" Inherits="web_module_web_CallBack" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../admin_js/sweetalert.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    dhvasd
    <asp:Button ID="btnLogin" runat="server" Text="Login with FaceBook" OnClick="btnLogin_Click" />
    <asp:Panel ID="pnlFaceBookUser" runat="server" Visible="false">
        <hr />
        <table>
            <tr>
                <td rowspan="5" valign="top">
                    <asp:Image ID="ProfileImage" runat="server" Width="50" Height="50" />
                </td>
            </tr>
            <tr>
                <td>ID:<asp:Label ID="lblId" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>UserName:<asp:Label ID="lblUserName" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>Name:<asp:Label ID="lblName" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>Email:<asp:Label ID="lblEmail" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
<%--Footer--%>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
