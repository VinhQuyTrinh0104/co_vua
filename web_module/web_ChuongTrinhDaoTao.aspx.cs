﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class web_module_web_ChuongTrinhDaoTao : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    SqlCommand cmd;
    cls_Alert alert = new cls_Alert();
    public string title, b;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["active_ctdt"] = "active";
        Session["active_tc"] = null;
        Session["active_gt"] = null;
        Session["active_drop"] = null;
        Session["active_tt"] = null;
        Session["active_td"] = null;
        Session["active_gph"] = null;
        Session["active_lh"] = null;
        var newsss = from ns in db.tbNewCates
                    join n in db.tbNews on ns.newcate_id equals n.newcate_id
                            where ns.newcate_id == Convert.ToInt32(RouteData.Values["id"]) && n.active == true
                            orderby n.news_id descending
                            select new
                            {
                                ns.newcate_id,
                                ns.newcate_title,
                                ns.link_seo,
                                n.news_id,
                                n.news_title,
                                n.news_image,
                                n.news_summary,
                                n.news_content,
                                news_datetime = String.Format("{0:dd/MM/yyyy}", n.news_datetime)


                            };
        title = newsss.First().newcate_title;
        rpNews.DataSource = newsss;
        rpNews.DataBind();
    }
}