﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_KhoaHoc.aspx.cs" Inherits="web_module_web_KhoaHoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section class="blog b-archives single section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 d-none d-sm-none d-lg-block col-12">
                    <div class="learnedu-sidebar left">
                        <div class="single-widget categories">
                            <h3 class="title"><span>Khóa Học</span></h3>
                            <ul>
                                <li><i class="fa fa-angle-right"></i><a href="#">Khóa Học Kỹ Năng<span></span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="#">Khóa Học Toán Tư Duy<span></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="single-main">
                        <div class="row">
                            <div class="col-lg-12 col-12">
                                <div class="blog-detail">
                                    <div class="detail-content">
                                        <h2 class="blog-title"><a href="#">Khóa Học Trung Tâm SuperFriends</a></h2>
                                        <%--<div>
                                            <img style="width: 100%; height: 370px" src="/images/14082019_044506_PM_2s.jpg" class=" rounded" />
                                        </div>--%>
                                        <p>
                                            Lợi ích của các sản phẩm công nghệ
                                                    
Không thể phủ nhận công dụng của các sản phẩm công nghệ đối với trẻ em trong thời đại hiện nay. Con trẻ có sự thuận lợi trong việc học tập và phát triển khi sử dụng máy tính, các thiết bị bị công nghệ,...

Có nghiên cứu đã cho thấy các bé 2 - 3 tuổi có tiếp xúc với máy tính sẽ học tập tốt hơn các bé hoàn toàn không biết đến máy tính. Ở một số mặt như trí thông minh, kỹ năng giao tiếp phi ngôn ngữ, lượng kiến thức được sắp xếp, trí nhớ, tính khéo léo, các kỹ năng nói, giải quyết vấn đề, kỹ năng suy tưởng và ý niệm ở bé sẽ phát triển tốt hơn.

Một lần nữa, việc gia đình tham gia vào hướng dẫn con mình cách dùng máy tính đúng đắn là rất cần thiết. Gia đình nên giúp con tìm ra những trang web về giáo dục để bé nâng cao khả năng học tập chứ không phải là những trang về trò chơi. Điều này sẽ giúp bé học vui và tương tác với việc học nhiều hơn.nvbnvbnvbnvbnvbn
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

