﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="web_ChiTiet.aspx.cs" Inherits="web_module_chitiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" Runat="Server">
</asp:Content>  
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" Runat="Server">
     <section class="blog b-archives single section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-main">
                        <div class="row">
                            <div class="col-lg-3 d-none d-sm-none d-lg-block col-12">
                                <div class="learnedu-sidebar left">
                                    <div class="single-widget categories">
                                        <h3 class="title">Tin tức</h3>
                                        <ul>
                                                    <li><a href="#"><i class="fa fa-angle-right"></i>Khóa Học</a></li>
                                                
                                                    <li><a href="#"><i class="fa fa-angle-right"></i>Tin Tức</a></li>
                                                
                                                    <li><a href="#"><i class="fa fa-angle-right"></i>English</a></li>
                                                     
                                                    <li><a href="#"><i class="fa fa-angle-right"></i>Soroban</a></li>
                                        </ul>
                                    </div>
                                    <!--/ End Categories -->
                                    <div class="single-widget fanpage">
                            <h3><span>Facebook</span></h3>
                            <div class="fb-page" data-href="https://www.facebook.com/tienganhthieunhisuperfriends/" data-tabs="timeline" data-height="210" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/tienganhthieunhisuperfriends/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/tienganhthieunhisuperfriends/">Trung Tâm Anh Ngữ & Toán Tư Duy SUPER FRIENDS </a></blockquote>
                            </div>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                        </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-12">
                                <div class="blog-detail">
                                    
                                            <div class="detail-content">
                                                <h2 class="blog-title"><a href="#">Lợi ích khi trẻ chơi trò chơi điện tử</a></h2>
                                                <img width="850" height="405" class="rounded" src="../../images/chitiet.jpg"  />
                                                <p style="box-sizing: border-box; padding: 0px 0px 16px; margin: 0px; list-style-type: none; border: 0px; font-size: 20px; color: #222222; line-height: 30px; font-family: &quot;Noto Serif script=all rev=2&quot;, &quot;Adobe Blank&quot;, serif; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"><span style="font-family: &quot;Times New Roman&quot;"><strong style="box-sizing: border-box;">Trò chơi điện tử được xem là món “khoái khẩu” không thể thiếu của rất nhiều trẻ em, nó có thể giúp trẻ phát huy tối đa những kỹ năng quan trọng trong cuộc sống.</strong></span></p><p style="box-sizing: border-box; padding: 0px 0px 16px; margin: 0px; list-style-type: none; border: 0px; line-height: 30px; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"><span style="font-family: &quot;Times New Roman&quot;"><span style="font-size: 14pt;">Giúp bé thông minh hơn trong việc học.</span></span></p><p style="box-sizing: border-box; padding: 0px 0px 16px; margin: 0px; list-style-type: none; border: 0px; font-size: 20px; color: #222222; line-height: 30px; font-family: &quot;Noto Serif script=all rev=2&quot;, &quot;Adobe Blank&quot;, serif; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;">Nghiên cứu của các nhà khoa học Úc cho thấy rằng, trò chơi đòi hỏi các thao tác chủ động sẽ giúp trẻ thông minh hơn bằng cách nâng cao khả năng giải quyết vấn đề, lòng tự trọng và khuyến khích trẻ vận động. Với tâm lý muốn "thắng" trong trò chơi điện tử khiến trẻ phải luôn tập trung và nghĩ ra phương thức thực hiện tốt, lâu dần sẽ tạo cho trẻ thói quen tự tin và cố gắng, khi gặp một vấn đề khó giải quyết.<br/> 
- Việc vui chơi, khám phá thế giới bên ngoài của bé sẽ giúp bé phát triển trí não tốt hơn ngay từ đầu.
- Giúp trẻ hình thành cách ứng xử với người xung quanh: Khi chung với các trẻ khác sẽ giúp cho trẻ hòa nhập với nhau, gần gũi, chia sẻ, biết cách đối thoại. Khi cùng chơisẽ tạo cho trẻ tính kiên nhẫn, tôn trọng người khác, biết cách thương lượng, thỏa thuận.
<br/>                                                 
- Giúp trẻ phát huy tư duy sáng tạo: Nghiên cứu của các nhà khoa học chỉ ra việc chơi các trò chơi điện tử sẽ giúp trẻ có thể phát triển tư duy sáng tạo và tạo tâm lý thoải mái, tự tin khi đưa ra các quyết định, các ý kiến của bản thân.
<br/>
- Giúp cải thiện khả năng đọc của trẻ: Với trẻ nhỏ khi bắt đầu tập đọc, khó khăn thường gặp phải khi đọc là do sự thiếu tập trung, ảnh hưởng đến thị giác chứ không phải là vấn đề về ngôn ngữ. Chơi các trò chơi điện tử sẽ giúp trẻ nâng cao sự chú ý thị giác.
<br/>
<strong style="box-sizing: border-box;"><span style="color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; text-decoration-style: initial; text-decoration-color: initial;"></span></strong></p><div style="text-align: center;"><span style="font-family: &quot;Times New Roman&quot;"><strong style="box-sizing: border-box;"><img src="/editorimages/4%20-%20So%20do%20Hoi%20sach(2).jpg" alt="" /></strong></span></div><span style="font-family: &quot;Times New Roman&quot;;"><strong style="box-sizing: border-box;"><br /></strong></span><p style="box-sizing: border-box; padding: 0px 0px 16px; margin: 0px; list-style-type: none; border: 0px; font-size: 20px; color: #222222; line-height: 30px; font-family: &quot;Noto Serif script=all rev=2&quot; &quot;Adobe Blank&quot; serif; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial;"><span style="font-family: &quot;Times New Roman&quot;"></span></p><img src="/editorimages/IMG-9873.JPG" alt="" /><div style="text-align: center;"><br /></div><div style="text-align: center;"><span style="font-size: 14pt;"><em><span style="font-family: &quot;Times New Roman&quot;"><span style="color: #111111;"></span></span></em></span></div><p style="text-align: center;"><em><span style="font-family: &quot;Times New Roman&quot;"><span style="color: #111111; font-size: 11pt;"><br /></span></span></em></p><p style="list-style: none; margin-top: 0px; font-size: 11pt; line-height: 27px; color: #111111; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; text-align: justify;"><span style="font-family: &quot;Times New Roman&quot;"><span style="font-size: 14pt;"></span></span></p><p style="list-style: none; margin-top: 0px; font-size: 11pt; line-height: 27px; color: #111111; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; text-align: justify;"><span style="font-family: &quot;Times New Roman&quot;"><span style="font-size: 14pt;"></span></span></p><p style="list-style: none; margin-top: 0px; font-size: 11pt; line-height: 27px; color: #111111; background-color: #ffffff; text-decoration-style: initial; text-decoration-color: initial; text-align: justify;"><span style="font-family: &quot;Times New Roman&quot;"><span style="font-size: 14pt;"></span></span></p>
                                            </div>
                                        
                                    <div class="courses single">
                                        <div class="course-required">
                                            <h4>Tin liên quan :</h4>
                                            <ul>
                                                
                                                        <li><a href="#">
                                                            <span>1</span>Nông Trại Xanh
                                                        </a></li>
                                                    
                                                        <li><a href="#">
                                                            <span>2</span>Đấu Trường Nước
                                                        </a></li>
                                                    
                                                        <li><a href="#">
                                                            <span>3</span>Happy Birthday các bé tháng 6
                                                        </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="comment-main">
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" Runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" Runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" Runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" Runat="Server">
</asp:Content>

