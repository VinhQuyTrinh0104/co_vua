﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SoLienLacMasterPage.master" AutoEventWireup="true" CodeFile="Default_SoLienLac.aspx.cs" Inherits="Default_SoLienLac" %>

<%@ Register Src="~/web_usercontrol/SLLDT/SLLDT_Hearder.ascx" TagPrefix="uc1" TagName="SLLDT_Hearder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Header" runat="Server">
    <link href="/css/pageIndex2.css?v=1" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
   <%-- <meta name="viewport" content="width=device-width, initial-scale=1.0" />--%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="full-screen" content="yes" />
    <meta name="x5-full-screen" content="true" />
    <meta name="360-full-screen" content="true" />
    <meta name="mobile-web-app-capable" content="yes" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Menu" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopWrapper" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Wrapper" runat="Server">
    <div class="home page-view">
        <uc1:SLLDT_Hearder runat="server" ID="SLLDT_Hearder" />
        <div id="section-slide">
            <div class="block-main">
                <div id="slide-main" class="pt-4">
                    <img src="/images/vjlc-chuongtrinhngoaikhoa.gif" />
                </div>
                <%--<div id="slide-main" class="owl-carousel owl-theme">
                    <asp:Repeater ID="rpSlide" runat="server">
                        <ItemTemplate>
                            <div class="item">
                                <img src="<%#Eval("slide_image") %>" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>--%>
            </div>
        </div>
        <h5 class="ribon-1 noti mt-4">THÔNG BÁO TỪ TRUNG TÂM</h5>
        <div id="section-alert" class="mb-4">
            <div class="container-fluid padding">
                <div class="card-view">
                    <div class="alert-slide-block">
                        <div id="slide-alert-school" class="owl-carousel owl-theme">
                            <asp:Repeater ID="rpThongBaoTruong" runat="server">
                                <ItemTemplate>
                                    <div class="item">
                                        <div class="block-alert">
                                            <a href="/slldt-thong-bao-truong-<%#Eval("thongbaotruong_id") %>" class="block-main">
                                                <div class="cate">
                                                    <%#Eval("thongbaotruong_createdate","{0: dd/MM/yyyy}") %>
                                                    <span class="status" style="<%#Eval("thongbaomoi") %>">
                                                        <img src="/images/new-1.gif" /></span>
                                                </div>
                                                <div class="content">
                                                    <span class="text"><%#Eval("thongbaotruong_title") %></span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h5 class="ribon-1 study">HỌC TẬP</h5>
        <div id="section-block-2" class="bg-img-1">
            <div class="card-view">
                <div class="form-row mt-2">
                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <asp:Repeater ID="rpChuongTrinhHoc" runat="server">
                                <ItemTemplate>
                                    <a href="slldt-chuong-trinh-hoc-<%#Eval("chuongtrinhhoc_id") %>" class="block-main">
                                        <span class="status" style="<%#Eval("newStyle") %>">
                                            <img src="/images/new-1.gif" />
                                        </span>
                                        <div class="icon">
                                            <img src="/images/TrangChu_SLLDTLienCap/ChươngTrinhHoc.jpg" />
                                        </div>
                                        <span class="text">Chương trình học</span>
                                    </a>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <a href="slldt-thoi-khoa-bieu" class="block-main">
                                <span class="status day">Còn lại: <%=sobuoiconlai %> buổi</span>
                                <%--  <span class="status day"><%=sobuoidiemdanh %>/<%=sobuoidangky %> buổi</span>--%>
                                <div class="icon">
                                    <img src="/images/TrangChu_SLLDTLienCap/ThoiKhoaBieu.jpg" />
                                </div>
                                <span class="text">Thời khóa biểu</span></a>
                        </div>
                    </div>
                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <a href="/slldt-hom-nay-cua-be" class="block-main">
                                <span class="status" style="<%=new_nhanxet%>">
                                    <img src="/images/new-1.gif" />
                                </span>
                                <div class="icon">
                                    <img src="/images/TrangChu_SLLDTLienCap/HomNay.jpg" />
                                </div>
                                <span class="text">Hôm nay của bé</span></a>
                        </div>
                    </div>
                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <a href="slldt-bai-kiem-tra" class="block-main">
                                <span class="status" style="<%=new_kiemtra%>">
                                    <img src="/images/new-1.gif" />
                                </span>
                                <div class="icon">
                                    <img src="/images/TrangChu_SLLDTLienCap/BaiKiemTra.jpg" />
                                </div>
                                <span class="text">Bài kiểm tra</span></a>
                        </div>
                    </div>

                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <a href="slldt-danh-gia" class="block-main">
                                <span class="status" style="<%=new_danhgia%>">
                                    <img src="/images/new-1.gif" />
                                </span>
                                <div class="icon">
                                    <img src="/images/TrangChu_SLLDTLienCap/DanhGia.jpg" />
                                </div>
                                <span class="text">Đánh giá</span></a>
                        </div>
                    </div>
                    <div class="col-4 pading">
                        <div class="icon-menu-cate">
                            <a href="slldt-sach" class="block-main">
                                <span class="status"></span>
                                <div class="icon">
                                    <img src="/images/TrangChu_SLLDTLienCap/LuyenTap.jpg" />
                                </div>
                                <span class="text">Luyện tập</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div style="height: 46px;"></div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BottomWrapper" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Footer" runat="Server">
    <script>
        //jQuery("#slide-main").owlCarousel({
        //    animateOut: "slideOutDown",
        //    animateIn: "flipInX",
        //    items: 1,
        //    loop: false,
        //    dots: true,
        //    margin: 0,
        //    nav: false,
        //    //navText: [
        //    //    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        //    //    '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        //    //],
        //});
        jQuery('#slide-alert-class').owlCarousel({
            autoWidth: true,
            items: 4,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
        jQuery('#slide-alert-school').owlCarousel({
            autoWidth: true,
            items: 4,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
        jQuery('#slide-course').owlCarousel({
            autoWidth: true,
            items: 4,
            loop: false,
            rewind: false,
            margin: 10,
            dots: false,

        });
    </script>
</asp:Content>

