﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            admin_User logedMember = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
            var checkButton = (from gu in db.admin_GroupUsers
                               join u in db.admin_Users on gu.groupuser_id equals u.groupuser_id
                               where u.username_id == logedMember.username_id
                               select gu).FirstOrDefault();
            rpThongBaoChung.DataSource = from tbchung in db.tbThongBaos
                                         where tbchung.thongbao_id == 1 
                                         select new { 
                                             tbchung.thongbao_id,
                                         tbchung.thongbao_createdate,
                                         tbchung.thongbao_title
                                         };
            rpThongBaoChung.DataBind();
            rpThongBaoDaoTao.DataSource = from tbchung in db.tbThongBaos
                                          where tbchung.thongbao_id == 2
                                          select new
                                          {
                                              tbchung.thongbao_id,
                                              tbchung.thongbao_createdate,
                                              tbchung.thongbao_title
                                          };
            rpThongBaoDaoTao.DataBind();
            rpThongBaoGiaoVien.DataSource = from tbchung in db.tbThongBaos
                                          where tbchung.thongbao_id == 3
                                          select new
                                          {
                                              tbchung.thongbao_id,
                                              tbchung.thongbao_createdate,
                                              tbchung.thongbao_title
                                          };
            rpThongBaoGiaoVien.DataBind();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
}