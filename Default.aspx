﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link rel="stylesheet" href="/css/index.min.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet" />
    <style>
        .block-card.staff .block-main .block-col.info .title {
            height: 23px;
        }

        .block-card.staff .block-main .block-col.info .summary {
            height: 91px;
        }

        .block-card.staff .block-main .block-col.image {
            min-width: 80px;
            width: 80px;
        }

            .block-card.staff .block-main .block-col.image img {
                min-width: 80px;
                width: 80px;
                height: 80px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <section id="section-marquee" class="wow fadeInDown">
        <div class="container-fluid">
            <marquee class="txt-marquee" behavior="" direction="">
                <asp:Repeater ID="rpNoiDungChay" runat="server">
                    <ItemTemplate>
                        <%-- bảng tbChuongTrinhDaoTao--%>
                        <%#Eval("chuongtrinhdaotao_summary") %>
                    </ItemTemplate>
                </asp:Repeater>
            </marquee>
        </div>
    </section>

    <section id="section-slide" class=" wow fadeInDown" data-wow-delay="0.25s">
        <div class="container-fluid">
            <div class="row">
               <%-- <div class="d-none d-lg-block col-lg-3">
                    <div class="banner-left">
                        <asp:Repeater ID="rpPoster" runat="server">
                            <ItemTemplate>
                                <img src="<%#Eval("poster_image") %>" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>--%>
                <div class="col-lg-12">
                    <div id="slide-main" class="owl-carousel owl-theme">
                        <asp:Repeater ID="rpSlide" runat="server">
                            <ItemTemplate>
                                <div class="item">
                                    <a href="#">
                                        <%--bảng tbSlide--%>
                                        <img src="<%#Eval("slide_image") %>" style="object-fit: fill" /></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-education-program" class="">
        <img src="/images/img-stick-1.png" alt="" class="img-stick stick-1" />
        <img src="/images/img-stick-2.png" alt="" class="img-stick stick-2" />
        <img src="/images/img-stick-3.png" alt="" class="img-stick stick-3" />
        <img src="/images/img-stick-4.png" alt="" class="img-stick stick-4" />
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <asp:Repeater ID="rpChuongTrinhDaotao" runat="server">
                        <ItemTemplate>
                            <div class="block-title wow fadeInDown">
                                <h3 class="title"><%#Eval("chuongtrinhdaotao_title") %></h3>
                                <div class="summary">
                                    <%#Eval("chuongtrinhdaotao_summary") %>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="block-education-program">
                <div class="form-row">
                    <asp:Repeater ID="rpGioiThieuKhoaHoc" runat="server">
                        <ItemTemplate>
                            <div class="col-xl-4 col-lg-4 col-sm-6">
                                <div class="block-card education-program">
                                    <a href="vjlc-chuong-trinh-<%#Eval("introkh_link") %>" class="block-main">
                                        <div class="block-row image">
                                            <%--  bảng tbIntroduce_KhoaHoc--%>
                                            <img src="<%#Eval("introkh_image") %>" style="width: 100%; height: max-content" alt="<%#Eval("introkh_title") %>" />
                                        </div>
                                        <div class="block-row title">
                                            <%#Eval("introkh_icon") %>
                                            <%#Eval("introkh_title") %>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </section>
    <section id="section-staff" class="">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <asp:Repeater ID="rpDoiNguNhanSu_summary" runat="server">
                        <ItemTemplate>
                            <div class="block-title wow fadeInDown">
                                <%-- bảng tbChuongTrinhDaoTao--%>
                                <h3 class="title"><%#Eval("chuongtrinhdaotao_title") %></h3>
                                <div class="summary">
                                    <%#Eval("chuongtrinhdaotao_summary")%>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-xl-12 ">
                    <div id="slide-profile" class="owl-carousel owl-theme ">
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-body">
                                <div class="d-flex justify-content-center">
                                    <div style="width: 60%; height: max-content; padding: 10px; border-radius: 45% 10px 45% 10px; background-color: #f14d4dad;">
                                        <img style="border-radius: 50%" src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Alternate Text" />
                                    </div>
                                </div>
                                <div class="text-center mt-4 card-name">Võ Thị Như Phương</div>
                                <div>
                                    <div style="font-size: 20px; font-weight: 500;">HLV quốc gia</div>
                                    <div style="font-size: 20px; font-weight: 500;">Giới thiệu</div>
                                    <div>Làm quen với cờ từ năm 8 tuổi. Tham gia các giải đấu phong trào cờ vua từ năm 10 tuổi cho đến thời kỳ sinh viên.</div>
                                    <div style="font-size: 20px; font-weight: 500;">Bằng cấp và thành tích</div>
                                    <ul>
                                        <li>HCV đồng đội cờ vua </li>
                                        <li>HCB đồng đội cờ vua </li>
                                        <li>HCĐ đồng đội cờ vua </li>
                                    </ul>
                                    <div class="d-flex justify-content-end">
                                        <a href="javascript:void(0)">Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-md-7">
                    <img class="mb-1 img-profile"
                        src="/images/img-skill-3.jpg" />
                </div>
                <div class="col-md-5">
                    <div id="slide-profile" class="owl-carousel owl-theme ">
                        <div class="item">
                            <div class=" card-profile">
                                <div style="display: grid; justify-items: center;">
                                    <img src="http://vjlc.vn/uploadimages/giao_vien/vo-thi-nhu-phuong.jpg" alt="Avatar" style="width: 150px; height: 150px; border-radius: 50%" />
                                    <div class="text-center card-name">Võ Thị Thu Phương</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </section>
    <section id="section-numbers">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="block-title wow fadeInDown">
                        <h3 class="title">Sự kiện và hoạt động</h3>
                        <div class="summary">
                            Nội dung luôn được cập nhật hàng ngày
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-3 mt-2">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-1.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-2.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-3.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="block-row button col-lg-12 d-flex justify-content-center mt-3">
                        <a
                            href="javascript:void(0)"
                            class="btn btn-red pl-4 pr-4 btn-pill">Chi Tiết</a>
                    </div>
                </div>
                <div class="col-lg-3 mt-2">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-4.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-5.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-6.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="block-row button col-lg-12 d-flex justify-content-center mt-3">
                        <a
                            href="javascript:void(0)"
                            class="btn btn-red pl-4 pr-4 btn-pill">Chi Tiết</a>
                    </div>
                </div>
                <div class="col-lg-3 mt-2">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-7.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-8.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-9.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="block-row button col-lg-12 d-flex justify-content-center mt-3">
                        <a
                            href="javascript:void(0)"
                            class="btn btn-red pl-4 pr-4 btn-pill">Chi Tiết</a>
                    </div>
                </div>
                <div class="col-lg-3 mt-2">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-10.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-11.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="col-lg-12 d-flex justify-content-center">
                        <img src="images/co-vua-nhi-12.jpg" class="card-img mt-2" alt="Alternate Text" />
                    </div>
                    <div class="block-row button col-lg-12 d-flex justify-content-center mt-3">
                        <a
                            href="javascript:void(0)"
                            class="btn btn-red pl-4 pr-4 btn-pill">Chi Tiết</a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section id="section-table">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8">
                    <div class="block-title wow fadeInDown">
                        <h3 class="title">Bảng thành tích</h3>
                        <div class="summary">
                            Ghi chú: Đ là điểm, SVĐ là số ván đấu
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="block-title wow fadeInDown" style="margin-bottom: 0%">
                        <h3 class="title">Khoá Basic</h3>
                    </div>
                    <div class="tableFixHead table-scrollbar">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Họ tên</th>
                                    <th>Đ</th>
                                    <th>SVĐ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="block-title wow fadeInDown" style="margin-bottom: 0%">
                        <h3 class="title">Khoá Intermediate</h3>
                    </div>
                    <div class="tableFixHead table-scrollbar">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Họ tên</th>
                                    <th>Đ</th>
                                    <th>SVĐ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="block-title wow fadeInDown" style="margin-bottom: 0%">
                        <h3 class="title">Khoá Advanced</h3>
                    </div>
                    <div class="tableFixHead table-scrollbar">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Họ tên</th>
                                    <th>Đ</th>
                                    <th>SVĐ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Trịnh Xuân Vinh Quy</td>
                                    <td>20</td>
                                    <td>10</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="section-register">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10 col-xl-8">
                    <div class="block-register">
                        <div class="block-main">
                            <div class="block-row header">
                                <asp:Repeater ID="rpBlockTuyenSinh" runat="server">
                                    <ItemTemplate>
                                        <div class="title"><%#Eval("chuongtrinhdaotao_title") %></div>
                                        <div class="summary">
                                            <%#Eval("chuongtrinhdaotao_summary") %>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="block-row form">
                                <div class="block-col left">
                                    <div class="mb-2">
                                        <input
                                            type="text"
                                            class="form-control form-control-register"
                                            placeholder="Họ và tên phụ huynh" />
                                    </div>
                                    <div class="mb-2">
                                        <input
                                            type="text"
                                            class="form-control form-control-register"
                                            placeholder="Số điện thoại liên hệ" />
                                    </div>
                                    <div class="mb-2">
                                        <asp:DropDownList ID="ddlKhoaHoc" runat="server" class="form-control form-control-register">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="block-col right">
                                    <div class="mb-2">
                                        <input
                                            type="text"
                                            class="form-control form-control-register"
                                            placeholder="Họ và tên học viên" />
                                    </div>
                                    <div class="mb-2">
                                        <input
                                            type="text"
                                            class="form-control form-control-register"
                                            placeholder="Tuổi của học viên" />
                                    </div>
                                    <div class="mb-2">
                                        <input
                                            type="text"
                                            class="form-control form-control-register"
                                            placeholder="Ghi chú" />
                                    </div>
                                </div>
                            </div>
                            <div class="block-row button">
                                <a
                                    href="javascript:void(0)"
                                    class="btn btn-red pl-4 pr-4 btn-pill">ĐĂNG KÍ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
    <script type="text/javascript" src="/js/isotope.pkgd.min.js"></script>
    <script> 
        jQuery(document).ready(function ($) {
            //***ISOTOPE***
            // init Isotope
            var $grid = $(".isogrid").isotope({
                itemSelector: ".grid-item",
                layoutMode: "masonry"
            });
            // filter items on button click
            jQuery('.gallery-cate-click').click(function () {
                let getvalue = jQuery(this).attr("data-filter");

                //if (jQuery('.choise-list > .check-all').hasClass('active'))
                //    jQuery('.choise-list > .check-all').removeClass("active");
                jQuery('.gallery-cate-click').removeClass('active');
                jQuery(this).addClass("active");
                //var isoFilters = [];
                //var elems = jQuery('.choise-list').find('button.active');
                //jQuery.each(elems, function (i, e) {
                //    isoFilters.push(jQuery(e).attr('data-filter'));
                //});
                //var selector = isoFilters.join(', ');
                //console.log({ selector });
                $grid.isotope({ filter: getvalue });



                //if (getvalue === '*') {
                //    jQuery('.gallery-cate-click').removeClass("active");
                //    if (!jQuery(this).hasClass('active'))
                //        jQuery(this).addClass("active");


                //    $grid.isotope({ filter: getvalue });

                //}
                //else {
                //    if (jQuery('.choise-list > .check-all').hasClass('active'))
                //        jQuery('.choise-list > .check-all').removeClass("active");
                //    jQuery(this).toggleClass("active");
                //    var isoFilters = [];
                //    var elems = jQuery('.choise-list').find('button.active');
                //    jQuery.each(elems, function (i, e) {
                //        isoFilters.push(jQuery(e).attr('data-filter'));
                //    });
                //    var selector = isoFilters.join(', ');
                //    console.log({ selector });
                //    $grid.isotope({ filter: selector });

                //}
            });
            //debugger;
            let buttonget = jQuery('.choise-list').find('button.active');
            buttonget.click();
        });
        jQuery("#slide-main").owlCarousel({
            //animateOut: "slideOutDown",
            //animateIn: "flipInX",
            items: 1,
            loop: true,
            dots: false,
            autoplay: true,
            autoplayTimeout: 5000,
            margin: 0,
            nav: false,
            navText: [
                '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>',
            ],
        });
        
        jQuery("#slide-profile").owlCarousel({
            loop: false,
            margin: 20,
            nav: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 4,
                    loop: false,
                }
            }

        });
    </script>
    <script>

</script>
</asp:Content>

