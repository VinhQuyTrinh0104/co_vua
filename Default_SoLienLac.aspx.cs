﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default_SoLienLac : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int hstl_id, sobuoidiemdanh, sobuoidangky;
    public string newStyle, new_kiemtra, new_danhgia, new_nhanxet;
    public string sobuoiconlai;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["hocsinh_vjlc"] != null)
        {
            // Để tính số buổi còn lại: 
            // Trước tiên chúng ta tính số buổi 
            // Hiển thị số buổi còn lại
            // Bước 1: Số buổi đăng ký khóa cũ lấy trong bảng học phí chi tiết + lại hết cho dù có chuyển lớp thì cũng phải lấy bên bảng học phí chi tiết.
            var gethocsinhtronglop = (from hstl in db.tbhocsinhtronglops
                                      join ac in db.tbAccounts on hstl.account_id equals ac.account_id
                                      join l in db.tbLops on hstl.lop_id equals l.lop_id
                                      where ac.account_code == Request.Cookies["hocsinh_vjlc"].Value
                                      && hstl.hstl_hidden == false && ac.hidden == false
                                      orderby hstl.hstl_id descending
                                      select new
                                      {
                                          hstl.hstl_id,
                                          ac.account_id,
                                          l.lop_id,
                                          l.lop_dotuoi,
                                      }).FirstOrDefault();
            // Số buổi còn lại sẽ lấy tổng số buổi đăng ký học phí - tổng số buổi điểm danh
            sobuoiconlai = ((from hpct in db.tbHocPhiChiTiets
                            where hpct.hocsinh_id == gethocsinhtronglop.account_id
                            select hpct).Sum(x => x.hocphichitiet_sobuoihoc)
                            -
                            (from dd in db.tbDiemDanhs
                             join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                             where hstl.account_id == gethocsinhtronglop.account_id && dd.diemdanh_vang != "Bảo lưu"
                             select dd).Count())>0? ((from hpct in db.tbHocPhiChiTiets
                                                      where hpct.hocsinh_id == gethocsinhtronglop.account_id
                                                      select hpct).Sum(x => x.hocphichitiet_sobuoihoc)
                            -
                            (from dd in db.tbDiemDanhs
                             join hstl in db.tbhocsinhtronglops on dd.hstl_id equals hstl.hstl_id
                             where hstl.account_id == gethocsinhtronglop.account_id && dd.diemdanh_vang != "Bảo lưu"
                             select dd).Count())+ "" : "0";
            //


            // get số buổi đăng ký khóa học

            var getThongBaoTruong = from tbt in db.tbSLLDT_ThongBaoTruongs
                                    orderby tbt.thongbaotruong_id descending
                                    select new
                                    {
                                        tbt.thongbaotruong_id,
                                        tbt.thongbaotruong_title,
                                        tbt.thongbaotruong_content,
                                        tbt.thongbaotruong_createdate,
                                        thongbaomoi = (from ls in db.tbSLLDT_ThongBaoTruong_LichSus
                                                       where ls.thongbaotruong_id == tbt.thongbaotruong_id
                                                       && ls.hstl_id == gethocsinhtronglop.hstl_id
                                                       select ls).Count() > 0 ? "display:none" : ""
                                    };
            rpThongBaoTruong.DataSource = getThongBaoTruong;
            rpThongBaoTruong.DataBind();
            //
            //var getData = (from u in db.tbAccounts
            //               join hstl in db.tbhocsinhtronglops on u.account_id equals hstl.account_id
            //               join l in db.tbLops on hstl.lop_id equals l.lop_id
            //               where u.account_code == Request.Cookies["hocsinh_vjlc"].Value
            //               orderby hstl.hstl_id descending
            //               select new { 
            //                 l.lop_dotuoi,
            //                 hstl.hstl_id,
            //               }).FirstOrDefault();
            if (gethocsinhtronglop.lop_dotuoi == "1")
            {
                var getChuongTrinhHoc = from cth in db.tbSLLDT_ChuongTrinhHocs
                                        where cth.chuongtrinhhoc_id == 13
                                        select new
                                        {
                                            cth.chuongtrinhhoc_id,
                                            newStyle = (from nd in db.tbSLLDT_NoiDungBaiHocs
                                                        where nd.lop_id == gethocsinhtronglop.lop_id && nd.noidungbaihoc_tinhtrang == "Đã duyệt"
                                                        && !db.tbSLLDT_NoiDungBaiHoc_LichSus.Any(f => f.noidung_id == nd.noidungbaihoc_id && f.hstl_id == gethocsinhtronglop.hstl_id)
                                                        select nd).Count() > 0 ? "" : "display:none"


                                        };
                rpChuongTrinhHoc.DataSource = getChuongTrinhHoc;
                rpChuongTrinhHoc.DataBind();
            }
            else
            {
                var getChuongTrinhHoc = from cth in db.tbSLLDT_ChuongTrinhHocs
                                        where cth.chuongtrinhhoc_id == 1
                                        select new
                                        {
                                            cth.chuongtrinhhoc_id,
                                            newStyle = (from nd in db.tbSLLDT_NoiDungBaiHocs
                                                        where nd.lop_id == gethocsinhtronglop.lop_id && nd.noidungbaihoc_tinhtrang == "Đã duyệt"
                                                        && !db.tbSLLDT_NoiDungBaiHoc_LichSus.Any(f => f.noidung_id == nd.noidungbaihoc_id && f.hstl_id == gethocsinhtronglop.hstl_id)
                                                        select nd).Count() > 0 ? "" : "display:none"
                                        };
                rpChuongTrinhHoc.DataSource = getChuongTrinhHoc;
                rpChuongTrinhHoc.DataBind();
            }
            hstl_id = gethocsinhtronglop.hstl_id;
            //var getBeHocGiHomNay_LichSu = (from hncb in db.tbSLLDT_BeHocGiHomNays
            //                               where hncb.account_id == gethocsinhtronglop.account_id
            //                               orderby hncb.behocgihomnay_createdate descending
            //                               select new
            //                               {
            //                                   newStyle = hncb.behocgihomnay_tinhtrang == "Đã xem" ? "display:none" : ""
            //                               }).Take(1);
            //rpBeHocGiHomNay.DataSource = getBeHocGiHomNay_LichSu;
            //rpBeHocGiHomNay.DataBind();
            //icon new bài kiểm tra
            if (db.tbSLLDT_BangDiems.Any(x => x.account_id == gethocsinhtronglop.account_id && x.bangdiem_new == "Chưa xem"))
                new_kiemtra = "";
            else
                new_kiemtra = "display:none";
            //icon new đánh giá
            if (db.tbDanhGias.Any(x => x.hstl_id == gethocsinhtronglop.hstl_id && x.danhgia_tinhtrangxem == "Chưa xem"))
                new_danhgia = "";
            else
                new_danhgia = "display:none";
            //icon new nhận xét
            if (db.tbSLLDT_BeHocGiHomNays.Any(x => x.hstl_id == gethocsinhtronglop.hstl_id && x.behocgihomnay_tinhtrangxem == "Chưa xem" && x.behocgihomnay_tinhtrang == "Đã duyệt"))
                new_nhanxet = "";
            else
                new_nhanxet = "display:none";
        }
        else
        {
            Response.Redirect("/slldt-login-user");
        }

    }

}