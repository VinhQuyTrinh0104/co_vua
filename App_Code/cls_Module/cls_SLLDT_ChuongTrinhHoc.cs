﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_SLLDT_ChuongTrinhHoc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_SLLDT_ChuongTrinhHoc()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string chuongtrinhhoc_title,  string content)
    {
        tbSLLDT_ChuongTrinhHoc insert = new tbSLLDT_ChuongTrinhHoc();
        insert.chuongtrinhhoc_title = chuongtrinhhoc_title;
        insert.chuongtrinhhoc_content = content;
        db.tbSLLDT_ChuongTrinhHocs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int introduce_id, string chuongtrinhhoc_title,  string content )
    {

        tbSLLDT_ChuongTrinhHoc update = db.tbSLLDT_ChuongTrinhHocs.Where(x => x.chuongtrinhhoc_id == introduce_id).FirstOrDefault();
        update.chuongtrinhhoc_title = chuongtrinhhoc_title;
        update.chuongtrinhhoc_content = content;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int introduce_id)
    {
        tbSLLDT_ChuongTrinhHoc delete = db.tbSLLDT_ChuongTrinhHocs.Where(x => x.chuongtrinhhoc_id == introduce_id).FirstOrDefault();
        db.tbSLLDT_ChuongTrinhHocs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}