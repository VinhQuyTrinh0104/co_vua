﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_Lop
{
    dbcsdlDataContext db = new dbcsdlDataContext();
 
    public cls_Lop()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int cs,string lop_name, string tugio, string dengio, bool t2, bool t3, bool t4, bool t5, bool t6, bool t7, bool cn, int gvnn, int gvvn, int gvtg, string ghichu )
    {
       
        tbLop insert = new tbLop();
        insert.coso_id = cs;
        insert.lop_name = lop_name;
        insert.giohoc_batdau = tugio;
        insert.giohoc_ketthuc = dengio;
        insert.lop_thu2 = t2;
        insert.lop_thu3 = t3;
        insert.lop_thu4 = t4;
        insert.lop_thu5 = t5;
        insert.lop_thu6 = t6;
        insert.lop_thu7 = t7;
        insert.lop_chunhat = cn;
        insert.username_idEn = gvnn;
        insert.username_idVn = gvvn;
        insert.username_trogiang_id = gvtg;
        insert.hidden = false;
        insert.lop_ghichu= ghichu;
        db.tbLops.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            tbAccount insertAccount = new tbAccount();
            insertAccount.hidden = true;
            insertAccount.account_acctive = true;
            db.tbAccounts.InsertOnSubmit(insertAccount);
            db.SubmitChanges();
            tbhocsinhtronglop insertDetail = new tbhocsinhtronglop();
            insertDetail.lop_id = insert.lop_id;
            insertDetail.account_id = insertAccount.account_id;
            insertDetail.hstl_hidden = true;
            db.tbhocsinhtronglops.InsertOnSubmit(insertDetail);
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int lop_id, int cs, string lop_name, string tugio, string dengio, bool t2, bool t3, bool t4, bool t5, bool t6, bool t7, bool cn, int gvnn, int gvvn, int gvtg, string ghichu)
    { 
        tbLop update = db.tbLops.Where(x => x.lop_id == lop_id).FirstOrDefault();
        update.coso_id = cs;
        update.lop_name = lop_name;
        update.giohoc_batdau = tugio;
        update.giohoc_ketthuc = dengio;
        update.lop_thu2 = t2;
        update.lop_thu3 = t3;
        update.lop_thu4 = t4;
        update.lop_thu5 = t5;
        update.lop_thu6 = t6;
        update.lop_thu7 = t7;
        update.lop_chunhat = cn;
        update.username_idEn = gvnn;
        update.username_idVn = gvvn;
        update.username_trogiang_id = gvtg;
        update.lop_ghichu = ghichu;
        update.hidden = false;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int lop_id)
    {
        tbLop delete = db.tbLops.Where(x => x.lop_id == lop_id).FirstOrDefault();
        delete.hidden=true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_ChuyenLopCu(int lop_id)
    {
        tbLop LopCu = db.tbLops.Where(x => x.lop_id == lop_id).FirstOrDefault();
        LopCu.lop_tinhtrang = "Lớp cũ";
        LopCu.lop_NgayNghiHan = DateTime.Now;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}