﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_AlbumImage
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_AlbumImage()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them( string image,int cate)
    {

        tbAlbum_Image insert = new tbAlbum_Image();
        insert.album_image = image;
        insert.album_image_cate_id = cate;
        // insert.news_link= 
        db.tbAlbum_Images.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int news_id, string news_image, int cate)
    {
        tbAlbum_Image update = db.tbAlbum_Images.Where(x => x.album_image_id == news_id).FirstOrDefault();
        update.album_image = news_image;
        update.album_image_cate_id = cate;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int news_id)
    {
        tbAlbum_Image delete = db.tbAlbum_Images.Where(x => x.album_image_id == news_id).FirstOrDefault();
        db.tbAlbum_Images.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}