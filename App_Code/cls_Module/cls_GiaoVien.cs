﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_GiaoVien
{
    dbcsdlDataContext db = new dbcsdlDataContext();
 
    public cls_GiaoVien()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int loai, string taikhoan, string hoten, string email, string sdt)
    {
       
        admin_User insert = new admin_User();
        insert.groupuser_id = loai;
        insert.username_username = taikhoan;
        insert.username_fullname = hoten;
        insert.username_email = email;
        insert.username_phone = sdt;
        insert.username_active = true;
        insert.username_password = "12378248145104161527610811213823414203124130";
        db.admin_Users.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int user_id, int loai, string taikhoan, string hoten, string email, string sdt)
    {
        admin_User update = db.admin_Users.Where(x => x.username_id == user_id).FirstOrDefault();
        update.groupuser_id = loai;
        update.username_username = taikhoan;
        update.username_fullname = hoten;
        update.username_email = email;
        update.username_phone = sdt;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int user_id)
    {
        admin_User delete = db.admin_Users.Where(x => x.username_id == user_id).FirstOrDefault();
        delete.username_active = false;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}