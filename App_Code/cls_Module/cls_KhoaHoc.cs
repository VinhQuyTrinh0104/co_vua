﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_KhoaHoc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_KhoaHoc()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string khoahocs_name, string khoahocs_summary,int cate,string linkseo )
    {
       
        tbKhoaHoc insert = new tbKhoaHoc();
        insert.khoahoc_name = khoahocs_name; 
        insert.khoahoc_summary = khoahocs_summary;
        insert.loaikhoahoc_id = cate;
        insert.link_seo = linkseo;
       
        // insert.news_link= 
        db.tbKhoaHocs.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int khoahocs_id, string khoahocs_name , string khoahocs_summary ,int cate , string linkseo)
    {
        
        tbKhoaHoc update = db.tbKhoaHocs.Where(x => x.khoahoc_id == khoahocs_id).FirstOrDefault();
        update.khoahoc_name = khoahocs_name;
         
        update.khoahoc_summary = khoahocs_summary;
        update.loaikhoahoc_id = cate;
        update.link_seo = linkseo;
        
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int khoahocs_id)
    {
        tbKhoaHoc delete = db.tbKhoaHocs.Where(x => x.khoahoc_id == khoahocs_id).FirstOrDefault();
        db.tbKhoaHocs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}