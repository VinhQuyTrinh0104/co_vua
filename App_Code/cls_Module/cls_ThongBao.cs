﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_ThongBao
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_ThongBao()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string thongbao_title,int cate , DateTime date, string content)
    {
        tbThongBao insert = new tbThongBao();
        insert.thongbao_title = thongbao_title;
        insert.thongbao_content = content;
        insert.loaithongbao_id = cate;
        insert.thongbao_createdate = date;
        db.tbThongBaos.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int thongbao_id, string thongbao_title,   int cate, DateTime date, string content)
    { 
        tbThongBao update = db.tbThongBaos.Where(x => x.thongbao_id == thongbao_id).FirstOrDefault();
        update.thongbao_title = thongbao_title;
        update.thongbao_content = content;
        update.loaithongbao_id = cate;
        update.thongbao_createdate = date;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int thongbao_id)
    {
        tbThongBao delete = db.tbThongBaos.Where(x => x.thongbao_id == thongbao_id).FirstOrDefault();
        db.tbThongBaos.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}