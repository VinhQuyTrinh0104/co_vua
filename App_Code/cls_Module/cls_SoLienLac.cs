﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_SoLienLac
/// </summary>
public class cls_SoLienLac
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_SoLienLac()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int account_id, string content, DateTime date, bool trangthai, string dgnl)
    {

        tbSoLienLac insert = new tbSoLienLac();
        insert.account_id = account_id;
        insert.sll_danhgiagv = content;
        insert.sll_date = date;
        insert.sll_trangthai = trangthai;
        insert.sll_nangluchs = dgnl;

        // insert.news_link= 
        db.tbSoLienLacs.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    //public bool Linq_Sua(int lop_id, int account_id, string content, DateTime date, bool trangthai, string dgnl)
    //{
    //    tbSoLienLac update = db.tbSoLienLacs.Where(x => x.sll_id == lop_id).FirstOrDefault();
    //    update.account_id = account_id;
    //    update.sll_content = content;
    //    update.sll_date = date;
    //    update.sll_trangthai = trangthai;
    //    update.sll_nangluc = dgnl;


    //    try
    //    {
    //        db.SubmitChanges();
    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}
    public bool Linq_Xoa(int lop_id)
    {
        tbSoLienLac delete = db.tbSoLienLacs.Where(x => x.sll_id == lop_id).FirstOrDefault();
        db.tbSoLienLacs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}