﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_Coso
{
    dbcsdlDataContext db = new dbcsdlDataContext();
 
    public cls_Coso()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string cs_name, string cs_diachi, string cs_phone, string email, string ghichu )
    {
       
        tbCoso insert = new tbCoso();
        insert.coso_name = cs_name;
        insert.coso_diachi = cs_diachi;
        insert.coso_phone = cs_phone;
        insert.coso_email= email;
        insert.coso_hidden = false;
        insert.coso_ghichu = ghichu;
        db.tbCosos.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int cs_id, string cs_name, string cs_diachi, string cs_phone, string email, string ghichu)
    {
        tbCoso update = db.tbCosos.Where(x => x.coso_id == cs_id).FirstOrDefault();
        update.coso_id = cs_id;
        update.coso_name = cs_name;
        update.coso_diachi = cs_diachi;
        update.coso_phone = cs_phone;
        update.coso_email = email;
        update.coso_ghichu = ghichu;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int cs_id)
    {
        tbCoso delete = db.tbCosos.Where(x => x.coso_id == cs_id).FirstOrDefault();
        delete.coso_hidden=true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}