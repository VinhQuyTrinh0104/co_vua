﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_SLLDT_NoiDungBaiHoc
{
    dbcsdlDataContext db = new dbcsdlDataContext();

    public cls_SLLDT_NoiDungBaiHoc()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int lop, string content, int user)
    {

        tbSLLDT_NoiDungBaiHoc insert = new tbSLLDT_NoiDungBaiHoc();
        insert.lop_id = lop;
        insert.noidungbaihoc_content = content;
        insert.username_id = user;
        insert.noidungbaihoc_tinhtrang = "Chưa duyệt";
        insert.noidungbaihoc_createdate = DateTime.Now;
        db.tbSLLDT_NoiDungBaiHocs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int nd_id, int lop, string content, int user)
    {
        tbSLLDT_NoiDungBaiHoc update = db.tbSLLDT_NoiDungBaiHocs.Where(x => x.noidungbaihoc_id == nd_id).FirstOrDefault();
        update.lop_id = lop;
        update.noidungbaihoc_content = content;
        update.username_id = user;
        update.noidungbaihoc_tinhtrang = "Chưa duyệt";
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int nd_id)
    {
        tbSLLDT_NoiDungBaiHoc delete = db.tbSLLDT_NoiDungBaiHocs.Where(x => x.noidungbaihoc_id == nd_id).FirstOrDefault();
        db.tbSLLDT_NoiDungBaiHocs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_ChuyenLopCu(int lop_id)
    {
        tbLop LopCu = db.tbLops.Where(x => x.lop_id == lop_id).FirstOrDefault();
        LopCu.lop_tinhtrang = "Lớp cũ";
        LopCu.lop_NgayNghiHan = DateTime.Now;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}