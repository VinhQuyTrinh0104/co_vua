﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_TieuDeTrangChu
{
    dbcsdlDataContext db = new dbcsdlDataContext();
 
    public cls_TieuDeTrangChu()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string cs_name, string cs_diachi)
    {
       
        tbChuongTrinhDaoTao insert = new tbChuongTrinhDaoTao();
        insert.chuongtrinhdaotao_title = cs_name;
        insert.chuongtrinhdaotao_summary = cs_diachi;
        db.tbChuongTrinhDaoTaos.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int cs_id, string cs_name, string cs_diachi)
    {
        tbChuongTrinhDaoTao update = db.tbChuongTrinhDaoTaos.Where(x => x.chuongtrinhdaotao_id == cs_id).FirstOrDefault();
        update.chuongtrinhdaotao_title = cs_name;
        update.chuongtrinhdaotao_summary = cs_diachi;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int cs_id)
    {
        tbChuongTrinhDaoTao delete = db.tbChuongTrinhDaoTaos.Where(x => x.chuongtrinhdaotao_id == cs_id).FirstOrDefault();
        db.tbChuongTrinhDaoTaos.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}