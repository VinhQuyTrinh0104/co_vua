﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_LoaiKhoaHoc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_LoaiKhoaHoc()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string loaikhoahocs_name , int loaikhoahoc_position)
    {
        tbLoaiKhoaHoc insert = new tbLoaiKhoaHoc();
        insert.loaikhoahoc_name = loaikhoahocs_name;
        insert.loaikhoahoc_position = loaikhoahoc_position;
        db.tbLoaiKhoaHocs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int loaikhoahocs_id, string loaikhoahocs_name, int loaikhoahoc_position)
    {

        tbLoaiKhoaHoc update = db.tbLoaiKhoaHocs.Where(x => x.loaikhoahoc_id == loaikhoahocs_id).FirstOrDefault();
        update.loaikhoahoc_name = loaikhoahocs_name;
        update.loaikhoahoc_position = loaikhoahoc_position;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int contact_id)
    {
        tbLoaiKhoaHoc delete = db.tbLoaiKhoaHocs.Where(x => x.loaikhoahoc_id == contact_id).FirstOrDefault();
        db.tbLoaiKhoaHocs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}