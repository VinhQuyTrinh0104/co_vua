﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_PhuHuynh
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_PhuHuynh()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string phuhuynh_name, string image,  string phuhuynh_summary  )
    {
       
        tbPhuHuynh insert = new tbPhuHuynh();
        insert.phuhuynh_name = phuhuynh_name;
        insert.phuhuynh_image = image;
        insert.phuhuynh_summary = phuhuynh_summary;
        insert.hidden = false;
      
       
        
       
        // insert.news_link= 
        db.tbPhuHuynhs.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int phuhuynh_id,string phuhuynh_name, string image, string phuhuynh_summary )
    { 
        tbPhuHuynh update = db.tbPhuHuynhs.Where(x => x.phuhunh_id == phuhuynh_id).FirstOrDefault();
        update.phuhuynh_name = phuhuynh_name;
            update.phuhuynh_image = image;
        update.phuhuynh_summary = phuhuynh_summary; 
 
        
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int phuhuynh_id)
    {
        tbPhuHuynh delete = db.tbPhuHuynhs.Where(x => x.phuhunh_id == phuhuynh_id).FirstOrDefault();
        delete.hidden=true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}