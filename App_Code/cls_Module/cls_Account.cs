﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_Account
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_Account()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string GetMaHocSinh(string str)
    {
        string res = "";
        string[] tu = str.Trim().Split(' ');
        int len = tu.Length;
        for (int i = 0; i < len; i++)
        {
            res += tu[i].Substring(0, 1).ToLower();
        }
        res += Matutang();
        return res;
    }
    //Hàm tự tăng
    public string Matutang()
    {

        int year = DateTime.Now.Year;
        var list = from nk in db.tbAccounts select nk;
        string s = "";
        if (list.Count() <= 0)
            s = "00001";
        else
        {
            var list1 = from nk in db.tbAccounts orderby nk.accout_codetutang descending select nk;
            string chuoi = list1.First().accout_codetutang;
            int k;
            k = Convert.ToInt32(chuoi.Substring(0, 5));
            k = k + 1;
            if (k < 10) s = s + "0000";
            else if (k < 100)
                s = s + "000";
            else if (k < 1000)
                s = s + "00";
            else if (k < 10000)
                s = s + "0";
            s = s + k.ToString();
        }
        return s;
    }
    //txtFullName.Text, Convert.ToDateTime(dteNgaySinh.Value), txtSDTHocSinh.Value, txtTruongHoc.Value, txtenEn.Text, gioitinh, txtEmailHocSinh.Value, txtNoiSinh.Value, txtChoOHienTai.Value,txtTenBa.Text, txtsdtBa.Text, txtNgheNghiepBa.Text, txtEmailBa.Text, txtTenMe.Text,txtSDTMe.Text,txtNgheNghiepMe.Text,txtEmailMe.Text,txtMongMuon.Text, ddlcoso.Text
    public bool Linq_Them(string tenhocsinh, DateTime ngaysinh, string sdthocsinh, string truonghoc,
        string bietdanh, bool gioitinh, string emailhs, string noisinh, string choohientai, 
        string tenba, string sdtba, string nghenghiepba, string emailba, string tenme, string sdtnme, 
        string nghenghiepme, string emailme, string mongmuon , string account_coso, int lop)
    {
        tbAccount insert = new tbAccount();
        insert.account_vn = tenhocsinh;
        //insert.account_code = code;
        insert.account_ngaydangky = DateTime.Now;
        insert.account_ngaysinh = ngaysinh;
        insert.account_phone = sdthocsinh;
        insert.account_truonghoc = truonghoc;
        insert.account_en = bietdanh;
        insert.account_gioitinh = gioitinh;
        insert.account_email = emailhs;
        insert.account_noisinh = noisinh;
        insert.account_noiohientai = choohientai;
        insert.account_tenba = tenba;
        insert.account_sdtba = sdtba;
        insert.account_nghenghiepba = nghenghiepba;
        insert.account_emailba = emailba;
        insert.account_tenme = tenme;
        insert.account_sdtme = sdtnme;
        insert.account_nghenghiepme = nghenghiepme;
        insert.account_emailme = emailme;
        insert.account_mongmuon = mongmuon;
        cls_security md5 = new cls_security();
        insert.pass = md5.HashCode("12345");
        insert.account_coso = account_coso;
        insert.hidden = false;
        insert.account_acctive = false;
        db.tbAccounts.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            tbhocsinhtronglop inserthstl = new tbhocsinhtronglop();
            inserthstl.account_id = insert.account_id;
            inserthstl.lop_id = Convert.ToInt32(lop);
            //insert.monhoc_id = Convert.ToInt32(ddlmh.Value.ToString());
            inserthstl.hstl_hidden = false;
            //inserthstl.hstl_thoigianhoc = txtTongSoBuoiHoc.Value;
            //inserthstl.hstl_ngaybatdauhoc = Convert.ToDateTime(dteNgayBatDauHoc.Value);
            //inserthstl.hstl_ngayketthuchoc = dteNgayKetThuc.Value;
            inserthstl.hstl_baoluu = false;
            db.tbhocsinhtronglops.InsertOnSubmit(inserthstl);
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int account_id, string tenhocsinh, DateTime ngaysinh, string sdthocsinh, string truonghoc,
        string bietdanh, bool gioitinh, string emailhs, string noisinh, string choohientai,
        string tenba, string sdtba, string nghenghiepba, string emailba, string tenme, string sdtnme,
        string nghenghiepme, string emailme, string mongmuon, string account_coso, int lop)
    {
        tbAccount update = db.tbAccounts.Where(x => x.account_id == account_id).FirstOrDefault();
        update.account_vn = tenhocsinh;
        update.account_ngaysinh = ngaysinh;
        update.account_phone = sdthocsinh;
        update.account_truonghoc = truonghoc;
        update.account_en = bietdanh;
        update.account_gioitinh = gioitinh;
        update.account_email = emailhs;
        update.account_noisinh = noisinh;
        update.account_noiohientai = choohientai;
        update.account_tenba = tenba;
        update.account_sdtba = sdtba;
        update.account_nghenghiepba = nghenghiepba;
        update.account_emailba = emailba;
        update.account_tenme = tenme;
        update.account_sdtme = sdtnme;
        update.account_nghenghiepme = nghenghiepme;
        update.account_emailme = emailme;
        update.account_mongmuon = mongmuon;
        update.account_coso = account_coso;
        update.hidden = false;
        update.account_acctive = false;
        try
        {
            db.SubmitChanges();
            var CapNhatHSTl = (from hstl in db.tbhocsinhtronglops where hstl.account_id == account_id orderby hstl.account_id descending select hstl).FirstOrDefault();
            CapNhatHSTl.lop_id = Convert.ToInt32(lop);
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int account_id)
    {
        tbAccount delete = db.tbAccounts.Where(x => x.account_id == account_id).FirstOrDefault();
        delete.hidden = true;
        delete.account_acctive = true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}