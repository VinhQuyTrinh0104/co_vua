﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_BaiKiemTraChiTiet
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_BaiKiemTraChiTiet()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string bkt_sohang1, string bkt_sohang2,  string bkt_sohang3,string bkt_ketqua, int cate)
    {
       
        tb_BaiKiemTraChiTiet  insert = new tb_BaiKiemTraChiTiet();
        insert.bkt_sohang1 = bkt_sohang1;
        insert.bkt_sohang2 = bkt_sohang2;
        insert.bkt_sohang3 = bkt_sohang3;
        insert.bkt_ketqua = bkt_ketqua;
        insert.bkt_id = cate;
      
       
        
       
        // insert.news_link= 
        db.tb_BaiKiemTraChiTiets.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int bktct_id, string bkt_sohang1, string bkt_sohang2, string bkt_sohang3, string bkt_ketqua, int cate)
    { 
        tb_BaiKiemTraChiTiet update = db.tb_BaiKiemTraChiTiets.Where(x => x.bktct_id == bktct_id).FirstOrDefault();
        update.bkt_sohang1 = bkt_sohang1;
        update.bkt_sohang2 = bkt_sohang2;
        update.bkt_sohang3 = bkt_sohang3;
        update.bkt_ketqua = bkt_ketqua;
        update.bkt_id = cate;

        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int bktct_id)
    {
        tb_BaiKiemTraChiTiet delete = db.tb_BaiKiemTraChiTiets.Where(x => x.bktct_id == bktct_id).FirstOrDefault();
        db.tb_BaiKiemTraChiTiets.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}