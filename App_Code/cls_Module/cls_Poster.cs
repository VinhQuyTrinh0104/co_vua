﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_Poster
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_Poster()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them( string image,string poster_name)
    {
       
        tbPoster insert = new tbPoster();
        insert.poster_name = poster_name;
        insert.poster_image = image;
        // insert.news_link= 
        db.tbPosters.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int poster_id, string image, string poster_name)
    { 
        tbPoster update = db.tbPosters.Where(x => x.poster_id == poster_id).FirstOrDefault();
        update.poster_name = poster_name;
        if (image != "x")
            update.poster_image = image;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int poster_id)
    {
        tbPoster delete = db.tbPosters.Where(x => x.poster_id == poster_id).FirstOrDefault();
        db.tbPosters.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}