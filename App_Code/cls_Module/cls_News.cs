﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_News
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_News()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string news_title, string news_summary, string image, string content, int cate, DateTime date)
    {

        tbNew insert = new tbNew();
        insert.news_title = news_title;
        insert.news_image = image;
        insert.news_summary = news_summary;
        insert.news_content = content;
        insert.newcate_id = cate;
        insert.news_datetime = date;
        // insert.news_link= 
        db.tbNews.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int news_id, string news_title, string news_summary, string news_image, string content, DateTime date)
    {
        tbNew update = db.tbNews.Where(x => x.news_id == news_id).FirstOrDefault();
        update.news_title = news_title;
        update.news_image = news_image;
        update.news_summary = news_summary;
        update.news_content = content;
        // update.newcate_id = cate;
        update.news_datetime = date;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int news_id)
    {
        tbNew delete = db.tbNews.Where(x => x.news_id == news_id).FirstOrDefault();
        db.tbNews.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}