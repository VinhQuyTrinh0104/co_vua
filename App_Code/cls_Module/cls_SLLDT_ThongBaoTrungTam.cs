﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_SLLDT_ThongBaoTrungTam
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_SLLDT_ThongBaoTrungTam()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string thongbaotruong_title,  string content)
    {
       
        tbSLLDT_ThongBaoTruong insert = new tbSLLDT_ThongBaoTruong();
        insert.thongbaotruong_title = thongbaotruong_title;
        insert.thongbaotruong_content = content;
        insert.thongbaotruong_createdate = DateTime.Now;
        // insert.news_link= 
        db.tbSLLDT_ThongBaoTruongs.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int thongbaotruong_id,  string thongbaotruong_title, string content)
    { 
        tbSLLDT_ThongBaoTruong update = db.tbSLLDT_ThongBaoTruongs.Where(x => x.thongbaotruong_id == thongbaotruong_id).FirstOrDefault();
        update.thongbaotruong_title = thongbaotruong_title;
        update.thongbaotruong_content = content;
        update.thongbaotruong_createdate = DateTime.Now;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int thongbaotruong_id)
    {
        tbSLLDT_ThongBaoTruong delete = db.tbSLLDT_ThongBaoTruongs.Where(x => x.thongbaotruong_id == thongbaotruong_id).FirstOrDefault();
        db.tbSLLDT_ThongBaoTruongs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}