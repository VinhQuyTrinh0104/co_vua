﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_BaiTapKiemTraHocSinh
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_BaiTapKiemTraHocSinh()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string bkt_name, string bkt_loaibai,  string bkt_thoigianlambai,string bkt_tongdiem, int cate,int star,DateTime date,int cate_id)
    {
       
        tb_BaiTapKiemTraHocSinh  insert = new tb_BaiTapKiemTraHocSinh();
        insert.bkt_name = bkt_name;
        insert.bkt_loaibai = bkt_loaibai;
        insert.bkt_thoigianlambai = bkt_thoigianlambai;
        insert.bkt_tongdiem = bkt_tongdiem;
        insert.bkt_startdate = date;
        insert.hstl_id = cate;
        insert.Star = star;
        insert.account_id = cate_id;
      
       
        
       
        // insert.news_link= 
        db.tb_BaiTapKiemTraHocSinhs.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int bkt_id, string bkt_name, string bkt_loaibai, string bkt_thoigianlambai, string bkt_tongdiem, int cate, int star, DateTime date,int cate_id)
    { 
        tb_BaiTapKiemTraHocSinh update = db.tb_BaiTapKiemTraHocSinhs.Where(x => x.bkt_id == bkt_id).FirstOrDefault();
        update.bkt_name = bkt_name;
        update.bkt_loaibai = bkt_loaibai;
        update.bkt_thoigianlambai = bkt_thoigianlambai;
        update.bkt_tongdiem = bkt_tongdiem;
        update.bkt_startdate = date;
        update.hstl_id = cate;
        update.Star = star;
        update.account_id = cate_id;

        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int bkt_id)
    {
        tb_BaiTapKiemTraHocSinh delete = db.tb_BaiTapKiemTraHocSinhs.Where(x => x.bkt_id == bkt_id).FirstOrDefault();
        db.tb_BaiTapKiemTraHocSinhs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}