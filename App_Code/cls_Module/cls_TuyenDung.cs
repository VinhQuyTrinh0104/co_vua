﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_TuyenDung
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_TuyenDung()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string tuyendung_title,  string content)
    {
        tbTuyenDung insert = new tbTuyenDung();
        insert.tuyendung_title = tuyendung_title;
        insert.tuyendung_content = content;
        db.tbTuyenDungs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int introduce_id, string tuyendung_title,  string content )
    {

        tbTuyenDung update = db.tbTuyenDungs.Where(x => x.tuyendung_id == introduce_id).FirstOrDefault();
        update.tuyendung_title = tuyendung_title;
        update.tuyendung_content = content;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int introduce_id)
    {
        tbTuyenDung delete = db.tbTuyenDungs.Where(x => x.tuyendung_id == introduce_id).FirstOrDefault();
        db.tbTuyenDungs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}