﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_Introduce_KhoaHoc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_Introduce_KhoaHoc()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string introkh_title,  string introkh_content, string linkyoutube)
    {
        tbIntroduce_KhoaHoc insert = new tbIntroduce_KhoaHoc();
        insert.introkh_title = introkh_title; 
        insert.introkh_content = introkh_content;
        insert.introkh_linkyoutube = linkyoutube;
        db.tbIntroduce_KhoaHocs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int introkh_id, string introkh_title, string introkh_content,   string linkyoutube)
    {

        tbIntroduce_KhoaHoc update = db.tbIntroduce_KhoaHocs.Where(x => x.introkh_id == introkh_id).FirstOrDefault();
        update.introkh_title = introkh_title;
        update.introkh_content = introkh_content; 
        update.introkh_linkyoutube = linkyoutube;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int introkh_id)
    {
        tbIntroduce_KhoaHoc delete = db.tbIntroduce_KhoaHocs.Where(x => x.introkh_id == introkh_id).FirstOrDefault();
        db.tbIntroduce_KhoaHocs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}