﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_HistorySLL
/// </summary>
public class cls_HistorySLL
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_HistorySLL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Sua(int sll_id, string dggv)
    {
        tbSoLienLac update = db.tbSoLienLacs.Where(x => x.sll_id == sll_id).FirstOrDefault();
        update.sll_danhgiagv = dggv;
        //update.sll_date = DateTime.Now;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int sll_id)
    {
        tbSoLienLac update = db.tbSoLienLacs.Where(x => x.sll_id == sll_id).FirstOrDefault();
        update.sll_hidden=true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}