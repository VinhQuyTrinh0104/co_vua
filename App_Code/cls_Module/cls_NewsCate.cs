﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_NewsCate
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_NewsCate()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string newscate_title,string summary)
    {
        tbNewCate insert = new tbNewCate();
        insert.newcate_title = newscate_title;
        insert.newcate_summary = summary;
       
        db.tbNewCates.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int newscate_id, string news_title,string summary)
    {

        tbNewCate update = db.tbNewCates.Where(x => x.newcate_id == newscate_id).FirstOrDefault();
        update.newcate_title = news_title;
        update.newcate_summary = summary;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int contact_id)
    {
        tbNewCate delete = db.tbNewCates.Where(x => x.newcate_id == contact_id).FirstOrDefault();
        db.tbNewCates.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}