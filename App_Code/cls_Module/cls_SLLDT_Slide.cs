﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_SLLDT_Slide
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_SLLDT_Slide()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string image, string slide_title, string content)
    {

        tbSLLDT_Slide insert = new tbSLLDT_Slide();
        insert.slide_title = slide_title;
        insert.slide_image = image;
        insert.slide_content = content;

        // insert.news_link= 
        db.tbSLLDT_Slides.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int slide_id, string image, string slide_title, string content)
    {
        tbSLLDT_Slide update = db.tbSLLDT_Slides.Where(x => x.slide_id == slide_id).FirstOrDefault();
        update.slide_title = slide_title;
        if (image != "x")
            update.slide_image = image;
        update.slide_content = content;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int slide_id)
    {
        tbSLLDT_Slide delete = db.tbSLLDT_Slides.Where(x => x.slide_id == slide_id).FirstOrDefault();
        db.tbSLLDT_Slides.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}