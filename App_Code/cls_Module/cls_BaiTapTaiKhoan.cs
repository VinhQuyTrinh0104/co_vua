﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_BaiTapTaiKhoan
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_BaiTapTaiKhoan()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int account_id,int baitap_id,string linkseo)
    {
       
        tbBaiTapTaiKhoan  insert = new tbBaiTapTaiKhoan();
        insert.baitap_id = baitap_id;
        insert.account_id = account_id;
        insert.link_seo = linkseo; 
        // insert.news_link= 
        db.tbBaiTapTaiKhoans.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int baitaptaikhoan_id, int account_id, int baitap_id, string linkseo)
    {
        
        tbBaiTapTaiKhoan update = db.tbBaiTapTaiKhoans.Where(x => x.baitaptaikhoan_id == baitaptaikhoan_id).FirstOrDefault();
        update.baitap_id = baitap_id;
         
        update.link_seo = linkseo; 
        update.account_id = account_id;
        
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int baitaptaikhoan_id)
    {
        tbBaiTapTaiKhoan delete = db.tbBaiTapTaiKhoans.Where(x => x.baitaptaikhoan_id == baitaptaikhoan_id).FirstOrDefault();
        db.tbBaiTapTaiKhoans.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}