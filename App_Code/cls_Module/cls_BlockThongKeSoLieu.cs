﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_BlockThongKeSoLieu
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_BlockThongKeSoLieu()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string solieu, string noidung, string image)
    {

        tbBlock_ThongKeSoLieu insert = new tbBlock_ThongKeSoLieu();
        insert.block_thongke_solieu = solieu;
        insert.block_thongke_icon = image;
        insert.block_thongke_content = noidung;
        // insert.news_link= 
        db.tbBlock_ThongKeSoLieus.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int news_id, string solieu, string noidung, string image)
    {
        tbBlock_ThongKeSoLieu update = db.tbBlock_ThongKeSoLieus.Where(x => x.block_thongke_id == news_id).FirstOrDefault();
        update.block_thongke_solieu = solieu;
        //update.block_thongke_icon = image;
        update.block_thongke_content = noidung;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int news_id)
    {
        tbBlock_ThongKeSoLieu delete = db.tbBlock_ThongKeSoLieus.Where(x => x.block_thongke_id == news_id).FirstOrDefault();
        db.tbBlock_ThongKeSoLieus.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}