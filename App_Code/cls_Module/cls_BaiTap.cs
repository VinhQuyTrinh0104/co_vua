﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_BaiTap
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_BaiTap()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string baitap_name, string image,  string link_seo,int cate)
    {
       
        tbBaiTap  insert = new tbBaiTap();
        insert.baitap_name = baitap_name;
        insert.baitap_image = image;
        insert.link_seo = link_seo;
       
        insert.khoahoc_id = cate;
      
       
        
       
        // insert.news_link= 
        db.tbBaiTaps.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int baitap_id, string baitap_name, string image, string link_seo, int cate)
    { 
        tbBaiTap update = db.tbBaiTaps.Where(x => x.baitap_id == baitap_id).FirstOrDefault();
        update.baitap_name = baitap_name;
        if (image != "x")
            update.baitap_image = image;
        update.link_seo = link_seo; 
        update.khoahoc_id = cate;
        
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int baitap_id)
    {
        tbBaiTap delete = db.tbBaiTaps.Where(x => x.baitap_id == baitap_id).FirstOrDefault();
        db.tbBaiTaps.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}