﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_LoaiThongBao
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_LoaiThongBao()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string loaithongbao_title,int cate)
    {
       
        tbLoaiThongBao insert = new tbLoaiThongBao();
        insert.loaithongbao_title = loaithongbao_title; 
        insert.groupuser_id = cate;
        db.tbLoaiThongBaos.InsertOnSubmit(insert);
        try
        {
           
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int loaithongbao_id,   string loaithongbao_title,int cate)
    {
 
        tbLoaiThongBao update = db.tbLoaiThongBaos.Where(x => x.loaithongbao_id == loaithongbao_id).FirstOrDefault();
        update.loaithongbao_title = loaithongbao_title;
        update.groupuser_id = cate;
        
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int loaithongbao_id)
    {
        tbLoaiThongBao delete = db.tbLoaiThongBaos.Where(x => x.loaithongbao_id == loaithongbao_id).FirstOrDefault();
        db.tbLoaiThongBaos.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}