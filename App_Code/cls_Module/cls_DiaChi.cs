﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_DiaChi
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_DiaChi()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string diachi_title,string diachi_summary, string diachi_phone)
    {
        tbDiaChi insert = new tbDiaChi();
        insert.diachi_title = diachi_title;
        insert.diachi_summary = diachi_summary;
        insert.diachi_phone = diachi_phone;
        db.tbDiaChis.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int diachi_id, string diachi_title, string diachi_summary, string diachi_phone)
    {

        tbDiaChi update = db.tbDiaChis.Where(x => x.diachi_id == diachi_id).FirstOrDefault();
        update.diachi_title = diachi_title;
        update.diachi_summary = diachi_summary;
        update.diachi_phone = diachi_phone;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int diachi_id)
    {
        tbDiaChi delete = db.tbDiaChis.Where(x => x.diachi_id == diachi_id).FirstOrDefault();
        db.tbDiaChis.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}