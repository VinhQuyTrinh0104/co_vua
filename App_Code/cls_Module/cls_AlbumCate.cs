﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_AlbumCate
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_AlbumCate()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string newscate_title)
    {
        tbAlbum_Image_Cate insert = new tbAlbum_Image_Cate();
        insert.album_image_cate_name = newscate_title;
       
        db.tbAlbum_Image_Cates.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int newscate_id, string news_title)
    {

        tbAlbum_Image_Cate update = db.tbAlbum_Image_Cates.Where(x => x.album_image_cate_id == newscate_id).FirstOrDefault();
        update.album_image_cate_name = news_title;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int contact_id)
    {
        tbAlbum_Image_Cate delete = db.tbAlbum_Image_Cates.Where(x => x.album_image_cate_id == contact_id).FirstOrDefault();
        db.tbAlbum_Image_Cates.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}