﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_Hocsinhtronglop
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_Hocsinhtronglop()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Time(DateTime date)
    {

        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int hstl_id, int lop_id)
    {

        tbhocsinhtronglop update = db.tbhocsinhtronglops.Where(x => x.hstl_id == hstl_id).FirstOrDefault();
        update.lop_id = lop_id;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool Linq_NghiHoc(int hstl_id)
    {

        tbhocsinhtronglop update = db.tbhocsinhtronglops.Where(x => x.hstl_id == hstl_id).FirstOrDefault();
        update.hstl_hidden = true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    //public bool Linq_Xoa(int hstl_id)
    //{
    //    tbhocsinhtronglop delete = db.tbhocsinhtronglops.Where(x => x.hstl_id == hstl_id).FirstOrDefault();
    //    db.tbhocsinhtronglops.DeleteOnSubmit(delete);
    //    try
    //    {
    //        db.SubmitChanges();
    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}
}