﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_News
/// </summary>
public class cls_NhapLesson
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_NhapLesson()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(int id_sach, int id_unit, string lesson_title, string link_lesson, string lesson_back, string lesson_next, string avatar, string tuvung)
    {

        tbSLLDT_lesson insert = new tbSLLDT_lesson();
        insert.unit_id = id_unit;
        insert.lesson_title = lesson_title;
        insert.lesson_link = link_lesson;
        insert.lesson_back = lesson_back;
        insert.lesson_next = lesson_next;
        insert.lesson_avatar = avatar;
        insert.lesson_tuvung = tuvung;
        db.tbSLLDT_lessons.InsertOnSubmit(insert);
        try
        {

            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int id_lesson,int id_sach, int id_unit, string lesson_title, string link_lesson, string lesson_back, string lesson_next, string avatar, string tuvung)
    {
        tbSLLDT_lesson update = db.tbSLLDT_lessons.Where(x => x.lesson_id == id_lesson).FirstOrDefault();
        update.unit_id = id_unit;
        update.lesson_title = lesson_title;
        update.lesson_link = link_lesson;
        update.lesson_back = lesson_back;
        update.lesson_next = lesson_next;
        update.lesson_avatar = avatar;
        update.lesson_tuvung = tuvung;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int id_lesson)
    {
        tbSLLDT_lesson delete = db.tbSLLDT_lessons.Where(x => x.lesson_id == id_lesson).FirstOrDefault();
        db.tbSLLDT_lessons.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}