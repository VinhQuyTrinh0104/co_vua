﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for webui
/// </summary>
public class webui
{
    public webui()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public List<string> UrlRoutes()
    {
        List<string> list = new List<string>();
        list.Add("webTrangChu|trung-tam-anh-ngu-vjlc|~/Default.aspx");

        // Introduce
        list.Add("webgioithieu|gioi-thieu-trung-tam-anh-ngu-vjlc|~/web_module/web_GioiThieu.aspx");
        list.Add("webtuyendung|tuyen-dung|~//web_module/web_TuyenDung.aspx");

        // Đăng ký khóa học
        // Tin tức
        // Tin tức chi tiết
        // Liên hệ
        list.Add("weblienhe|lien-he|~/web_module/web_LienHe.aspx");
        // khách hàng nổi bật -> Khóa học nổi bật

        list.Add("webkhoahoctieubieu|khoa-hoc-tieu-bieu|~/web_module/web_KhoaHoc.aspx");

        //  list.Add("weblienhe|lien-he|~/web_module/web_Contact.aspx");
        //list.Add("webkhachhang|khach-hang-noi-bat/{name}-{id}|~/web_module/web_KhachHang.aspx");
        list.Add("webkhoahocnoibat|khoa-hoc-noi-bat/{name}-{id}|~/web_module/web_KhoaHocDetail.aspx");
        //Sticker
        list.Add("websticker|sticker|~/web_module/web_Sticker.aspx");
        //WEBSITE
        // CHƯƠNG TRÌNH KHÓA HỌC
        list.Add("websitechuongtrinhkhoahoc|vjlc-chuong-trinh-{name}|~//web_module/website_vjlc/web_vjlc_Chuong_Trinh_Khoa_hoc.aspx");
        // tin tức

        list.Add("webtintuc|tin-tuc-10|~//web_module/web_News.aspx");
        list.Add("webgocwebphuhuynh|goc-phu-huynh-11|~//web_module/web_GocPhuHuynh.aspx");

        list.Add("webgocphuhuynhchitiet|goc-phu-huynh/{name}-{id}|~//web_module/web_TinTucChiTiet.aspx");
        list.Add("webtuyendungchitiet|tuyen-dung/{name}-{id}|~//web_module/web_TinTucChiTiet.aspx");
        list.Add("webtintucchitiet|tin-tuc/{name}-{id}|~//web_module/web_TinTucChiTiet.aspx");

        //SLLDT
        list.Add("websolldt|slldt-home|~/Default_SoLienLac.aspx");
        list.Add("websolldt-login-user|slldt-login-user|~/web_module/SLLDT/SLLDT_LoginForUser.aspx");
        // list.Add("websolldt-forget-pass|slldt-forget-pass|~/web_module/SLLDT/web_QuenMatKhau.aspx");
        list.Add("websolldt-forget-pass|slldt-forget-pass|~/web_module/SLLDT/web_QuenMatKhau.aspx");

        list.Add("websolienlacthongbaotruong|slldt-thong-bao-truong-{id}|~/web_module/SLLDT/SLLDT_ThongBaoTruong.aspx");
        list.Add("Websolienlacthongbaolop|slldt-thong-bao-lop-{id}|~/web_module/SLLDT/SLLDT_ThongBaoLop.aspx");
        list.Add("soLLDT_thoikhoabieu|slldt-thoi-khoa-bieu|~/web_module/SLLDT/SLLDT_ThoiKhoaBieu.aspx");
        list.Add("soLLDT_thongtintre|slldt-thong-tin-tre|~/web_module/SLLDT/SLLDT_ThongTinCaNhanCuaBe_main.aspx");
        // list.Add("soLLDT_homnaycuabe|slldt-hom-nay-cua-be-{id}|~/web_module/SLLDT/SLLDT_HomNayCuaBe.aspx");
        list.Add("soLLDT_chuongtrinhhoc|slldt-chuong-trinh-hoc-{id}|~/web_module/SLLDT/SLLDT_ChuongTrinhHoc.aspx");
        list.Add("soLLDT_Danhgia|slldt-danh-gia|~/web_module/SLLDT/SLLDT_DanhGia.aspx");
        list.Add("soLLDT_coso|slldt-co-so|~/web_module/SLLDT/SLLDT_CoSo.aspx");
        list.Add("soLLDT_hn|slldt-hom-nay-cua-be|~/web_module/SLLDT/SLLDT_HomNayCuaBe_main.aspx");
        list.Add("soLLDT_add|slldt-thay-anh-dai-dien|~/web_module/SLLDT/SLLDT_ThayDoiAnhDaiDien.aspx");
        list.Add("soLLDT_Sach|slldt-sach|~/web_module/SLLDT/SLLDT_Sach.aspx");
        list.Add("soLLDT_changepassword|slldt-change-password|~/web_module/SLLDT/SLLDT_ChangePassword.aspx");
        //list.Add("soLLDT_Chonbaihoc|slldt-chon-bai-hoc|~/web_module/SLLDT/SLLDT_DoodleTown_1.aspx");
        list.Add("soLLDT_Chonbaihoc|slldt-chon-bai-hoc-{id}|~/web_module/SLLDT/SLLDT_DoodleTown_1.aspx");
        list.Add("soLLDT_Video|slldt-video-bai-hoc-{lesson-id}|~/web_module/SLLDT/SLLDT_Video.aspx");

        //sách doodletown nursery
        list.Add("soLLDT_DanhMucSachDoodleTownNursery|slldt-danh-muc-sach-doodletown-nursery-{id}|~/web_module/SLLDT/SLLDT_DanhMucSach_DoodleNursery.aspx");
        //--unit 1
        /*nhận biết*/
        list.Add("soLLDT_DoodleTownNurseryUnit1Lesson1|slldt-doodletown-nursery-vocabulary-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/DoodleTownNersery/Unit1_Lesson1_2.aspx");
        list.Add("soLLDT_DoodleTownNurseryUnit1Lesson2|slldt-doodletown-nursery-8-23-43|~/web_module/SLLDT/DoodleTownNersery/Unit1_Lesson3_4_5.aspx");
        //--unit 3
        list.Add("soLLDT_DoodleTownNurseryUnit3Lesson345|slldt-doodletown-nursery-8-25-49|~/web_module/SLLDT/DoodleTownNersery/Unit3_Lesson3_4_5.aspx");
        //--unit 4
        list.Add("soLLDT_DoodleTownNurseryUnit4Lesson345|slldt-doodletown-nursery-8-26-52|~/web_module/SLLDT/DoodleTownNersery/Unit4_Lesson3_4_5.aspx");
        //--unit 5
        list.Add("soLLDT_DoodleTownNurseryChonCapDapAn|slldt-doodletown-nursery-chon-cap-8-27-55|~/web_module/SLLDT/DoodleTownNersery/Unit5_GameChonCap.aspx");
        //--unit 6
        list.Add("soLLDT_DoodleTownNurseryUnit6Lesson345|slldt-doodletown-nursery-8-28-58|~/web_module/SLLDT/DoodleTownNersery/Unit6_Lesson3_4_5.aspx");
        //--unit 7
        list.Add("soLLDT_DoodleTownNurseryUnit7Lesson345|slldt-doodletown-nursery-8-29-61|~/web_module/SLLDT/DoodleTownNersery/Unit7_Lesson3_4_5.aspx");
        //--unit 8
        list.Add("soLLDT_DoodleTownNurseryUnit8Lesson345|slldt-doodletown-nursery-8-30-64|~/web_module/SLLDT/DoodleTownNersery/Unit8_GameChonCap.aspx");
        //--unit 9
        list.Add("soLLDT_DoodleTownNurseryUnit9Lesson345|slldt-doodletown-nursery-8-31-67|~/web_module/SLLDT/DoodleTownNersery/Unit9_GameChonCap.aspx");
        //--unit 10f
        list.Add("soLLDT_DoodleTownNurseryUnit10Lesson6|slldt-doodletown-nursery-8-32-71|~/web_module/SLLDT/DoodleTownNersery/Unit10_Lesson6.aspx");
        //-review 
        list.Add("soLLDT_DoodleTownNurseryReView|slldt-doodletown-nursery-review-{sach-id}-{lesson-id}/{id}|~/web_module/SLLDT/DoodleTownNersery/Review.aspx");

        //sách doodletown 1
        //list.Add("soLLDT_DoodleTown1Unit1Lesson134|slldt-doodletown-video-vocabulary-conversation-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Unit1_Video_Vocabulary_Conversation.aspx");
        list.Add("soLLDT_DoodleTown1choncap2hinh|slldt-doodletown-choose-pair-of-picture-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Web_PairOfPicture.aspx");
        //list.Add("soLLDT_DoodleTown1choncaptheoso|slldt-doodletown-choose-pairofpicture-number-{number}-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Web_PairOfPictureNumber.aspx");
        list.Add("soLLDT_DoodleTown1tracnghiem|slldt-doodletown-mutiple-choise-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Web_MultiChoise.aspx");
        //list.Add("soLLDT_DoodleTown1lathinh|slldt-doodletown-flip-picture-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Web_FlipPicture.aspx");
        list.Add("soLLDT_DoodleTown1choncapamthanh|slldt-doodletown-choose-pairofpicture-sound-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Web_PairOfPictureSound.aspx");
        list.Add("soLLDT_DoodleTown1Unit1Lesson7|slldt-doodletown-9-1-161|~/web_module/SLLDT/DoodleTown1/Unit1_Lesson_7.aspx");
        list.Add("soLLDT_DoodleTown1videovachoncap|slldt-doodletown-9-2-163|~/web_module/SLLDT/DoodleTown1/web_Video_PairOfPicture.aspx");
        list.Add("soLLDT_DoodleTown1Unit2Lesson7|slldt-doodletown-9-2-164|~/web_module/SLLDT/DoodleTown1/Unit2_lession_7_ucDragRead.aspx");
        list.Add("soLLDT_DoodleTown1Unit2Lesson34|slldt-doodletown-9-2-4|~/web_module/SLLDT/DoodleTown1/Web_DragDropGame.aspx");
        list.Add("soLLDT_DoodleTown1Unit3Lesson56|slldt-doodletown-9-3-166|~/web_module/SLLDT/DoodleTown1/Unit3_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit8Lesson34|slldt-doodletown-9-8-16|~/web_module/SLLDT/DoodleTown1/Unit8_Lesson3_4.aspx");
        list.Add("soLLDT_DoodleTown1Unit9Lesson34|slldt-doodletown-9-9-18|~/web_module/SLLDT/DoodleTown1/Unit9_Lesson3_4.aspx");
        list.Add("soLLDT_DoodleTown1Unit1Lesson56|slldt-doodletown-9-1-89|~/web_module/SLLDT/DoodleTown1/Unit1_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit4Lesson56|slldt-doodletown-9-4-169|~/web_module/SLLDT/DoodleTown1/Unit4_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit5Lesson56|slldt-doodletown-9-5-172|~/web_module/SLLDT/DoodleTown1/Unit5_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit6Lesson56|slldt-doodletown-9-6-175|~/web_module/SLLDT/DoodleTown1/Unit6_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit7Lesson56|slldt-doodletown-9-7-178|~/web_module/SLLDT/DoodleTown1/Unit7_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit8Lesson56|slldt-doodletown-9-8-181|~/web_module/SLLDT/DoodleTown1/Unit8_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown1Unit9Lesson56|slldt-doodletown-9-9-184|~/web_module/SLLDT/DoodleTown1/Unit9_Lesson5_6.aspx");



        list.Add("soLLDT_DoodleTownVideoAndVocabulary|slldt-doodletown-video-vocabulary-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/DoodleTown1/Unit3_Lesson5_6.aspx");



        // sách doodletown 2
        list.Add("soLLDT_DoodleTown2Unit1Lesson78|slldt-doodletown2-10-11-21|~/web_module/SLLDT/DoodleTown2/Unit1_Lesson7_8_Video.aspx");
        list.Add("soLLDT_DoodleTown2Unit2Lesson78|slldt-doodletown2-10-12-202|~/web_module/SLLDT/DoodleTown2/Unit2_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit3Lesson56|slldt-doodletown2-10-13-27|~/web_module/SLLDT/DoodleTown2/Unit3_Lesson5_6_Vocabulary_DragDrop.aspx");
        list.Add("soLLDT_DoodleTown2Unit3Lesson78|slldt-doodletown2-10-13-206|~/web_module/SLLDT/DoodleTown2/Unit3_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit4Lesson56|slldt-doodletown2-10-14-210|~/web_module/SLLDT/DoodleTown2/Unit4_Lesson5_6_Vocabulary_DragDrop.aspx");
        list.Add("soLLDT_DoodleTown2Unit4Lesson78|slldt-doodletown2-10-14-30|~/web_module/SLLDT/DoodleTown2/Unit4_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit5Lesson78|slldt-doodletown2-10-15-213|~/web_module/SLLDT/DoodleTown2/Unit5_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit6Lesson78|slldt-doodletown2-10-16-218|~/web_module/SLLDT/DoodleTown2/Unit6_Lesson7_8_Video_ImageAudio.aspx");
        list.Add("soLLDT_DoodleTown2Unit7Lesson78|slldt-doodletown2-10-17-223|~/web_module/SLLDT/DoodleTown2/Unit7_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit8Lesson78|slldt-doodletown2-10-18-229|~/web_module/SLLDT/DoodleTown2/Unit8_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit9Lesson78|slldt-doodletown2-10-62-233|~/web_module/SLLDT/DoodleTown2/Unit9_Lesson7_8_Video_Conversation.aspx");
        list.Add("soLLDT_DoodleTown2Unit10|slldt-doodletown2-10-64-237|~/web_module/SLLDT/DoodleTown2/Unit10_Vocabulary_ImagesAudio.aspx");
        //slldt-doodletown-vocabulary-multichoise-10-16-217

        list.Add("soLLDT_DoodleTown2Unit8Lesson12|slldt-doodletown2-Vocabulary_MultiChoise-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/globalVideo_Vocabulary_MultiChoise.aspx");
        //global
        list.Add("soLLDT_DoodleTownVideoVocabularyConversation|slldt-doodletown-videovocabularyconversation-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Video_Vocabulary_Conversation.aspx");
        list.Add("soLLDT_DoodleTownVocabularyMultichoisecolor|slldt-doodletown-vocabulary-multichoise-color-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_MultiChoiseColor.aspx");
        list.Add("soLLDT_DoodleTownVocabularyMultichoise|slldt-doodletown-vocabulary-multichoise-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_MultiChoiseGame.aspx");
        list.Add("soLLDT_DoodleTownMultiChoiseImageNoGame|slldt-doodletown-multichoise-image-nogame-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_MultiChoiseImageNoAudio.aspx");
        list.Add("soLLDT_DoodleTownMultiChoiseImage|slldt-doodletown-multichoise-image-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_MultiChoiseImage.aspx");
        list.Add("soLLDT_DoodleTownMultichoise|slldt-doodletown-multichoise-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_MultiChoise.aspx");
        list.Add("soLLDT_DoodleTownVocabularyFlipPicture|slldt-doodletown-vocabulary-flip-picture-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_FilpPicture.aspx");
        list.Add("soLLDT_DoodleTownFlipPicture|slldt-doodletown-flip-picture-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_FlipPicture.aspx");
        list.Add("soLLDT_DoodleTownFlipPictureNumber|slldt-doodletown-choose-pairofpicture-number-{number}-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_PairOfPictureNumber.aspx");
        //list.Add("soLLDT_DoodleTownVocabularyFlipPicture|slldt-doodletown-vocabulary-pair-of-picture-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_PairOfPicture.aspx");
        list.Add("soLLDT_DoodleTownVideoVocabularyMultiChoise|slldt-doodletown-videovocabularymultichoise-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Video_Vocabulary_MultiChoise.aspx");
        list.Add("soLLDT_DoodleTownVocabularyDragDrop|slldt-doodletown-vocabulary-drag-and-drop-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_DragDrop.aspx");
        list.Add("soLLDT_DoodleTownVocabularyPairOfPictureSound|slldt-doodletown-vocabulary-pair-of-pictue-sound-{sach-id}-{lesson-id}-{unit-id}|~/web_module/SLLDT/global/global_Vocabulary_PairOfPictureSound.aspx");

        //Academy Star S
        list.Add("soLLDT_AcademyStartSWelcome|slldt-academy-stars-starter-1-59-129|~/web_module/SLLDT/AcademyStarS/Welcome_lesson_1.aspx");
        /*Lesson 1+2 */
        list.Add("soLLDT_AcademyStarsStarterLesson1-2|slldt-academy-stars-starter-vocabulary-1-52-130|~/web_module/SLLDT/AcademyStarS/Unit03_Lesson01_02.aspx");
        list.Add("soLLDT_AcademyStarsStarterLesson1--2|slldt-academy-stars-starter-vocabulary-1-53-117|~/web_module/SLLDT/AcademyStarS/Unit04_Lesson01_02.aspx");
        list.Add("soLLDT_AcademyStarsStarterLesson12|slldt-academy-stars-starter-vocabulary-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/AcademyStarS/web_GameVocabury_MultipleChoice.aspx");
        /*Lesson 3*/
        list.Add("soLLDT_AcademyStartStarterLesson3|slldt-academy-stars-starter-grammer-lesson-3-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/AcademyStarS/Unit1_Lesson3.aspx");
        /*Lesson 4*/
        list.Add("soLLDT_AcademyStartStarterLesson4|slldt-academy-stars-starter-grammer-lesson-4-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/AcademyStarS/Unit1_Lesson4.aspx");
        /*Lesson 5+6*/
        list.Add("soLLDT_AcademyStartStarterLesson56|slldt-academy-stars-starter-listening-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/AcademyStarS/Unit1_Lesson5_6.aspx");
        //Unit1
        list.Add("soLLDT_AcademyStartSUnit1Lesson8|slldt-academy-stars-starter-1-21-106|~/web_module/SLLDT/AcademyStarS/Unit1_Lesson8.aspx");
        //Unit2 
        list.Add("soLLDT_AcademyStartSnit2Lesson8|slldt-academy-stars-starter-lat-hinh-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/AcademyStarS/Unit02_Lesson08.aspx");
        //unit 3
        list.Add("soLLDT_AcademyStartSunit3Lesson3|slldt-academy-stars-starter-1-52-131|~/web_module/SLLDT/AcademyStarS/Unit03_Lesson03.aspx");
        list.Add("soLLDT_AcademyStartSunit3Lesson8|slldt-academy-stars-starter-1-52-134|~/web_module/SLLDT/AcademyStarS/Unit03_Lesson08.aspx");
        //Unit4
        list.Add("soLLDT_AcademyStartSUnit4Lesson4|slldt-academy-stars-starter-1-53-119|~/web_module/SLLDT/AcademyStarS/Unit04_Lesson04.aspx");
        //Unit6
        list.Add("soLLDT_AcademyStartSnit6Lesson4|slldt-academy-stars-starter-1-55-142|~/web_module/SLLDT/AcademyStarS/Unit6_GameChonCap.aspx");
        list.Add("soLLDT_AcademyStartSunit6Lesson3|slldt-academy-stars-starter-1-55-141|~/web_module/SLLDT/AcademyStarS/Unit06_Lesson03.aspx");
        //Unit8
        list.Add("soLLDT_AcademyStartSunit8Lesson12|slldt-academy-stars-starter-1-57-150|~/web_module/SLLDT/AcademyStarS/Unit08_Lesson1_2.aspx");
        list.Add("soLLDT_AcademyStartSunit8Lesson8|slldt-academy-stars-starter-1-57-154|~/web_module/SLLDT/AcademyStarS/Unit08_Lesson08.aspx");


        //Academy Star 1
        //Welcome
        list.Add("soLLDT_AcademyStart1WelcomeLesson1|slldt-academystar-2-19-37|~/web_module/SLLDT/AcademyStar1/Welcome_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart1WelcomeLesson2|slldt-academystar-2-19-38|~/web_module/SLLDT/AcademyStar1/Welcome_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart1WelcomeLesson3|slldt-academystar-2-19-77|~/web_module/SLLDT/AcademyStar1/Welcome_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart1WelcomeLesson4|slldt-academystar-2-19-78|~/web_module/SLLDT/AcademyStar1/Welcome_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart1WelcomeLesson5|slldt-academystar-2-19-79|~/web_module/SLLDT/AcademyStar1/Welcome_Lesson5.aspx");
        //Unit 1
        list.Add("soLLDT_AcademyStart1unit1Lesson1|slldt-academystar-2-20-80|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson2|slldt-academystar-2-20-81|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson3|slldt-academystar-2-20-82|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson4|slldt-academystar-2-20-83|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson5|slldt-academystar-2-20-84|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson6|slldt-academystar-2-20-85|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson7|slldt-academystar-2-20-86|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1unit1Lesson8|slldt-academystar-2-20-87|~/web_module/SLLDT/AcademyStar1/Unit01_Lesson08.aspx");
        //Unit 2
        list.Add("soLLDT_AcademyStart1Unit2Lesson1|slldt-academystar-2-37-96|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson2|slldt-academystar-2-37-97|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson3|slldt-academystar-2-37-98|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson4|slldt-academystar-2-37-99|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson5|slldt-academystar-2-37-100|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson6|slldt-academystar-2-37-101|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson7|slldt-academystar-2-37-102|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit2Lesson8|slldt-academystar-2-37-103|~/web_module/SLLDT/AcademyStar1/Unit02_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1Review1|slldt-academystar-2-38-239|~/web_module/SLLDT/AcademyStar1/Unit02_Review01.aspx");
        //list.Add("soLLDT_AcademyStart1Unit2Lesson8|slldt-academystar-2-37-103|~/web_module/SLLDT/AcademyStar1/Unit2/Unit2_Lesson8.aspx");
        //Unit3
        list.Add("soLLDT_AcademyStart1Unit3Lesson1|slldt-academystar-2-39-122|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson2|slldt-academystar-2-39-123|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson3|slldt-academystar-2-39-124|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson4|slldt-academystar-2-39-125|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson5|slldt-academystar-2-39-126|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson6|slldt-academystar-2-39-127|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson7|slldt-academystar-2-39-128|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit3Lesson8|slldt-academystar-2-39-238|~/web_module/SLLDT/AcademyStar1/Unit03_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1ReadingTime1|slldt-academystar-2-65-240|~/web_module/SLLDT/AcademyStar1/ReadingTime01.aspx");
        //unit 4
        list.Add("soLLDT_AcademyStart1Unit4Lesson1|slldt-academystar-2-40-241|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson2|slldt-academystar-2-40-242|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson3|slldt-academystar-2-40-243|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson4|slldt-academystar-2-40-244|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson5|slldt-academystar-2-40-245|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson6|slldt-academystar-2-40-246|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson7|slldt-academystar-2-40-247|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit4Lesson8|slldt-academystar-2-40-248|~/web_module/SLLDT/AcademyStar1/Unit04_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1Review2|slldt-academystar-2-41-249|~/web_module/SLLDT/AcademyStar1/Review02.aspx");
        //Unit 5
        list.Add("soLLDT_AcademyStart1Unit5Lesson1|slldt-academystar-2-42-250|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson2|slldt-academystar-2-42-251|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson3|slldt-academystar-2-42-252|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson4|slldt-academystar-2-42-253|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson5|slldt-academystar-2-42-254|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson6|slldt-academystar-2-42-255|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson7|slldt-academystar-2-42-256|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit5Lesson8|slldt-academystar-2-42-257|~/web_module/SLLDT/AcademyStar1/Unit05_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1ReadingTime2|slldt-academystar-2-66-258|~/web_module/SLLDT/AcademyStar1/ReadingTime02.aspx");
        //Unit 6
        list.Add("soLLDT_AcademyStart1Unit6Lesson1|slldt-academystar-2-43-259|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson2|slldt-academystar-2-43-260|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson3|slldt-academystar-2-43-261|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson4|slldt-academystar-2-43-262|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson5|slldt-academystar-2-43-263|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson6|slldt-academystar-2-43-264|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson7|slldt-academystar-2-43-265|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit6Lesson8|slldt-academystar-2-43-266|~/web_module/SLLDT/AcademyStar1/Unit06_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1Review3|slldt-academystar-2-44-267|~/web_module/SLLDT/AcademyStar1/Review03.aspx");

        //Unit 7
        list.Add("soLLDT_AcademyStart1Unit7Lesson1|slldt-academystar-2-45-268|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson2|slldt-academystar-2-45-269|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson3|slldt-academystar-2-45-270|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson4|slldt-academystar-2-45-271|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson5|slldt-academystar-2-45-272|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson6|slldt-academystar-2-45-273|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson7|slldt-academystar-2-45-274|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit7Lesson8|slldt-academystar-2-45-275|~/web_module/SLLDT/AcademyStar1/Unit07_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1ReadingTime3|slldt-academystar-2-67-276|~/web_module/SLLDT/AcademyStar1/ReadingTime03.aspx");
        //Unit 8
        list.Add("soLLDT_AcademyStart1Unit8Lesson1|slldt-academystar-2-46-277|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson2|slldt-academystar-2-46-278|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson3|slldt-academystar-2-46-279|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson4|slldt-academystar-2-46-280|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson5|slldt-academystar-2-46-281|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson6|slldt-academystar-2-46-282|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson7|slldt-academystar-2-46-283|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit8Lesson8|slldt-academystar-2-46-284|~/web_module/SLLDT/AcademyStar1/Unit08_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1Review4|slldt-academystar-2-47-285|~/web_module/SLLDT/AcademyStar1/Review04.aspx");
        //Unit 9
        list.Add("soLLDT_AcademyStart1Unit9Lesson1|slldt-academystar-2-48-286|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson2|slldt-academystar-2-48-287|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson3|slldt-academystar-2-48-288|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson4|slldt-academystar-2-48-289|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson5|slldt-academystar-2-48-290|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson6|slldt-academystar-2-48-291|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson7|slldt-academystar-2-48-292|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit9Lesson8|slldt-academystar-2-48-293|~/web_module/SLLDT/AcademyStar1/Unit09_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1ReadingTime4|slldt-academystar-2-68-294|~/web_module/SLLDT/AcademyStar1/ReadingTime04.aspx");
        //Unit 10
        list.Add("soLLDT_AcademyStart1Unit10Lesson1|slldt-academystar-2-49-295|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson01.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson2|slldt-academystar-2-49-296|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson02.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson3|slldt-academystar-2-49-297|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson03.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson4|slldt-academystar-2-49-298|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson04.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson5|slldt-academystar-2-49-299|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson05.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson6|slldt-academystar-2-49-300|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson06.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson7|slldt-academystar-2-49-301|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson07.aspx");
        list.Add("soLLDT_AcademyStart1Unit10Lesson8|slldt-academystar-2-49-302|~/web_module/SLLDT/AcademyStar1/Unit10_Lesson08.aspx");
        list.Add("soLLDT_AcademyStart1Review5|slldt-academystar-2-50-477|~/web_module/SLLDT/AcademyStar1/Review05.aspx");
        //Chi tiết điểm tiếng anh
        list.Add("soLLDT_ChiTietDiem|slldt-bai-kiem-tra|~/web_module/SLLDT/SLLDT_ChiTietDiem.aspx");



        //Academy Star 3
        //Welcome
        list.Add("soLLDT_AcademyStarts3WelcomeLesson1|slldt-academystar-4-69-390|~/web_module/SLLDT/AcademyStar3/Welcome_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars3WelcomeLesson2|slldt-academystar-4-69-391|~/web_module/SLLDT/AcademyStar3/Welcome_Lesson02.aspx");
        //Unit 1
        //list.Add("soLLDT_AcademyStars3unit1Lesson1|slldt-academystar-4-70-392|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit1Lesson2|slldt-academystar-4-70-393|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson3|slldt-academystar-4-70-394|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson4|slldt-academystar-4-70-395|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson5|slldt-academystar-4-70-396|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson6|slldt-academystar-4-70-397|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson7|slldt-academystar-4-70-398|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson8|slldt-academystar-4-70-399|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson08.aspx");
        //Reading time 1
        list.Add("soLLDT_AcademyStart3ReadingTime1|slldt-academystar-4-101-478|~/web_module/SLLDT/AcademyStar3/ReadingTime01.aspx");
        //Unit 2
        //list.Add("soLLDT_AcademyStars3unit2Lesson1|slldt-academystar-4-71-400|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit2Lesson2|slldt-academystar-4-71-401|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson3|slldt-academystar-4-71-402|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson4|slldt-academystar-4-71-403|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson5|slldt-academystar-4-71-404|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson6|slldt-academystar-4-71-405|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson7|slldt-academystar-4-71-406|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit2Lesson8|slldt-academystar-4-71-407|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson08.aspx");
        //review 1
        list.Add("soLLDT_AcademyStars3review1|slldt-academystar-4-72-408|~/web_module/SLLDT/AcademyStar3/Review01.aspx");
        //Unit 3
        //list.Add("soLLDT_AcademyStars3unit3Lesson1|slldt-academystar-4-73-409|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit3Lesson2|slldt-academystar-4-73-410|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson3|slldt-academystar-4-73-411|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson4|slldt-academystar-4-73-412|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson5|slldt-academystar-4-73-413|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson6|slldt-academystar-4-73-414|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson7|slldt-academystar-4-73-415|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit3Lesson8|slldt-academystar-4-73-416|~/web_module/SLLDT/AcademyStar3/Unit03_Lesson08.aspx");
        //Reading time 2
        list.Add("soLLDT_AcademyStart3ReadingTime2|slldt-academystar-4-102-479|~/web_module/SLLDT/AcademyStar3/ReadingTime02.aspx");
        //Unit 4
        list.Add("soLLDT_AcademyStars3unit4Lesson1|slldt-academystar-4-74-417|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson2|slldt-academystar-4-74-418|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson3|slldt-academystar-4-74-419|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson4|slldt-academystar-4-74-420|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson5|slldt-academystar-4-74-421|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson6|slldt-academystar-4-74-422|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson7|slldt-academystar-4-74-423|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit4Lesson8|slldt-academystar-4-74-424|~/web_module/SLLDT/AcademyStar3/Unit04_Lesson08.aspx");
        //review 2
        list.Add("soLLDT_AcademyStars3review2|slldt-academystar-4-75-425|~/web_module/SLLDT/AcademyStar3/Review02.aspx");
        //Unit 5
        //list.Add("soLLDT_AcademyStars3unit5Lesson1|slldt-academystar-4-76-426|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson2|slldt-academystar-4-76-427|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson3|slldt-academystar-4-76-428|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson4|slldt-academystar-4-76-429|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson5|slldt-academystar-4-76-430|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson6|slldt-academystar-4-76-431|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson7|slldt-academystar-4-76-432|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit5Lesson8|slldt-academystar-4-76-433|~/web_module/SLLDT/AcademyStar3/Unit05_Lesson08.aspx");
        //Unit 6
        //list.Add("soLLDT_AcademyStars3unit6Lesson1|slldt-academystar-4-77-434|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit6Lesson2|slldt-academystar-4-77-435|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson3|slldt-academystar-4-77-436|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson4|slldt-academystar-4-77-437|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson5|slldt-academystar-4-77-438|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson6|slldt-academystar-4-77-439|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson7|slldt-academystar-4-77-440|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit6Lesson8|slldt-academystar-4-77-441|~/web_module/SLLDT/AcademyStar3/Unit06_Lesson08.aspx");
        //review 3
        list.Add("soLLDT_AcademyStars3review3|slldt-academystar-4-78-442|~/web_module/SLLDT/AcademyStar3/Review03.aspx");
        //Unit 7
        //list.Add("soLLDT_AcademyStars3unit7Lesson1|slldt-academystar-4-79-443|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson2|slldt-academystar-4-79-444|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson3|slldt-academystar-4-79-445|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson4|slldt-academystar-4-79-446|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson5|slldt-academystar-4-79-447|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson6|slldt-academystar-4-79-448|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson7|slldt-academystar-4-79-449|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit7Lesson8|slldt-academystar-4-79-450|~/web_module/SLLDT/AcademyStar3/Unit07_Lesson08.aspx");
        //Reading time 3
        list.Add("soLLDT_AcademyStart3ReadingTime3|slldt-academystar-4-103-480|~/web_module/SLLDT/AcademyStar3/ReadingTime03.aspx");
        //Unit 8
        //list.Add("soLLDT_AcademyStars3unit8Lesson1|slldt-academystar-4-80-451|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit8Lesson2|slldt-academystar-4-80-452|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson3|slldt-academystar-4-80-453|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson4|slldt-academystar-4-80-454|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson5|slldt-academystar-4-80-455|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson6|slldt-academystar-4-80-456|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson7|slldt-academystar-4-80-457|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit8Lesson8|slldt-academystar-4-80-458|~/web_module/SLLDT/AcademyStar3/Unit08_Lesson08.aspx");
        //review 4
        list.Add("soLLDT_AcademyStars3review4|slldt-academystar-4-81-459|~/web_module/SLLDT/AcademyStar3/Review04.aspx");
        //Unit 9
        //list.Add("soLLDT_AcademyStars3unit9Lesson1|slldt-academystar-4-82-460|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars3unit9Lesson2|slldt-academystar-4-82-461|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson3|slldt-academystar-4-82-462|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson4|slldt-academystar-4-82-463|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson5|slldt-academystar-4-82-464|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson6|slldt-academystar-4-82-465|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson7|slldt-academystar-4-82-466|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars3unit9Lesson8|slldt-academystar-4-82-467|~/web_module/SLLDT/AcademyStar3/Unit09_Lesson08.aspx");
        //Reading time 4
        list.Add("soLLDT_AcademyStart3ReadingTime4|slldt-academystar-4-104-481|~/web_module/SLLDT/AcademyStar3/ReadingTime04.aspx");
        //Unit 10
        //list.Add("soLLDT_AcademyStars3unit10Lesson1|slldt-academystar-4-83-468|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson2|slldt-academystar-4-83-469|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson3|slldt-academystar-4-83-470|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson4|slldt-academystar-4-83-471|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson5|slldt-academystar-4-83-472|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson6|slldt-academystar-4-83-473|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars3unit10Lesson7|slldt-academystar-4-83-474|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson07.aspx");
        //list.Add("soLLDT_AcademyStars3unit10Lesson8|slldt-academystar-4-83-475|~/web_module/SLLDT/AcademyStar3/Unit10_Lesson08.aspx");
        //review 5
        list.Add("soLLDT_AcademyStars3review5|slldt-academystar-4-84-476|~/web_module/SLLDT/AcademyStar3/Review05.aspx");
        //list.Add("soLLDT_AcademyStars2unit2Lesson6|slldt-academystar-3-{unit-id}-{lesson-id}|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");

        // academy 2
        // Welcome
        list.Add("soLLDT_AcademyStars2welcomeLesson1|slldt-academystar-3-85-303|~/web_module/SLLDT/AcademyStar2/Welcome_Lesson1.aspx");
        list.Add("soLLDT_AcademyStars2welcomeLesson2|slldt-academystar-3-85-304|~/web_module/SLLDT/AcademyStar2/Welcome_Lesson2.aspx");
        //Academy Star 2
        //Unit 1
        list.Add("soLLDT_AcademyStart2Unit1Lesson1|slldt-academystar-3-86-305|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson3|slldt-academystar-3-86-307|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson2|slldt-academystar-3-86-306|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson2.aspx");
        //list.Add("soLLDT_AcademyStart2Unit1Lesson4|slldt-academystar-3-86-308|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson5|slldt-academystar-3-86-309|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson6|slldt-academystar-3-86-310|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson7|slldt-academystar-3-86-311|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit1Lesson8|slldt-academystar-3-86-312|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson8.aspx");
        //Unit 2
        list.Add("soLLDT_AcademyStart2Unit2Lesson1|slldt-academystar-3-87-313|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit2Lesson2|slldt-academystar-3-87-314|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson2.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson3|slldt-academystar-3-87-315|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson3.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson4|slldt-academystar-3-87-316|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson4.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson5|slldt-academystar-3-87-317|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson5.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson6|slldt-academystar-3-87-318|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson7|slldt-academystar-3-87-319|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson7.aspx");
        list.Add("soLLDT_AcademyStars2unit2Lesson8|slldt-academystar-3-87-320|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson8.aspx");
        //Unit 3
        list.Add("soLLDT_AcademyStart2Unit3Lesson1|slldt-academystar-3-89-322|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson2|slldt-academystar-3-89-323|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson3|slldt-academystar-3-89-324|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson4|slldt-academystar-3-89-325|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson5|slldt-academystar-3-89-326|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson6|slldt-academystar-3-89-327|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson7|slldt-academystar-3-89-328|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit3Lesson8|slldt-academystar-3-89-329|~/web_module/SLLDT/AcademyStar2/Unit03_Lesson8.aspx");
        //Unit 4
        list.Add("soLLDT_AcademyStart2Unit4Lesson1|slldt-academystar-3-90-330|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson2|slldt-academystar-3-90-331|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson3|slldt-academystar-3-90-332|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson4|slldt-academystar-3-90-333|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson5|slldt-academystar-3-90-334|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson6|slldt-academystar-3-90-335|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson7|slldt-academystar-3-90-336|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit4Lesson8|slldt-academystar-3-90-337|~/web_module/SLLDT/AcademyStar2/Unit04_Lesson8.aspx");
        //Unit 5
        list.Add("soLLDT_AcademyStart2Unit5Lesson1|slldt-academystar-3-92-339|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson2|slldt-academystar-3-92-340|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson3|slldt-academystar-3-92-341|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson4|slldt-academystar-3-92-342|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson5|slldt-academystar-3-92-343|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson6|slldt-academystar-3-92-344|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson7|slldt-academystar-3-92-345|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit5Lesson8|slldt-academystar-3-92-346|~/web_module/SLLDT/AcademyStar2/Unit05_Lesson8.aspx");
        //Unit 6
        list.Add("soLLDT_AcademyStart2Unit6Lesson1|slldt-academystar-3-93-347|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson2|slldt-academystar-3-93-348|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson3|slldt-academystar-3-93-349|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson4|slldt-academystar-3-93-350|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson5|slldt-academystar-3-93-351|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson6|slldt-academystar-3-93-352|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson7|slldt-academystar-3-93-353|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit6Lesson8|slldt-academystar-3-93-354|~/web_module/SLLDT/AcademyStar2/Unit06_Lesson8.aspx");
        //Unit 7
        list.Add("soLLDT_AcademyStart2Unit7Lesson1|slldt-academystar-3-95-356|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson2|slldt-academystar-3-95-357|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson3|slldt-academystar-3-95-358|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson4|slldt-academystar-3-95-359|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson5|slldt-academystar-3-95-360|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson6|slldt-academystar-3-95-361|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson7|slldt-academystar-3-95-362|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit7Lesson8|slldt-academystar-3-95-363|~/web_module/SLLDT/AcademyStar2/Unit07_Lesson8.aspx");
        //Unit 8
        list.Add("soLLDT_AcademyStart2Unit8Lesson1|slldt-academystar-3-96-364|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson2|slldt-academystar-3-96-365|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson3|slldt-academystar-3-96-366|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson4|slldt-academystar-3-96-367|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson5|slldt-academystar-3-96-368|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson6|slldt-academystar-3-96-369|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson7|slldt-academystar-3-96-370|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit8Lesson8|slldt-academystar-3-96-371|~/web_module/SLLDT/AcademyStar2/Unit08_Lesson8.aspx");
        //Unit 9
        list.Add("soLLDT_AcademyStart2Unit9Lesson1|slldt-academystar-3-98-373|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson2|slldt-academystar-3-98-374|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson3|slldt-academystar-3-98-375|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson4|slldt-academystar-3-98-376|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson5|slldt-academystar-3-98-377|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson6|slldt-academystar-3-98-378|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson7|slldt-academystar-3-98-379|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit9Lesson8|slldt-academystar-3-98-380|~/web_module/SLLDT/AcademyStar2/Unit09_Lesson8.aspx");
        //Unit 10
        list.Add("soLLDT_AcademyStart2Unit10Lesson1|slldt-academystar-3-99-381|~/web_module/SLLDT/AcademyStar2/Unit01_Lesson1.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson2|slldt-academystar-3-99-382|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson2.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson3|slldt-academystar-3-99-383|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson3.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson4|slldt-academystar-3-99-384|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson4.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson5|slldt-academystar-3-99-385|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson5.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson6|slldt-academystar-3-99-386|~/web_module/SLLDT/AcademyStar2/Unit02_Lesson6.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson7|slldt-academystar-3-99-387|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson7.aspx");
        list.Add("soLLDT_AcademyStart2Unit10Lesson8|slldt-academystar-3-99-388|~/web_module/SLLDT/AcademyStar2/Unit10_Lesson8.aspx");
        // Review
        list.Add("soLLDT_AcademyStart2Review01|slldt-academystar-3-88-321|~/web_module/SLLDT/AcademyStar2/Review01.aspx");
        list.Add("soLLDT_AcademyStart2Review02|slldt-academystar-3-91-338|~/web_module/SLLDT/AcademyStar2/Review02.aspx");
        list.Add("soLLDT_AcademyStart2Review03|slldt-academystar-3-94-355|~/web_module/SLLDT/AcademyStar2/Review03.aspx");
        list.Add("soLLDT_AcademyStart2Review04|slldt-academystar-3-97-372|~/web_module/SLLDT/AcademyStar2/Review04.aspx");
        list.Add("soLLDT_AcademyStart2Review05|slldt-academystar-3-100-389|~/web_module/SLLDT/AcademyStar2/Review05.aspx");
        //reivew chơi trắc nghiệm
        list.Add("soLLDT_reivewtheounit|review-{sach-id}-{unit-id}|~/web_module/SLLDT/SLLDT_TracNghiemTheoUnit.aspx");
        list.Add("soLLDT_gamelathinh|game-lat-hinh-{sach-id}-{unit-id}|~/web_module/SLLDT/SLLDT_GameLatHinhTheoUnit.aspx");
        list.Add("soLLDT_DoodleTownNurseryUnit2Lesson6|slldt-doodletown-nursery-mutiple-choise-{sach-id}-{unit-id}-{lesson-id}|~/web_module/SLLDT/DoodleTownNersery/Unit2_Lesson6.aspx");
        //Reading time
        list.Add("soLLDT_AcademyStart2readingtime01|slldt-academystar-3-105-482|~/web_module/SLLDT/AcademyStar2/ReadingTime01.aspx");
        list.Add("soLLDT_AcademyStart2readingtime02|slldt-academystar-3-106-483|~/web_module/SLLDT/AcademyStar2/ReadingTime02.aspx");
        list.Add("soLLDT_AcademyStart2readingtime03|slldt-academystar-3-107-484|~/web_module/SLLDT/AcademyStar2/ReadingTime03.aspx");
        list.Add("soLLDT_AcademyStart2readingtime04|slldt-academystar-3-108-485|~/web_module/SLLDT/AcademyStar2/ReadingTime04.aspx");

       

        //Academy Star 4
        //Welcome
        list.Add("soLLDT_AcademyStars4WelcomeLesson1|slldt-academystar-5-109-486|~/web_module/SLLDT/AcademyStar4/Welcome_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars4WelcomeLesson2|slldt-academystar-5-109-487|~/web_module/SLLDT/AcademyStar4/Welcome_Lesson02.aspx");
        //Unit 1
        //list.Add("soLLDT_AcademyStars4unit1Lesson1|slldt-academystar-5-110-488|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson2|slldt-academystar-5-110-489|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson3|slldt-academystar-5-110-490|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson4|slldt-academystar-5-110-491|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson5|slldt-academystar-5-110-492|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson6|slldt-academystar-5-110-493|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson7|slldt-academystar-5-110-494|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit1Lesson8|slldt-academystar-5-110-495|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson08.aspx");
        //Reading time 1
        list.Add("soLLDT_AcademyStart4ReadingTime1|slldt-academystar-5-111-496|~/web_module/SLLDT/AcademyStar4/ReadingTime01.aspx");
        //Unit 2
        //list.Add("soLLDT_AcademyStars4unit2Lesson1|slldt-academystar-5-112-497|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson2|slldt-academystar-5-112-498|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson3|slldt-academystar-5-112-499|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson4|slldt-academystar-5-112-500|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson5|slldt-academystar-5-112-501|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson6|slldt-academystar-5-112-502|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson7|slldt-academystar-5-112-503|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit2Lesson8|slldt-academystar-5-112-504|~/web_module/SLLDT/AcademyStar4/Unit02_Lesson08.aspx");
        //Review 1
        list.Add("soLLDT_AcademyStars4review1|slldt-academystar-5-113-505|~/web_module/SLLDT/AcademyStar4/Review01.aspx");
        //Unit 3
        list.Add("soLLDT_AcademyStars4unit3Lesson1|slldt-academystar-5-114-506|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson2|slldt-academystar-5-114-507|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson3|slldt-academystar-5-114-508|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson4|slldt-academystar-5-114-509|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson5|slldt-academystar-5-114-510|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson6|slldt-academystar-5-114-511|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson7|slldt-academystar-5-114-512|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit3Lesson8|slldt-academystar-5-114-513|~/web_module/SLLDT/AcademyStar4/Unit03_Lesson08.aspx");
        //Reading time 2
        list.Add("soLLDT_AcademyStart4ReadingTime2|slldt-academystar-5-115-514|~/web_module/SLLDT/AcademyStar4/ReadingTime02.aspx");
        //Unit 4
        list.Add("soLLDT_AcademyStars4unit4Lesson1|slldt-academystar-5-116-515|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson01.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson2|slldt-academystar-5-116-516|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson3|slldt-academystar-5-116-517|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson4|slldt-academystar-5-116-518|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson5|slldt-academystar-5-116-519|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson6|slldt-academystar-5-116-520|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson7|slldt-academystar-5-116-521|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit4Lesson8|slldt-academystar-5-116-522|~/web_module/SLLDT/AcademyStar4/Unit04_Lesson08.aspx");
        //Review 2
        list.Add("soLLDT_AcademyStars4review2|slldt-academystar-5-117-523|~/web_module/SLLDT/AcademyStar4/Review02.aspx");
        //Unit 5
        list.Add("soLLDT_AcademyStars4unit5Lesson1|slldt-academystar-5-118-524|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit5Lesson2|slldt-academystar-5-118-525|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson3|slldt-academystar-5-118-526|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson4|slldt-academystar-5-118-527|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson5|slldt-academystar-5-118-528|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson6|slldt-academystar-5-118-529|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson7|slldt-academystar-5-118-530|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit5Lesson8|slldt-academystar-5-118-531|~/web_module/SLLDT/AcademyStar4/Unit05_Lesson08.aspx");
        //Unit 6
        list.Add("soLLDT_AcademyStars4unit6Lesson1|slldt-academystar-5-119-532|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit6Lesson2|slldt-academystar-5-119-533|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson3|slldt-academystar-5-119-534|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson4|slldt-academystar-5-119-535|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson5|slldt-academystar-5-119-536|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson6|slldt-academystar-5-119-537|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson7|slldt-academystar-5-119-538|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit6Lesson8|slldt-academystar-5-119-539|~/web_module/SLLDT/AcademyStar4/Unit06_Lesson08.aspx");
        //Review 3
        list.Add("soLLDT_AcademyStars4review3|slldt-academystar-5-120-540|~/web_module/SLLDT/AcademyStar4/Review03.aspx");
        //Unit 7
        list.Add("soLLDT_AcademyStars4unit7Lesson1|slldt-academystar-5-121-541|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit7Lesson2|slldt-academystar-5-121-542|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson3|slldt-academystar-5-121-543|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson4|slldt-academystar-5-121-544|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson5|slldt-academystar-5-121-545|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson6|slldt-academystar-5-121-546|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson7|slldt-academystar-5-121-547|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit7Lesson8|slldt-academystar-5-121-548|~/web_module/SLLDT/AcademyStar4/Unit07_Lesson08.aspx");
        //Reading time 3
        list.Add("soLLDT_AcademyStart4ReadingTime3|slldt-academystar-5-122-549|~/web_module/SLLDT/AcademyStar4/ReadingTime03.aspx");
        //Unit 8
        list.Add("soLLDT_AcademyStars4unit8Lesson1|slldt-academystar-5-123-550|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit8Lesson2|slldt-academystar-5-123-551|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson3|slldt-academystar-5-123-552|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson4|slldt-academystar-5-123-553|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson5|slldt-academystar-5-123-554|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson6|slldt-academystar-5-123-555|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson7|slldt-academystar-5-123-556|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit8Lesson8|slldt-academystar-5-123-557|~/web_module/SLLDT/AcademyStar4/Unit08_Lesson08.aspx");
        //Review 4
        list.Add("soLLDT_AcademyStars4review4|slldt-academystar-5-124-558|~/web_module/SLLDT/AcademyStar4/Review04.aspx");
        //Unit 9
        list.Add("soLLDT_AcademyStars4unit9Lesson1|slldt-academystar-5-125-559|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit9Lesson2|slldt-academystar-5-125-560|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson3|slldt-academystar-5-125-561|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson4|slldt-academystar-5-125-562|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson5|slldt-academystar-5-125-563|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson6|slldt-academystar-5-125-564|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson7|slldt-academystar-5-125-565|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit9Lesson8|slldt-academystar-5-125-566|~/web_module/SLLDT/AcademyStar4/Unit09_Lesson08.aspx");
        //Reading time 4
        list.Add("soLLDT_AcademyStart4ReadingTime4|slldt-academystar-5-126-567|~/web_module/SLLDT/AcademyStar4/ReadingTime04.aspx");
        //Unit 10
        list.Add("soLLDT_AcademyStars4unit10Lesson1|slldt-academystar-5-127-568|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson01.aspx");
        //list.Add("soLLDT_AcademyStars4unit10Lesson2|slldt-academystar-5-127-569|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson02.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson3|slldt-academystar-5-127-571|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson03.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson4|slldt-academystar-5-127-572|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson04.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson5|slldt-academystar-5-127-573|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson6|slldt-academystar-5-127-574|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson06.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson7|slldt-academystar-5-127-575|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson07.aspx");
        list.Add("soLLDT_AcademyStars4unit10Lesson8|slldt-academystar-5-127-576|~/web_module/SLLDT/AcademyStar4/Unit10_Lesson08.aspx");
        //Review 5
        list.Add("soLLDT_AcademyStars4review5|slldt-academystar-5-128-578|~/web_module/SLLDT/AcademyStar4/Review05.aspx");

        // sách doodletown 3
        list.Add("soLLDT_DoodleTown3Unit1Lesson56|slldt-doodletown3-11-129-579|~/web_module/SLLDT/DoodleTown3/Unit01_Lesson5_6.aspx");
        list.Add("soLLDT_DoodleTown3Unit1Lesson78|slldt-doodletown3-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/DoodleTown3/Unit01_Lesson7_8.aspx");

        //Academy Star 4 chung
        list.Add("soLLDT_AcademyStars4unit1Lesson1|slldt-academystar-notebook-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/AcademyStar4/Unit01_Lesson01.aspx");
        //Academy Star 3 chung
        list.Add("soLLDT_AcademyStars3unit1Lesson2|slldt-academystar-paragraph-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson02.aspx");
        //list.Add("soLLDT_AcademyStars3unit2Lesson5|slldt-academystar-language-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/AcademyStar3/Unit02_Lesson05.aspx");
        list.Add("soLLDT_AcademyStars3unit1Lesson1|slldt-academystar-{sach-id}-{unit-id}-{lesson_id}|~/web_module/SLLDT/AcademyStar3/Unit01_Lesson01.aspx");
        
        return list;
    }
}