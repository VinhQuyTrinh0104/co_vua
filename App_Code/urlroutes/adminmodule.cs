﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for adminmodule
/// </summary>
public class adminmodule
{
	public adminmodule()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public List<string> UrlRoutes()
    {
        List<string> list = new List<string>();
        //Module SEO
        list.Add("moduleseo|admin-seo|~/admin_page/module_function/module_SEO.aspx");
        //Module Language
        list.Add("modulelanguage|admin-ngon-ngu|~/admin_page/module_function/admin_LanguagePage.aspx");
        //I Quản lý website
        //1 Poster - slide
        list.Add("moduleposter|admin-poster|~/admin_page/module_function/module_Poster.aspx");
        list.Add("modulethongkesolieu|admin-block-so-lieu-thong-ke|~/admin_page/module_function/module_BlockThongkeSoLieu.aspx");
        list.Add("moduleslide|admin-slide|~/admin_page/module_function/module_Slide.aspx");
        list.Add("moduletieudetrangchu|admin-tieu-de-website-vjlc|~/admin_page/module_function/module_TieuDe_TrenTrangChu.aspx");

        // 2. Giới thiệu khóa học
        list.Add("modulegioithieu|admin-gioi-thieu-vjlc|~/admin_page/module_function/module_Introduce.aspx");
        list.Add("moduleintro|admin-gioi-thieu-khoa-hoc|~/admin_page/module_function/module_Introduce_KhoaHoc.aspx");
        list.Add("moduleintrogiaoiven|admin-gioi-thieu-giao-vien-{id}|~/admin_page/module_function/module_GiaoVien_GioiThieu.aspx");
        //4 Module tin tức
        list.Add("modulenewscate|admin-loai-tin-tuc|~/admin_page/module_function/module_NewsCate.aspx");
        list.Add("modulenews|admin-tin-tuc|~/admin_page/module_function/module_News.aspx");
        list.Add("modulelienhe|admin-lien-he|~/admin_page/module_function/module_LienHe.aspx");
        //5 Module tuyển dụng
        list.Add("moduletuyendung|admin-tuyen-dung|~/admin_page/module_function/module_TuyenDung.aspx");
        // 6 block album ảnh
        list.Add("modulealbumcate|admin-loai-album|~/admin_page/module_function/module_AlbumCate.aspx");
        list.Add("modulealbumimage|admin-album-image|~/admin_page/module_function/module_Album_Image.aspx");
        list.Add("modulequangcao|admin-quang-cao|~/admin_page/module_function/module_QuangCao.aspx");
        list.Add("modulecontact|admin-thong-bao|~/admin_page/module_function/module_ThongBao.aspx");
        list.Add("modulecontactxem|admin-xem-thong-bao-{id}|~/admin_page/module_function/module_ThongBao_Xem.aspx");
        list.Add("modulenghihan|admin-nghi-han|~/admin_page/module_function/module_NghiHan.aspx");
        list.Add("modulekhachhang|admin-khoa-hoc-tieu-bieu|~/admin_page/module_function/module_KhachHang.aspx");
        list.Add("modulekhoahoc|admin-khoa-hoc|~/admin_page/module_function/module_KhoaHoc.aspx");
        list.Add("moduleaccount|admin-accounths|~/admin_page/module_function/module_Account.aspx");

        // module đào tạo
        list.Add("modulegroupctdt|admin-nhom-chuong-trinh-dao-tao|~/admin_page/module_function/module_LoaiKhoaHoc.aspx");
        list.Add("modulcatectdt|admin-loai-chuong-trinh-dao-tao|~/admin_page/module_function/module_KhoaHoc.aspx");
        list.Add("modulctdt|admin-chuong-trinh-dao-tao|~/admin_page/module_function/module_Program.aspx");
        list.Add("modulcatecbt|admin-bai-tap|~/admin_page/module_function/module_BaiTap.aspx");
        list.Add("modulcatebktct|admin-bai-kiem-tra-chi-tiet|~/admin_page/module_function/module_BaiKiemTraChiTiet.aspx");
        list.Add("modulcatebtkths|admin-bai-tap-kiem-tra-hoc-sinh|~/admin_page/module_function/module_BaiTapKiemTraHocSinh.aspx");
        list.Add("modulcatebttk|admin-bai-tap-tai-khoan|~/admin_page/module_function/module_BaiTapTaiKhoan.aspx");
        //Quản Lý Lớp
        list.Add("modulcatelop|admin-lop|~/admin_page/module_function/module_Lop.aspx");
        list.Add("modullopnghihan|admin-lop-nghi-han|~/admin_page/module_function/module_Lop_NghiHan.aspx");
        list.Add("modulegiaovien|admin-giao-vien|~/admin_page/module_function/module_GiaoVien.aspx");
        list.Add("modulcatemh|admin-mon-hoc|~/admin_page/module_function/module_MonHoc.aspx");
        list.Add("modulcatehs|admin-hoc-sinh|~/admin_page/module_function/module_Account.aspx");
        list.Add("modulcatehstl|admin-hoc-sinh-trong-lop|~/admin_page/module_function/module_Hocsinhtronglop.aspx");
        list.Add("modulcatethstl|admin-tao-hoc-sinh-trong-lop|~/admin_page/module_function/module_TaoHocsinhtronglop.aspx");
        list.Add("modulcateph|admin-phu-huynh|~/admin_page/module_function/module_PhuHuynh.aspx");
        list.Add("modulgvtl|admin-tao-giao-vien-trong-lop|~/admin_page/module_function/module_GiaoVienTrongLop.aspx");
        list.Add("modullstt|admin-lich-su-thao-tac|~/admin_page/module_function/module_LichSuThaoTac.aspx");
        // Lịch khóa học
        list.Add("modulelichkhoahoc|admin-lich-khoa-hoc|~/admin_page/module_function/module_LichKhoaHoc.aspx");
        list.Add("moduledangkykhoahoc|admin-dang-ky-khoa-hoc|~/admin_page/module_function/module_DanhSachDangKy.aspx");
        // Khảo thí
        list.Add("modulekhaothi|admin-khao-thi|~/admin_page/module_function/module_KhaoThi.aspx");
        // học trực tuyến
        list.Add("modulehoctructuyen|admin-hoc-truc-tuyen|~/admin_page/module_function/module_HocTrucTuyen.aspx");
        
        // Thư viện
        list.Add("modulelibrarycate|admin-loai-thu-vien|~/admin_page/module_function/module_LoaiThongBao.aspx");
        list.Add("moduleaddress|admin-dia-chi|~/admin_page/module_function/module_DiaChi.aspx");
        //sổ liên lạc
        list.Add("modulesll|admin-so-lien-lac-dien-tu|~/admin_page/module_function/module_SoLienLac.aspx");
        list.Add("moduleslls|admin-history-so-lien-lac-dien-tu|~/admin_page/module_function/module_HistorySLL.aspx");
        //Đánh giá
    
        //Phản hồi phụ huynh
        list.Add("modulephph|admin-phan-hoi-phu-huynh|~/admin_page/module_function/module_PhanHoiPhuHuynh.aspx");
        //Qui trình đóng học phí
        list.Add("moduleQuitrinhdonghocphi|admin-qui-trinh-dong-hoc-phi|~/admin_page/module_function/module_QuiTrinhDongHocPhi.aspx");
        list.Add("moduledanhmuchocphi|admin-danh-muc-bao-luu|~/admin_page/module_function/module_BaoLuu_DanhSach.aspx");
        list.Add("modulehocphi|admin-dong-hoc-phi|~/admin_page/module_function/module_DongHocPhi.aspx");
        list.Add("moduledangkyhocphi|dang-ky-hoc-phi-{id}|~/admin_page/module_function/module_TaoHocsinhtronglop.aspx");
        list.Add("moduleddsbaoluu|admin-ds-bao-luu|~/admin_page/module_function/module_BaoLuu_DanhSach.aspx");
        list.Add("moduledbaoluu|admin-bao-luu-{id}|~/admin_page/module_function/module_BaoLuu.aspx");
        list.Add("moduledchuyenlop|admin-chuyen-lop|~/admin_page/module_function/module_ChuyenLop.aspx");
        list.Add("moduleddiemdanhthongke|admin-thong-ke-diem-danh|~/admin_page/module_function/module_DiemDanh_DanhSach.aspx");
        list.Add("moduleddiemdanh|admin-diem-danh|~/admin_page/module_function/module_DiemDanh.aspx");
        list.Add("moduleddiemdanhlai|admin-diem-danh-lai|~/admin_page/module_function/module_DiemDanh_Lai.aspx");
        list.Add("moduleddiemdanhgiaovien|admin-giao-vien-diem-danh|~/admin_page/module_function/module_DiemDanh_GiaoVien.aspx");
        list.Add("moduleddiemdanhlichsu|admin-lich-su-diem-danh|~/admin_page/module_function/module_DiemDanh_LichSu.aspx");
        list.Add("moduleddiemdanhlichsuhangngay|admin-lich-su-diem-danh-hang-ngay|~/admin_page/module_function/module_DiemDanh_LichSu_HangNgay.aspx");
        list.Add("moduledanhgia|admin-danh-gia|~/admin_page/module_function/module_DanhGia.aspx");
        list.Add("moduledthongkedanhgia|admin-thong-ke-danh-gia|~/admin_page/module_function/module_DanhGia_ThongKe.aspx");
        list.Add("moduledthongkedstong|admin-ds-tong|~/admin_page/module_function/module_DanhSach_Tong.aspx");
        list.Add("moduledcoso|admin-co-so|~/admin_page/module_function/module_CoSo.aspx");
        list.Add("modulethongkehocphithang|admin-hoc-phi-thang|~/admin_page/module_function/module_HocPhi_Thang.aspx");
        
        list.Add("moduledanhsachhocsinhchitietnghihan|admin-xem-chi-tiet-hoc-sinh-nghi-han-{id}|~/admin_page/module_function/module_DanhSach_HocSinh_ThongKeChiTietNghiHan.aspx");
        list.Add("moduledanhsachhocsinhchitiet|admin-xem-chi-tiet-hoc-sinh-{id}|~/admin_page/module_function/module_DanhSach_HocSinh_ThongKeChiTiet.aspx");
        // Module Sổ liên lạc điện tử
        list.Add("moduleslldtthongbaotruong|admin-slldt-thong-bao-trung-tam|~/admin_page/module_function/SLLDT/module_SLLDT_ThongBaoTrungTam.aspx");
        list.Add("moduleslldtslide|admin-slldt-slide|~/admin_page/module_function/SLLDT/module_SLLDT_Slide.aspx");
        list.Add("moduleslldtchuongtrinhhoc|admin-slldt-chuong-trinh-hoc|~/admin_page/module_function/SLLDT/module_SLLDT_ChuongTrinhHoc.aspx");
        list.Add("moduleslldtbehocgihomnayduyet|admin-duyet-be-hoc-gi-hom-nay|~/admin_page/module_function/SLLDT/module_SLLDT_BeHocGiHomNay_main_Duyet.aspx");
        list.Add("moduleslldtbehocgihomnayxemduyet|admin-xem-duyet-be-hoc-gi-hom-nay-{id}|~/admin_page/module_function/SLLDT/module_SLLDT_BeHocGiHomNay_Duyet.aspx");
        list.Add("moduleslldtbehocgihomnayid|admin-be-hoc-gi-hom-nay-{id}|~/admin_page/module_function/SLLDT/module_SLLDT_BeHocGiHomNay.aspx");
        list.Add("moduleslldtbehocgihomnay|admin-be-hoc-gi-hom-nay|~/admin_page/module_function/SLLDT/module_SLLDT_BeHocGiHomNay_main.aspx");
        list.Add("moduleslldtnoidungbaihoc|admin-noi-dung-bai-hoc|~/admin_page/module_function/SLLDT/module_SLLDT_NoiDungBaiHoc.aspx");
        list.Add("moduleslldtnoidungbaihocduyet|admin-duyet-noi-dung-bai-hoc|~/admin_page/module_function/SLLDT/module_SLLDT_NoiDungBaiHoc_Duyet.aspx");
        list.Add("moduleslldtnoidungbaihocduyetlichsu|admin-lich-su-duyet-noi-dung-bai-hoc|~/admin_page/module_function/SLLDT/module_SLLDT_NoiDungBaiHoc_Da_Duyet.aspx");

        // Module điểm 
        list.Add("modulenhapdiemtienganh|admin-nhap-diem-tieng-anh|~/admin_page/module_function/SLLDT/module_SLLDT_NhapDiem.aspx");


        return list;
    }
}