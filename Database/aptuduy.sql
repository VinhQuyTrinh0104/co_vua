﻿-- tạo csdl App_TuDuy
IF EXISTS ( SELECT  name FROM sys.databases WHERE   name = 'App_TuDuy' )
    DROP DATABASE App_TuDuy
GO
CREATE DATABASE App_TuDuy
GO
USE App_TuDuy
GO
-- tạo bảng tbUser
CREATE TABLE tbAccount
(
account_id	INT IDENTITY primary key,
account_code nVARCHAR(max),
pass nVARCHAR(max),
account_vn	NVARCHAR(MAX),
account_en	NVARCHAR(MAX),
account_phuhuynh		NVARCHAR(MAX),
account_phone				NVARCHAR(MAX),
account_email					NVARCHAR(MAX),
hidden				BIT
)

--tạo bảng tbintroduce_web
CREATE TABLE tbIntroduce
(
introduct_id int identity primary key,
introduce_title		 NVARCHAR(MAX),
introduce_summary nvarchar(max),
introduce_content	 NVARCHAR(MAX),
)
--tạo bảng tbLoaiKhoaHoc	
CREATE TABLE tbLoaiKhoaHoc
(
loaikhoahoc_id INT IDENTITY primary key,
loaikhoahoc_name NVARCHAR(MAX),
hidden			BIT
)
-- tạo bảng tbThongTinKhoaHoc
CREATE TABLE tbKhoaHoc
(
khoahoc_id INT IDENTITY primary key,
khoahoc_name		NVARCHAR(MAX),
khoahoc_summary nvarchar(max),
loaikhoahoc_id			INT,
hidden				BIT
)
--tạo bảng tbSlide
CREATE TABLE tbSlide
(
slide_id INT IDENTITY primary key,
slide_image			NVARCHAR(MAX),
slide_title				NVARCHAR(MAX),
slide_link				NVARCHAR(MAX),
slide_summary		NVARCHAR(MAX),
slide_content			NVARCHAR(MAX),
hidden				BIT
)
--tạo bảng tbImage

--tạo bảng tbIntriduce_KhoaHoc
CREATE TABLE tbIntroduce_KhoaHoc
(
introkh_id					INT IDENTITY primary key,
introkh_title				NVARCHAR(MAX),
introkh_summary			NVARCHAR(MAX),
introkh_content			NVARCHAR(MAX),
introkh_linkyoutube	NVARCHAR(MAX),
hidden						BIT
)
-- tạo bảng tbLienHe
CREATE TABLE tbLienHe
(
lienhe_id int identity primary key,
lienhe_title nvarchar(max),
lienhe_summary nvarchar(max),
lienhe_email		NVARCHAR(MAX),
lienhe_phone		NVARCHAR(MAX),
lienhe_address		NVARCHAR(MAX)
)
-- tạo bảng tbNewcate
CREATE TABLE tbNewCate
(
newcate_id	INT IDENTITY primary key,
newcate_title				NVARCHAR(MAX),
newcate_summary	NVARCHAR(MAX),
hidden bit
)
--tạo bảng tbNews
CREATE TABLE tbNews
(
news_id					INT IDENTITY primary key,
news_title				NVARCHAR(MAX),
news_summary		NVARCHAR(MAX),
news_image	NVARCHAR(MAX),
news_content	NVARCHAR(MAX),
newcate_id			INT,
hidden				BIT,

)