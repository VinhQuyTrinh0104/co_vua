USE [App_TuDuy]
GO
/****** Object:  Table [dbo].[admin_AccessGroupUserForm]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserForm](
	[guf_id] [int] IDENTITY(1,1) NOT NULL,
	[guf_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[guf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessGroupUserModule]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserModule](
	[gum_id] [int] IDENTITY(1,1) NOT NULL,
	[gum_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[gum_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessUserForm]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessUserForm](
	[uf_id] [int] IDENTITY(1,1) NOT NULL,
	[uf_active] [bit] NULL,
	[username_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[uf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Form]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Form](
	[form_id] [int] IDENTITY(1,1) NOT NULL,
	[form_position] [int] NULL,
	[form_name] [nvarchar](max) NULL,
	[form_link] [nvarchar](max) NULL,
	[form_active] [bit] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[form_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_GroupUser]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_GroupUser](
	[groupuser_id] [int] IDENTITY(1,1) NOT NULL,
	[groupuser_name] [nvarchar](max) NULL,
	[groupuser_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[groupuser_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Module]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Module](
	[module_id] [int] IDENTITY(1,1) NOT NULL,
	[module_position] [int] NULL,
	[module_name] [nvarchar](max) NULL,
	[module_icon] [nvarchar](max) NULL,
	[module_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[module_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_User]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_User](
	[username_id] [int] IDENTITY(1,1) NOT NULL,
	[username_username] [nvarchar](max) NULL,
	[username_password] [nvarchar](max) NULL,
	[username_fullname] [nvarchar](max) NULL,
	[username_gender] [bit] NULL,
	[username_phone] [nvarchar](max) NULL,
	[username_email] [nvarchar](max) NULL,
	[username_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[bophan_id] [int] NULL,
	[username_truongbophan] [bit] NULL,
	[bophan_thongkebaocao_nhom] [int] NULL,
	[coso_id] [int] NULL,
	[username_diachi] [nvarchar](max) NULL,
	[chucvu_name] [nvarchar](max) NULL,
	[username_coso] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[username_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_BaiKiemTraChiTiet]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_BaiKiemTraChiTiet](
	[bktct_id] [int] IDENTITY(1,1) NOT NULL,
	[bkt_sohang1] [nvarchar](max) NULL,
	[bkt_sohang2] [nvarchar](max) NULL,
	[bkt_sohang3] [nvarchar](max) NULL,
	[bkt_ketqua] [nvarchar](max) NULL,
	[bkt_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[bktct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_BaiTapKiemTraHocSinh]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_BaiTapKiemTraHocSinh](
	[bkt_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[bkt_name] [nvarchar](max) NULL,
	[bkt_loaibai] [nvarchar](max) NULL,
	[bkt_thoigianlambai] [nvarchar](max) NULL,
	[bkt_tongdiem] [nvarchar](max) NULL,
	[bkt_startdate] [datetime] NULL,
	[Star] [int] NULL,
	[hstl_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[bkt_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAccount]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAccount](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[account_code] [nvarchar](max) NULL,
	[pass] [nvarchar](max) NULL,
	[account_vn] [nvarchar](max) NULL,
	[account_en] [nvarchar](max) NULL,
	[account_phuhuynh] [nvarchar](max) NULL,
	[account_phone] [nvarchar](max) NULL,
	[account_email] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[accout_codetutang] [nvarchar](max) NULL,
	[account_acctive] [bit] NULL,
	[account_namsinh] [datetime] NULL,
	[username_id] [int] NULL,
	[username_username] [nvarchar](max) NULL,
	[account_coso] [nvarchar](max) NULL,
	[account_buoihoc] [int] NULL,
	[account_ngayduyet] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbBaiTap]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBaiTap](
	[baitap_id] [int] IDENTITY(1,1) NOT NULL,
	[baitap_name] [nvarchar](max) NULL,
	[baitap_image] [nvarchar](max) NULL,
	[link_seo] [nvarchar](max) NULL,
	[khoahoc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[baitap_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbBaiTapTaiKhoan]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBaiTapTaiKhoan](
	[baitaptaikhoan_id] [int] IDENTITY(1,1) NOT NULL,
	[baitap_id] [int] NULL,
	[account_id] [int] NULL,
	[link_seo] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[baitap_name] [nvarchar](max) NULL,
	[parent] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[baitaptaikhoan_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCoso]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCoso](
	[coso_id] [int] IDENTITY(1,1) NOT NULL,
	[coso_name] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[coso_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbDayOfWeek]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDayOfWeek](
	[day_id] [int] IDENTITY(1,1) NOT NULL,
	[day_name] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[day_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbDiaChi]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDiaChi](
	[diachi_id] [int] IDENTITY(1,1) NOT NULL,
	[diachi_title] [nvarchar](max) NULL,
	[diachi_summary] [nvarchar](max) NULL,
	[diachi_phone] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[diachi_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbHocPhi]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbHocPhi](
	[hp_id] [int] IDENTITY(1,1) NOT NULL,
	[hp_ghichu] [nvarchar](max) NULL,
	[account_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[hp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbhocsinhtronglop]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbhocsinhtronglop](
	[hstl_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[lop_id] [int] NULL,
	[monhoc_id] [int] NULL,
	[hstl_hidden] [bit] NULL,
	[hstl_ngaybatdauhoc] [datetime] NULL,
	[hstl_ngayketthuchoc] [datetime] NULL,
	[hstl_thoigianhoc] [nvarchar](max) NULL,
	[hstl_baoluu] [bit] NULL,
	[hstl_ghichu] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[hstl_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbIntroduce]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbIntroduce](
	[introduct_id] [int] IDENTITY(1,1) NOT NULL,
	[introduce_title] [nvarchar](max) NULL,
	[introduce_summary] [nvarchar](max) NULL,
	[introduce_content] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[introduct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbIntroduce_KhoaHoc]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbIntroduce_KhoaHoc](
	[introkh_id] [int] IDENTITY(1,1) NOT NULL,
	[introkh_title] [nvarchar](max) NULL,
	[introkh_summary] [nvarchar](max) NULL,
	[introkh_content] [nvarchar](max) NULL,
	[introkh_linkyoutube] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[introkh_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbKhoaHoc]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbKhoaHoc](
	[khoahoc_id] [int] IDENTITY(1,1) NOT NULL,
	[khoahoc_name] [nvarchar](max) NULL,
	[khoahoc_summary] [nvarchar](max) NULL,
	[loaikhoahoc_id] [int] NULL,
	[hidden] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
	[khoahoc_position] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[khoahoc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbLienHe]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbLienHe](
	[lienhe_id] [int] IDENTITY(1,1) NOT NULL,
	[lienhe_title] [nvarchar](max) NULL,
	[lienhe_summary] [nvarchar](max) NULL,
	[lienhe_email] [nvarchar](max) NULL,
	[lienhe_phone] [nvarchar](max) NULL,
	[lienhe_address] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[lienhe_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbLoaiKhoaHoc]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbLoaiKhoaHoc](
	[loaikhoahoc_id] [int] IDENTITY(1,1) NOT NULL,
	[loaikhoahoc_name] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[loaikhoahoc_position] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[loaikhoahoc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbLoaiThongBao]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbLoaiThongBao](
	[loaithongbao_id] [int] IDENTITY(1,1) NOT NULL,
	[loaithongbao_title] [nvarchar](max) NULL,
	[groupuser_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[loaithongbao_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbLop]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbLop](
	[lop_id] [int] IDENTITY(1,1) NOT NULL,
	[lop_name] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[username_idVn] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[lop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbMonHoc]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbMonHoc](
	[monhoc_id] [int] IDENTITY(1,1) NOT NULL,
	[monhoc_name] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[monhoc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNewCate]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNewCate](
	[newcate_id] [int] IDENTITY(1,1) NOT NULL,
	[newcate_title] [nvarchar](max) NULL,
	[newcate_summary] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
	[newcate_parent] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[newcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNews]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNews](
	[news_id] [int] IDENTITY(1,1) NOT NULL,
	[news_title] [nvarchar](max) NULL,
	[news_summary] [nvarchar](max) NULL,
	[news_image] [nvarchar](max) NULL,
	[news_content] [nvarchar](max) NULL,
	[newcate_id] [int] NULL,
	[hidden] [bit] NULL,
	[active] [bit] NULL,
	[news_datetime] [datetime] NULL,
	[news_position] [int] NULL,
	[link_seo] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[news_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbOperationHistory]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbOperationHistory](
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_day] [datetime] NULL,
	[username_id] [int] NULL,
	[history_content] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbPhanHoiPhuHuynh]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPhanHoiPhuHuynh](
	[phph_id] [int] IDENTITY(1,1) NOT NULL,
	[phph_noidung] [nvarchar](max) NULL,
	[phph_datePh] [datetime] NULL,
	[account_id] [int] NULL,
	[phph_tinhtrang] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[phph_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbPhuHuynh]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPhuHuynh](
	[phuhunh_id] [int] IDENTITY(1,1) NOT NULL,
	[phuhuynh_name] [nvarchar](max) NULL,
	[phuhuynh_image] [nvarchar](max) NULL,
	[phuhuynh_summary] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[phuhunh_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSlide]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSlide](
	[slide_id] [int] IDENTITY(1,1) NOT NULL,
	[slide_image] [nvarchar](max) NULL,
	[slide_title] [nvarchar](max) NULL,
	[slide_link] [nvarchar](max) NULL,
	[slide_summary] [nvarchar](max) NULL,
	[slide_content] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[slide_title1] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[slide_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSoLienLac]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSoLienLac](
	[sll_id] [int] IDENTITY(1,1) NOT NULL,
	[sll_danhgiagv] [nvarchar](max) NULL,
	[sll_hidden] [bit] NULL,
	[account_id] [int] NULL,
	[sll_date] [datetime] NULL,
	[sll_trangthai] [bit] NULL,
	[sll_nangluchs] [nvarchar](max) NULL,
	[monhoc_id] [int] NULL,
	[sll_kqcuoiky] [nvarchar](max) NULL,
	[sll_kqgiuaky] [nvarchar](max) NULL,
	[account_vn] [nvarchar](max) NULL,
	[username_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sll_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSoLienLacHistory]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSoLienLacHistory](
	[htr_id] [int] IDENTITY(1,1) NOT NULL,
	[htr_date] [datetime] NULL,
	[htr_danhgiagv] [nvarchar](max) NULL,
	[username_id] [int] NULL,
	[account_id] [int] NULL,
	[monhoc_id] [int] NULL,
	[htr_trangthai] [bit] NULL,
	[htr_kqgiuaky] [nvarchar](max) NULL,
	[htr_kqcuoiky] [nvarchar](max) NULL,
	[account_vn] [nvarchar](max) NULL,
	[htr_hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[htr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbThongBao]    Script Date: 12/7/2022 9:23:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbThongBao](
	[thongbao_id] [int] IDENTITY(1,1) NOT NULL,
	[thongbao_title] [nvarchar](max) NULL,
	[thongbao_content] [nvarchar](max) NULL,
	[loaithongbao_id] [int] NULL,
	[thongbao_createdate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[thongbao_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] ON 

INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (4, 1, 1, 5)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (5, 1, 1, 6)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] ON 

INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (2, 1, 1, 5)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] ON 

INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (6, 1, 1, 6)
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_Form] ON 

INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (1, 1, N'Quản lý phân quyền', N'admin-access', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (2, 2, N'Quản lý Module', N'admin-module', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (3, 3, N'Quản lý Form', N'admin-form', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (4, 1, N'Quản lý tài khoản', N'admin-account', 1, 2)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (5, NULL, N'slide', N'admin-slide', 1, 5)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (6, NULL, N'Giới thiệu khóa học', N'admin-introduced', 1, 5)
SET IDENTITY_INSERT [dbo].[admin_Form] OFF
SET IDENTITY_INSERT [dbo].[admin_GroupUser] ON 

INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (1, N'ROOT', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (2, N'admin', 1)
SET IDENTITY_INSERT [dbo].[admin_GroupUser] OFF
SET IDENTITY_INSERT [dbo].[admin_Module] ON 

INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (1, 1, N'Phân quyền', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (2, 2, N'Tài khoản', NULL, 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (5, NULL, N'website', N'', 1)
SET IDENTITY_INSERT [dbo].[admin_Module] OFF
SET IDENTITY_INSERT [dbo].[admin_User] ON 

INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id], [bophan_id], [username_truongbophan], [bophan_thongkebaocao_nhom], [coso_id], [username_diachi], [chucvu_name], [username_coso]) VALUES (1, N'root', N'12378248145104161527610811213823414203124130', N'root', 1, N'0818795939', N'ducpn@vjis.edu.vn', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id], [bophan_id], [username_truongbophan], [bophan_thongkebaocao_nhom], [coso_id], [username_diachi], [chucvu_name], [username_coso]) VALUES (2, N'ducpn', N'12378248145104161527610811213823414203124130', N'ducpn', 1, N'0818795939', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[admin_User] OFF
SET IDENTITY_INSERT [dbo].[tbAccount] ON 

INSERT [dbo].[tbAccount] ([account_id], [account_code], [pass], [account_vn], [account_en], [account_phuhuynh], [account_phone], [account_email], [hidden], [accout_codetutang], [account_acctive], [account_namsinh], [username_id], [username_username], [account_coso], [account_buoihoc], [account_ngayduyet]) VALUES (1, N'HS00001', N'12345', N'HS00001', N'HS00001', NULL, NULL, NULL, 0, N'true', NULL, NULL, 1, NULL, N'1', NULL, NULL)
INSERT [dbo].[tbAccount] ([account_id], [account_code], [pass], [account_vn], [account_en], [account_phuhuynh], [account_phone], [account_email], [hidden], [accout_codetutang], [account_acctive], [account_namsinh], [username_id], [username_username], [account_coso], [account_buoihoc], [account_ngayduyet]) VALUES (2, N'HS00002', NULL, N'HS00002', N'HS00002', NULL, NULL, NULL, 0, N'true', NULL, NULL, 1, NULL, N'1', NULL, NULL)
INSERT [dbo].[tbAccount] ([account_id], [account_code], [pass], [account_vn], [account_en], [account_phuhuynh], [account_phone], [account_email], [hidden], [accout_codetutang], [account_acctive], [account_namsinh], [username_id], [username_username], [account_coso], [account_buoihoc], [account_ngayduyet]) VALUES (3, N'HS00003', NULL, N'HS00003', N'HS00003', NULL, NULL, NULL, 0, N'true', NULL, NULL, 1, NULL, N'1', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbAccount] OFF
SET IDENTITY_INSERT [dbo].[tbDayOfWeek] ON 

INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (1, N'2')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (2, N'3')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (3, N'4')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (5, N'5')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (6, N'6')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (7, N'7')
INSERT [dbo].[tbDayOfWeek] ([day_id], [day_name]) VALUES (8, N'CN')
SET IDENTITY_INSERT [dbo].[tbDayOfWeek] OFF
SET IDENTITY_INSERT [dbo].[tbhocsinhtronglop] ON 

INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (1, 3, 1, 1, 0, CAST(N'2022-12-09 00:00:00.000' AS DateTime), CAST(N'2023-02-26 00:00:00.000' AS DateTime), N'3', 0, NULL)
INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (2, 2, 1, 1, 0, CAST(N'2022-12-09 00:00:00.000' AS DateTime), CAST(N'2023-02-26 00:00:00.000' AS DateTime), N'3', 0, NULL)
INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (3, 1, 1, 1, 0, CAST(N'2022-12-09 00:00:00.000' AS DateTime), CAST(N'2023-02-26 00:00:00.000' AS DateTime), N'3', 0, NULL)
INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (4, 3, 2, 1, 0, CAST(N'2022-12-02 00:00:00.000' AS DateTime), CAST(N'2023-04-16 00:00:00.000' AS DateTime), N'5', 0, NULL)
INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (5, 2, 2, 1, 0, CAST(N'2022-12-02 00:00:00.000' AS DateTime), CAST(N'2023-04-16 00:00:00.000' AS DateTime), N'5', 0, NULL)
INSERT [dbo].[tbhocsinhtronglop] ([hstl_id], [account_id], [lop_id], [monhoc_id], [hstl_hidden], [hstl_ngaybatdauhoc], [hstl_ngayketthuchoc], [hstl_thoigianhoc], [hstl_baoluu], [hstl_ghichu]) VALUES (6, 1, 2, 1, 0, CAST(N'2022-12-02 00:00:00.000' AS DateTime), CAST(N'2023-04-16 00:00:00.000' AS DateTime), N'5', 0, NULL)
SET IDENTITY_INSERT [dbo].[tbhocsinhtronglop] OFF
SET IDENTITY_INSERT [dbo].[tbIntroduce] ON 

INSERT [dbo].[tbIntroduce] ([introduct_id], [introduce_title], [introduce_summary], [introduce_content]) VALUES (1, N'slide 1', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbIntroduce] OFF
SET IDENTITY_INSERT [dbo].[tbIntroduce_KhoaHoc] ON 

INSERT [dbo].[tbIntroduce_KhoaHoc] ([introkh_id], [introkh_title], [introkh_summary], [introkh_content], [introkh_linkyoutube], [hidden]) VALUES (1, N'khóa học 1', N'Nội dung tóm tắt 1', N'Nội dung tóm tắt 1', NULL, 1)
INSERT [dbo].[tbIntroduce_KhoaHoc] ([introkh_id], [introkh_title], [introkh_summary], [introkh_content], [introkh_linkyoutube], [hidden]) VALUES (2, N'khóa học 2', N'Nội dung tóm tắt 2', N'Nội dung tóm tắt 2', NULL, 1)
INSERT [dbo].[tbIntroduce_KhoaHoc] ([introkh_id], [introkh_title], [introkh_summary], [introkh_content], [introkh_linkyoutube], [hidden]) VALUES (3, N'khóa học 3', N'Nội dung tóm tắt 3', N'Nội dung tóm tắt 3', NULL, 1)
SET IDENTITY_INSERT [dbo].[tbIntroduce_KhoaHoc] OFF
SET IDENTITY_INSERT [dbo].[tbKhoaHoc] ON 

INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (1, N'Khóa học 1', N'Tóm tắt 1', 1, 1, NULL, 1)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (2, N'Khóa học 2', N'Tóm tắt 2', 1, 1, NULL, 2)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (3, N'Khóa học 3', N'Tóm tắt 3', 1, 1, NULL, 3)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (4, N'Khóa học 1', N'Tóm tắt 1', 2, 1, NULL, 1)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (5, N'Khóa học 2', N'Tóm tắt 2', 2, 1, NULL, 2)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (6, N'Khóa học 3', N'Tóm tắt 3', 2, 1, NULL, 3)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (7, N'Khóa học 1', N'Tóm tắt 1', 3, 1, NULL, 1)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (8, N'Khóa học 2', N'Tóm tắt 2', 3, 1, NULL, 2)
INSERT [dbo].[tbKhoaHoc] ([khoahoc_id], [khoahoc_name], [khoahoc_summary], [loaikhoahoc_id], [hidden], [link_seo], [khoahoc_position]) VALUES (9, N'Khóa học 3', N'Tóm tắt 3', 3, 1, NULL, 3)
SET IDENTITY_INSERT [dbo].[tbKhoaHoc] OFF
SET IDENTITY_INSERT [dbo].[tbLoaiKhoaHoc] ON 

INSERT [dbo].[tbLoaiKhoaHoc] ([loaikhoahoc_id], [loaikhoahoc_name], [hidden], [loaikhoahoc_position]) VALUES (1, N'Loại khóa học 1', 1, 1)
INSERT [dbo].[tbLoaiKhoaHoc] ([loaikhoahoc_id], [loaikhoahoc_name], [hidden], [loaikhoahoc_position]) VALUES (2, N'Loại khóa học 2', 1, 2)
INSERT [dbo].[tbLoaiKhoaHoc] ([loaikhoahoc_id], [loaikhoahoc_name], [hidden], [loaikhoahoc_position]) VALUES (3, N'Loại khóa học 3', 1, 3)
SET IDENTITY_INSERT [dbo].[tbLoaiKhoaHoc] OFF
SET IDENTITY_INSERT [dbo].[tbLop] ON 

INSERT [dbo].[tbLop] ([lop_id], [lop_name], [hidden], [username_idVn]) VALUES (1, N'AV1', 0, 1)
INSERT [dbo].[tbLop] ([lop_id], [lop_name], [hidden], [username_idVn]) VALUES (2, N'AV2', 0, 2)
SET IDENTITY_INSERT [dbo].[tbLop] OFF
SET IDENTITY_INSERT [dbo].[tbMonHoc] ON 

INSERT [dbo].[tbMonHoc] ([monhoc_id], [monhoc_name]) VALUES (1, N'Anh văn')
INSERT [dbo].[tbMonHoc] ([monhoc_id], [monhoc_name]) VALUES (2, N'Tiếng Nhật')
SET IDENTITY_INSERT [dbo].[tbMonHoc] OFF
SET IDENTITY_INSERT [dbo].[tbNewCate] ON 

INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo], [newcate_parent]) VALUES (1, N'Loại 1', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo], [newcate_parent]) VALUES (2, N'Loại 2', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo], [newcate_parent]) VALUES (3, N'Loại 3', NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo], [newcate_parent]) VALUES (4, N'Loại 4', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbNewCate] OFF
SET IDENTITY_INSERT [dbo].[tbNews] ON 

INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (1, N'khóa 1', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (2, N'khóa 2', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (3, N'khóa 3', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (4, N'khóa 1', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (5, N'khóa 2', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (6, N'khóa 3', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (7, N'khóa 1', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (8, N'khóa 2', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (9, N'khóa 3', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (10, N'khóa 1', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (11, N'khóa 2', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [news_datetime], [news_position], [link_seo]) VALUES (12, N'khóa 3', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbNews] OFF
SET IDENTITY_INSERT [dbo].[tbOperationHistory] ON 

INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (1, CAST(N'2022-12-03 20:37:11.353' AS DateTime), 1, N'Thêm học sinh HS00003 vào lớp AV1, Môn học : Anh văn')
INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (2, CAST(N'2022-12-03 20:37:11.367' AS DateTime), 1, N'Thêm học sinh HS00002 vào lớp AV1, Môn học : Anh văn')
INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (3, CAST(N'2022-12-03 20:37:11.377' AS DateTime), 1, N'Thêm học sinh HS00001 vào lớp AV1, Môn học : Anh văn')
INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (4, CAST(N'2022-12-03 20:52:28.047' AS DateTime), 1, N'Thêm học sinh HS00003 vào lớp AV2, Môn học : Anh văn')
INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (5, CAST(N'2022-12-03 20:52:28.060' AS DateTime), 1, N'Thêm học sinh HS00002 vào lớp AV2, Môn học : Anh văn')
INSERT [dbo].[tbOperationHistory] ([history_id], [history_day], [username_id], [history_content]) VALUES (6, CAST(N'2022-12-03 20:52:28.067' AS DateTime), 1, N'Thêm học sinh HS00001 vào lớp AV2, Môn học : Anh văn')
SET IDENTITY_INSERT [dbo].[tbOperationHistory] OFF
SET IDENTITY_INSERT [dbo].[tbSlide] ON 

INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_link], [slide_summary], [slide_content], [hidden], [slide_title1]) VALUES (1, N'/uploadimages/anh_slide/ypgahnzh.mz1.jpg', N'slide 1', NULL, N'thử nghiệm 1', N'thử nghiệm 1', NULL, NULL)
INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_link], [slide_summary], [slide_content], [hidden], [slide_title1]) VALUES (2, N'/uploadimages/anh_slide/ypgahnzh.mz1.jpg', N'slide 2', NULL, N'thử nghiệm 2', N'thử nghiệm 2', NULL, NULL)
INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_link], [slide_summary], [slide_content], [hidden], [slide_title1]) VALUES (3, N'/uploadimages/anh_slide/ypgahnzh.mz1.jpg', N'slide 3', NULL, N'thử nghiệm 3', N'thử nghiệm 3', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbSlide] OFF
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([username_id])
REFERENCES [dbo].[admin_User] ([username_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_Form]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_User]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
